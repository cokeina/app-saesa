package com.facebook.imagepipeline.backends.okhttp3;

import android.os.Looper;
import android.os.SystemClock;
import com.facebook.common.logging.FLog;
import com.facebook.imagepipeline.image.EncodedImage;
import com.facebook.imagepipeline.producers.BaseNetworkFetcher;
import com.facebook.imagepipeline.producers.BaseProducerContextCallbacks;
import com.facebook.imagepipeline.producers.Consumer;
import com.facebook.imagepipeline.producers.FetchState;
import com.facebook.imagepipeline.producers.NetworkFetcher.Callback;
import com.facebook.imagepipeline.producers.ProducerContext;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.Call.Factory;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class OkHttpNetworkFetcher extends BaseNetworkFetcher<OkHttpNetworkFetchState> {
    private static final String FETCH_TIME = "fetch_time";
    private static final String IMAGE_SIZE = "image_size";
    private static final String QUEUE_TIME = "queue_time";
    private static final String TAG = "OkHttpNetworkFetchProducer";
    private static final String TOTAL_TIME = "total_time";
    private final Factory mCallFactory;
    /* access modifiers changed from: private */
    public Executor mCancellationExecutor;

    public static class OkHttpNetworkFetchState extends FetchState {
        public long fetchCompleteTime;
        public long responseTime;
        public long submitTime;

        public OkHttpNetworkFetchState(Consumer<EncodedImage> consumer, ProducerContext producerContext) {
            super(consumer, producerContext);
        }
    }

    public OkHttpNetworkFetcher(OkHttpClient okHttpClient) {
        this(okHttpClient, okHttpClient.dispatcher().executorService());
    }

    public OkHttpNetworkFetcher(Factory callFactory, Executor cancellationExecutor) {
        this.mCallFactory = callFactory;
        this.mCancellationExecutor = cancellationExecutor;
    }

    public OkHttpNetworkFetchState createFetchState(Consumer<EncodedImage> consumer, ProducerContext context) {
        return new OkHttpNetworkFetchState(consumer, context);
    }

    public void fetch(OkHttpNetworkFetchState fetchState, Callback callback) {
        fetchState.submitTime = SystemClock.elapsedRealtime();
        try {
            fetchWithRequest(fetchState, callback, new Builder().cacheControl(new CacheControl.Builder().noStore().build()).url(fetchState.getUri().toString()).get().build());
        } catch (Exception e) {
            callback.onFailure(e);
        }
    }

    public void onFetchCompletion(OkHttpNetworkFetchState fetchState, int byteSize) {
        fetchState.fetchCompleteTime = SystemClock.elapsedRealtime();
    }

    public Map<String, String> getExtraMap(OkHttpNetworkFetchState fetchState, int byteSize) {
        Map<String, String> extraMap = new HashMap<>(4);
        extraMap.put(QUEUE_TIME, Long.toString(fetchState.responseTime - fetchState.submitTime));
        extraMap.put(FETCH_TIME, Long.toString(fetchState.fetchCompleteTime - fetchState.responseTime));
        extraMap.put(TOTAL_TIME, Long.toString(fetchState.fetchCompleteTime - fetchState.submitTime));
        extraMap.put(IMAGE_SIZE, Integer.toString(byteSize));
        return extraMap;
    }

    /* access modifiers changed from: protected */
    public void fetchWithRequest(final OkHttpNetworkFetchState fetchState, final Callback callback, Request request) {
        final Call call = this.mCallFactory.newCall(request);
        fetchState.getContext().addCallbacks(new BaseProducerContextCallbacks() {
            public void onCancellationRequested() {
                if (Looper.myLooper() != Looper.getMainLooper()) {
                    call.cancel();
                } else {
                    OkHttpNetworkFetcher.this.mCancellationExecutor.execute(new Runnable() {
                        public void run() {
                            call.cancel();
                        }
                    });
                }
            }
        });
        call.enqueue(new okhttp3.Callback() {
            public void onResponse(Call call, Response response) throws IOException {
                fetchState.responseTime = SystemClock.elapsedRealtime();
                ResponseBody body = response.body();
                try {
                    if (!response.isSuccessful()) {
                        OkHttpNetworkFetcher.this.handleException(call, new IOException("Unexpected HTTP code " + response), callback);
                        try {
                        } catch (Exception e) {
                            FLog.w(OkHttpNetworkFetcher.TAG, "Exception when closing response body", (Throwable) e);
                        }
                    } else {
                        long contentLength = body.contentLength();
                        if (contentLength < 0) {
                            contentLength = 0;
                        }
                        callback.onResponse(body.byteStream(), (int) contentLength);
                        try {
                            body.close();
                        } catch (Exception e2) {
                            FLog.w(OkHttpNetworkFetcher.TAG, "Exception when closing response body", (Throwable) e2);
                        }
                    }
                } catch (Exception e3) {
                    OkHttpNetworkFetcher.this.handleException(call, e3, callback);
                    try {
                    } catch (Exception e4) {
                        FLog.w(OkHttpNetworkFetcher.TAG, "Exception when closing response body", (Throwable) e4);
                    }
                } finally {
                    try {
                        body.close();
                    } catch (Exception e5) {
                        FLog.w(OkHttpNetworkFetcher.TAG, "Exception when closing response body", (Throwable) e5);
                    }
                }
            }

            public void onFailure(Call call, IOException e) {
                OkHttpNetworkFetcher.this.handleException(call, e, callback);
            }
        });
    }

    /* access modifiers changed from: private */
    public void handleException(Call call, Exception e, Callback callback) {
        if (call.isCanceled()) {
            callback.onCancellation();
        } else {
            callback.onFailure(e);
        }
    }
}
