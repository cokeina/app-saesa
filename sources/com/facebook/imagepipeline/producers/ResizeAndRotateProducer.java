package com.facebook.imagepipeline.producers;

import com.facebook.common.internal.Closeables;
import com.facebook.common.internal.ImmutableMap;
import com.facebook.common.internal.Preconditions;
import com.facebook.common.internal.VisibleForTesting;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.memory.PooledByteBufferFactory;
import com.facebook.common.memory.PooledByteBufferOutputStream;
import com.facebook.common.references.CloseableReference;
import com.facebook.common.util.TriState;
import com.facebook.imageformat.DefaultImageFormats;
import com.facebook.imageformat.ImageFormat;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imagepipeline.image.EncodedImage;
import com.facebook.imagepipeline.nativecode.JpegTranscoder;
import com.facebook.imagepipeline.producers.JobScheduler.JobRunnable;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import javax.annotation.Nullable;

public class ResizeAndRotateProducer implements Producer<EncodedImage> {
    @VisibleForTesting
    static final int DEFAULT_JPEG_QUALITY = 85;
    private static final String DOWNSAMPLE_ENUMERATOR_KEY = "downsampleEnumerator";
    private static final String FRACTION_KEY = "Fraction";
    private static final int FULL_ROUND = 360;
    @VisibleForTesting
    static final int MAX_JPEG_SCALE_NUMERATOR = 8;
    @VisibleForTesting
    static final int MIN_TRANSFORM_INTERVAL_MS = 100;
    private static final String ORIGINAL_SIZE_KEY = "Original size";
    public static final String PRODUCER_NAME = "ResizeAndRotateProducer";
    private static final String REQUESTED_SIZE_KEY = "Requested size";
    private static final String ROTATION_ANGLE_KEY = "rotationAngle";
    private static final String SOFTWARE_ENUMERATOR_KEY = "softwareEnumerator";
    /* access modifiers changed from: private */
    public final Executor mExecutor;
    private final Producer<EncodedImage> mInputProducer;
    /* access modifiers changed from: private */
    public final PooledByteBufferFactory mPooledByteBufferFactory;
    /* access modifiers changed from: private */
    public final boolean mResizingEnabled;
    /* access modifiers changed from: private */
    public final boolean mUseDownsamplingRatio;

    private class TransformingConsumer extends DelegatingConsumer<EncodedImage, EncodedImage> {
        /* access modifiers changed from: private */
        public boolean mIsCancelled = false;
        /* access modifiers changed from: private */
        public final JobScheduler mJobScheduler;
        /* access modifiers changed from: private */
        public final ProducerContext mProducerContext;

        public TransformingConsumer(final Consumer<EncodedImage> consumer, ProducerContext producerContext) {
            super(consumer);
            this.mProducerContext = producerContext;
            this.mJobScheduler = new JobScheduler(ResizeAndRotateProducer.this.mExecutor, new JobRunnable(ResizeAndRotateProducer.this) {
                public void run(EncodedImage encodedImage, boolean isLast) {
                    TransformingConsumer.this.doTransform(encodedImage, isLast);
                }
            }, 100);
            this.mProducerContext.addCallbacks(new BaseProducerContextCallbacks(ResizeAndRotateProducer.this) {
                public void onIsIntermediateResultExpectedChanged() {
                    if (TransformingConsumer.this.mProducerContext.isIntermediateResultExpected()) {
                        TransformingConsumer.this.mJobScheduler.scheduleJob();
                    }
                }

                public void onCancellationRequested() {
                    TransformingConsumer.this.mJobScheduler.clearJob();
                    TransformingConsumer.this.mIsCancelled = true;
                    consumer.onCancellation();
                }
            });
        }

        /* access modifiers changed from: protected */
        public void onNewResultImpl(@Nullable EncodedImage newResult, boolean isLast) {
            if (!this.mIsCancelled) {
                if (newResult != null) {
                    TriState shouldTransform = ResizeAndRotateProducer.shouldTransform(this.mProducerContext.getImageRequest(), newResult, ResizeAndRotateProducer.this.mResizingEnabled);
                    if (!isLast && shouldTransform == TriState.UNSET) {
                        return;
                    }
                    if (shouldTransform != TriState.YES) {
                        getConsumer().onNewResult(newResult, isLast);
                    } else if (!this.mJobScheduler.updateJob(newResult, isLast)) {
                    } else {
                        if (isLast || this.mProducerContext.isIntermediateResultExpected()) {
                            this.mJobScheduler.scheduleJob();
                        }
                    }
                } else if (isLast) {
                    getConsumer().onNewResult(null, true);
                }
            }
        }

        /* access modifiers changed from: private */
        public void doTransform(EncodedImage encodedImage, boolean isLast) {
            int numerator;
            this.mProducerContext.getListener().onProducerStart(this.mProducerContext.getId(), ResizeAndRotateProducer.PRODUCER_NAME);
            ImageRequest imageRequest = this.mProducerContext.getImageRequest();
            PooledByteBufferOutputStream outputStream = ResizeAndRotateProducer.this.mPooledByteBufferFactory.newOutputStream();
            Map<String, String> extraMap = null;
            InputStream is = null;
            try {
                int softwareNumerator = ResizeAndRotateProducer.getSoftwareNumerator(imageRequest, encodedImage, ResizeAndRotateProducer.this.mResizingEnabled);
                int downsampleNumerator = ResizeAndRotateProducer.calculateDownsampleNumerator(DownsampleUtil.determineSampleSize(imageRequest, encodedImage));
                if (ResizeAndRotateProducer.this.mUseDownsamplingRatio) {
                    numerator = downsampleNumerator;
                } else {
                    numerator = softwareNumerator;
                }
                int rotationAngle = ResizeAndRotateProducer.getRotationAngle(imageRequest.getRotationOptions(), encodedImage);
                extraMap = getExtraMap(encodedImage, imageRequest, numerator, downsampleNumerator, softwareNumerator, rotationAngle);
                is = encodedImage.getInputStream();
                JpegTranscoder.transcodeJpeg(is, outputStream, rotationAngle, numerator, 85);
                CloseableReference<PooledByteBuffer> ref = CloseableReference.of(outputStream.toByteBuffer());
                try {
                    EncodedImage ret = new EncodedImage(ref);
                    try {
                        ret.setImageFormat(DefaultImageFormats.JPEG);
                        ret.parseMetaData();
                        this.mProducerContext.getListener().onProducerFinishWithSuccess(this.mProducerContext.getId(), ResizeAndRotateProducer.PRODUCER_NAME, extraMap);
                        getConsumer().onNewResult(ret, isLast);
                        EncodedImage.closeSafely(ret);
                        try {
                            CloseableReference.closeSafely(ref);
                            Closeables.closeQuietly(is);
                            outputStream.close();
                            EncodedImage encodedImage2 = ret;
                        } catch (Exception e) {
                            e = e;
                            EncodedImage encodedImage3 = ret;
                            try {
                                this.mProducerContext.getListener().onProducerFinishWithFailure(this.mProducerContext.getId(), ResizeAndRotateProducer.PRODUCER_NAME, e, extraMap);
                                getConsumer().onFailure(e);
                                Closeables.closeQuietly(is);
                                outputStream.close();
                            } catch (Throwable th) {
                                th = th;
                                Closeables.closeQuietly(is);
                                outputStream.close();
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            EncodedImage encodedImage4 = ret;
                            Closeables.closeQuietly(is);
                            outputStream.close();
                            throw th;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        EncodedImage encodedImage5 = ret;
                        CloseableReference.closeSafely(ref);
                        throw th;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    CloseableReference.closeSafely(ref);
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
                this.mProducerContext.getListener().onProducerFinishWithFailure(this.mProducerContext.getId(), ResizeAndRotateProducer.PRODUCER_NAME, e, extraMap);
                getConsumer().onFailure(e);
                Closeables.closeQuietly(is);
                outputStream.close();
            }
        }

        private Map<String, String> getExtraMap(EncodedImage encodedImage, ImageRequest imageRequest, int numerator, int downsampleNumerator, int softwareNumerator, int rotationAngle) {
            String requestedSize;
            if (!this.mProducerContext.getListener().requiresExtraMap(this.mProducerContext.getId())) {
                return null;
            }
            String originalSize = encodedImage.getWidth() + "x" + encodedImage.getHeight();
            if (imageRequest.getResizeOptions() != null) {
                requestedSize = imageRequest.getResizeOptions().width + "x" + imageRequest.getResizeOptions().height;
            } else {
                requestedSize = "Unspecified";
            }
            String fraction = numerator > 0 ? numerator + "/8" : "";
            Map<String, String> map = new HashMap<>();
            map.put(ResizeAndRotateProducer.ORIGINAL_SIZE_KEY, originalSize);
            map.put(ResizeAndRotateProducer.REQUESTED_SIZE_KEY, requestedSize);
            map.put(ResizeAndRotateProducer.FRACTION_KEY, fraction);
            map.put("queueTime", String.valueOf(this.mJobScheduler.getQueuedTime()));
            map.put(ResizeAndRotateProducer.DOWNSAMPLE_ENUMERATOR_KEY, Integer.toString(downsampleNumerator));
            map.put(ResizeAndRotateProducer.SOFTWARE_ENUMERATOR_KEY, Integer.toString(softwareNumerator));
            map.put(ResizeAndRotateProducer.ROTATION_ANGLE_KEY, Integer.toString(rotationAngle));
            return ImmutableMap.copyOf(map);
        }
    }

    public ResizeAndRotateProducer(Executor executor, PooledByteBufferFactory pooledByteBufferFactory, boolean resizingEnabled, Producer<EncodedImage> inputProducer, boolean useDownsamplingRatio) {
        this.mExecutor = (Executor) Preconditions.checkNotNull(executor);
        this.mPooledByteBufferFactory = (PooledByteBufferFactory) Preconditions.checkNotNull(pooledByteBufferFactory);
        this.mResizingEnabled = resizingEnabled;
        this.mInputProducer = (Producer) Preconditions.checkNotNull(inputProducer);
        this.mUseDownsamplingRatio = useDownsamplingRatio;
    }

    public void produceResults(Consumer<EncodedImage> consumer, ProducerContext context) {
        this.mInputProducer.produceResults(new TransformingConsumer(consumer, context), context);
    }

    /* access modifiers changed from: private */
    public static TriState shouldTransform(ImageRequest request, EncodedImage encodedImage, boolean resizingEnabled) {
        if (encodedImage == null || encodedImage.getImageFormat() == ImageFormat.UNKNOWN) {
            return TriState.UNSET;
        }
        if (encodedImage.getImageFormat() != DefaultImageFormats.JPEG) {
            return TriState.NO;
        }
        return TriState.valueOf(shouldRotate(request.getRotationOptions(), encodedImage) || shouldResize(getSoftwareNumerator(request, encodedImage, resizingEnabled)));
    }

    @VisibleForTesting
    static float determineResizeRatio(ResizeOptions resizeOptions, int width, int height) {
        if (resizeOptions == null) {
            return 1.0f;
        }
        float ratio = Math.max(((float) resizeOptions.width) / ((float) width), ((float) resizeOptions.height) / ((float) height));
        if (((float) width) * ratio > resizeOptions.maxBitmapSize) {
            ratio = resizeOptions.maxBitmapSize / ((float) width);
        }
        if (((float) height) * ratio > resizeOptions.maxBitmapSize) {
            return resizeOptions.maxBitmapSize / ((float) height);
        }
        return ratio;
    }

    @VisibleForTesting
    static int roundNumerator(float maxRatio, float roundUpFraction) {
        return (int) ((8.0f * maxRatio) + roundUpFraction);
    }

    /* access modifiers changed from: private */
    public static int getSoftwareNumerator(ImageRequest imageRequest, EncodedImage encodedImage, boolean resizingEnabled) {
        int widthAfterRotation;
        int heightAfterRotation;
        if (!resizingEnabled) {
            return 8;
        }
        ResizeOptions resizeOptions = imageRequest.getResizeOptions();
        if (resizeOptions == null) {
            return 8;
        }
        int rotationAngle = getRotationAngle(imageRequest.getRotationOptions(), encodedImage);
        boolean swapDimensions = rotationAngle == 90 || rotationAngle == 270;
        if (swapDimensions) {
            widthAfterRotation = encodedImage.getHeight();
        } else {
            widthAfterRotation = encodedImage.getWidth();
        }
        if (swapDimensions) {
            heightAfterRotation = encodedImage.getWidth();
        } else {
            heightAfterRotation = encodedImage.getHeight();
        }
        int numerator = roundNumerator(determineResizeRatio(resizeOptions, widthAfterRotation, heightAfterRotation), resizeOptions.roundUpFraction);
        if (numerator > 8) {
            return 8;
        }
        if (numerator < 1) {
            return 1;
        }
        return numerator;
    }

    /* access modifiers changed from: private */
    public static int getRotationAngle(RotationOptions rotationOptions, EncodedImage encodedImage) {
        if (!rotationOptions.rotationEnabled()) {
            return 0;
        }
        int rotationFromMetadata = extractOrientationFromMetadata(encodedImage);
        return !rotationOptions.useImageMetadata() ? (rotationOptions.getForcedAngle() + rotationFromMetadata) % FULL_ROUND : rotationFromMetadata;
    }

    private static int extractOrientationFromMetadata(EncodedImage encodedImage) {
        switch (encodedImage.getRotationAngle()) {
            case 90:
            case RotationOptions.ROTATE_180 /*180*/:
            case RotationOptions.ROTATE_270 /*270*/:
                return encodedImage.getRotationAngle();
            default:
                return 0;
        }
    }

    private static boolean shouldResize(int numerator) {
        return numerator < 8;
    }

    private static boolean shouldRotate(RotationOptions rotationOptions, EncodedImage encodedImage) {
        return !rotationOptions.canDeferUntilRendered() && getRotationAngle(rotationOptions, encodedImage) != 0;
    }

    @VisibleForTesting
    static int calculateDownsampleNumerator(int downsampleRatio) {
        return Math.max(1, 8 / downsampleRatio);
    }
}
