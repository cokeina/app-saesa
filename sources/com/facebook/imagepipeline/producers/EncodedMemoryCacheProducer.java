package com.facebook.imagepipeline.producers;

import com.facebook.cache.common.CacheKey;
import com.facebook.common.internal.ImmutableMap;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.cache.CacheKeyFactory;
import com.facebook.imagepipeline.cache.MemoryCache;
import com.facebook.imagepipeline.image.EncodedImage;
import com.facebook.imagepipeline.request.ImageRequest.RequestLevel;
import java.util.Map;

public class EncodedMemoryCacheProducer implements Producer<EncodedImage> {
    public static final String EXTRA_CACHED_VALUE_FOUND = "cached_value_found";
    public static final String PRODUCER_NAME = "EncodedMemoryCacheProducer";
    private final CacheKeyFactory mCacheKeyFactory;
    private final Producer<EncodedImage> mInputProducer;
    private final MemoryCache<CacheKey, PooledByteBuffer> mMemoryCache;

    private static class EncodedMemoryCacheConsumer extends DelegatingConsumer<EncodedImage, EncodedImage> {
        private final MemoryCache<CacheKey, PooledByteBuffer> mMemoryCache;
        private final CacheKey mRequestedCacheKey;

        public EncodedMemoryCacheConsumer(Consumer<EncodedImage> consumer, MemoryCache<CacheKey, PooledByteBuffer> memoryCache, CacheKey requestedCacheKey) {
            super(consumer);
            this.mMemoryCache = memoryCache;
            this.mRequestedCacheKey = requestedCacheKey;
        }

        public void onNewResultImpl(EncodedImage newResult, boolean isLast) {
            if (!isLast || newResult == null) {
                getConsumer().onNewResult(newResult, isLast);
                return;
            }
            CloseableReference<PooledByteBuffer> ref = newResult.getByteBufferRef();
            if (ref != null) {
                try {
                    CloseableReference<PooledByteBuffer> cachedResult = this.mMemoryCache.cache(newResult.getEncodedCacheKey() != null ? newResult.getEncodedCacheKey() : this.mRequestedCacheKey, ref);
                    if (cachedResult != null) {
                        try {
                            EncodedImage cachedEncodedImage = new EncodedImage(cachedResult);
                            cachedEncodedImage.copyMetaDataFrom(newResult);
                            try {
                                getConsumer().onProgressUpdate(1.0f);
                                getConsumer().onNewResult(cachedEncodedImage, true);
                                return;
                            } finally {
                                EncodedImage.closeSafely(cachedEncodedImage);
                            }
                        } finally {
                            CloseableReference.closeSafely(cachedResult);
                        }
                    }
                } finally {
                    CloseableReference.closeSafely(ref);
                }
            }
            getConsumer().onNewResult(newResult, true);
        }
    }

    public EncodedMemoryCacheProducer(MemoryCache<CacheKey, PooledByteBuffer> memoryCache, CacheKeyFactory cacheKeyFactory, Producer<EncodedImage> inputProducer) {
        this.mMemoryCache = memoryCache;
        this.mCacheKeyFactory = cacheKeyFactory;
        this.mInputProducer = inputProducer;
    }

    public void produceResults(Consumer<EncodedImage> consumer, ProducerContext producerContext) {
        EncodedImage cachedEncodedImage;
        Map map = null;
        String requestId = producerContext.getId();
        ProducerListener listener = producerContext.getListener();
        listener.onProducerStart(requestId, PRODUCER_NAME);
        CacheKey cacheKey = this.mCacheKeyFactory.getEncodedCacheKey(producerContext.getImageRequest(), producerContext.getCallerContext());
        CloseableReference<PooledByteBuffer> cachedReference = this.mMemoryCache.get(cacheKey);
        if (cachedReference != null) {
            try {
                cachedEncodedImage = new EncodedImage(cachedReference);
                cachedEncodedImage.setEncodedCacheKey(cacheKey);
                String str = PRODUCER_NAME;
                if (listener.requiresExtraMap(requestId)) {
                    map = ImmutableMap.of("cached_value_found", "true");
                }
                listener.onProducerFinishWithSuccess(requestId, str, map);
                listener.onUltimateProducerReached(requestId, PRODUCER_NAME, true);
                consumer.onProgressUpdate(1.0f);
                consumer.onNewResult(cachedEncodedImage, true);
                EncodedImage.closeSafely(cachedEncodedImage);
                CloseableReference.closeSafely(cachedReference);
            } catch (Throwable th) {
                CloseableReference.closeSafely(cachedReference);
                throw th;
            }
        } else if (producerContext.getLowestPermittedRequestLevel().getValue() >= RequestLevel.ENCODED_MEMORY_CACHE.getValue()) {
            String str2 = PRODUCER_NAME;
            if (listener.requiresExtraMap(requestId)) {
                map = ImmutableMap.of("cached_value_found", "false");
            }
            listener.onProducerFinishWithSuccess(requestId, str2, map);
            listener.onUltimateProducerReached(requestId, PRODUCER_NAME, false);
            consumer.onNewResult(null, true);
            CloseableReference.closeSafely(cachedReference);
        } else {
            Consumer consumerOfInputProducer = new EncodedMemoryCacheConsumer(consumer, this.mMemoryCache, cacheKey);
            String str3 = PRODUCER_NAME;
            if (listener.requiresExtraMap(requestId)) {
                map = ImmutableMap.of("cached_value_found", "false");
            }
            listener.onProducerFinishWithSuccess(requestId, str3, map);
            this.mInputProducer.produceResults(consumerOfInputProducer, producerContext);
            CloseableReference.closeSafely(cachedReference);
        }
    }
}
