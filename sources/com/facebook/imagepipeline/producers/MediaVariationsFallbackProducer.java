package com.facebook.imagepipeline.producers;

import bolts.Continuation;
import bolts.Task;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.internal.ImmutableMap;
import com.facebook.common.internal.VisibleForTesting;
import com.facebook.imagepipeline.cache.BufferedDiskCache;
import com.facebook.imagepipeline.cache.CacheKeyFactory;
import com.facebook.imagepipeline.cache.DiskCachePolicy;
import com.facebook.imagepipeline.cache.MediaIdExtractor;
import com.facebook.imagepipeline.cache.MediaVariationsIndex;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.image.EncodedImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequest.CacheChoice;
import com.facebook.imagepipeline.request.MediaVariations;
import com.facebook.imagepipeline.request.MediaVariations.Variant;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Nullable;

public class MediaVariationsFallbackProducer implements Producer<EncodedImage> {
    public static final String EXTRA_CACHED_VALUE_FOUND = "cached_value_found";
    public static final String EXTRA_CACHED_VALUE_USED_AS_LAST = "cached_value_used_as_last";
    public static final String EXTRA_VARIANTS_COUNT = "variants_count";
    public static final String EXTRA_VARIANTS_SOURCE = "variants_source";
    public static final String PRODUCER_NAME = "MediaVariationsFallbackProducer";
    /* access modifiers changed from: private */
    public final CacheKeyFactory mCacheKeyFactory;
    private final BufferedDiskCache mDefaultBufferedDiskCache;
    /* access modifiers changed from: private */
    public final DiskCachePolicy mDiskCachePolicy;
    private final Producer<EncodedImage> mInputProducer;
    @Nullable
    private MediaIdExtractor mMediaIdExtractor;
    /* access modifiers changed from: private */
    public final MediaVariationsIndex mMediaVariationsIndex;
    private final BufferedDiskCache mSmallImageBufferedDiskCache;

    @VisibleForTesting
    class MediaVariationsConsumer extends DelegatingConsumer<EncodedImage, EncodedImage> {
        private final String mMediaId;
        private final ProducerContext mProducerContext;

        public MediaVariationsConsumer(Consumer<EncodedImage> consumer, ProducerContext producerContext, String mediaId) {
            super(consumer);
            this.mProducerContext = producerContext;
            this.mMediaId = mediaId;
        }

        /* access modifiers changed from: protected */
        public void onNewResultImpl(EncodedImage newResult, boolean isLast) {
            if (isLast && newResult != null) {
                storeResultInDatabase(newResult);
            }
            getConsumer().onNewResult(newResult, isLast);
        }

        private void storeResultInDatabase(EncodedImage newResult) {
            ImageRequest imageRequest = this.mProducerContext.getImageRequest();
            if (imageRequest.isDiskCacheEnabled() && this.mMediaId != null) {
                MediaVariationsFallbackProducer.this.mMediaVariationsIndex.saveCachedVariant(this.mMediaId, MediaVariationsFallbackProducer.this.mDiskCachePolicy.getCacheChoiceForResult(imageRequest, newResult), MediaVariationsFallbackProducer.this.mCacheKeyFactory.getEncodedCacheKey(imageRequest, this.mProducerContext.getCallerContext()), newResult);
            }
        }
    }

    @VisibleForTesting
    static class VariantComparator implements Comparator<Variant> {
        private final ResizeOptions mResizeOptions;

        VariantComparator(ResizeOptions resizeOptions) {
            this.mResizeOptions = resizeOptions;
        }

        public int compare(Variant o1, Variant o2) {
            boolean o1BigEnough = MediaVariationsFallbackProducer.isBigEnoughForRequestedSize(o1, this.mResizeOptions);
            boolean o2BigEnough = MediaVariationsFallbackProducer.isBigEnoughForRequestedSize(o2, this.mResizeOptions);
            if (o1BigEnough && o2BigEnough) {
                return o1.getWidth() - o2.getWidth();
            }
            if (o1BigEnough) {
                return -1;
            }
            if (o2BigEnough) {
                return 1;
            }
            return o2.getWidth() - o1.getWidth();
        }
    }

    public MediaVariationsFallbackProducer(BufferedDiskCache defaultBufferedDiskCache, BufferedDiskCache smallImageBufferedDiskCache, CacheKeyFactory cacheKeyFactory, MediaVariationsIndex mediaVariationsIndex, @Nullable MediaIdExtractor mediaIdExtractor, DiskCachePolicy diskCachePolicy, Producer<EncodedImage> inputProducer) {
        this.mDefaultBufferedDiskCache = defaultBufferedDiskCache;
        this.mSmallImageBufferedDiskCache = smallImageBufferedDiskCache;
        this.mCacheKeyFactory = cacheKeyFactory;
        this.mMediaVariationsIndex = mediaVariationsIndex;
        this.mMediaIdExtractor = mediaIdExtractor;
        this.mDiskCachePolicy = diskCachePolicy;
        this.mInputProducer = inputProducer;
    }

    public void produceResults(Consumer<EncodedImage> consumer, ProducerContext producerContext) {
        final String mediaId;
        String source;
        ImageRequest imageRequest = producerContext.getImageRequest();
        ResizeOptions resizeOptions = imageRequest.getResizeOptions();
        MediaVariations mediaVariations = imageRequest.getMediaVariations();
        if (!imageRequest.isDiskCacheEnabled() || resizeOptions == null || resizeOptions.height <= 0 || resizeOptions.width <= 0) {
            startInputProducerWithExistingConsumer(consumer, producerContext);
            return;
        }
        if (mediaVariations != null) {
            mediaId = mediaVariations.getMediaId();
            source = MediaVariations.SOURCE_INDEX_DB;
        } else if (this.mMediaIdExtractor == null) {
            mediaId = null;
            source = null;
        } else {
            mediaId = this.mMediaIdExtractor.getMediaIdFrom(imageRequest.getSourceUri());
            source = MediaVariations.SOURCE_ID_EXTRACTOR;
        }
        if (mediaVariations == null && mediaId == null) {
            startInputProducerWithExistingConsumer(consumer, producerContext);
            return;
        }
        producerContext.getListener().onProducerStart(producerContext.getId(), PRODUCER_NAME);
        AtomicBoolean isCancelled = new AtomicBoolean(false);
        if (mediaVariations == null || mediaVariations.getVariantsCount() <= 0) {
            final Consumer<EncodedImage> consumer2 = consumer;
            final ProducerContext producerContext2 = producerContext;
            final ImageRequest imageRequest2 = imageRequest;
            final ResizeOptions resizeOptions2 = resizeOptions;
            final AtomicBoolean atomicBoolean = isCancelled;
            this.mMediaVariationsIndex.getCachedVariants(mediaId, MediaVariations.newBuilderForMediaId(mediaId).setForceRequestForSpecifiedUri(mediaVariations != null && mediaVariations.shouldForceRequestForSpecifiedUri()).setSource(source)).continueWith(new Continuation<MediaVariations, Object>() {
                public Object then(Task<MediaVariations> task) throws Exception {
                    if (task.isCancelled() || task.isFaulted()) {
                        return task;
                    }
                    try {
                        if (task.getResult() != null) {
                            return MediaVariationsFallbackProducer.this.chooseFromVariants(consumer2, producerContext2, imageRequest2, (MediaVariations) task.getResult(), resizeOptions2, atomicBoolean);
                        }
                        MediaVariationsFallbackProducer.this.startInputProducerWithWrappedConsumer(consumer2, producerContext2, mediaId);
                        return null;
                    } catch (Exception e) {
                        return null;
                    }
                }
            });
        } else {
            chooseFromVariants(consumer, producerContext, imageRequest, mediaVariations, resizeOptions, isCancelled);
        }
        subscribeTaskForRequestCancellation(isCancelled, producerContext);
    }

    /* access modifiers changed from: private */
    public Task chooseFromVariants(Consumer<EncodedImage> consumer, ProducerContext producerContext, ImageRequest imageRequest, MediaVariations mediaVariations, ResizeOptions resizeOptions, AtomicBoolean isCancelled) {
        if (mediaVariations.getVariantsCount() == 0) {
            return Task.forResult(null).continueWith(onFinishDiskReads(consumer, producerContext, imageRequest, mediaVariations, Collections.emptyList(), 0, isCancelled));
        }
        return attemptCacheReadForVariant(consumer, producerContext, imageRequest, mediaVariations, mediaVariations.getSortedVariants(new VariantComparator(resizeOptions)), 0, isCancelled);
    }

    /* access modifiers changed from: private */
    public Task attemptCacheReadForVariant(Consumer<EncodedImage> consumer, ProducerContext producerContext, ImageRequest imageRequest, MediaVariations mediaVariations, List<Variant> sortedVariants, int index, AtomicBoolean isCancelled) {
        CacheChoice cacheChoice;
        Variant variant = (Variant) sortedVariants.get(index);
        CacheKey cacheKey = this.mCacheKeyFactory.getEncodedCacheKey(imageRequest, variant.getUri(), producerContext.getCallerContext());
        if (variant.getCacheChoice() == null) {
            cacheChoice = imageRequest.getCacheChoice();
        } else {
            cacheChoice = variant.getCacheChoice();
        }
        return (cacheChoice == CacheChoice.SMALL ? this.mSmallImageBufferedDiskCache : this.mDefaultBufferedDiskCache).get(cacheKey, isCancelled).continueWith(onFinishDiskReads(consumer, producerContext, imageRequest, mediaVariations, sortedVariants, index, isCancelled));
    }

    /* access modifiers changed from: private */
    public static boolean isBigEnoughForRequestedSize(Variant variant, ResizeOptions resizeOptions) {
        return variant.getWidth() >= resizeOptions.width && variant.getHeight() >= resizeOptions.height;
    }

    private Continuation<EncodedImage, Void> onFinishDiskReads(Consumer<EncodedImage> consumer, ProducerContext producerContext, ImageRequest imageRequest, MediaVariations mediaVariations, List<Variant> sortedVariants, int variantsIndex, AtomicBoolean isCancelled) {
        final String requestId = producerContext.getId();
        final ProducerListener listener = producerContext.getListener();
        final Consumer<EncodedImage> consumer2 = consumer;
        final ProducerContext producerContext2 = producerContext;
        final MediaVariations mediaVariations2 = mediaVariations;
        final List<Variant> list = sortedVariants;
        final int i = variantsIndex;
        final ImageRequest imageRequest2 = imageRequest;
        final AtomicBoolean atomicBoolean = isCancelled;
        return new Continuation<EncodedImage, Void>() {
            public Void then(Task<EncodedImage> task) throws Exception {
                boolean triggerNextProducer;
                if (MediaVariationsFallbackProducer.isTaskCancelled(task)) {
                    listener.onProducerFinishWithCancellation(requestId, MediaVariationsFallbackProducer.PRODUCER_NAME, null);
                    consumer2.onCancellation();
                    triggerNextProducer = false;
                } else if (task.isFaulted()) {
                    listener.onProducerFinishWithFailure(requestId, MediaVariationsFallbackProducer.PRODUCER_NAME, task.getError(), null);
                    MediaVariationsFallbackProducer.this.startInputProducerWithWrappedConsumer(consumer2, producerContext2, mediaVariations2.getMediaId());
                    triggerNextProducer = true;
                } else {
                    EncodedImage cachedReference = (EncodedImage) task.getResult();
                    if (cachedReference != null) {
                        boolean useAsLastResult = !mediaVariations2.shouldForceRequestForSpecifiedUri() && MediaVariationsFallbackProducer.isBigEnoughForRequestedSize((Variant) list.get(i), imageRequest2.getResizeOptions());
                        listener.onProducerFinishWithSuccess(requestId, MediaVariationsFallbackProducer.PRODUCER_NAME, MediaVariationsFallbackProducer.getExtraMap(listener, requestId, true, list.size(), mediaVariations2.getSource(), useAsLastResult));
                        if (useAsLastResult) {
                            listener.onUltimateProducerReached(requestId, MediaVariationsFallbackProducer.PRODUCER_NAME, true);
                            consumer2.onProgressUpdate(1.0f);
                        }
                        consumer2.onNewResult(cachedReference, useAsLastResult);
                        cachedReference.close();
                        triggerNextProducer = !useAsLastResult;
                    } else if (i < list.size() - 1) {
                        MediaVariationsFallbackProducer.this.attemptCacheReadForVariant(consumer2, producerContext2, imageRequest2, mediaVariations2, list, i + 1, atomicBoolean);
                        triggerNextProducer = false;
                    } else {
                        listener.onProducerFinishWithSuccess(requestId, MediaVariationsFallbackProducer.PRODUCER_NAME, MediaVariationsFallbackProducer.getExtraMap(listener, requestId, false, list.size(), mediaVariations2.getSource(), false));
                        triggerNextProducer = true;
                    }
                }
                if (triggerNextProducer) {
                    MediaVariationsFallbackProducer.this.startInputProducerWithWrappedConsumer(consumer2, producerContext2, mediaVariations2.getMediaId());
                }
                return null;
            }
        };
    }

    private void startInputProducerWithExistingConsumer(Consumer<EncodedImage> consumer, ProducerContext producerContext) {
        this.mInputProducer.produceResults(consumer, producerContext);
    }

    /* access modifiers changed from: private */
    public void startInputProducerWithWrappedConsumer(Consumer<EncodedImage> consumer, ProducerContext producerContext, String mediaId) {
        this.mInputProducer.produceResults(new MediaVariationsConsumer(consumer, producerContext, mediaId), producerContext);
    }

    /* access modifiers changed from: private */
    public static boolean isTaskCancelled(Task<?> task) {
        return task.isCancelled() || (task.isFaulted() && (task.getError() instanceof CancellationException));
    }

    @VisibleForTesting
    static Map<String, String> getExtraMap(ProducerListener listener, String requestId, boolean valueFound, int variantsCount, String variantsSource, boolean useAsLastResult) {
        if (!listener.requiresExtraMap(requestId)) {
            return null;
        }
        if (valueFound) {
            return ImmutableMap.of("cached_value_found", String.valueOf(true), EXTRA_CACHED_VALUE_USED_AS_LAST, String.valueOf(useAsLastResult), EXTRA_VARIANTS_COUNT, String.valueOf(variantsCount), EXTRA_VARIANTS_SOURCE, variantsSource);
        }
        return ImmutableMap.of("cached_value_found", String.valueOf(false), EXTRA_VARIANTS_COUNT, String.valueOf(variantsCount), EXTRA_VARIANTS_SOURCE, variantsSource);
    }

    private void subscribeTaskForRequestCancellation(final AtomicBoolean isCancelled, ProducerContext producerContext) {
        producerContext.addCallbacks(new BaseProducerContextCallbacks() {
            public void onCancellationRequested() {
                isCancelled.set(true);
            }
        });
    }
}
