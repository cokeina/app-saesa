package com.facebook.imagepipeline.cache;

import bolts.Task;
import com.facebook.cache.common.CacheKey;
import com.facebook.imagepipeline.image.EncodedImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequest.CacheChoice;
import java.util.concurrent.atomic.AtomicBoolean;

public class SmallCacheIfRequestedDiskCachePolicy implements DiskCachePolicy {
    private final CacheKeyFactory mCacheKeyFactory;
    private final BufferedDiskCache mDefaultBufferedDiskCache;
    private final BufferedDiskCache mSmallImageBufferedDiskCache;

    public SmallCacheIfRequestedDiskCachePolicy(BufferedDiskCache defaultBufferedDiskCache, BufferedDiskCache smallImageBufferedDiskCache, CacheKeyFactory cacheKeyFactory) {
        this.mDefaultBufferedDiskCache = defaultBufferedDiskCache;
        this.mSmallImageBufferedDiskCache = smallImageBufferedDiskCache;
        this.mCacheKeyFactory = cacheKeyFactory;
    }

    public Task<EncodedImage> createAndStartCacheReadTask(ImageRequest imageRequest, Object callerContext, AtomicBoolean isCancelled) {
        CacheKey cacheKey = this.mCacheKeyFactory.getEncodedCacheKey(imageRequest, callerContext);
        if (imageRequest.getCacheChoice() == CacheChoice.SMALL) {
            return this.mSmallImageBufferedDiskCache.get(cacheKey, isCancelled);
        }
        return this.mDefaultBufferedDiskCache.get(cacheKey, isCancelled);
    }

    public void writeToCache(EncodedImage newResult, ImageRequest imageRequest, Object callerContext) {
        CacheKey cacheKey = this.mCacheKeyFactory.getEncodedCacheKey(imageRequest, callerContext);
        if (getCacheChoiceForResult(imageRequest, newResult) == CacheChoice.SMALL) {
            this.mSmallImageBufferedDiskCache.put(cacheKey, newResult);
        } else {
            this.mDefaultBufferedDiskCache.put(cacheKey, newResult);
        }
    }

    public CacheChoice getCacheChoiceForResult(ImageRequest imageRequest, EncodedImage encodedImage) {
        if (imageRequest.getCacheChoice() == null) {
            return CacheChoice.DEFAULT;
        }
        return imageRequest.getCacheChoice();
    }
}
