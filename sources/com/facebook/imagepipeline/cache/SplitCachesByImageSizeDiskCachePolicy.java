package com.facebook.imagepipeline.cache;

import bolts.Continuation;
import bolts.Task;
import com.facebook.cache.common.CacheKey;
import com.facebook.imagepipeline.image.EncodedImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequest.CacheChoice;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicBoolean;

public class SplitCachesByImageSizeDiskCachePolicy implements DiskCachePolicy {
    private final CacheKeyFactory mCacheKeyFactory;
    private final BufferedDiskCache mDefaultBufferedDiskCache;
    private final int mForceSmallCacheThresholdBytes;
    private final BufferedDiskCache mSmallImageBufferedDiskCache;

    public SplitCachesByImageSizeDiskCachePolicy(BufferedDiskCache defaultBufferedDiskCache, BufferedDiskCache smallImageBufferedDiskCache, CacheKeyFactory cacheKeyFactory, int forceSmallCacheThresholdBytes) {
        this.mDefaultBufferedDiskCache = defaultBufferedDiskCache;
        this.mSmallImageBufferedDiskCache = smallImageBufferedDiskCache;
        this.mCacheKeyFactory = cacheKeyFactory;
        this.mForceSmallCacheThresholdBytes = forceSmallCacheThresholdBytes;
    }

    public Task<EncodedImage> createAndStartCacheReadTask(ImageRequest imageRequest, Object callerContext, final AtomicBoolean isCancelled) {
        final BufferedDiskCache secondCache;
        BufferedDiskCache firstCache;
        final CacheKey cacheKey = this.mCacheKeyFactory.getEncodedCacheKey(imageRequest, callerContext);
        boolean alreadyInSmall = this.mSmallImageBufferedDiskCache.containsSync(cacheKey);
        boolean alreadyInMain = this.mDefaultBufferedDiskCache.containsSync(cacheKey);
        if (alreadyInSmall || !alreadyInMain) {
            firstCache = this.mSmallImageBufferedDiskCache;
            secondCache = this.mDefaultBufferedDiskCache;
        } else {
            firstCache = this.mDefaultBufferedDiskCache;
            secondCache = this.mSmallImageBufferedDiskCache;
        }
        return firstCache.get(cacheKey, isCancelled).continueWithTask(new Continuation<EncodedImage, Task<EncodedImage>>() {
            public Task<EncodedImage> then(Task<EncodedImage> task) throws Exception {
                if (!SplitCachesByImageSizeDiskCachePolicy.isTaskCancelled(task)) {
                    return (task.isFaulted() || task.getResult() == null) ? secondCache.get(cacheKey, isCancelled) : task;
                }
                return task;
            }
        });
    }

    public void writeToCache(EncodedImage newResult, ImageRequest imageRequest, Object callerContext) {
        CacheKey cacheKey = this.mCacheKeyFactory.getEncodedCacheKey(imageRequest, callerContext);
        switch (getCacheChoiceForResult(imageRequest, newResult)) {
            case DEFAULT:
                this.mDefaultBufferedDiskCache.put(cacheKey, newResult);
                return;
            case SMALL:
                this.mSmallImageBufferedDiskCache.put(cacheKey, newResult);
                return;
            default:
                return;
        }
    }

    public CacheChoice getCacheChoiceForResult(ImageRequest imageRequest, EncodedImage encodedImage) {
        int size = encodedImage.getSize();
        if (size < 0 || size >= this.mForceSmallCacheThresholdBytes) {
            return CacheChoice.DEFAULT;
        }
        return CacheChoice.SMALL;
    }

    /* access modifiers changed from: private */
    public static boolean isTaskCancelled(Task<?> task) {
        return task.isCancelled() || (task.isFaulted() && (task.getError() instanceof CancellationException));
    }
}
