package com.facebook.imagepipeline.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;
import bolts.Task;
import com.facebook.cache.common.CacheKey;
import com.facebook.cache.common.CacheKeyUtil;
import com.facebook.common.internal.VisibleForTesting;
import com.facebook.common.logging.FLog;
import com.facebook.imagepipeline.image.EncodedImage;
import com.facebook.imagepipeline.request.ImageRequest.CacheChoice;
import com.facebook.imagepipeline.request.MediaVariations;
import com.facebook.imagepipeline.request.MediaVariations.Builder;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;

public class MediaVariationsIndexDatabase implements MediaVariationsIndex {
    private static final String[] PROJECTION = {IndexEntry.COLUMN_NAME_CACHE_CHOICE, IndexEntry.COLUMN_NAME_CACHE_KEY, "width", "height"};
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS media_variations_index";
    private static final String TAG = MediaVariationsIndexDatabase.class.getSimpleName();
    @GuardedBy("MediaVariationsIndexDatabase.class")
    private final LazyIndexDbOpenHelper mDbHelper;
    private final Executor mReadExecutor;
    private final Executor mWriteExecutor;

    private static class IndexDbOpenHelper extends SQLiteOpenHelper {
        public static final String DATABASE_NAME = "FrescoMediaVariationsIndex.db";
        public static final int DATABASE_VERSION = 2;
        private static final String INTEGER_TYPE = " INTEGER";
        private static final String SQL_CREATE_ENTRIES = "CREATE TABLE media_variations_index (_id INTEGER PRIMARY KEY,media_id TEXT,width INTEGER,height INTEGER,cache_choice TEXT,cache_key TEXT,resource_id TEXT UNIQUE )";
        private static final String SQL_CREATE_INDEX = "CREATE INDEX index_media_id ON media_variations_index (media_id)";
        private static final String TEXT_TYPE = " TEXT";

        public IndexDbOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, 2);
        }

        public void onCreate(SQLiteDatabase db) {
            db.beginTransaction();
            try {
                db.execSQL(SQL_CREATE_ENTRIES);
                db.execSQL(SQL_CREATE_INDEX);
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
        }

        /* JADX INFO: finally extract failed */
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.beginTransaction();
            try {
                db.execSQL(MediaVariationsIndexDatabase.SQL_DELETE_ENTRIES);
                db.setTransactionSuccessful();
                db.endTransaction();
                onCreate(db);
            } catch (Throwable th) {
                db.endTransaction();
                throw th;
            }
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }

    private static final class IndexEntry implements BaseColumns {
        public static final String COLUMN_NAME_CACHE_CHOICE = "cache_choice";
        public static final String COLUMN_NAME_CACHE_KEY = "cache_key";
        public static final String COLUMN_NAME_HEIGHT = "height";
        public static final String COLUMN_NAME_MEDIA_ID = "media_id";
        public static final String COLUMN_NAME_RESOURCE_ID = "resource_id";
        public static final String COLUMN_NAME_WIDTH = "width";
        public static final String TABLE_NAME = "media_variations_index";

        private IndexEntry() {
        }
    }

    private static class LazyIndexDbOpenHelper {
        private final Context mContext;
        @Nullable
        private IndexDbOpenHelper mIndexDbOpenHelper;

        private LazyIndexDbOpenHelper(Context context) {
            this.mContext = context;
        }

        public synchronized SQLiteDatabase getWritableDatabase() {
            if (this.mIndexDbOpenHelper == null) {
                this.mIndexDbOpenHelper = new IndexDbOpenHelper(this.mContext);
            }
            return this.mIndexDbOpenHelper.getWritableDatabase();
        }
    }

    public MediaVariationsIndexDatabase(Context context, Executor readExecutor, Executor writeExecutor) {
        this.mDbHelper = new LazyIndexDbOpenHelper(context);
        this.mReadExecutor = readExecutor;
        this.mWriteExecutor = writeExecutor;
    }

    public Task<MediaVariations> getCachedVariants(final String mediaId, final Builder mediaVariationsBuilder) {
        try {
            return Task.call((Callable<TResult>) new Callable<MediaVariations>() {
                public MediaVariations call() throws Exception {
                    return MediaVariationsIndexDatabase.this.getCachedVariantsSync(mediaId, mediaVariationsBuilder);
                }
            }, this.mReadExecutor);
        } catch (Exception exception) {
            FLog.w(TAG, (Throwable) exception, "Failed to schedule query task for %s", mediaId);
            return Task.forError(exception);
        }
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public MediaVariations getCachedVariantsSync(String mediaId, Builder mediaVariationsBuilder) {
        MediaVariations build;
        CacheChoice valueOf;
        synchronized (MediaVariationsIndexDatabase.class) {
            Cursor c = null;
            try {
                c = this.mDbHelper.getWritableDatabase().query(IndexEntry.TABLE_NAME, PROJECTION, "media_id = ?", new String[]{mediaId}, null, null, null);
                if (c.getCount() == 0) {
                    build = mediaVariationsBuilder.build();
                    if (c != null) {
                        c.close();
                    }
                } else {
                    int columnIndexCacheKey = c.getColumnIndexOrThrow(IndexEntry.COLUMN_NAME_CACHE_KEY);
                    int columnIndexWidth = c.getColumnIndexOrThrow("width");
                    int columnIndexHeight = c.getColumnIndexOrThrow("height");
                    int columnIndexCacheChoice = c.getColumnIndexOrThrow(IndexEntry.COLUMN_NAME_CACHE_CHOICE);
                    while (c.moveToNext()) {
                        String cacheChoiceStr = c.getString(columnIndexCacheChoice);
                        Uri parse = Uri.parse(c.getString(columnIndexCacheKey));
                        int i = c.getInt(columnIndexWidth);
                        int i2 = c.getInt(columnIndexHeight);
                        if (TextUtils.isEmpty(cacheChoiceStr)) {
                            valueOf = null;
                        } else {
                            valueOf = CacheChoice.valueOf(cacheChoiceStr);
                        }
                        mediaVariationsBuilder.addVariant(parse, i, i2, valueOf);
                    }
                    build = mediaVariationsBuilder.build();
                    if (c != null) {
                        c.close();
                    }
                }
            } catch (SQLException x) {
                FLog.e(TAG, (Throwable) x, "Error reading for %s", mediaId);
                throw x;
            } catch (Throwable th) {
                if (c != null) {
                    c.close();
                }
                throw th;
            }
        }
        return build;
    }

    public void saveCachedVariant(String mediaId, CacheChoice cacheChoice, CacheKey cacheKey, EncodedImage encodedImage) {
        final String str = mediaId;
        final CacheChoice cacheChoice2 = cacheChoice;
        final CacheKey cacheKey2 = cacheKey;
        final EncodedImage encodedImage2 = encodedImage;
        this.mWriteExecutor.execute(new Runnable() {
            public void run() {
                MediaVariationsIndexDatabase.this.saveCachedVariantSync(str, cacheChoice2, cacheKey2, encodedImage2);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void saveCachedVariantSync(String mediaId, CacheChoice cacheChoice, CacheKey cacheKey, EncodedImage encodedImage) {
        synchronized (MediaVariationsIndexDatabase.class) {
            SQLiteDatabase db = this.mDbHelper.getWritableDatabase();
            try {
                db.beginTransaction();
                ContentValues contentValues = new ContentValues();
                contentValues.put(IndexEntry.COLUMN_NAME_MEDIA_ID, mediaId);
                contentValues.put("width", Integer.valueOf(encodedImage.getWidth()));
                contentValues.put("height", Integer.valueOf(encodedImage.getHeight()));
                contentValues.put(IndexEntry.COLUMN_NAME_CACHE_CHOICE, cacheChoice.name());
                contentValues.put(IndexEntry.COLUMN_NAME_CACHE_KEY, cacheKey.getUriString());
                contentValues.put(IndexEntry.COLUMN_NAME_RESOURCE_ID, CacheKeyUtil.getFirstResourceId(cacheKey));
                db.replaceOrThrow(IndexEntry.TABLE_NAME, null, contentValues);
                db.setTransactionSuccessful();
                db.endTransaction();
            } catch (Exception x) {
                FLog.e(TAG, (Throwable) x, "Error writing for %s", mediaId);
                db.endTransaction();
            } catch (Throwable th) {
                db.endTransaction();
                throw th;
            }
        }
    }
}
