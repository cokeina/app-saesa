package com.facebook.imagepipeline.common;

import android.graphics.Bitmap.Config;
import com.facebook.imagepipeline.decoder.ImageDecoder;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
public class ImageDecodeOptions {
    private static final ImageDecodeOptions DEFAULTS = newBuilder().build();
    public final Config bitmapConfig;
    @Nullable
    public final ImageDecoder customImageDecoder;
    public final boolean decodeAllFrames;
    public final boolean decodePreviewFrame;
    public final boolean forceStaticImage;
    public final int minDecodeIntervalMs;
    public final boolean useLastFrameForPreview;

    public ImageDecodeOptions(ImageDecodeOptionsBuilder b) {
        this.minDecodeIntervalMs = b.getMinDecodeIntervalMs();
        this.decodePreviewFrame = b.getDecodePreviewFrame();
        this.useLastFrameForPreview = b.getUseLastFrameForPreview();
        this.decodeAllFrames = b.getDecodeAllFrames();
        this.forceStaticImage = b.getForceStaticImage();
        this.bitmapConfig = b.getBitmapConfig();
        this.customImageDecoder = b.getCustomImageDecoder();
    }

    public static ImageDecodeOptions defaults() {
        return DEFAULTS;
    }

    public static ImageDecodeOptionsBuilder newBuilder() {
        return new ImageDecodeOptionsBuilder();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ImageDecodeOptions that = (ImageDecodeOptions) o;
        if (this.decodePreviewFrame != that.decodePreviewFrame) {
            return false;
        }
        if (this.useLastFrameForPreview != that.useLastFrameForPreview) {
            return false;
        }
        if (this.decodeAllFrames != that.decodeAllFrames) {
            return false;
        }
        if (this.forceStaticImage != that.forceStaticImage) {
            return false;
        }
        if (this.bitmapConfig != that.bitmapConfig) {
            return false;
        }
        if (this.customImageDecoder != that.customImageDecoder) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 1;
        int i4 = 0;
        int i5 = ((this.minDecodeIntervalMs * 31) + (this.decodePreviewFrame ? 1 : 0)) * 31;
        if (this.useLastFrameForPreview) {
            i = 1;
        } else {
            i = 0;
        }
        int i6 = (i5 + i) * 31;
        if (this.decodeAllFrames) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 31;
        if (!this.forceStaticImage) {
            i3 = 0;
        }
        int ordinal = (((i7 + i3) * 31) + this.bitmapConfig.ordinal()) * 31;
        if (this.customImageDecoder != null) {
            i4 = this.customImageDecoder.hashCode();
        }
        return ordinal + i4;
    }

    public String toString() {
        return String.format(null, "%d-%b-%b-%b-%b-%s-%s", new Object[]{Integer.valueOf(this.minDecodeIntervalMs), Boolean.valueOf(this.decodePreviewFrame), Boolean.valueOf(this.useLastFrameForPreview), Boolean.valueOf(this.decodeAllFrames), Boolean.valueOf(this.forceStaticImage), this.bitmapConfig.name(), this.customImageDecoder});
    }
}
