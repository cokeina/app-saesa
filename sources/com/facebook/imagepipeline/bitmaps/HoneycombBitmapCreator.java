package com.facebook.imagepipeline.bitmaps;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Build.VERSION;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.CloseableReference;
import com.facebook.common.webp.BitmapCreator;
import com.facebook.imageformat.DefaultImageFormats;
import com.facebook.imagepipeline.image.EncodedImage;
import com.facebook.imagepipeline.memory.FlexByteArrayPool;
import com.facebook.imagepipeline.memory.PoolFactory;

public class HoneycombBitmapCreator implements BitmapCreator {
    private final FlexByteArrayPool mFlexByteArrayPool;
    private final EmptyJpegGenerator mJpegGenerator;

    public HoneycombBitmapCreator(PoolFactory poolFactory) {
        this.mFlexByteArrayPool = poolFactory.getFlexByteArrayPool();
        this.mJpegGenerator = new EmptyJpegGenerator(poolFactory.getPooledByteBufferFactory());
    }

    @TargetApi(12)
    public Bitmap createNakedBitmap(int width, int height, Config bitmapConfig) {
        CloseableReference<PooledByteBuffer> jpgRef = this.mJpegGenerator.generate((short) width, (short) height);
        EncodedImage encodedImage = null;
        CloseableReference<byte[]> encodedBytesArrayRef = null;
        try {
            EncodedImage encodedImage2 = new EncodedImage(jpgRef);
            try {
                encodedImage2.setImageFormat(DefaultImageFormats.JPEG);
                Options options = getBitmapFactoryOptions(encodedImage2.getSampleSize(), bitmapConfig);
                int length = ((PooledByteBuffer) jpgRef.get()).size();
                PooledByteBuffer pooledByteBuffer = (PooledByteBuffer) jpgRef.get();
                encodedBytesArrayRef = this.mFlexByteArrayPool.get(length + 2);
                byte[] encodedBytesArray = (byte[]) encodedBytesArrayRef.get();
                pooledByteBuffer.read(0, encodedBytesArray, 0, length);
                Bitmap bitmap = BitmapFactory.decodeByteArray(encodedBytesArray, 0, length, options);
                bitmap.setHasAlpha(true);
                bitmap.eraseColor(0);
                CloseableReference.closeSafely(encodedBytesArrayRef);
                EncodedImage.closeSafely(encodedImage2);
                CloseableReference.closeSafely(jpgRef);
                return bitmap;
            } catch (Throwable th) {
                th = th;
                encodedImage = encodedImage2;
                CloseableReference.closeSafely(encodedBytesArrayRef);
                EncodedImage.closeSafely(encodedImage);
                CloseableReference.closeSafely(jpgRef);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            CloseableReference.closeSafely(encodedBytesArrayRef);
            EncodedImage.closeSafely(encodedImage);
            CloseableReference.closeSafely(jpgRef);
            throw th;
        }
    }

    private static Options getBitmapFactoryOptions(int sampleSize, Config bitmapConfig) {
        Options options = new Options();
        options.inDither = true;
        options.inPreferredConfig = bitmapConfig;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inSampleSize = sampleSize;
        if (VERSION.SDK_INT >= 11) {
            options.inMutable = true;
        }
        return options;
    }
}
