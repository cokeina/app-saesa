package com.facebook.jni;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.util.concurrent.atomic.AtomicReference;

public class DestructorThread {
    /* access modifiers changed from: private */
    public static DestructorList sDestructorList = new DestructorList();
    /* access modifiers changed from: private */
    public static DestructorStack sDestructorStack = new DestructorStack();
    /* access modifiers changed from: private */
    public static ReferenceQueue sReferenceQueue = new ReferenceQueue();
    private static Thread sThread = new Thread("HybridData DestructorThread") {
        public void run() {
            while (true) {
                try {
                    Destructor current = (Destructor) DestructorThread.sReferenceQueue.remove();
                    current.destruct();
                    if (current.previous == null) {
                        DestructorThread.sDestructorStack.transferAllToList();
                    }
                    DestructorList.drop(current);
                } catch (InterruptedException e) {
                }
            }
        }
    };

    public static abstract class Destructor extends PhantomReference<Object> {
        /* access modifiers changed from: private */
        public Destructor next;
        /* access modifiers changed from: private */
        public Destructor previous;

        /* access modifiers changed from: 0000 */
        public abstract void destruct();

        Destructor(Object referent) {
            super(referent, DestructorThread.sReferenceQueue);
            DestructorThread.sDestructorStack.push(this);
        }

        private Destructor() {
            super(null, DestructorThread.sReferenceQueue);
        }
    }

    private static class DestructorList {
        private Destructor mHead = new Terminus();

        public DestructorList() {
            this.mHead.next = new Terminus();
            this.mHead.next.previous = this.mHead;
        }

        public void enqueue(Destructor current) {
            current.next = this.mHead.next;
            this.mHead.next = current;
            current.next.previous = current;
            current.previous = this.mHead;
        }

        /* access modifiers changed from: private */
        public static void drop(Destructor current) {
            current.next.previous = current.previous;
            current.previous.next = current.next;
        }
    }

    private static class DestructorStack {
        private AtomicReference<Destructor> mHead;

        private DestructorStack() {
            this.mHead = new AtomicReference<>();
        }

        public void push(Destructor newHead) {
            Destructor oldHead;
            do {
                oldHead = (Destructor) this.mHead.get();
                newHead.next = oldHead;
            } while (!this.mHead.compareAndSet(oldHead, newHead));
        }

        public void transferAllToList() {
            Destructor current = (Destructor) this.mHead.getAndSet(null);
            while (current != null) {
                Destructor next = current.next;
                DestructorThread.sDestructorList.enqueue(current);
                current = next;
            }
        }
    }

    private static class Terminus extends Destructor {
        private Terminus() {
            super();
        }

        /* access modifiers changed from: 0000 */
        public void destruct() {
            throw new IllegalStateException("Cannot destroy Terminus Destructor.");
        }
    }

    static {
        sThread.start();
    }
}
