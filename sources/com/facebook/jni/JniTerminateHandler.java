package com.facebook.jni;

import java.lang.Thread.UncaughtExceptionHandler;

public class JniTerminateHandler {
    public static void handleTerminate(Throwable t) throws Throwable {
        UncaughtExceptionHandler h = Thread.getDefaultUncaughtExceptionHandler();
        if (h != null) {
            h.uncaughtException(Thread.currentThread(), t);
        }
    }
}
