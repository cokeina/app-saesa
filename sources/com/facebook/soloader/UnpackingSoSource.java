package com.facebook.soloader;

import android.content.Context;
import android.os.Parcel;
import android.util.Log;
import java.io.Closeable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

public abstract class UnpackingSoSource extends DirectorySoSource {
    private static final String DEPS_FILE_NAME = "dso_deps";
    private static final String LOCK_FILE_NAME = "dso_lock";
    private static final String MANIFEST_FILE_NAME = "dso_manifest";
    private static final byte MANIFEST_VERSION = 1;
    private static final byte STATE_CLEAN = 1;
    private static final byte STATE_DIRTY = 0;
    private static final String STATE_FILE_NAME = "dso_state";
    private static final String TAG = "fb-UnpackingSoSource";
    protected final Context mContext;

    public static class Dso {
        public final String hash;
        public final String name;

        public Dso(String name2, String hash2) {
            this.name = name2;
            this.hash = hash2;
        }
    }

    public static final class DsoManifest {
        public final Dso[] dsos;

        public DsoManifest(Dso[] dsos2) {
            this.dsos = dsos2;
        }

        static final DsoManifest read(DataInput xdi) throws IOException {
            if (xdi.readByte() != 1) {
                throw new RuntimeException("wrong dso manifest version");
            }
            int nrDso = xdi.readInt();
            if (nrDso < 0) {
                throw new RuntimeException("illegal number of shared libraries");
            }
            Dso[] dsos2 = new Dso[nrDso];
            for (int i = 0; i < nrDso; i++) {
                dsos2[i] = new Dso(xdi.readUTF(), xdi.readUTF());
            }
            return new DsoManifest(dsos2);
        }

        public final void write(DataOutput xdo) throws IOException {
            xdo.writeByte(1);
            xdo.writeInt(this.dsos.length);
            for (int i = 0; i < this.dsos.length; i++) {
                xdo.writeUTF(this.dsos[i].name);
                xdo.writeUTF(this.dsos[i].hash);
            }
        }
    }

    protected static final class InputDso implements Closeable {
        public final InputStream content;
        public final Dso dso;

        public InputDso(Dso dso2, InputStream content2) {
            this.dso = dso2;
            this.content = content2;
        }

        public void close() throws IOException {
            this.content.close();
        }
    }

    protected static abstract class InputDsoIterator implements Closeable {
        public abstract boolean hasNext();

        public abstract InputDso next() throws IOException;

        protected InputDsoIterator() {
        }

        public void close() throws IOException {
        }
    }

    protected static abstract class Unpacker implements Closeable {
        /* access modifiers changed from: protected */
        public abstract DsoManifest getDsoManifest() throws IOException;

        /* access modifiers changed from: protected */
        public abstract InputDsoIterator openDsoIterator() throws IOException;

        protected Unpacker() {
        }

        public void close() throws IOException {
        }
    }

    /* access modifiers changed from: protected */
    public abstract Unpacker makeUnpacker() throws IOException;

    protected UnpackingSoSource(Context context, String name) {
        super(getSoStorePath(context, name), 1);
        this.mContext = context;
    }

    public static File getSoStorePath(Context context, String name) {
        return new File(context.getApplicationInfo().dataDir + "/" + name);
    }

    /* access modifiers changed from: private */
    public static void writeState(File stateFileName, byte state) throws IOException {
        RandomAccessFile stateFile = new RandomAccessFile(stateFileName, "rw");
        Throwable th = null;
        try {
            stateFile.seek(0);
            stateFile.write(state);
            stateFile.setLength(stateFile.getFilePointer());
            stateFile.getFD().sync();
            if (stateFile == null) {
                return;
            }
            if (th != null) {
                try {
                    stateFile.close();
                    return;
                } catch (Throwable x2) {
                    th.addSuppressed(x2);
                    return;
                }
            } else {
                stateFile.close();
                return;
            }
        } catch (Throwable th2) {
            Throwable th3 = th2;
            th = r2;
            th = th3;
        }
        if (stateFile != null) {
            if (th != null) {
                try {
                    stateFile.close();
                } catch (Throwable x22) {
                    th.addSuppressed(x22);
                }
            } else {
                stateFile.close();
            }
        }
        throw th;
        throw th;
    }

    private void deleteUnmentionedFiles(Dso[] dsos) throws IOException {
        String[] existingFiles = this.soDirectory.list();
        if (existingFiles == null) {
            throw new IOException("unable to list directory " + this.soDirectory);
        }
        for (String fileName : existingFiles) {
            if (!fileName.equals(STATE_FILE_NAME) && !fileName.equals(LOCK_FILE_NAME) && !fileName.equals(DEPS_FILE_NAME) && !fileName.equals(MANIFEST_FILE_NAME)) {
                boolean found = false;
                int j = 0;
                while (!found && j < dsos.length) {
                    if (dsos[j].name.equals(fileName)) {
                        found = true;
                    }
                    j++;
                }
                if (!found) {
                    File fileNameToDelete = new File(this.soDirectory, fileName);
                    Log.v(TAG, "deleting unaccounted-for file " + fileNameToDelete);
                    SysUtil.dumbDeleteRecursive(fileNameToDelete);
                }
            }
        }
    }

    private void extractDso(InputDso iDso, byte[] ioBuffer) throws IOException {
        RandomAccessFile dsoFile;
        Log.i(TAG, "extracting DSO " + iDso.dso.name);
        File dsoFileName = new File(this.soDirectory, iDso.dso.name);
        try {
            dsoFile = new RandomAccessFile(dsoFileName, "rw");
        } catch (IOException ex) {
            Log.w(TAG, "error overwriting " + dsoFileName + " trying to delete and start over", ex);
            dsoFileName.delete();
            dsoFile = new RandomAccessFile(dsoFileName, "rw");
        }
        try {
            int sizeHint = iDso.content.available();
            if (sizeHint > 1) {
                SysUtil.fallocateIfSupported(dsoFile.getFD(), (long) sizeHint);
            }
            SysUtil.copyBytes(dsoFile, iDso.content, Integer.MAX_VALUE, ioBuffer);
            dsoFile.setLength(dsoFile.getFilePointer());
            if (!dsoFileName.setExecutable(true, false)) {
                throw new IOException("cannot make file executable: " + dsoFileName);
            }
        } finally {
            dsoFile.close();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ac, code lost:
        r11 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b4, code lost:
        if (r12 != null) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00be, code lost:
        r11 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00bf, code lost:
        r12 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00cf, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00d0, code lost:
        r12.addSuppressed(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00e9, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x00ea, code lost:
        r12.addSuppressed(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x00ee, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x00f2, code lost:
        r11 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x00f3, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005c A[Catch:{ Throwable -> 0x00ac, all -> 0x00be }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00be A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0040 A[SYNTHETIC, Splitter:B:6:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:95:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void regenerate(byte r17, com.facebook.soloader.UnpackingSoSource.DsoManifest r18, com.facebook.soloader.UnpackingSoSource.InputDsoIterator r19) throws java.io.IOException {
        /*
            r16 = this;
            java.lang.String r11 = "fb-UnpackingSoSource"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "regenerating DSO store "
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.Class r13 = r16.getClass()
            java.lang.String r13 = r13.getName()
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r12 = r12.toString()
            android.util.Log.v(r11, r12)
            java.io.File r8 = new java.io.File
            r0 = r16
            java.io.File r11 = r0.soDirectory
            java.lang.String r12 = "dso_manifest"
            r8.<init>(r11, r12)
            java.io.RandomAccessFile r7 = new java.io.RandomAccessFile
            java.lang.String r11 = "rw"
            r7.<init>(r8, r11)
            r13 = 0
            r2 = 0
            r11 = 1
            r0 = r17
            if (r0 != r11) goto L_0x0096
            com.facebook.soloader.UnpackingSoSource$DsoManifest r2 = com.facebook.soloader.UnpackingSoSource.DsoManifest.read(r7)     // Catch:{ Exception -> 0x008e }
            r3 = r2
        L_0x003e:
            if (r3 != 0) goto L_0x00f7
            com.facebook.soloader.UnpackingSoSource$DsoManifest r2 = new com.facebook.soloader.UnpackingSoSource$DsoManifest     // Catch:{ Throwable -> 0x00f2, all -> 0x00be }
            r11 = 0
            com.facebook.soloader.UnpackingSoSource$Dso[] r11 = new com.facebook.soloader.UnpackingSoSource.Dso[r11]     // Catch:{ Throwable -> 0x00f2, all -> 0x00be }
            r2.<init>(r11)     // Catch:{ Throwable -> 0x00f2, all -> 0x00be }
        L_0x0048:
            r0 = r18
            com.facebook.soloader.UnpackingSoSource$Dso[] r11 = r0.dsos     // Catch:{ Throwable -> 0x00ac, all -> 0x00be }
            r0 = r16
            r0.deleteUnmentionedFiles(r11)     // Catch:{ Throwable -> 0x00ac, all -> 0x00be }
            r11 = 32768(0x8000, float:4.5918E-41)
            byte[] r6 = new byte[r11]     // Catch:{ Throwable -> 0x00ac, all -> 0x00be }
        L_0x0056:
            boolean r11 = r19.hasNext()     // Catch:{ Throwable -> 0x00ac, all -> 0x00be }
            if (r11 == 0) goto L_0x00d8
            com.facebook.soloader.UnpackingSoSource$InputDso r5 = r19.next()     // Catch:{ Throwable -> 0x00ac, all -> 0x00be }
            r12 = 0
            r9 = 1
            r4 = 0
        L_0x0063:
            if (r9 == 0) goto L_0x0098
            com.facebook.soloader.UnpackingSoSource$Dso[] r11 = r2.dsos     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            int r11 = r11.length     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            if (r4 >= r11) goto L_0x0098
            com.facebook.soloader.UnpackingSoSource$Dso[] r11 = r2.dsos     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            r11 = r11[r4]     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            java.lang.String r11 = r11.name     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            com.facebook.soloader.UnpackingSoSource$Dso r14 = r5.dso     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            java.lang.String r14 = r14.name     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            boolean r11 = r11.equals(r14)     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            if (r11 == 0) goto L_0x008b
            com.facebook.soloader.UnpackingSoSource$Dso[] r11 = r2.dsos     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            r11 = r11[r4]     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            java.lang.String r11 = r11.hash     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            com.facebook.soloader.UnpackingSoSource$Dso r14 = r5.dso     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            java.lang.String r14 = r14.hash     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            boolean r11 = r11.equals(r14)     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
            if (r11 == 0) goto L_0x008b
            r9 = 0
        L_0x008b:
            int r4 = r4 + 1
            goto L_0x0063
        L_0x008e:
            r1 = move-exception
            java.lang.String r11 = "fb-UnpackingSoSource"
            java.lang.String r12 = "error reading existing DSO manifest"
            android.util.Log.i(r11, r12, r1)     // Catch:{ Throwable -> 0x00ac, all -> 0x00be }
        L_0x0096:
            r3 = r2
            goto L_0x003e
        L_0x0098:
            if (r9 == 0) goto L_0x009f
            r0 = r16
            r0.extractDso(r5, r6)     // Catch:{ Throwable -> 0x00c1, all -> 0x00f5 }
        L_0x009f:
            if (r5 == 0) goto L_0x0056
            if (r12 == 0) goto L_0x00ba
            r5.close()     // Catch:{ Throwable -> 0x00a7, all -> 0x00be }
            goto L_0x0056
        L_0x00a7:
            r10 = move-exception
            r12.addSuppressed(r10)     // Catch:{ Throwable -> 0x00ac, all -> 0x00be }
            goto L_0x0056
        L_0x00ac:
            r11 = move-exception
        L_0x00ad:
            throw r11     // Catch:{ all -> 0x00ae }
        L_0x00ae:
            r12 = move-exception
            r15 = r12
            r12 = r11
            r11 = r15
        L_0x00b2:
            if (r7 == 0) goto L_0x00b9
            if (r12 == 0) goto L_0x00ee
            r7.close()     // Catch:{ Throwable -> 0x00e9 }
        L_0x00b9:
            throw r11
        L_0x00ba:
            r5.close()     // Catch:{ Throwable -> 0x00ac, all -> 0x00be }
            goto L_0x0056
        L_0x00be:
            r11 = move-exception
            r12 = r13
            goto L_0x00b2
        L_0x00c1:
            r11 = move-exception
            throw r11     // Catch:{ all -> 0x00c3 }
        L_0x00c3:
            r12 = move-exception
            r15 = r12
            r12 = r11
            r11 = r15
        L_0x00c7:
            if (r5 == 0) goto L_0x00ce
            if (r12 == 0) goto L_0x00d4
            r5.close()     // Catch:{ Throwable -> 0x00cf, all -> 0x00be }
        L_0x00ce:
            throw r11     // Catch:{ Throwable -> 0x00ac, all -> 0x00be }
        L_0x00cf:
            r10 = move-exception
            r12.addSuppressed(r10)     // Catch:{ Throwable -> 0x00ac, all -> 0x00be }
            goto L_0x00ce
        L_0x00d4:
            r5.close()     // Catch:{ Throwable -> 0x00ac, all -> 0x00be }
            goto L_0x00ce
        L_0x00d8:
            if (r7 == 0) goto L_0x00df
            if (r13 == 0) goto L_0x00e5
            r7.close()     // Catch:{ Throwable -> 0x00e0 }
        L_0x00df:
            return
        L_0x00e0:
            r10 = move-exception
            r13.addSuppressed(r10)
            goto L_0x00df
        L_0x00e5:
            r7.close()
            goto L_0x00df
        L_0x00e9:
            r10 = move-exception
            r12.addSuppressed(r10)
            goto L_0x00b9
        L_0x00ee:
            r7.close()
            goto L_0x00b9
        L_0x00f2:
            r11 = move-exception
            r2 = r3
            goto L_0x00ad
        L_0x00f5:
            r11 = move-exception
            goto L_0x00c7
        L_0x00f7:
            r2 = r3
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.UnpackingSoSource.regenerate(byte, com.facebook.soloader.UnpackingSoSource$DsoManifest, com.facebook.soloader.UnpackingSoSource$InputDsoIterator):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0130, code lost:
        r17 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:?, code lost:
        r8.addSuppressed(r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0137, code lost:
        r3 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0138, code lost:
        r5 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0140, code lost:
        r17 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0141, code lost:
        r5.addSuppressed(r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0147, code lost:
        r16.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x015a, code lost:
        r17 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x015b, code lost:
        r5.addSuppressed(r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0161, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00ec, code lost:
        r17 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        r5.addSuppressed(r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00fd, code lost:
        if (r5 != null) goto L_0x00ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        r16.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x010d, code lost:
        if (r5 != null) goto L_0x010f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0117, code lost:
        r3 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0118, code lost:
        r5 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x011c, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x011d, code lost:
        if (r13 != null) goto L_0x011f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x011f, code lost:
        if (r5 != null) goto L_0x0121;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
        r13.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0125, code lost:
        r17 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0126, code lost:
        r5.addSuppressed(r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x012c, code lost:
        r13.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0137 A[Catch:{ all -> 0x0137, all -> 0x0105 }, ExcHandler: all (th java.lang.Throwable), Splitter:B:11:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0117 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:21:0x009b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean refreshLocked(com.facebook.soloader.FileLocker r24, int r25, byte[] r26) throws java.io.IOException {
        /*
            r23 = this;
            java.io.File r7 = new java.io.File
            r0 = r23
            java.io.File r3 = r0.soDirectory
            java.lang.String r5 = "dso_state"
            r7.<init>(r3, r5)
            java.io.RandomAccessFile r15 = new java.io.RandomAccessFile
            java.lang.String r3 = "rw"
            r15.<init>(r7, r3)
            r5 = 0
            byte r14 = r15.readByte()     // Catch:{ EOFException -> 0x00c2, Throwable -> 0x00d1, all -> 0x019f }
            r3 = 1
            if (r14 == r3) goto L_0x0045
            java.lang.String r3 = "fb-UnpackingSoSource"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ EOFException -> 0x00c2, Throwable -> 0x00d1, all -> 0x019f }
            r8.<init>()     // Catch:{ EOFException -> 0x00c2, Throwable -> 0x00d1, all -> 0x019f }
            java.lang.String r18 = "dso store "
            r0 = r18
            java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ EOFException -> 0x00c2, Throwable -> 0x00d1, all -> 0x019f }
            r0 = r23
            java.io.File r0 = r0.soDirectory     // Catch:{ EOFException -> 0x00c2, Throwable -> 0x00d1, all -> 0x019f }
            r18 = r0
            r0 = r18
            java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ EOFException -> 0x00c2, Throwable -> 0x00d1, all -> 0x019f }
            java.lang.String r18 = " regeneration interrupted: wiping clean"
            r0 = r18
            java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ EOFException -> 0x00c2, Throwable -> 0x00d1, all -> 0x019f }
            java.lang.String r8 = r8.toString()     // Catch:{ EOFException -> 0x00c2, Throwable -> 0x00d1, all -> 0x019f }
            android.util.Log.v(r3, r8)     // Catch:{ EOFException -> 0x00c2, Throwable -> 0x00d1, all -> 0x019f }
            r14 = 0
        L_0x0045:
            if (r15 == 0) goto L_0x004c
            if (r5 == 0) goto L_0x00cc
            r15.close()     // Catch:{ Throwable -> 0x00c5 }
        L_0x004c:
            java.io.File r4 = new java.io.File
            r0 = r23
            java.io.File r3 = r0.soDirectory
            java.lang.String r5 = "dso_deps"
            r4.<init>(r3, r5)
            r10 = 0
            java.io.RandomAccessFile r9 = new java.io.RandomAccessFile
            java.lang.String r3 = "rw"
            r9.<init>(r4, r3)
            r18 = 0
            long r20 = r9.length()     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            r0 = r20
            int r3 = (int) r0     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            byte[] r12 = new byte[r3]     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            int r3 = r9.read(r12)     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            int r5 = r12.length     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            if (r3 == r5) goto L_0x0079
            java.lang.String r3 = "fb-UnpackingSoSource"
            java.lang.String r5 = "short read of so store deps file: marking unclean"
            android.util.Log.v(r3, r5)     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            r14 = 0
        L_0x0079:
            r0 = r26
            boolean r3 = java.util.Arrays.equals(r12, r0)     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            if (r3 != 0) goto L_0x0089
            java.lang.String r3 = "fb-UnpackingSoSource"
            java.lang.String r5 = "deps mismatch on deps store: regenerating"
            android.util.Log.v(r3, r5)     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            r14 = 0
        L_0x0089:
            if (r14 != 0) goto L_0x00b7
            java.lang.String r3 = "fb-UnpackingSoSource"
            java.lang.String r5 = "so store dirty: regenerating"
            android.util.Log.v(r3, r5)     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            r3 = 0
            writeState(r7, r3)     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            com.facebook.soloader.UnpackingSoSource$Unpacker r16 = r23.makeUnpacker()     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            r8 = 0
            com.facebook.soloader.UnpackingSoSource$DsoManifest r10 = r16.getDsoManifest()     // Catch:{ Throwable -> 0x00f3, all -> 0x0117 }
            com.facebook.soloader.UnpackingSoSource$InputDsoIterator r13 = r16.openDsoIterator()     // Catch:{ Throwable -> 0x00f3, all -> 0x0117 }
            r5 = 0
            r0 = r23
            r0.regenerate(r14, r10, r13)     // Catch:{ Throwable -> 0x011a }
            if (r13 == 0) goto L_0x00b0
            if (r5 == 0) goto L_0x0113
            r13.close()     // Catch:{ Throwable -> 0x00ec, all -> 0x0117 }
        L_0x00b0:
            if (r16 == 0) goto L_0x00b7
            if (r8 == 0) goto L_0x013b
            r16.close()     // Catch:{ Throwable -> 0x0130, all -> 0x0137 }
        L_0x00b7:
            if (r9 == 0) goto L_0x00be
            if (r18 == 0) goto L_0x0155
            r9.close()     // Catch:{ Throwable -> 0x014b }
        L_0x00be:
            if (r10 != 0) goto L_0x0165
            r3 = 0
        L_0x00c1:
            return r3
        L_0x00c2:
            r11 = move-exception
            r14 = 0
            goto L_0x0045
        L_0x00c5:
            r17 = move-exception
            r0 = r17
            r5.addSuppressed(r0)
            goto L_0x004c
        L_0x00cc:
            r15.close()
            goto L_0x004c
        L_0x00d1:
            r3 = move-exception
            throw r3     // Catch:{ all -> 0x00d3 }
        L_0x00d3:
            r5 = move-exception
            r22 = r5
            r5 = r3
            r3 = r22
        L_0x00d9:
            if (r15 == 0) goto L_0x00e0
            if (r5 == 0) goto L_0x00e8
            r15.close()     // Catch:{ Throwable -> 0x00e1 }
        L_0x00e0:
            throw r3
        L_0x00e1:
            r17 = move-exception
            r0 = r17
            r5.addSuppressed(r0)
            goto L_0x00e0
        L_0x00e8:
            r15.close()
            goto L_0x00e0
        L_0x00ec:
            r17 = move-exception
            r0 = r17
            r5.addSuppressed(r0)     // Catch:{ Throwable -> 0x00f3, all -> 0x0117 }
            goto L_0x00b0
        L_0x00f3:
            r3 = move-exception
            throw r3     // Catch:{ all -> 0x00f5 }
        L_0x00f5:
            r5 = move-exception
            r22 = r5
            r5 = r3
            r3 = r22
        L_0x00fb:
            if (r16 == 0) goto L_0x0102
            if (r5 == 0) goto L_0x0147
            r16.close()     // Catch:{ Throwable -> 0x0140, all -> 0x0137 }
        L_0x0102:
            throw r3     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
        L_0x0103:
            r3 = move-exception
            throw r3     // Catch:{ all -> 0x0105 }
        L_0x0105:
            r5 = move-exception
            r22 = r5
            r5 = r3
            r3 = r22
        L_0x010b:
            if (r9 == 0) goto L_0x0112
            if (r5 == 0) goto L_0x0161
            r9.close()     // Catch:{ Throwable -> 0x015a }
        L_0x0112:
            throw r3
        L_0x0113:
            r13.close()     // Catch:{ Throwable -> 0x00f3, all -> 0x0117 }
            goto L_0x00b0
        L_0x0117:
            r3 = move-exception
            r5 = r8
            goto L_0x00fb
        L_0x011a:
            r5 = move-exception
            throw r5     // Catch:{ all -> 0x011c }
        L_0x011c:
            r3 = move-exception
            if (r13 == 0) goto L_0x0124
            if (r5 == 0) goto L_0x012c
            r13.close()     // Catch:{ Throwable -> 0x0125, all -> 0x0117 }
        L_0x0124:
            throw r3     // Catch:{ Throwable -> 0x00f3, all -> 0x0117 }
        L_0x0125:
            r17 = move-exception
            r0 = r17
            r5.addSuppressed(r0)     // Catch:{ Throwable -> 0x00f3, all -> 0x0117 }
            goto L_0x0124
        L_0x012c:
            r13.close()     // Catch:{ Throwable -> 0x00f3, all -> 0x0117 }
            goto L_0x0124
        L_0x0130:
            r17 = move-exception
            r0 = r17
            r8.addSuppressed(r0)     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            goto L_0x00b7
        L_0x0137:
            r3 = move-exception
            r5 = r18
            goto L_0x010b
        L_0x013b:
            r16.close()     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            goto L_0x00b7
        L_0x0140:
            r17 = move-exception
            r0 = r17
            r5.addSuppressed(r0)     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            goto L_0x0102
        L_0x0147:
            r16.close()     // Catch:{ Throwable -> 0x0103, all -> 0x0137 }
            goto L_0x0102
        L_0x014b:
            r17 = move-exception
            r0 = r18
            r1 = r17
            r0.addSuppressed(r1)
            goto L_0x00be
        L_0x0155:
            r9.close()
            goto L_0x00be
        L_0x015a:
            r17 = move-exception
            r0 = r17
            r5.addSuppressed(r0)
            goto L_0x0112
        L_0x0161:
            r9.close()
            goto L_0x0112
        L_0x0165:
            r6 = r10
            com.facebook.soloader.UnpackingSoSource$1 r2 = new com.facebook.soloader.UnpackingSoSource$1
            r3 = r23
            r5 = r26
            r8 = r24
            r2.<init>(r4, r5, r6, r7, r8)
            r3 = r25 & 1
            if (r3 == 0) goto L_0x019b
            java.lang.Thread r3 = new java.lang.Thread
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r8 = "SoSync:"
            java.lang.StringBuilder r5 = r5.append(r8)
            r0 = r23
            java.io.File r8 = r0.soDirectory
            java.lang.String r8 = r8.getName()
            java.lang.StringBuilder r5 = r5.append(r8)
            java.lang.String r5 = r5.toString()
            r3.<init>(r2, r5)
            r3.start()
        L_0x0198:
            r3 = 1
            goto L_0x00c1
        L_0x019b:
            r2.run()
            goto L_0x0198
        L_0x019f:
            r3 = move-exception
            goto L_0x00d9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.UnpackingSoSource.refreshLocked(com.facebook.soloader.FileLocker, int, byte[]):boolean");
    }

    /* access modifiers changed from: protected */
    public byte[] getDepsBlock() throws IOException {
        Parcel parcel = Parcel.obtain();
        Unpacker u = makeUnpacker();
        Throwable th = null;
        try {
            Dso[] dsos = u.getDsoManifest().dsos;
            parcel.writeByte(1);
            parcel.writeInt(dsos.length);
            for (int i = 0; i < dsos.length; i++) {
                parcel.writeString(dsos[i].name);
                parcel.writeString(dsos[i].hash);
            }
            if (u != null) {
                if (th != null) {
                    try {
                        u.close();
                    } catch (Throwable x2) {
                        th.addSuppressed(x2);
                    }
                } else {
                    u.close();
                }
            }
            byte[] depsBlock = parcel.marshall();
            parcel.recycle();
            return depsBlock;
        } catch (Throwable th2) {
            Throwable th3 = th2;
            th = r6;
            th = th3;
        }
        throw th;
        if (u != null) {
            if (th != null) {
                try {
                    u.close();
                } catch (Throwable x22) {
                    th.addSuppressed(x22);
                }
            } else {
                u.close();
            }
        }
        throw th;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public void prepare(int flags) throws IOException {
        SysUtil.mkdirOrThrow(this.soDirectory);
        FileLocker lock = FileLocker.lock(new File(this.soDirectory, LOCK_FILE_NAME));
        try {
            Log.v(TAG, "locked dso store " + this.soDirectory);
            if (refreshLocked(lock, flags, getDepsBlock())) {
                lock = null;
            } else {
                Log.i(TAG, "dso store is up-to-date: " + this.soDirectory);
            }
            if (lock != null) {
                Log.v(TAG, "releasing dso store lock for " + this.soDirectory);
                lock.close();
                return;
            }
            Log.v(TAG, "not releasing dso store lock for " + this.soDirectory + " (syncer thread started)");
        } catch (Throwable th) {
            if (lock != null) {
                Log.v(TAG, "releasing dso store lock for " + this.soDirectory);
                lock.close();
            } else {
                Log.v(TAG, "not releasing dso store lock for " + this.soDirectory + " (syncer thread started)");
            }
            throw th;
        }
    }
}
