package com.facebook.react.devsupport;

import android.app.Activity;
import com.facebook.react.bridge.JavaJSExecutor.Factory;
import javax.annotation.Nullable;

public interface ReactInstanceManagerDevHelper {
    @Nullable
    Activity getCurrentActivity();

    void onJSBundleLoadedFromServer();

    void onReloadWithJSDebugger(Factory factory);

    void toggleElementInspector();
}
