package com.facebook.react.devsupport;

import com.facebook.common.util.UriUtil;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.devsupport.interfaces.StackFrame;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StackTraceHelper {
    public static final String COLUMN_KEY = "column";
    public static final String LINE_NUMBER_KEY = "lineNumber";
    private static final Pattern STACK_FRAME_PATTERN = Pattern.compile("^(?:(.*?)@)?(.*?)\\:([0-9]+)\\:([0-9]+)$");

    public static class StackFrameImpl implements StackFrame {
        private final int mColumn;
        private final String mFile;
        private final String mFileName;
        private final int mLine;
        private final String mMethod;

        private StackFrameImpl(String file, String method, int line, int column) {
            this.mFile = file;
            this.mMethod = method;
            this.mLine = line;
            this.mColumn = column;
            this.mFileName = file != null ? new File(file).getName() : "";
        }

        private StackFrameImpl(String file, String fileName, String method, int line, int column) {
            this.mFile = file;
            this.mFileName = fileName;
            this.mMethod = method;
            this.mLine = line;
            this.mColumn = column;
        }

        public String getFile() {
            return this.mFile;
        }

        public String getMethod() {
            return this.mMethod;
        }

        public int getLine() {
            return this.mLine;
        }

        public int getColumn() {
            return this.mColumn;
        }

        public String getFileName() {
            return this.mFileName;
        }

        public JSONObject toJSON() {
            return new JSONObject(MapBuilder.of(UriUtil.LOCAL_FILE_SCHEME, getFile(), "methodName", getMethod(), StackTraceHelper.LINE_NUMBER_KEY, Integer.valueOf(getLine()), StackTraceHelper.COLUMN_KEY, Integer.valueOf(getColumn())));
        }
    }

    public static StackFrame[] convertJsStackTrace(@Nullable ReadableArray stack) {
        int size = stack != null ? stack.size() : 0;
        StackFrame[] result = new StackFrame[size];
        for (int i = 0; i < size; i++) {
            ReadableType type = stack.getType(i);
            if (type == ReadableType.Map) {
                ReadableMap frame = stack.getMap(i);
                String methodName = frame.getString("methodName");
                String fileName = frame.getString(UriUtil.LOCAL_FILE_SCHEME);
                int lineNumber = -1;
                if (frame.hasKey(LINE_NUMBER_KEY) && !frame.isNull(LINE_NUMBER_KEY)) {
                    lineNumber = frame.getInt(LINE_NUMBER_KEY);
                }
                int columnNumber = -1;
                if (frame.hasKey(COLUMN_KEY) && !frame.isNull(COLUMN_KEY)) {
                    columnNumber = frame.getInt(COLUMN_KEY);
                }
                result[i] = new StackFrameImpl(fileName, methodName, lineNumber, columnNumber);
            } else if (type == ReadableType.String) {
                result[i] = new StackFrameImpl((String) null, stack.getString(i), -1, -1);
            }
        }
        return result;
    }

    public static StackFrame[] convertJsStackTrace(JSONArray stack) {
        int size = stack != null ? stack.length() : 0;
        StackFrame[] result = new StackFrame[size];
        int i = 0;
        while (i < size) {
            try {
                JSONObject frame = stack.getJSONObject(i);
                String methodName = frame.getString("methodName");
                String fileName = frame.getString(UriUtil.LOCAL_FILE_SCHEME);
                int lineNumber = -1;
                if (frame.has(LINE_NUMBER_KEY) && !frame.isNull(LINE_NUMBER_KEY)) {
                    lineNumber = frame.getInt(LINE_NUMBER_KEY);
                }
                int columnNumber = -1;
                if (frame.has(COLUMN_KEY) && !frame.isNull(COLUMN_KEY)) {
                    columnNumber = frame.getInt(COLUMN_KEY);
                }
                result[i] = new StackFrameImpl(fileName, methodName, lineNumber, columnNumber);
                i++;
            } catch (JSONException exception) {
                throw new RuntimeException(exception);
            }
        }
        return result;
    }

    public static StackFrame[] convertJsStackTrace(String stack) {
        String[] stackTrace = stack.split(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        StackFrame[] result = new StackFrame[stackTrace.length];
        for (int i = 0; i < stackTrace.length; i++) {
            Matcher matcher = STACK_FRAME_PATTERN.matcher(stackTrace[i]);
            if (matcher.find()) {
                result[i] = new StackFrameImpl(matcher.group(2), matcher.group(1) == null ? "(unknown)" : matcher.group(1), Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)));
            } else {
                result[i] = new StackFrameImpl((String) null, stackTrace[i], -1, -1);
            }
        }
        return result;
    }

    public static StackFrame[] convertJavaStackTrace(Throwable exception) {
        StackTraceElement[] stackTrace = exception.getStackTrace();
        StackFrame[] result = new StackFrame[stackTrace.length];
        for (int i = 0; i < stackTrace.length; i++) {
            result[i] = new StackFrameImpl(stackTrace[i].getClassName(), stackTrace[i].getFileName(), stackTrace[i].getMethodName(), stackTrace[i].getLineNumber(), -1);
        }
        return result;
    }

    public static String formatFrameSource(StackFrame frame) {
        StringBuilder lineInfo = new StringBuilder();
        lineInfo.append(frame.getFileName());
        int line = frame.getLine();
        if (line > 0) {
            lineInfo.append(":").append(line);
            int column = frame.getColumn();
            if (column > 0) {
                lineInfo.append(":").append(column);
            }
        }
        return lineInfo.toString();
    }

    public static String formatStackTrace(String title, StackFrame[] stack) {
        StringBuilder stackTrace = new StringBuilder();
        stackTrace.append(title).append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        for (StackFrame frame : stack) {
            stackTrace.append(frame.getMethod()).append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE).append("    ").append(formatFrameSource(frame)).append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        }
        return stackTrace.toString();
    }
}
