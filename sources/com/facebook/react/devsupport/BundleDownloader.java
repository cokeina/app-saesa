package com.facebook.react.devsupport;

import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.os.EnvironmentCompat;
import android.util.Log;
import com.facebook.common.logging.FLog;
import com.facebook.infer.annotation.Assertions;
import com.facebook.react.common.DebugServerException;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.devsupport.MultipartStreamReader.ChunkListener;
import com.facebook.react.devsupport.interfaces.DevBundleDownloadListener;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request.Builder;
import okhttp3.Response;
import okio.Buffer;
import okio.BufferedSource;
import okio.Okio;
import okio.Sink;
import okio.Source;
import org.json.JSONException;
import org.json.JSONObject;

public class BundleDownloader {
    private static final int FILES_CHANGED_COUNT_NOT_BUILT_BY_BUNDLER = -2;
    private static final String TAG = "BundleDownloader";
    private final BundleDeltaClient mBundleDeltaClient = new BundleDeltaClient();
    private final OkHttpClient mClient;
    /* access modifiers changed from: private */
    @Nullable
    public Call mDownloadBundleFromURLCall;

    public static class BundleInfo {
        /* access modifiers changed from: private */
        public int mFilesChangedCount;
        /* access modifiers changed from: private */
        @Nullable
        public String mUrl;

        @Nullable
        public static BundleInfo fromJSONString(String jsonStr) {
            if (jsonStr == null) {
                return null;
            }
            BundleInfo info = new BundleInfo();
            try {
                JSONObject obj = new JSONObject(jsonStr);
                info.mUrl = obj.getString("url");
                info.mFilesChangedCount = obj.getInt("filesChangedCount");
                return info;
            } catch (JSONException e) {
                Log.e(BundleDownloader.TAG, "Invalid bundle info: ", e);
                return null;
            }
        }

        @Nullable
        public String toJSONString() {
            JSONObject obj = new JSONObject();
            try {
                obj.put("url", this.mUrl);
                obj.put("filesChangedCount", this.mFilesChangedCount);
                return obj.toString();
            } catch (JSONException e) {
                Log.e(BundleDownloader.TAG, "Can't serialize bundle info: ", e);
                return null;
            }
        }

        public String getUrl() {
            return this.mUrl != null ? this.mUrl : EnvironmentCompat.MEDIA_UNKNOWN;
        }

        public int getFilesChangedCount() {
            return this.mFilesChangedCount;
        }
    }

    public BundleDownloader(OkHttpClient client) {
        this.mClient = client;
    }

    public void downloadBundleFromURL(final DevBundleDownloadListener callback, final File outputFile, String bundleURL, @Nullable final BundleInfo bundleInfo) {
        this.mDownloadBundleFromURLCall = (Call) Assertions.assertNotNull(this.mClient.newCall(new Builder().url(this.mBundleDeltaClient.toDeltaUrl(bundleURL)).build()));
        this.mDownloadBundleFromURLCall.enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                if (BundleDownloader.this.mDownloadBundleFromURLCall == null || BundleDownloader.this.mDownloadBundleFromURLCall.isCanceled()) {
                    BundleDownloader.this.mDownloadBundleFromURLCall = null;
                    return;
                }
                BundleDownloader.this.mDownloadBundleFromURLCall = null;
                callback.onFailure(DebugServerException.makeGeneric("Could not connect to development server.", "URL: " + call.request().url().toString(), e));
            }

            public void onResponse(Call call, Response response) throws IOException {
                if (BundleDownloader.this.mDownloadBundleFromURLCall == null || BundleDownloader.this.mDownloadBundleFromURLCall.isCanceled()) {
                    BundleDownloader.this.mDownloadBundleFromURLCall = null;
                    return;
                }
                BundleDownloader.this.mDownloadBundleFromURLCall = null;
                final String url = response.request().url().toString();
                Matcher match = Pattern.compile("multipart/mixed;.*boundary=\"([^\"]+)\"").matcher(response.header("content-type"));
                if (match.find()) {
                    final Response response2 = response;
                    if (!new MultipartStreamReader(response.body().source(), match.group(1)).readAllParts(new ChunkListener() {
                        public void onChunkComplete(Map<String, String> headers, Buffer body, boolean isLastChunk) throws IOException {
                            if (isLastChunk) {
                                int status = response2.code();
                                if (headers.containsKey("X-Http-Status")) {
                                    status = Integer.parseInt((String) headers.get("X-Http-Status"));
                                }
                                BundleDownloader.this.processBundleResult(url, status, Headers.of(headers), body, outputFile, bundleInfo, callback);
                            } else if (headers.containsKey("Content-Type") && ((String) headers.get("Content-Type")).equals("application/json")) {
                                try {
                                    JSONObject progress = new JSONObject(body.readUtf8());
                                    String status2 = null;
                                    if (progress.has("status")) {
                                        status2 = progress.getString("status");
                                    }
                                    Integer done = null;
                                    if (progress.has("done")) {
                                        done = Integer.valueOf(progress.getInt("done"));
                                    }
                                    Integer total = null;
                                    if (progress.has("total")) {
                                        total = Integer.valueOf(progress.getInt("total"));
                                    }
                                    callback.onProgress(status2, done, total);
                                } catch (JSONException e) {
                                    FLog.e(ReactConstants.TAG, "Error parsing progress JSON. " + e.toString());
                                }
                            }
                        }

                        public void onChunkProgress(Map<String, String> headers, long loaded, long total) throws IOException {
                            if ("application/javascript".equals(headers.get("Content-Type"))) {
                                callback.onProgress("Downloading JavaScript bundle", Integer.valueOf((int) (loaded / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID)), Integer.valueOf((int) (total / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID)));
                            }
                        }
                    })) {
                        callback.onFailure(new DebugServerException("Error while reading multipart response.\n\nResponse code: " + response.code() + "\n\nURL: " + call.request().url().toString() + "\n\n"));
                        return;
                    }
                    return;
                }
                BundleDownloader.this.processBundleResult(url, response.code(), response.headers(), Okio.buffer((Source) response.body().source()), outputFile, bundleInfo, callback);
            }
        });
    }

    /* access modifiers changed from: private */
    public void processBundleResult(String url, int statusCode, Headers headers, BufferedSource body, File outputFile, BundleInfo bundleInfo, DevBundleDownloadListener callback) throws IOException {
        boolean bundleUpdated;
        if (statusCode != 200) {
            String bodyString = body.readUtf8();
            DebugServerException debugServerException = DebugServerException.parse(bodyString);
            if (debugServerException != null) {
                callback.onFailure(debugServerException);
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("The development server returned response error code: ").append(statusCode).append("\n\n").append("URL: ").append(url).append("\n\n").append("Body:\n").append(bodyString);
            callback.onFailure(new DebugServerException(sb.toString()));
            return;
        }
        if (bundleInfo != null) {
            populateBundleInfo(url, headers, bundleInfo);
        }
        File tmpFile = new File(outputFile.getPath() + ".tmp");
        if (BundleDeltaClient.isDeltaUrl(url)) {
            bundleUpdated = this.mBundleDeltaClient.storeDeltaInFile(body, tmpFile);
        } else {
            this.mBundleDeltaClient.reset();
            bundleUpdated = storePlainJSInFile(body, tmpFile);
        }
        if (!bundleUpdated || tmpFile.renameTo(outputFile)) {
            callback.onSuccess();
            return;
        }
        throw new IOException("Couldn't rename " + tmpFile + " to " + outputFile);
    }

    private static boolean storePlainJSInFile(BufferedSource body, File outputFile) throws IOException {
        Sink output = null;
        try {
            output = Okio.sink(outputFile);
            body.readAll(output);
            return true;
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    private static void populateBundleInfo(String url, Headers headers, BundleInfo bundleInfo) {
        bundleInfo.mUrl = url;
        String filesChangedCountStr = headers.get("X-Metro-Files-Changed-Count");
        if (filesChangedCountStr != null) {
            try {
                bundleInfo.mFilesChangedCount = Integer.parseInt(filesChangedCountStr);
            } catch (NumberFormatException e) {
                bundleInfo.mFilesChangedCount = -2;
            }
        }
    }
}
