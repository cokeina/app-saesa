package com.facebook.react.devsupport;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import okio.Buffer;
import okio.BufferedSource;
import okio.ByteString;

public class MultipartStreamReader {
    private static final String CRLF = "\r\n";
    private final String mBoundary;
    private long mLastProgressEvent;
    private final BufferedSource mSource;

    public interface ChunkListener {
        void onChunkComplete(Map<String, String> map, Buffer buffer, boolean z) throws IOException;

        void onChunkProgress(Map<String, String> map, long j, long j2) throws IOException;
    }

    public MultipartStreamReader(BufferedSource source, String boundary) {
        this.mSource = source;
        this.mBoundary = boundary;
    }

    private Map<String, String> parseHeaders(Buffer data) {
        String[] lines;
        Map<String, String> headers = new HashMap<>();
        for (String line : data.readUtf8().split(CRLF)) {
            int indexOfSeparator = line.indexOf(":");
            if (indexOfSeparator != -1) {
                headers.put(line.substring(0, indexOfSeparator).trim(), line.substring(indexOfSeparator + 1).trim());
            }
        }
        return headers;
    }

    private void emitChunk(Buffer chunk, boolean done, ChunkListener listener) throws IOException {
        ByteString marker = ByteString.encodeUtf8("\r\n\r\n");
        long indexOfMarker = chunk.indexOf(marker);
        if (indexOfMarker == -1) {
            listener.onChunkComplete(null, chunk, done);
            return;
        }
        Buffer headers = new Buffer();
        Buffer body = new Buffer();
        chunk.read(headers, indexOfMarker);
        chunk.skip((long) marker.size());
        chunk.readAll(body);
        listener.onChunkComplete(parseHeaders(headers), body, done);
    }

    private void emitProgress(Map<String, String> headers, long contentLength, boolean isFinal, ChunkListener listener) throws IOException {
        if (headers != null && listener != null) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - this.mLastProgressEvent > 16 || isFinal) {
                this.mLastProgressEvent = currentTime;
                listener.onChunkProgress(headers, contentLength, headers.get("Content-Length") != null ? Long.parseLong((String) headers.get("Content-Length")) : 0);
            }
        }
    }

    public boolean readAllParts(ChunkListener listener) throws IOException {
        ByteString delimiter = ByteString.encodeUtf8("\r\n--" + this.mBoundary + CRLF);
        ByteString closeDelimiter = ByteString.encodeUtf8("\r\n--" + this.mBoundary + "--" + CRLF);
        ByteString headersDelimiter = ByteString.encodeUtf8("\r\n\r\n");
        long chunkStart = 0;
        long bytesSeen = 0;
        Buffer content = new Buffer();
        Map<String, String> currentHeaders = null;
        long currentHeadersLength = 0;
        while (true) {
            boolean isCloseDelimiter = false;
            long searchStart = Math.max(bytesSeen - ((long) closeDelimiter.size()), chunkStart);
            long indexOfDelimiter = content.indexOf(delimiter, searchStart);
            if (indexOfDelimiter == -1) {
                isCloseDelimiter = true;
                indexOfDelimiter = content.indexOf(closeDelimiter, searchStart);
            }
            if (indexOfDelimiter == -1) {
                bytesSeen = content.size();
                if (currentHeaders == null) {
                    long indexOfHeaders = content.indexOf(headersDelimiter, searchStart);
                    if (indexOfHeaders >= 0) {
                        this.mSource.read(content, indexOfHeaders);
                        Buffer headers = new Buffer();
                        content.copyTo(headers, searchStart, indexOfHeaders - searchStart);
                        currentHeadersLength = headers.size() + ((long) headersDelimiter.size());
                        currentHeaders = parseHeaders(headers);
                    }
                } else {
                    emitProgress(currentHeaders, content.size() - currentHeadersLength, false, listener);
                }
                if (this.mSource.read(content, (long) 4096) <= 0) {
                    return false;
                }
            } else {
                long chunkEnd = indexOfDelimiter;
                long length = chunkEnd - chunkStart;
                if (chunkStart > 0) {
                    Buffer chunk = new Buffer();
                    content.skip(chunkStart);
                    content.read(chunk, length);
                    emitProgress(currentHeaders, chunk.size() - currentHeadersLength, true, listener);
                    emitChunk(chunk, isCloseDelimiter, listener);
                    currentHeaders = null;
                    currentHeadersLength = 0;
                } else {
                    content.skip(chunkEnd);
                }
                if (isCloseDelimiter) {
                    return true;
                }
                chunkStart = (long) delimiter.size();
                bytesSeen = chunkStart;
            }
        }
    }
}
