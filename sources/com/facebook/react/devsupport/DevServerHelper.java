package com.facebook.react.devsupport;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.Toast;
import com.facebook.common.logging.FLog;
import com.facebook.infer.annotation.Assertions;
import com.facebook.react.R;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.common.network.OkHttpCallUtil;
import com.facebook.react.devsupport.BundleDownloader.BundleInfo;
import com.facebook.react.devsupport.InspectorPackagerConnection.BundleStatusProvider;
import com.facebook.react.devsupport.interfaces.DevBundleDownloadListener;
import com.facebook.react.devsupport.interfaces.PackagerStatusCallback;
import com.facebook.react.devsupport.interfaces.StackFrame;
import com.facebook.react.modules.systeminfo.AndroidInfoHelpers;
import com.facebook.react.packagerconnection.FileIoHandler;
import com.facebook.react.packagerconnection.JSPackagerClient;
import com.facebook.react.packagerconnection.NotificationOnlyHandler;
import com.facebook.react.packagerconnection.ReconnectingWebSocket.ConnectionCallback;
import com.facebook.react.packagerconnection.RequestHandler;
import com.facebook.react.packagerconnection.RequestOnlyHandler;
import com.facebook.react.packagerconnection.Responder;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.ConnectionPool;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Okio;
import okio.Sink;
import okio.Source;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DevServerHelper {
    private static final String DEBUGGER_MSG_DISABLE = "{ \"id\":1,\"method\":\"Debugger.disable\" }";
    private static final int HTTP_CONNECT_TIMEOUT_MS = 5000;
    private static final int LONG_POLL_FAILURE_DELAY_MS = 5000;
    private static final int LONG_POLL_KEEP_ALIVE_DURATION_MS = 120000;
    private static final String PACKAGER_OK_STATUS = "packager-status:running";
    public static final String RELOAD_APP_EXTRA_JS_PROXY = "jsproxy";
    private final BundleDownloader mBundleDownloader = new BundleDownloader(this.mClient);
    /* access modifiers changed from: private */
    public BundleStatusProvider mBundlerStatusProvider;
    private final OkHttpClient mClient = new Builder().connectTimeout(5000, TimeUnit.MILLISECONDS).readTimeout(0, TimeUnit.MILLISECONDS).writeTimeout(0, TimeUnit.MILLISECONDS).build();
    /* access modifiers changed from: private */
    @Nullable
    public InspectorPackagerConnection mInspectorPackagerConnection;
    @Nullable
    private OkHttpClient mOnChangePollingClient;
    /* access modifiers changed from: private */
    public boolean mOnChangePollingEnabled;
    /* access modifiers changed from: private */
    @Nullable
    public OnServerContentChangeListener mOnServerContentChangeListener;
    /* access modifiers changed from: private */
    public final String mPackageName;
    /* access modifiers changed from: private */
    @Nullable
    public JSPackagerClient mPackagerClient;
    /* access modifiers changed from: private */
    public final Handler mRestartOnChangePollingHandler = new Handler();
    /* access modifiers changed from: private */
    public final DevInternalSettings mSettings;

    private enum BundleType {
        BUNDLE("bundle"),
        DELTA("delta"),
        MAP("map");
        
        private final String mTypeID;

        private BundleType(String typeID) {
            this.mTypeID = typeID;
        }

        public String typeID() {
            return this.mTypeID;
        }
    }

    public interface OnServerContentChangeListener {
        void onServerContentChanged();
    }

    public interface PackagerCommandListener {
        void onCaptureHeapCommand(Responder responder);

        void onPackagerConnected();

        void onPackagerDevMenuCommand();

        void onPackagerDisconnected();

        void onPackagerReloadCommand();

        void onPokeSamplingProfilerCommand(Responder responder);
    }

    public interface SymbolicationListener {
        void onSymbolicationComplete(@Nullable Iterable<StackFrame> iterable);
    }

    public DevServerHelper(DevInternalSettings settings, String packageName, BundleStatusProvider bundleStatusProvider) {
        this.mSettings = settings;
        this.mBundlerStatusProvider = bundleStatusProvider;
        this.mPackageName = packageName;
    }

    public void openPackagerConnection(final String clientId, final PackagerCommandListener commandListener) {
        if (this.mPackagerClient != null) {
            FLog.w(ReactConstants.TAG, "Packager connection already open, nooping.");
        } else {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... backgroundParams) {
                    Map<String, RequestHandler> handlers = new HashMap<>();
                    handlers.put("reload", new NotificationOnlyHandler() {
                        public void onNotification(@Nullable Object params) {
                            commandListener.onPackagerReloadCommand();
                        }
                    });
                    handlers.put("devMenu", new NotificationOnlyHandler() {
                        public void onNotification(@Nullable Object params) {
                            commandListener.onPackagerDevMenuCommand();
                        }
                    });
                    handlers.put("captureHeap", new RequestOnlyHandler() {
                        public void onRequest(@Nullable Object params, Responder responder) {
                            commandListener.onCaptureHeapCommand(responder);
                        }
                    });
                    handlers.put("pokeSamplingProfiler", new RequestOnlyHandler() {
                        public void onRequest(@Nullable Object params, Responder responder) {
                            commandListener.onPokeSamplingProfilerCommand(responder);
                        }
                    });
                    handlers.putAll(new FileIoHandler().handlers());
                    DevServerHelper.this.mPackagerClient = new JSPackagerClient(clientId, DevServerHelper.this.mSettings.getPackagerConnectionSettings(), handlers, new ConnectionCallback() {
                        public void onConnected() {
                            commandListener.onPackagerConnected();
                        }

                        public void onDisconnected() {
                            commandListener.onPackagerDisconnected();
                        }
                    });
                    DevServerHelper.this.mPackagerClient.init();
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    public void closePackagerConnection() {
        new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... params) {
                if (DevServerHelper.this.mPackagerClient != null) {
                    DevServerHelper.this.mPackagerClient.close();
                    DevServerHelper.this.mPackagerClient = null;
                }
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    public void openInspectorConnection() {
        if (this.mInspectorPackagerConnection != null) {
            FLog.w(ReactConstants.TAG, "Inspector connection already open, nooping.");
        } else {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... params) {
                    DevServerHelper.this.mInspectorPackagerConnection = new InspectorPackagerConnection(DevServerHelper.this.getInspectorDeviceUrl(), DevServerHelper.this.mPackageName, DevServerHelper.this.mBundlerStatusProvider);
                    DevServerHelper.this.mInspectorPackagerConnection.connect();
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    public void disableDebugger() {
        if (this.mInspectorPackagerConnection != null) {
            this.mInspectorPackagerConnection.sendEventToAllConnections(DEBUGGER_MSG_DISABLE);
        }
    }

    public void closeInspectorConnection() {
        new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... params) {
                if (DevServerHelper.this.mInspectorPackagerConnection != null) {
                    DevServerHelper.this.mInspectorPackagerConnection.closeQuietly();
                    DevServerHelper.this.mInspectorPackagerConnection = null;
                }
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    public void attachDebugger(final Context context, final String title) {
        new AsyncTask<Void, String, Boolean>() {
            /* access modifiers changed from: protected */
            public Boolean doInBackground(Void... ignore) {
                return Boolean.valueOf(doSync());
            }

            public boolean doSync() {
                try {
                    new OkHttpClient().newCall(new Request.Builder().url(DevServerHelper.this.getInspectorAttachUrl(title)).build()).execute();
                    return true;
                } catch (IOException e) {
                    FLog.e(ReactConstants.TAG, "Failed to send attach request to Inspector", (Throwable) e);
                    return false;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean result) {
                if (!result.booleanValue()) {
                    Toast.makeText(context, context.getString(R.string.catalyst_debugjs_nuclide_failure), 1).show();
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    public void symbolicateStackTrace(Iterable<StackFrame> stackFrames, final SymbolicationListener listener) {
        try {
            String symbolicateURL = createSymbolicateURL(this.mSettings.getPackagerConnectionSettings().getDebugServerHost());
            JSONArray jsonStackFrames = new JSONArray();
            for (StackFrame stackFrame : stackFrames) {
                jsonStackFrames.put(stackFrame.toJSON());
            }
            ((Call) Assertions.assertNotNull(this.mClient.newCall(new Request.Builder().url(symbolicateURL).post(RequestBody.create(MediaType.parse("application/json"), new JSONObject().put("stack", jsonStackFrames).toString())).build()))).enqueue(new Callback() {
                public void onFailure(Call call, IOException e) {
                    FLog.w(ReactConstants.TAG, "Got IOException when attempting symbolicate stack trace: " + e.getMessage());
                    listener.onSymbolicationComplete(null);
                }

                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        listener.onSymbolicationComplete(Arrays.asList(StackTraceHelper.convertJsStackTrace(new JSONObject(response.body().string()).getJSONArray("stack"))));
                    } catch (JSONException e) {
                        listener.onSymbolicationComplete(null);
                    }
                }
            });
        } catch (JSONException e) {
            FLog.w(ReactConstants.TAG, "Got JSONException when attempting symbolicate stack trace: " + e.getMessage());
        }
    }

    public void openStackFrameCall(StackFrame stackFrame) {
        ((Call) Assertions.assertNotNull(this.mClient.newCall(new Request.Builder().url(createOpenStackFrameURL(this.mSettings.getPackagerConnectionSettings().getDebugServerHost())).post(RequestBody.create(MediaType.parse("application/json"), stackFrame.toJSON().toString())).build()))).enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                FLog.w(ReactConstants.TAG, "Got IOException when attempting to open stack frame: " + e.getMessage());
            }

            public void onResponse(Call call, Response response) throws IOException {
            }
        });
    }

    public String getWebsocketProxyURL() {
        return String.format(Locale.US, "ws://%s/debugger-proxy?role=client", new Object[]{this.mSettings.getPackagerConnectionSettings().getDebugServerHost()});
    }

    /* access modifiers changed from: private */
    public String getInspectorDeviceUrl() {
        return String.format(Locale.US, "http://%s/inspector/device?name=%s&app=%s", new Object[]{this.mSettings.getPackagerConnectionSettings().getInspectorServerHost(), AndroidInfoHelpers.getFriendlyDeviceName(), this.mPackageName});
    }

    /* access modifiers changed from: private */
    public String getInspectorAttachUrl(String title) {
        return String.format(Locale.US, "http://%s/nuclide/attach-debugger-nuclide?title=%s&app=%s&device=%s", new Object[]{AndroidInfoHelpers.getServerHost(), title, this.mPackageName, AndroidInfoHelpers.getFriendlyDeviceName()});
    }

    public void downloadBundleFromURL(DevBundleDownloadListener callback, File outputFile, String bundleURL, BundleInfo bundleInfo) {
        this.mBundleDownloader.downloadBundleFromURL(callback, outputFile, bundleURL, bundleInfo);
    }

    private String getHostForJSProxy() {
        String host = (String) Assertions.assertNotNull(this.mSettings.getPackagerConnectionSettings().getDebugServerHost());
        int portOffset = host.lastIndexOf(58);
        if (portOffset > -1) {
            return AndroidInfoHelpers.DEVICE_LOCALHOST + host.substring(portOffset);
        }
        return AndroidInfoHelpers.DEVICE_LOCALHOST;
    }

    private boolean getDevMode() {
        return this.mSettings.isJSDevModeEnabled();
    }

    private boolean getJSMinifyMode() {
        return this.mSettings.isJSMinifyEnabled();
    }

    private String createBundleURL(String mainModuleID, BundleType type, String host) {
        return String.format(Locale.US, "http://%s/%s.%s?platform=android&dev=%s&minify=%s", new Object[]{host, mainModuleID, type.typeID(), Boolean.valueOf(getDevMode()), Boolean.valueOf(getJSMinifyMode())});
    }

    private String createBundleURL(String mainModuleID, BundleType type) {
        return createBundleURL(mainModuleID, type, this.mSettings.getPackagerConnectionSettings().getDebugServerHost());
    }

    private static String createResourceURL(String host, String resourcePath) {
        return String.format(Locale.US, "http://%s/%s", new Object[]{host, resourcePath});
    }

    private static String createSymbolicateURL(String host) {
        return String.format(Locale.US, "http://%s/symbolicate", new Object[]{host});
    }

    private static String createOpenStackFrameURL(String host) {
        return String.format(Locale.US, "http://%s/open-stack-frame", new Object[]{host});
    }

    public String getDevServerBundleURL(String jsModulePath) {
        return createBundleURL(jsModulePath, this.mSettings.isBundleDeltasEnabled() ? BundleType.DELTA : BundleType.BUNDLE, this.mSettings.getPackagerConnectionSettings().getDebugServerHost());
    }

    public void isPackagerRunning(final PackagerStatusCallback callback) {
        this.mClient.newCall(new Request.Builder().url(createPackagerStatusURL(this.mSettings.getPackagerConnectionSettings().getDebugServerHost())).build()).enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                FLog.w(ReactConstants.TAG, "The packager does not seem to be running as we got an IOException requesting its status: " + e.getMessage());
                callback.onPackagerStatusFetched(false);
            }

            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    FLog.e(ReactConstants.TAG, "Got non-success http code from packager when requesting status: " + response.code());
                    callback.onPackagerStatusFetched(false);
                    return;
                }
                ResponseBody body = response.body();
                if (body == null) {
                    FLog.e(ReactConstants.TAG, "Got null body response from packager when requesting status");
                    callback.onPackagerStatusFetched(false);
                } else if (!DevServerHelper.PACKAGER_OK_STATUS.equals(body.string())) {
                    FLog.e(ReactConstants.TAG, "Got unexpected response from packager when requesting status: " + body.string());
                    callback.onPackagerStatusFetched(false);
                } else {
                    callback.onPackagerStatusFetched(true);
                }
            }
        });
    }

    private static String createPackagerStatusURL(String host) {
        return String.format(Locale.US, "http://%s/status", new Object[]{host});
    }

    public void stopPollingOnChangeEndpoint() {
        this.mOnChangePollingEnabled = false;
        this.mRestartOnChangePollingHandler.removeCallbacksAndMessages(null);
        if (this.mOnChangePollingClient != null) {
            OkHttpCallUtil.cancelTag(this.mOnChangePollingClient, this);
            this.mOnChangePollingClient = null;
        }
        this.mOnServerContentChangeListener = null;
    }

    public void startPollingOnChangeEndpoint(OnServerContentChangeListener onServerContentChangeListener) {
        if (!this.mOnChangePollingEnabled) {
            this.mOnChangePollingEnabled = true;
            this.mOnServerContentChangeListener = onServerContentChangeListener;
            this.mOnChangePollingClient = new Builder().connectionPool(new ConnectionPool(1, 120000, TimeUnit.MINUTES)).connectTimeout(5000, TimeUnit.MILLISECONDS).build();
            enqueueOnChangeEndpointLongPolling();
        }
    }

    /* access modifiers changed from: private */
    public void handleOnChangePollingResponse(boolean didServerContentChanged) {
        if (this.mOnChangePollingEnabled) {
            if (didServerContentChanged) {
                UiThreadUtil.runOnUiThread(new Runnable() {
                    public void run() {
                        if (DevServerHelper.this.mOnServerContentChangeListener != null) {
                            DevServerHelper.this.mOnServerContentChangeListener.onServerContentChanged();
                        }
                    }
                });
            }
            enqueueOnChangeEndpointLongPolling();
        }
    }

    private void enqueueOnChangeEndpointLongPolling() {
        ((OkHttpClient) Assertions.assertNotNull(this.mOnChangePollingClient)).newCall(new Request.Builder().url(createOnChangeEndpointUrl()).tag(this).build()).enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                if (DevServerHelper.this.mOnChangePollingEnabled) {
                    FLog.d(ReactConstants.TAG, "Error while requesting /onchange endpoint", (Throwable) e);
                    DevServerHelper.this.mRestartOnChangePollingHandler.postDelayed(new Runnable() {
                        public void run() {
                            DevServerHelper.this.handleOnChangePollingResponse(false);
                        }
                    }, 5000);
                }
            }

            public void onResponse(Call call, Response response) throws IOException {
                DevServerHelper.this.handleOnChangePollingResponse(response.code() == 205);
            }
        });
    }

    private String createOnChangeEndpointUrl() {
        return String.format(Locale.US, "http://%s/onchange", new Object[]{this.mSettings.getPackagerConnectionSettings().getDebugServerHost()});
    }

    private String createLaunchJSDevtoolsCommandUrl() {
        return String.format(Locale.US, "http://%s/launch-js-devtools", new Object[]{this.mSettings.getPackagerConnectionSettings().getDebugServerHost()});
    }

    public void launchJSDevtools() {
        this.mClient.newCall(new Request.Builder().url(createLaunchJSDevtoolsCommandUrl()).build()).enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
            }

            public void onResponse(Call call, Response response) throws IOException {
            }
        });
    }

    public String getSourceMapUrl(String mainModuleName) {
        return createBundleURL(mainModuleName, BundleType.MAP);
    }

    public String getSourceUrl(String mainModuleName) {
        return createBundleURL(mainModuleName, this.mSettings.isBundleDeltasEnabled() ? BundleType.DELTA : BundleType.BUNDLE);
    }

    public String getJSBundleURLForRemoteDebugging(String mainModuleName) {
        return createBundleURL(mainModuleName, BundleType.BUNDLE, getHostForJSProxy());
    }

    @Nullable
    public File downloadBundleResourceFromUrlSync(String resourcePath, File outputFile) {
        Sink output;
        try {
            Response response = this.mClient.newCall(new Request.Builder().url(createResourceURL(this.mSettings.getPackagerConnectionSettings().getDebugServerHost(), resourcePath)).build()).execute();
            if (!response.isSuccessful()) {
                return null;
            }
            output = null;
            output = Okio.sink(outputFile);
            Okio.buffer((Source) response.body().source()).readAll(output);
            if (output == null) {
                return outputFile;
            }
            output.close();
            return outputFile;
        } catch (Exception ex) {
            FLog.e(ReactConstants.TAG, "Failed to fetch resource synchronously - resourcePath: \"%s\", outputFile: \"%s\"", resourcePath, outputFile.getAbsolutePath(), ex);
            return null;
        } catch (Throwable th) {
            if (output != null) {
                output.close();
            }
            throw th;
        }
    }
}
