package com.facebook.react.devsupport;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.view.LayoutInflater;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.facebook.common.logging.FLog;
import com.facebook.react.R;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.common.ReactConstants;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import javax.annotation.Nullable;

public class DevLoadingViewController {
    private static final int COLOR_DARK_GREEN = Color.parseColor("#035900");
    private static boolean sEnabled = true;
    private final Context mContext;
    @Nullable
    private PopupWindow mDevLoadingPopup;
    /* access modifiers changed from: private */
    public final TextView mDevLoadingView = ((TextView) ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(R.layout.dev_loading_view, null));
    private final ReactInstanceManagerDevHelper mReactInstanceManagerHelper;

    public static void setDevLoadingEnabled(boolean enabled) {
        sEnabled = enabled;
    }

    public DevLoadingViewController(Context context, ReactInstanceManagerDevHelper reactInstanceManagerHelper) {
        this.mContext = context;
        this.mReactInstanceManagerHelper = reactInstanceManagerHelper;
    }

    public void showMessage(final String message, final int color, final int backgroundColor) {
        if (sEnabled) {
            UiThreadUtil.runOnUiThread(new Runnable() {
                public void run() {
                    DevLoadingViewController.this.mDevLoadingView.setBackgroundColor(backgroundColor);
                    DevLoadingViewController.this.mDevLoadingView.setText(message);
                    DevLoadingViewController.this.mDevLoadingView.setTextColor(color);
                    DevLoadingViewController.this.showInternal();
                }
            });
        }
    }

    public void showForUrl(String url) {
        try {
            URL parsedURL = new URL(url);
            showMessage(this.mContext.getString(R.string.catalyst_loading_from_url, new Object[]{parsedURL.getHost() + ":" + parsedURL.getPort()}), -1, COLOR_DARK_GREEN);
        } catch (MalformedURLException e) {
            FLog.e(ReactConstants.TAG, "Bundle url format is invalid. \n\n" + e.toString());
        }
    }

    public void showForRemoteJSEnabled() {
        showMessage(this.mContext.getString(R.string.catalyst_remotedbg_message), -1, COLOR_DARK_GREEN);
    }

    public void updateProgress(@Nullable final String status, @Nullable final Integer done, @Nullable final Integer total) {
        if (sEnabled) {
            UiThreadUtil.runOnUiThread(new Runnable() {
                public void run() {
                    StringBuilder message = new StringBuilder();
                    message.append(status != null ? status : "Loading");
                    if (!(done == null || total == null || total.intValue() <= 0)) {
                        message.append(String.format(Locale.getDefault(), " %.1f%% (%d/%d)", new Object[]{Float.valueOf((((float) done.intValue()) / ((float) total.intValue())) * 100.0f), done, total}));
                    }
                    message.append("…");
                    DevLoadingViewController.this.mDevLoadingView.setText(message);
                }
            });
        }
    }

    public void show() {
        if (sEnabled) {
            UiThreadUtil.runOnUiThread(new Runnable() {
                public void run() {
                    DevLoadingViewController.this.showInternal();
                }
            });
        }
    }

    public void hide() {
        if (sEnabled) {
            UiThreadUtil.runOnUiThread(new Runnable() {
                public void run() {
                    DevLoadingViewController.this.hideInternal();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void showInternal() {
        if (this.mDevLoadingPopup == null || !this.mDevLoadingPopup.isShowing()) {
            Activity currentActivity = this.mReactInstanceManagerHelper.getCurrentActivity();
            if (currentActivity == null) {
                FLog.e(ReactConstants.TAG, "Unable to display loading message because react activity isn't available");
                return;
            }
            int topOffset = 0;
            if (VERSION.SDK_INT <= 19) {
                Rect rectangle = new Rect();
                currentActivity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
                topOffset = rectangle.top;
            }
            this.mDevLoadingPopup = new PopupWindow(this.mDevLoadingView, -1, -2);
            this.mDevLoadingPopup.setTouchable(false);
            this.mDevLoadingPopup.showAtLocation(currentActivity.getWindow().getDecorView(), 0, 0, topOffset);
        }
    }

    /* access modifiers changed from: private */
    public void hideInternal() {
        if (this.mDevLoadingPopup != null && this.mDevLoadingPopup.isShowing()) {
            this.mDevLoadingPopup.dismiss();
            this.mDevLoadingPopup = null;
        }
    }
}
