package com.facebook.react.devsupport;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Build.VERSION;
import android.provider.Settings;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import com.facebook.common.logging.FLog;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.common.ReactConstants;
import javax.annotation.Nullable;

class DebugOverlayController {
    @Nullable
    private FrameLayout mFPSDebugViewContainer;
    private final ReactContext mReactContext;
    private final WindowManager mWindowManager;

    public static void requestPermission(Context context) {
        if (VERSION.SDK_INT >= 23 && !Settings.canDrawOverlays(context)) {
            Intent intent = new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", Uri.parse("package:" + context.getPackageName()));
            intent.setFlags(268435456);
            FLog.w(ReactConstants.TAG, "Overlay permissions needs to be granted in order for react native apps to run in dev mode");
            if (canHandleIntent(context, intent)) {
                context.startActivity(intent);
            }
        }
    }

    private static boolean permissionCheck(Context context) {
        if (VERSION.SDK_INT < 23) {
            return hasPermission(context, "android.permission.SYSTEM_ALERT_WINDOW");
        }
        if (!Settings.canDrawOverlays(context)) {
            return false;
        }
        return true;
    }

    private static boolean hasPermission(Context context, String permission) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);
            if (info.requestedPermissions == null) {
                return false;
            }
            for (String p : info.requestedPermissions) {
                if (p.equals(permission)) {
                    return true;
                }
            }
            return false;
        } catch (NameNotFoundException e) {
            FLog.e(ReactConstants.TAG, "Error while retrieving package info", (Throwable) e);
            return false;
        }
    }

    private static boolean canHandleIntent(Context context, Intent intent) {
        return intent.resolveActivity(context.getPackageManager()) != null;
    }

    public DebugOverlayController(ReactContext reactContext) {
        this.mReactContext = reactContext;
        this.mWindowManager = (WindowManager) reactContext.getSystemService("window");
    }

    public void setFpsDebugViewVisible(boolean fpsDebugViewVisible) {
        if (!fpsDebugViewVisible || this.mFPSDebugViewContainer != null) {
            if (!fpsDebugViewVisible && this.mFPSDebugViewContainer != null) {
                this.mFPSDebugViewContainer.removeAllViews();
                this.mWindowManager.removeView(this.mFPSDebugViewContainer);
                this.mFPSDebugViewContainer = null;
            }
        } else if (!permissionCheck(this.mReactContext)) {
            FLog.d(ReactConstants.TAG, "Wait for overlay permission to be set");
        } else {
            this.mFPSDebugViewContainer = new FpsView(this.mReactContext);
            this.mWindowManager.addView(this.mFPSDebugViewContainer, new LayoutParams(-1, -1, WindowOverlayCompat.TYPE_SYSTEM_OVERLAY, 24, -3));
        }
    }
}
