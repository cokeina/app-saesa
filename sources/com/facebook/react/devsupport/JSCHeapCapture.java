package com.facebook.react.devsupport;

import com.facebook.react.bridge.JavaScriptModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;
import java.io.File;
import javax.annotation.Nullable;

@ReactModule(name = "JSCHeapCapture", needsEagerInit = true)
public class JSCHeapCapture extends ReactContextBaseJavaModule {
    @Nullable
    private CaptureCallback mCaptureInProgress = null;

    public interface CaptureCallback {
        void onFailure(CaptureException captureException);

        void onSuccess(File file);
    }

    public static class CaptureException extends Exception {
        CaptureException(String message) {
            super(message);
        }

        CaptureException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public interface HeapCapture extends JavaScriptModule {
        void captureHeap(String str);
    }

    public JSCHeapCapture(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    public synchronized void captureHeap(String path, CaptureCallback callback) {
        if (this.mCaptureInProgress != null) {
            callback.onFailure(new CaptureException("Heap capture already in progress."));
        } else {
            File f = new File(path + "/capture.json");
            f.delete();
            HeapCapture heapCapture = (HeapCapture) getReactApplicationContext().getJSModule(HeapCapture.class);
            if (heapCapture == null) {
                callback.onFailure(new CaptureException("Heap capture js module not registered."));
            } else {
                this.mCaptureInProgress = callback;
                heapCapture.captureHeap(f.getPath());
            }
        }
    }

    @ReactMethod
    public synchronized void captureComplete(String path, String error) {
        if (this.mCaptureInProgress != null) {
            if (error == null) {
                this.mCaptureInProgress.onSuccess(new File(path));
            } else {
                this.mCaptureInProgress.onFailure(new CaptureException(error));
            }
            this.mCaptureInProgress = null;
        }
    }

    public String getName() {
        return "JSCHeapCapture";
    }
}
