package com.facebook.react.devsupport;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager.NameNotFoundException;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.facebook.common.logging.FLog;
import com.facebook.debug.holder.PrinterHolder;
import com.facebook.debug.tags.ReactDebugOverlayTags;
import com.facebook.infer.annotation.Assertions;
import com.facebook.react.R;
import com.facebook.react.bridge.DefaultNativeModuleCallExceptionHandler;
import com.facebook.react.bridge.JavaJSExecutor;
import com.facebook.react.bridge.JavaJSExecutor.Factory;
import com.facebook.react.bridge.JavaScriptContextHolder;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactMarker;
import com.facebook.react.bridge.ReactMarkerConstants;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.common.DebugServerException;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.common.ShakeDetector;
import com.facebook.react.common.ShakeDetector.ShakeListener;
import com.facebook.react.common.futures.SimpleSettableFuture;
import com.facebook.react.devsupport.BundleDownloader.BundleInfo;
import com.facebook.react.devsupport.DevInternalSettings.Listener;
import com.facebook.react.devsupport.DevServerHelper.OnServerContentChangeListener;
import com.facebook.react.devsupport.DevServerHelper.PackagerCommandListener;
import com.facebook.react.devsupport.InspectorPackagerConnection.BundleStatus;
import com.facebook.react.devsupport.InspectorPackagerConnection.BundleStatusProvider;
import com.facebook.react.devsupport.JSCHeapCapture.CaptureCallback;
import com.facebook.react.devsupport.JSCHeapCapture.CaptureException;
import com.facebook.react.devsupport.JSCSamplingProfiler.ProfilerException;
import com.facebook.react.devsupport.JSDevSupport.DevSupportCallback;
import com.facebook.react.devsupport.WebsocketJavaScriptExecutor.JSExecutorConnectCallback;
import com.facebook.react.devsupport.interfaces.DevBundleDownloadListener;
import com.facebook.react.devsupport.interfaces.DevOptionHandler;
import com.facebook.react.devsupport.interfaces.DevSupportManager;
import com.facebook.react.devsupport.interfaces.ErrorCustomizer;
import com.facebook.react.devsupport.interfaces.PackagerStatusCallback;
import com.facebook.react.devsupport.interfaces.StackFrame;
import com.facebook.react.modules.debug.interfaces.DeveloperSettings;
import com.facebook.react.packagerconnection.RequestHandler;
import com.facebook.react.packagerconnection.Responder;
import com.facebook.react.uimanager.IllegalViewOperationException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.annotation.Nullable;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;

@TargetApi(11)
public class DevSupportManagerImpl implements DevSupportManager, PackagerCommandListener, Listener {
    public static final String EMOJI_FACE_WITH_NO_GOOD_GESTURE = " 🙅";
    public static final String EMOJI_HUNDRED_POINTS_SYMBOL = " 💯";
    private static final String EXOPACKAGE_LOCATION_FORMAT = "/data/local/tmp/exopackage/%s//secondary-dex";
    private static final int JAVA_ERROR_COOKIE = -1;
    private static final int JSEXCEPTION_ERROR_COOKIE = -1;
    private static final String JS_BUNDLE_FILE_NAME = "ReactNativeDevBundle.js";
    private static final String RELOAD_APP_ACTION_SUFFIX = ".RELOAD_APP_ACTION";
    /* access modifiers changed from: private */
    public final Context mApplicationContext;
    /* access modifiers changed from: private */
    @Nullable
    public DevBundleDownloadListener mBundleDownloadListener;
    /* access modifiers changed from: private */
    public BundleStatus mBundleStatus;
    /* access modifiers changed from: private */
    @Nullable
    public ReactContext mCurrentContext;
    private final LinkedHashMap<String, DevOptionHandler> mCustomDevOptions;
    @Nullable
    private DebugOverlayController mDebugOverlayController;
    private final DefaultNativeModuleCallExceptionHandler mDefaultNativeModuleCallExceptionHandler;
    /* access modifiers changed from: private */
    public final DevLoadingViewController mDevLoadingViewController;
    /* access modifiers changed from: private */
    public boolean mDevLoadingViewVisible;
    /* access modifiers changed from: private */
    @Nullable
    public AlertDialog mDevOptionsDialog;
    /* access modifiers changed from: private */
    public final DevServerHelper mDevServerHelper;
    /* access modifiers changed from: private */
    public DevInternalSettings mDevSettings;
    @Nullable
    private List<ErrorCustomizer> mErrorCustomizers;
    private final List<ExceptionLogger> mExceptionLoggers;
    private boolean mIsDevSupportEnabled;
    private boolean mIsReceiverRegistered;
    private boolean mIsShakeDetectorStarted;
    @Nullable
    private final String mJSAppBundleName;
    private final File mJSBundleTempFile;
    /* access modifiers changed from: private */
    public int mLastErrorCookie;
    @Nullable
    private StackFrame[] mLastErrorStack;
    @Nullable
    private String mLastErrorTitle;
    @Nullable
    private ErrorType mLastErrorType;
    /* access modifiers changed from: private */
    public final ReactInstanceManagerDevHelper mReactInstanceManagerHelper;
    /* access modifiers changed from: private */
    @Nullable
    public RedBoxDialog mRedBoxDialog;
    /* access modifiers changed from: private */
    @Nullable
    public RedBoxHandler mRedBoxHandler;
    private final BroadcastReceiver mReloadAppBroadcastReceiver;
    private final ShakeDetector mShakeDetector;

    private enum ErrorType {
        JS,
        NATIVE
    }

    private interface ExceptionLogger {
        void log(Exception exc);
    }

    private class JSExceptionLogger implements ExceptionLogger {
        private JSExceptionLogger() {
        }

        public void log(Exception e) {
            StringBuilder message = new StringBuilder(e.getMessage());
            for (Throwable cause = e.getCause(); cause != null; cause = cause.getCause()) {
                message.append("\n\n").append(cause.getMessage());
            }
            if (e instanceof JSException) {
                FLog.e(ReactConstants.TAG, "Exception in native call from JS", (Throwable) e);
                message.append("\n\n").append(((JSException) e).getStack());
                DevSupportManagerImpl.this.showNewError(message.toString(), new StackFrame[0], -1, ErrorType.JS);
                return;
            }
            DevSupportManagerImpl.this.showNewJavaError(message.toString(), e);
        }
    }

    private static class JscProfileTask extends AsyncTask<String, Void, Void> {
        private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        private final String mSourceUrl;

        private JscProfileTask(String sourceUrl) {
            this.mSourceUrl = sourceUrl;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... jsonData) {
            try {
                String jscProfileUrl = Uri.parse(this.mSourceUrl).buildUpon().path("/jsc-profile").query(null).build().toString();
                OkHttpClient client = new OkHttpClient();
                for (String json : jsonData) {
                    client.newCall(new Builder().url(jscProfileUrl).post(RequestBody.create(JSON, json)).build()).execute();
                }
            } catch (IOException e) {
                FLog.e(ReactConstants.TAG, "Failed not talk to server", (Throwable) e);
            }
            return null;
        }
    }

    private class StackOverflowExceptionLogger implements ExceptionLogger {
        private StackOverflowExceptionLogger() {
        }

        public void log(Exception e) {
            if ((e instanceof IllegalViewOperationException) && (e.getCause() instanceof StackOverflowError)) {
                View view = ((IllegalViewOperationException) e).getView();
                if (view != null) {
                    logDeepestJSHierarchy(view);
                }
            }
        }

        private void logDeepestJSHierarchy(View view) {
            if (DevSupportManagerImpl.this.mCurrentContext != null && view != null) {
                Pair<View, Integer> deepestPairView = getDeepestNativeView(view);
                Integer tagId = Integer.valueOf(((View) deepestPairView.first).getId());
                final int depth = ((Integer) deepestPairView.second).intValue();
                ((JSDevSupport) DevSupportManagerImpl.this.mCurrentContext.getNativeModule(JSDevSupport.class)).getJSHierarchy(tagId.toString(), new DevSupportCallback() {
                    public void onSuccess(String hierarchy) {
                        FLog.e(ReactConstants.TAG, "StackOverflowError when rendering JS Hierarchy (depth of native hierarchy = " + depth + "): \n" + hierarchy);
                    }

                    public void onFailure(Exception ex) {
                        FLog.e(ReactConstants.TAG, (Throwable) ex, "Error retrieving JS Hierarchy (depth of native hierarchy = " + depth + ").", new Object[0]);
                    }
                });
            }
        }

        private Pair<View, Integer> getDeepestNativeView(View root) {
            Queue<Pair<View, Integer>> queue = new LinkedList<>();
            Pair<View, Integer> maxPair = new Pair<>(root, Integer.valueOf(1));
            queue.add(maxPair);
            while (!queue.isEmpty()) {
                Pair<View, Integer> current = (Pair) queue.poll();
                if (((Integer) current.second).intValue() > ((Integer) maxPair.second).intValue()) {
                    maxPair = current;
                }
                if (current.first instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) current.first;
                    Integer depth = Integer.valueOf(((Integer) current.second).intValue() + 1);
                    for (int i = 0; i < viewGroup.getChildCount(); i++) {
                        queue.add(new Pair(viewGroup.getChildAt(i), depth));
                    }
                }
            }
            return maxPair;
        }
    }

    public DevSupportManagerImpl(Context applicationContext, ReactInstanceManagerDevHelper reactInstanceManagerHelper, @Nullable String packagerPathForJSBundleName, boolean enableOnCreate, int minNumShakes) {
        this(applicationContext, reactInstanceManagerHelper, packagerPathForJSBundleName, enableOnCreate, null, null, minNumShakes);
    }

    public DevSupportManagerImpl(Context applicationContext, ReactInstanceManagerDevHelper reactInstanceManagerHelper, @Nullable String packagerPathForJSBundleName, boolean enableOnCreate, @Nullable RedBoxHandler redBoxHandler, @Nullable DevBundleDownloadListener devBundleDownloadListener, int minNumShakes) {
        this.mExceptionLoggers = new ArrayList();
        this.mCustomDevOptions = new LinkedHashMap<>();
        this.mDevLoadingViewVisible = false;
        this.mIsReceiverRegistered = false;
        this.mIsShakeDetectorStarted = false;
        this.mIsDevSupportEnabled = false;
        this.mLastErrorCookie = 0;
        this.mReactInstanceManagerHelper = reactInstanceManagerHelper;
        this.mApplicationContext = applicationContext;
        this.mJSAppBundleName = packagerPathForJSBundleName;
        this.mDevSettings = new DevInternalSettings(applicationContext, this);
        this.mBundleStatus = new BundleStatus();
        this.mDevServerHelper = new DevServerHelper(this.mDevSettings, this.mApplicationContext.getPackageName(), new BundleStatusProvider() {
            public BundleStatus getBundleStatus() {
                return DevSupportManagerImpl.this.mBundleStatus;
            }
        });
        this.mBundleDownloadListener = devBundleDownloadListener;
        this.mShakeDetector = new ShakeDetector(new ShakeListener() {
            public void onShake() {
                DevSupportManagerImpl.this.showDevOptionsDialog();
            }
        }, minNumShakes);
        this.mReloadAppBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (DevSupportManagerImpl.getReloadAppAction(context).equals(intent.getAction())) {
                    if (intent.getBooleanExtra(DevServerHelper.RELOAD_APP_EXTRA_JS_PROXY, false)) {
                        DevSupportManagerImpl.this.mDevSettings.setRemoteJSDebugEnabled(true);
                        DevSupportManagerImpl.this.mDevServerHelper.launchJSDevtools();
                    } else {
                        DevSupportManagerImpl.this.mDevSettings.setRemoteJSDebugEnabled(false);
                    }
                    DevSupportManagerImpl.this.handleReloadJS();
                }
            }
        };
        this.mJSBundleTempFile = new File(applicationContext.getFilesDir(), JS_BUNDLE_FILE_NAME);
        this.mDefaultNativeModuleCallExceptionHandler = new DefaultNativeModuleCallExceptionHandler();
        setDevSupportEnabled(enableOnCreate);
        this.mRedBoxHandler = redBoxHandler;
        this.mDevLoadingViewController = new DevLoadingViewController(applicationContext, reactInstanceManagerHelper);
        this.mExceptionLoggers.add(new JSExceptionLogger());
        this.mExceptionLoggers.add(new StackOverflowExceptionLogger());
    }

    public void handleException(Exception e) {
        if (this.mIsDevSupportEnabled) {
            for (ExceptionLogger logger : this.mExceptionLoggers) {
                logger.log(e);
            }
            return;
        }
        this.mDefaultNativeModuleCallExceptionHandler.handleException(e);
    }

    public void showNewJavaError(String message, Throwable e) {
        FLog.e(ReactConstants.TAG, "Exception in native call", e);
        showNewError(message, StackTraceHelper.convertJavaStackTrace(e), -1, ErrorType.NATIVE);
    }

    public void addCustomDevOption(String optionName, DevOptionHandler optionHandler) {
        this.mCustomDevOptions.put(optionName, optionHandler);
    }

    public void showNewJSError(String message, ReadableArray details, int errorCookie) {
        showNewError(message, StackTraceHelper.convertJsStackTrace(details), errorCookie, ErrorType.JS);
    }

    public void registerErrorCustomizer(ErrorCustomizer errorCustomizer) {
        if (this.mErrorCustomizers == null) {
            this.mErrorCustomizers = new ArrayList();
        }
        this.mErrorCustomizers.add(errorCustomizer);
    }

    /* access modifiers changed from: private */
    public Pair<String, StackFrame[]> processErrorCustomizers(Pair<String, StackFrame[]> errorInfo) {
        if (this.mErrorCustomizers == null) {
            return errorInfo;
        }
        for (ErrorCustomizer errorCustomizer : this.mErrorCustomizers) {
            Pair<String, StackFrame[]> result = errorCustomizer.customizeErrorInfo(errorInfo);
            if (result != null) {
                errorInfo = result;
            }
        }
        return errorInfo;
    }

    public void updateJSError(final String message, final ReadableArray details, final int errorCookie) {
        UiThreadUtil.runOnUiThread(new Runnable() {
            public void run() {
                if (DevSupportManagerImpl.this.mRedBoxDialog != null && DevSupportManagerImpl.this.mRedBoxDialog.isShowing() && errorCookie == DevSupportManagerImpl.this.mLastErrorCookie) {
                    StackFrame[] stack = StackTraceHelper.convertJsStackTrace(details);
                    Pair<String, StackFrame[]> errorInfo = DevSupportManagerImpl.this.processErrorCustomizers(Pair.create(message, stack));
                    DevSupportManagerImpl.this.mRedBoxDialog.setExceptionDetails((String) errorInfo.first, (StackFrame[]) errorInfo.second);
                    DevSupportManagerImpl.this.updateLastErrorInfo(message, stack, errorCookie, ErrorType.JS);
                    if (DevSupportManagerImpl.this.mRedBoxHandler != null) {
                        DevSupportManagerImpl.this.mRedBoxHandler.handleRedbox(message, stack, com.facebook.react.devsupport.RedBoxHandler.ErrorType.JS);
                        DevSupportManagerImpl.this.mRedBoxDialog.resetReporting();
                    }
                    DevSupportManagerImpl.this.mRedBoxDialog.show();
                }
            }
        });
    }

    public void hideRedboxDialog() {
        if (this.mRedBoxDialog != null) {
            this.mRedBoxDialog.dismiss();
            this.mRedBoxDialog = null;
        }
    }

    private void hideDevOptionsDialog() {
        if (this.mDevOptionsDialog != null) {
            this.mDevOptionsDialog.dismiss();
            this.mDevOptionsDialog = null;
        }
    }

    /* access modifiers changed from: private */
    public void showNewError(String message, StackFrame[] stack, int errorCookie, ErrorType errorType) {
        final String str = message;
        final StackFrame[] stackFrameArr = stack;
        final int i = errorCookie;
        final ErrorType errorType2 = errorType;
        UiThreadUtil.runOnUiThread(new Runnable() {
            public void run() {
                if (DevSupportManagerImpl.this.mRedBoxDialog == null) {
                    Activity context = DevSupportManagerImpl.this.mReactInstanceManagerHelper.getCurrentActivity();
                    if (context == null || context.isFinishing()) {
                        FLog.e(ReactConstants.TAG, "Unable to launch redbox because react activity is not available, here is the error that redbox would've displayed: " + str);
                        return;
                    }
                    DevSupportManagerImpl.this.mRedBoxDialog = new RedBoxDialog(context, DevSupportManagerImpl.this, DevSupportManagerImpl.this.mRedBoxHandler);
                }
                if (!DevSupportManagerImpl.this.mRedBoxDialog.isShowing()) {
                    Pair<String, StackFrame[]> errorInfo = DevSupportManagerImpl.this.processErrorCustomizers(Pair.create(str, stackFrameArr));
                    DevSupportManagerImpl.this.mRedBoxDialog.setExceptionDetails((String) errorInfo.first, (StackFrame[]) errorInfo.second);
                    DevSupportManagerImpl.this.updateLastErrorInfo(str, stackFrameArr, i, errorType2);
                    if (DevSupportManagerImpl.this.mRedBoxHandler != null && errorType2 == ErrorType.NATIVE) {
                        DevSupportManagerImpl.this.mRedBoxHandler.handleRedbox(str, stackFrameArr, com.facebook.react.devsupport.RedBoxHandler.ErrorType.NATIVE);
                    }
                    DevSupportManagerImpl.this.mRedBoxDialog.resetReporting();
                    DevSupportManagerImpl.this.mRedBoxDialog.show();
                }
            }
        });
    }

    public void showDevOptionsDialog() {
        String remoteJsDebugMenuItemTitle;
        String string;
        String string2;
        String string3;
        if (this.mDevOptionsDialog == null && this.mIsDevSupportEnabled && !ActivityManager.isUserAMonkey()) {
            LinkedHashMap<String, DevOptionHandler> options = new LinkedHashMap<>();
            options.put(this.mApplicationContext.getString(R.string.catalyst_reloadjs), new DevOptionHandler() {
                public void onOptionSelected() {
                    DevSupportManagerImpl.this.handleReloadJS();
                }
            });
            if (this.mDevSettings.isNuclideJSDebugEnabled()) {
                options.put(this.mApplicationContext.getString(R.string.catalyst_debugjs_nuclide) + EMOJI_HUNDRED_POINTS_SYMBOL, new DevOptionHandler() {
                    public void onOptionSelected() {
                        DevSupportManagerImpl.this.mDevServerHelper.attachDebugger(DevSupportManagerImpl.this.mApplicationContext, ReactConstants.TAG);
                    }
                });
            }
            if (this.mDevSettings.isRemoteJSDebugEnabled()) {
                remoteJsDebugMenuItemTitle = this.mApplicationContext.getString(R.string.catalyst_debugjs_off);
            } else {
                remoteJsDebugMenuItemTitle = this.mApplicationContext.getString(R.string.catalyst_debugjs);
            }
            if (this.mDevSettings.isNuclideJSDebugEnabled()) {
                remoteJsDebugMenuItemTitle = remoteJsDebugMenuItemTitle + EMOJI_FACE_WITH_NO_GOOD_GESTURE;
            }
            options.put(remoteJsDebugMenuItemTitle, new DevOptionHandler() {
                public void onOptionSelected() {
                    DevSupportManagerImpl.this.mDevSettings.setRemoteJSDebugEnabled(!DevSupportManagerImpl.this.mDevSettings.isRemoteJSDebugEnabled());
                    DevSupportManagerImpl.this.handleReloadJS();
                }
            });
            if (this.mDevSettings.isReloadOnJSChangeEnabled()) {
                string = this.mApplicationContext.getString(R.string.catalyst_live_reload_off);
            } else {
                string = this.mApplicationContext.getString(R.string.catalyst_live_reload);
            }
            options.put(string, new DevOptionHandler() {
                public void onOptionSelected() {
                    DevSupportManagerImpl.this.mDevSettings.setReloadOnJSChangeEnabled(!DevSupportManagerImpl.this.mDevSettings.isReloadOnJSChangeEnabled());
                }
            });
            if (this.mDevSettings.isHotModuleReplacementEnabled()) {
                string2 = this.mApplicationContext.getString(R.string.catalyst_hot_module_replacement_off);
            } else {
                string2 = this.mApplicationContext.getString(R.string.catalyst_hot_module_replacement);
            }
            options.put(string2, new DevOptionHandler() {
                public void onOptionSelected() {
                    DevSupportManagerImpl.this.mDevSettings.setHotModuleReplacementEnabled(!DevSupportManagerImpl.this.mDevSettings.isHotModuleReplacementEnabled());
                    DevSupportManagerImpl.this.handleReloadJS();
                }
            });
            options.put(this.mApplicationContext.getString(R.string.catalyst_element_inspector), new DevOptionHandler() {
                public void onOptionSelected() {
                    DevSupportManagerImpl.this.mDevSettings.setElementInspectorEnabled(!DevSupportManagerImpl.this.mDevSettings.isElementInspectorEnabled());
                    DevSupportManagerImpl.this.mReactInstanceManagerHelper.toggleElementInspector();
                }
            });
            if (this.mDevSettings.isFpsDebugEnabled()) {
                string3 = this.mApplicationContext.getString(R.string.catalyst_perf_monitor_off);
            } else {
                string3 = this.mApplicationContext.getString(R.string.catalyst_perf_monitor);
            }
            options.put(string3, new DevOptionHandler() {
                public void onOptionSelected() {
                    if (!DevSupportManagerImpl.this.mDevSettings.isFpsDebugEnabled()) {
                        Context context = DevSupportManagerImpl.this.mReactInstanceManagerHelper.getCurrentActivity();
                        if (context == null) {
                            FLog.e(ReactConstants.TAG, "Unable to get reference to react activity");
                        } else {
                            DebugOverlayController.requestPermission(context);
                        }
                    }
                    DevSupportManagerImpl.this.mDevSettings.setFpsDebugEnabled(!DevSupportManagerImpl.this.mDevSettings.isFpsDebugEnabled());
                }
            });
            options.put(this.mApplicationContext.getString(R.string.catalyst_poke_sampling_profiler), new DevOptionHandler() {
                public void onOptionSelected() {
                    DevSupportManagerImpl.this.handlePokeSamplingProfiler();
                }
            });
            options.put(this.mApplicationContext.getString(R.string.catalyst_settings), new DevOptionHandler() {
                public void onOptionSelected() {
                    Intent intent = new Intent(DevSupportManagerImpl.this.mApplicationContext, DevSettingsActivity.class);
                    intent.setFlags(268435456);
                    DevSupportManagerImpl.this.mApplicationContext.startActivity(intent);
                }
            });
            if (this.mCustomDevOptions.size() > 0) {
                options.putAll(this.mCustomDevOptions);
            }
            final DevOptionHandler[] optionHandlers = (DevOptionHandler[]) options.values().toArray(new DevOptionHandler[0]);
            Activity context = this.mReactInstanceManagerHelper.getCurrentActivity();
            if (context == null || context.isFinishing()) {
                FLog.e(ReactConstants.TAG, "Unable to launch dev options menu because react activity isn't available");
                return;
            }
            this.mDevOptionsDialog = new AlertDialog.Builder(context).setItems((CharSequence[]) options.keySet().toArray(new String[0]), new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    optionHandlers[which].onOptionSelected();
                    DevSupportManagerImpl.this.mDevOptionsDialog = null;
                }
            }).setOnCancelListener(new OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    DevSupportManagerImpl.this.mDevOptionsDialog = null;
                }
            }).create();
            this.mDevOptionsDialog.show();
        }
    }

    public void setDevSupportEnabled(boolean isDevSupportEnabled) {
        this.mIsDevSupportEnabled = isDevSupportEnabled;
        reloadSettings();
    }

    public boolean getDevSupportEnabled() {
        return this.mIsDevSupportEnabled;
    }

    public DeveloperSettings getDevSettings() {
        return this.mDevSettings;
    }

    public void onNewReactContextCreated(ReactContext reactContext) {
        resetCurrentContext(reactContext);
    }

    public void onReactInstanceDestroyed(ReactContext reactContext) {
        if (reactContext == this.mCurrentContext) {
            resetCurrentContext(null);
        }
    }

    public String getSourceMapUrl() {
        if (this.mJSAppBundleName == null) {
            return "";
        }
        return this.mDevServerHelper.getSourceMapUrl((String) Assertions.assertNotNull(this.mJSAppBundleName));
    }

    public String getSourceUrl() {
        if (this.mJSAppBundleName == null) {
            return "";
        }
        return this.mDevServerHelper.getSourceUrl((String) Assertions.assertNotNull(this.mJSAppBundleName));
    }

    public String getJSBundleURLForRemoteDebugging() {
        return this.mDevServerHelper.getJSBundleURLForRemoteDebugging((String) Assertions.assertNotNull(this.mJSAppBundleName));
    }

    public String getDownloadedJSBundleFile() {
        return this.mJSBundleTempFile.getAbsolutePath();
    }

    public boolean hasUpToDateJSBundleInCache() {
        if (this.mIsDevSupportEnabled && this.mJSBundleTempFile.exists()) {
            try {
                String packageName = this.mApplicationContext.getPackageName();
                if (this.mJSBundleTempFile.lastModified() > this.mApplicationContext.getPackageManager().getPackageInfo(packageName, 0).lastUpdateTime) {
                    File exopackageDir = new File(String.format(Locale.US, EXOPACKAGE_LOCATION_FORMAT, new Object[]{packageName}));
                    if (!exopackageDir.exists() || this.mJSBundleTempFile.lastModified() > exopackageDir.lastModified()) {
                        return true;
                    }
                    return false;
                }
            } catch (NameNotFoundException e) {
                FLog.e(ReactConstants.TAG, "DevSupport is unable to get current app info");
            }
        }
        return false;
    }

    public boolean hasBundleInAssets(String bundleAssetName) {
        try {
            String[] assets = this.mApplicationContext.getAssets().list("");
            for (String equals : assets) {
                if (equals.equals(bundleAssetName)) {
                    return true;
                }
            }
        } catch (IOException e) {
            FLog.e(ReactConstants.TAG, "Error while loading assets list");
        }
        return false;
    }

    private void resetCurrentContext(@Nullable ReactContext reactContext) {
        if (this.mCurrentContext != reactContext) {
            this.mCurrentContext = reactContext;
            if (this.mDebugOverlayController != null) {
                this.mDebugOverlayController.setFpsDebugViewVisible(false);
            }
            if (reactContext != null) {
                this.mDebugOverlayController = new DebugOverlayController(reactContext);
            }
            if (this.mDevSettings.isHotModuleReplacementEnabled() && this.mCurrentContext != null) {
                try {
                    URL sourceUrl = new URL(getSourceUrl());
                    ((HMRClient) this.mCurrentContext.getJSModule(HMRClient.class)).enable("android", sourceUrl.getPath().substring(1), sourceUrl.getHost(), sourceUrl.getPort());
                } catch (MalformedURLException e) {
                    showNewJavaError(e.getMessage(), e);
                }
            }
            reloadSettings();
        }
    }

    public void reloadSettings() {
        if (UiThreadUtil.isOnUiThread()) {
            reload();
        } else {
            UiThreadUtil.runOnUiThread(new Runnable() {
                public void run() {
                    DevSupportManagerImpl.this.reload();
                }
            });
        }
    }

    public void onInternalSettingsChanged() {
        reloadSettings();
    }

    public void handleReloadJS() {
        UiThreadUtil.assertOnUiThread();
        ReactMarker.logMarker(ReactMarkerConstants.RELOAD);
        hideRedboxDialog();
        if (this.mDevSettings.isRemoteJSDebugEnabled()) {
            PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.RN_CORE, "RNCore: load from Proxy");
            this.mDevLoadingViewController.showForRemoteJSEnabled();
            this.mDevLoadingViewVisible = true;
            reloadJSInProxyMode();
            return;
        }
        PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.RN_CORE, "RNCore: load from Server");
        reloadJSFromServer(this.mDevServerHelper.getDevServerBundleURL((String) Assertions.assertNotNull(this.mJSAppBundleName)));
    }

    public void isPackagerRunning(PackagerStatusCallback callback) {
        this.mDevServerHelper.isPackagerRunning(callback);
    }

    @Nullable
    public File downloadBundleResourceFromUrlSync(String resourceURL, File outputFile) {
        return this.mDevServerHelper.downloadBundleResourceFromUrlSync(resourceURL, outputFile);
    }

    @Nullable
    public String getLastErrorTitle() {
        return this.mLastErrorTitle;
    }

    @Nullable
    public StackFrame[] getLastErrorStack() {
        return this.mLastErrorStack;
    }

    public void onPackagerConnected() {
    }

    public void onPackagerDisconnected() {
    }

    public void onPackagerReloadCommand() {
        this.mDevServerHelper.disableDebugger();
        UiThreadUtil.runOnUiThread(new Runnable() {
            public void run() {
                DevSupportManagerImpl.this.handleReloadJS();
            }
        });
    }

    public void onPackagerDevMenuCommand() {
        UiThreadUtil.runOnUiThread(new Runnable() {
            public void run() {
                DevSupportManagerImpl.this.showDevOptionsDialog();
            }
        });
    }

    public void onCaptureHeapCommand(final Responder responder) {
        UiThreadUtil.runOnUiThread(new Runnable() {
            public void run() {
                DevSupportManagerImpl.this.handleCaptureHeap(responder);
            }
        });
    }

    public void onPokeSamplingProfilerCommand(final Responder responder) {
        UiThreadUtil.runOnUiThread(new Runnable() {
            public void run() {
                if (DevSupportManagerImpl.this.mCurrentContext == null) {
                    responder.error("JSCContext is missing, unable to profile");
                    return;
                }
                try {
                    JavaScriptContextHolder jsContext = DevSupportManagerImpl.this.mCurrentContext.getJavaScriptContextHolder();
                    synchronized (jsContext) {
                        ((RequestHandler) Class.forName("com.facebook.react.packagerconnection.SamplingProfilerPackagerMethod").getConstructor(new Class[]{Long.TYPE}).newInstance(new Object[]{Long.valueOf(jsContext.get())})).onRequest(null, responder);
                    }
                } catch (Exception e) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void handleCaptureHeap(final Responder responder) {
        if (this.mCurrentContext != null) {
            ((JSCHeapCapture) this.mCurrentContext.getNativeModule(JSCHeapCapture.class)).captureHeap(this.mApplicationContext.getCacheDir().getPath(), new CaptureCallback() {
                public void onSuccess(File capture) {
                    responder.respond(capture.toString());
                }

                public void onFailure(CaptureException error) {
                    responder.error(error.toString());
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void handlePokeSamplingProfiler() {
        try {
            for (String result : JSCSamplingProfiler.poke(60000)) {
                Toast.makeText(this.mCurrentContext, result == null ? "Started JSC Sampling Profiler" : "Stopped JSC Sampling Profiler", 1).show();
                new JscProfileTask(getSourceUrl()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{result});
            }
        } catch (ProfilerException e) {
            showNewJavaError(e.getMessage(), e);
        }
    }

    /* access modifiers changed from: private */
    public void updateLastErrorInfo(String message, StackFrame[] stack, int errorCookie, ErrorType errorType) {
        this.mLastErrorTitle = message;
        this.mLastErrorStack = stack;
        this.mLastErrorCookie = errorCookie;
        this.mLastErrorType = errorType;
    }

    private void reloadJSInProxyMode() {
        this.mDevServerHelper.launchJSDevtools();
        this.mReactInstanceManagerHelper.onReloadWithJSDebugger(new Factory() {
            public JavaJSExecutor create() throws Exception {
                WebsocketJavaScriptExecutor executor = new WebsocketJavaScriptExecutor();
                SimpleSettableFuture<Boolean> future = new SimpleSettableFuture<>();
                executor.connect(DevSupportManagerImpl.this.mDevServerHelper.getWebsocketProxyURL(), DevSupportManagerImpl.this.getExecutorConnectCallback(future));
                try {
                    future.get(90, TimeUnit.SECONDS);
                    return executor;
                } catch (ExecutionException e) {
                    throw ((Exception) e.getCause());
                } catch (InterruptedException | TimeoutException e2) {
                    throw new RuntimeException(e2);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public JSExecutorConnectCallback getExecutorConnectCallback(final SimpleSettableFuture<Boolean> future) {
        return new JSExecutorConnectCallback() {
            public void onSuccess() {
                future.set(Boolean.valueOf(true));
                DevSupportManagerImpl.this.mDevLoadingViewController.hide();
                DevSupportManagerImpl.this.mDevLoadingViewVisible = false;
            }

            public void onFailure(Throwable cause) {
                DevSupportManagerImpl.this.mDevLoadingViewController.hide();
                DevSupportManagerImpl.this.mDevLoadingViewVisible = false;
                FLog.e(ReactConstants.TAG, "Unable to connect to remote debugger", cause);
                future.setException(new IOException(DevSupportManagerImpl.this.mApplicationContext.getString(R.string.catalyst_remotedbg_error), cause));
            }
        };
    }

    public void reloadJSFromServer(String bundleURL) {
        ReactMarker.logMarker(ReactMarkerConstants.DOWNLOAD_START);
        this.mDevLoadingViewController.showForUrl(bundleURL);
        this.mDevLoadingViewVisible = true;
        final BundleInfo bundleInfo = new BundleInfo();
        this.mDevServerHelper.downloadBundleFromURL(new DevBundleDownloadListener() {
            public void onSuccess() {
                DevSupportManagerImpl.this.mDevLoadingViewController.hide();
                DevSupportManagerImpl.this.mDevLoadingViewVisible = false;
                synchronized (DevSupportManagerImpl.this) {
                    DevSupportManagerImpl.this.mBundleStatus.isLastDownloadSucess = Boolean.valueOf(true);
                    DevSupportManagerImpl.this.mBundleStatus.updateTimestamp = System.currentTimeMillis();
                }
                if (DevSupportManagerImpl.this.mBundleDownloadListener != null) {
                    DevSupportManagerImpl.this.mBundleDownloadListener.onSuccess();
                }
                UiThreadUtil.runOnUiThread(new Runnable() {
                    public void run() {
                        ReactMarker.logMarker(ReactMarkerConstants.DOWNLOAD_END, bundleInfo.toJSONString());
                        DevSupportManagerImpl.this.mReactInstanceManagerHelper.onJSBundleLoadedFromServer();
                    }
                });
            }

            public void onProgress(@Nullable String status, @Nullable Integer done, @Nullable Integer total) {
                DevSupportManagerImpl.this.mDevLoadingViewController.updateProgress(status, done, total);
                if (DevSupportManagerImpl.this.mBundleDownloadListener != null) {
                    DevSupportManagerImpl.this.mBundleDownloadListener.onProgress(status, done, total);
                }
            }

            public void onFailure(final Exception cause) {
                DevSupportManagerImpl.this.mDevLoadingViewController.hide();
                DevSupportManagerImpl.this.mDevLoadingViewVisible = false;
                synchronized (DevSupportManagerImpl.this) {
                    DevSupportManagerImpl.this.mBundleStatus.isLastDownloadSucess = Boolean.valueOf(false);
                }
                if (DevSupportManagerImpl.this.mBundleDownloadListener != null) {
                    DevSupportManagerImpl.this.mBundleDownloadListener.onFailure(cause);
                }
                FLog.e(ReactConstants.TAG, "Unable to download JS bundle", (Throwable) cause);
                UiThreadUtil.runOnUiThread(new Runnable() {
                    public void run() {
                        if (cause instanceof DebugServerException) {
                            DevSupportManagerImpl.this.showNewJavaError(((DebugServerException) cause).getMessage(), cause);
                            return;
                        }
                        DevSupportManagerImpl.this.showNewJavaError(DevSupportManagerImpl.this.mApplicationContext.getString(R.string.catalyst_jsload_error), cause);
                    }
                });
            }
        }, this.mJSBundleTempFile, bundleURL, bundleInfo);
    }

    public void startInspector() {
        if (this.mIsDevSupportEnabled) {
            this.mDevServerHelper.openInspectorConnection();
        }
    }

    public void stopInspector() {
        this.mDevServerHelper.closeInspectorConnection();
    }

    /* access modifiers changed from: private */
    public void reload() {
        UiThreadUtil.assertOnUiThread();
        if (this.mIsDevSupportEnabled) {
            if (this.mDebugOverlayController != null) {
                this.mDebugOverlayController.setFpsDebugViewVisible(this.mDevSettings.isFpsDebugEnabled());
            }
            if (!this.mIsShakeDetectorStarted) {
                this.mShakeDetector.start((SensorManager) this.mApplicationContext.getSystemService("sensor"));
                this.mIsShakeDetectorStarted = true;
            }
            if (!this.mIsReceiverRegistered) {
                IntentFilter filter = new IntentFilter();
                filter.addAction(getReloadAppAction(this.mApplicationContext));
                this.mApplicationContext.registerReceiver(this.mReloadAppBroadcastReceiver, filter);
                this.mIsReceiverRegistered = true;
            }
            if (this.mDevLoadingViewVisible) {
                this.mDevLoadingViewController.show();
            }
            this.mDevServerHelper.openPackagerConnection(getClass().getSimpleName(), this);
            if (this.mDevSettings.isReloadOnJSChangeEnabled()) {
                this.mDevServerHelper.startPollingOnChangeEndpoint(new OnServerContentChangeListener() {
                    public void onServerContentChanged() {
                        DevSupportManagerImpl.this.handleReloadJS();
                    }
                });
            } else {
                this.mDevServerHelper.stopPollingOnChangeEndpoint();
            }
        } else {
            if (this.mDebugOverlayController != null) {
                this.mDebugOverlayController.setFpsDebugViewVisible(false);
            }
            if (this.mIsShakeDetectorStarted) {
                this.mShakeDetector.stop();
                this.mIsShakeDetectorStarted = false;
            }
            if (this.mIsReceiverRegistered) {
                this.mApplicationContext.unregisterReceiver(this.mReloadAppBroadcastReceiver);
                this.mIsReceiverRegistered = false;
            }
            hideRedboxDialog();
            hideDevOptionsDialog();
            this.mDevLoadingViewController.hide();
            this.mDevServerHelper.closePackagerConnection();
            this.mDevServerHelper.stopPollingOnChangeEndpoint();
        }
    }

    /* access modifiers changed from: private */
    public static String getReloadAppAction(Context context) {
        return context.getPackageName() + RELOAD_APP_ACTION_SUFFIX;
    }
}
