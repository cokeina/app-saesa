package com.facebook.react.devsupport;

import android.util.JsonReader;
import android.util.JsonToken;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import javax.annotation.Nullable;
import okio.BufferedSource;

public class BundleDeltaClient {
    @Nullable
    String mDeltaId;
    final LinkedHashMap<Number, byte[]> mDeltaModules = new LinkedHashMap<>();
    final LinkedHashMap<Number, byte[]> mPostModules = new LinkedHashMap<>();
    final LinkedHashMap<Number, byte[]> mPreModules = new LinkedHashMap<>();

    static boolean isDeltaUrl(String bundleUrl) {
        return bundleUrl.indexOf(".delta?") != -1;
    }

    public void reset() {
        this.mDeltaId = null;
        this.mDeltaModules.clear();
        this.mPreModules.clear();
        this.mPostModules.clear();
    }

    public String toDeltaUrl(String bundleURL) {
        if (!isDeltaUrl(bundleURL) || this.mDeltaId == null) {
            return bundleURL;
        }
        return bundleURL + "&deltaBundleId=" + this.mDeltaId;
    }

    public synchronized boolean storeDeltaInFile(BufferedSource body, File outputFile) throws IOException {
        boolean z;
        JsonReader jsonReader = new JsonReader(new InputStreamReader(body.inputStream()));
        jsonReader.beginObject();
        int numChangedModules = 0;
        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            if (name.equals("id")) {
                this.mDeltaId = jsonReader.nextString();
            } else if (name.equals("pre")) {
                numChangedModules += patchDelta(jsonReader, this.mPreModules);
            } else if (name.equals("post")) {
                numChangedModules += patchDelta(jsonReader, this.mPostModules);
            } else if (name.equals("delta")) {
                numChangedModules += patchDelta(jsonReader, this.mDeltaModules);
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        jsonReader.close();
        if (numChangedModules == 0) {
            z = false;
        } else {
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            try {
                for (byte[] code : this.mPreModules.values()) {
                    fileOutputStream.write(code);
                    fileOutputStream.write(10);
                }
                for (byte[] code2 : this.mDeltaModules.values()) {
                    fileOutputStream.write(code2);
                    fileOutputStream.write(10);
                }
                for (byte[] code3 : this.mPostModules.values()) {
                    fileOutputStream.write(code3);
                    fileOutputStream.write(10);
                }
                z = true;
            } finally {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        }
        return z;
    }

    private static int patchDelta(JsonReader jsonReader, LinkedHashMap<Number, byte[]> map) throws IOException {
        jsonReader.beginArray();
        int numModules = 0;
        while (jsonReader.hasNext()) {
            jsonReader.beginArray();
            int moduleId = jsonReader.nextInt();
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.skipValue();
                map.remove(Integer.valueOf(moduleId));
            } else {
                map.put(Integer.valueOf(moduleId), jsonReader.nextString().getBytes());
            }
            jsonReader.endArray();
            numModules++;
        }
        jsonReader.endArray();
        return numModules;
    }
}
