package com.facebook.react.devsupport;

import com.facebook.react.bridge.JavaScriptModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.devsupport.JSCHeapCapture.CaptureException;
import com.facebook.react.module.annotations.ReactModule;
import javax.annotation.Nullable;

@ReactModule(name = "JSDevSupport", needsEagerInit = true)
public class JSDevSupport extends ReactContextBaseJavaModule {
    static final String MODULE_NAME = "JSDevSupport";
    @Nullable
    private volatile DevSupportCallback mCurrentCallback = null;

    public interface DevSupportCallback {
        void onFailure(Exception exc);

        void onSuccess(String str);
    }

    public interface JSDevSupportModule extends JavaScriptModule {
        void getJSHierarchy(String str);
    }

    public JSDevSupport(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    public synchronized void getJSHierarchy(String reactTag, DevSupportCallback callback) {
        if (this.mCurrentCallback != null) {
            callback.onFailure(new RuntimeException("JS Hierarchy download already in progress."));
        } else {
            JSDevSupportModule jsDevSupportModule = (JSDevSupportModule) getReactApplicationContext().getJSModule(JSDevSupportModule.class);
            if (jsDevSupportModule == null) {
                callback.onFailure(new CaptureException("JSDevSupport module not registered."));
            } else {
                this.mCurrentCallback = callback;
                jsDevSupportModule.getJSHierarchy(reactTag);
            }
        }
    }

    @ReactMethod
    public synchronized void setResult(String data, String error) {
        if (this.mCurrentCallback != null) {
            if (error == null) {
                this.mCurrentCallback.onSuccess(data);
            } else {
                this.mCurrentCallback.onFailure(new RuntimeException(error));
            }
        }
        this.mCurrentCallback = null;
    }

    public String getName() {
        return MODULE_NAME;
    }
}
