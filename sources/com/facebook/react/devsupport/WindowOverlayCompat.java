package com.facebook.react.devsupport;

import android.os.Build.VERSION;

class WindowOverlayCompat {
    private static final int ANDROID_OREO = 26;
    private static final int TYPE_APPLICATION_OVERLAY = 2038;
    static final int TYPE_SYSTEM_ALERT = (VERSION.SDK_INT < 26 ? 2003 : TYPE_APPLICATION_OVERLAY);
    static final int TYPE_SYSTEM_OVERLAY;

    WindowOverlayCompat() {
    }

    static {
        int i = TYPE_APPLICATION_OVERLAY;
        if (VERSION.SDK_INT < 26) {
            i = 2006;
        }
        TYPE_SYSTEM_OVERLAY = i;
    }
}
