package com.facebook.react.util;

import com.facebook.common.util.UriUtil;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.devsupport.StackTraceHelper;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JSStackTrace {
    private static final Pattern mJsModuleIdPattern = Pattern.compile("(?:^|[/\\\\])(\\d+\\.js)$");

    public static String format(String message, ReadableArray stack) {
        StringBuilder stringBuilder = new StringBuilder(message).append(", stack:\n");
        for (int i = 0; i < stack.size(); i++) {
            ReadableMap frame = stack.getMap(i);
            stringBuilder.append(frame.getString("methodName")).append("@").append(stackFrameToModuleId(frame)).append(frame.getInt(StackTraceHelper.LINE_NUMBER_KEY));
            if (frame.hasKey(StackTraceHelper.COLUMN_KEY) && !frame.isNull(StackTraceHelper.COLUMN_KEY) && frame.getType(StackTraceHelper.COLUMN_KEY) == ReadableType.Number) {
                stringBuilder.append(":").append(frame.getInt(StackTraceHelper.COLUMN_KEY));
            }
            stringBuilder.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        }
        return stringBuilder.toString();
    }

    private static String stackFrameToModuleId(ReadableMap frame) {
        if (frame.hasKey(UriUtil.LOCAL_FILE_SCHEME) && !frame.isNull(UriUtil.LOCAL_FILE_SCHEME) && frame.getType(UriUtil.LOCAL_FILE_SCHEME) == ReadableType.String) {
            Matcher matcher = mJsModuleIdPattern.matcher(frame.getString(UriUtil.LOCAL_FILE_SCHEME));
            if (matcher.find()) {
                return matcher.group(1) + ":";
            }
        }
        return "";
    }
}
