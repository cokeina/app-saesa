package com.facebook.react;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import com.facebook.react.bridge.MemoryPressureListener;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class MemoryPressureRouter implements ComponentCallbacks2 {
    private final Set<MemoryPressureListener> mListeners = Collections.synchronizedSet(new LinkedHashSet());

    MemoryPressureRouter(Context context) {
        context.getApplicationContext().registerComponentCallbacks(this);
    }

    public void destroy(Context context) {
        context.getApplicationContext().unregisterComponentCallbacks(this);
    }

    public void addMemoryPressureListener(MemoryPressureListener listener) {
        this.mListeners.add(listener);
    }

    public void removeMemoryPressureListener(MemoryPressureListener listener) {
        this.mListeners.remove(listener);
    }

    public void onTrimMemory(int level) {
        dispatchMemoryPressure(level);
    }

    public void onConfigurationChanged(Configuration newConfig) {
    }

    public void onLowMemory() {
    }

    private void dispatchMemoryPressure(int level) {
        for (MemoryPressureListener listener : (MemoryPressureListener[]) this.mListeners.toArray(new MemoryPressureListener[this.mListeners.size()])) {
            listener.handleMemoryPressure(level);
        }
    }
}
