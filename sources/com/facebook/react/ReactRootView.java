package com.facebook.react;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import com.facebook.common.logging.FLog;
import com.facebook.infer.annotation.Assertions;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.CatalystInstance;
import com.facebook.react.bridge.GuardedRunnable;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactMarker;
import com.facebook.react.bridge.ReactMarkerConstants;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.common.annotations.VisibleForTesting;
import com.facebook.react.modules.appregistry.AppRegistry;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;
import com.facebook.react.modules.deviceinfo.DeviceInfoModule;
import com.facebook.react.uimanager.DisplayMetricsHolder;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.facebook.react.uimanager.JSTouchDispatcher;
import com.facebook.react.uimanager.MeasureSpecProvider;
import com.facebook.react.uimanager.PixelUtil;
import com.facebook.react.uimanager.RootView;
import com.facebook.react.uimanager.SizeMonitoringFrameLayout;
import com.facebook.react.uimanager.UIManagerModule;
import com.facebook.systrace.Systrace;
import javax.annotation.Nullable;

public class ReactRootView extends SizeMonitoringFrameLayout implements RootView, MeasureSpecProvider {
    @Nullable
    private Bundle mAppProperties;
    @Nullable
    private CustomGlobalLayoutListener mCustomGlobalLayoutListener;
    private int mHeightMeasureSpec = MeasureSpec.makeMeasureSpec(0, 0);
    /* access modifiers changed from: private */
    public boolean mIsAttachedToInstance;
    @Nullable
    private Runnable mJSEntryPoint;
    @Nullable
    private String mJSModuleName;
    private final JSTouchDispatcher mJSTouchDispatcher = new JSTouchDispatcher(this);
    /* access modifiers changed from: private */
    @Nullable
    public ReactInstanceManager mReactInstanceManager;
    @Nullable
    private ReactRootViewEventListener mRootViewEventListener;
    private int mRootViewTag;
    private boolean mShouldLogContentAppeared;
    private boolean mWasMeasured = false;
    private int mWidthMeasureSpec = MeasureSpec.makeMeasureSpec(0, 0);

    private class CustomGlobalLayoutListener implements OnGlobalLayoutListener {
        private int mDeviceRotation = 0;
        private int mKeyboardHeight = 0;
        private final int mMinKeyboardHeightDetected;
        private DisplayMetrics mScreenMetrics = new DisplayMetrics();
        private final Rect mVisibleViewArea;
        private DisplayMetrics mWindowMetrics = new DisplayMetrics();

        CustomGlobalLayoutListener() {
            DisplayMetricsHolder.initDisplayMetricsIfNotInitialized(ReactRootView.this.getContext().getApplicationContext());
            this.mVisibleViewArea = new Rect();
            this.mMinKeyboardHeightDetected = (int) PixelUtil.toPixelFromDIP(60.0f);
        }

        public void onGlobalLayout() {
            if (ReactRootView.this.mReactInstanceManager != null && ReactRootView.this.mIsAttachedToInstance && ReactRootView.this.mReactInstanceManager.getCurrentReactContext() != null) {
                checkForKeyboardEvents();
                checkForDeviceOrientationChanges();
                checkForDeviceDimensionsChanges();
            }
        }

        private void checkForKeyboardEvents() {
            ReactRootView.this.getRootView().getWindowVisibleDisplayFrame(this.mVisibleViewArea);
            int heightDiff = DisplayMetricsHolder.getWindowDisplayMetrics().heightPixels - this.mVisibleViewArea.bottom;
            if (this.mKeyboardHeight != heightDiff && heightDiff > this.mMinKeyboardHeightDetected) {
                this.mKeyboardHeight = heightDiff;
                WritableMap params = Arguments.createMap();
                WritableMap coordinates = Arguments.createMap();
                coordinates.putDouble("screenY", (double) PixelUtil.toDIPFromPixel((float) this.mVisibleViewArea.bottom));
                coordinates.putDouble("screenX", (double) PixelUtil.toDIPFromPixel((float) this.mVisibleViewArea.left));
                coordinates.putDouble("width", (double) PixelUtil.toDIPFromPixel((float) this.mVisibleViewArea.width()));
                coordinates.putDouble("height", (double) PixelUtil.toDIPFromPixel((float) this.mKeyboardHeight));
                params.putMap("endCoordinates", coordinates);
                sendEvent("keyboardDidShow", params);
            } else if (this.mKeyboardHeight != 0 && heightDiff <= this.mMinKeyboardHeightDetected) {
                this.mKeyboardHeight = 0;
                sendEvent("keyboardDidHide", null);
            }
        }

        private void checkForDeviceOrientationChanges() {
            int rotation = ((WindowManager) ReactRootView.this.getContext().getSystemService("window")).getDefaultDisplay().getRotation();
            if (this.mDeviceRotation != rotation) {
                this.mDeviceRotation = rotation;
                emitOrientationChanged(rotation);
            }
        }

        private void checkForDeviceDimensionsChanges() {
            DisplayMetricsHolder.initDisplayMetrics(ReactRootView.this.getContext());
            if (!areMetricsEqual(this.mWindowMetrics, DisplayMetricsHolder.getWindowDisplayMetrics()) || !areMetricsEqual(this.mScreenMetrics, DisplayMetricsHolder.getScreenDisplayMetrics())) {
                this.mWindowMetrics.setTo(DisplayMetricsHolder.getWindowDisplayMetrics());
                this.mScreenMetrics.setTo(DisplayMetricsHolder.getScreenDisplayMetrics());
                emitUpdateDimensionsEvent();
            }
        }

        private boolean areMetricsEqual(DisplayMetrics displayMetrics, DisplayMetrics otherMetrics) {
            if (VERSION.SDK_INT >= 17) {
                return displayMetrics.equals(otherMetrics);
            }
            return displayMetrics.widthPixels == otherMetrics.widthPixels && displayMetrics.heightPixels == otherMetrics.heightPixels && displayMetrics.density == otherMetrics.density && displayMetrics.densityDpi == otherMetrics.densityDpi && displayMetrics.scaledDensity == otherMetrics.scaledDensity && displayMetrics.xdpi == otherMetrics.xdpi && displayMetrics.ydpi == otherMetrics.ydpi;
        }

        private void emitOrientationChanged(int newRotation) {
            String name;
            double rotationDegrees;
            boolean isLandscape = false;
            switch (newRotation) {
                case 0:
                    name = "portrait-primary";
                    rotationDegrees = 0.0d;
                    break;
                case 1:
                    name = "landscape-primary";
                    rotationDegrees = -90.0d;
                    isLandscape = true;
                    break;
                case 2:
                    name = "portrait-secondary";
                    rotationDegrees = 180.0d;
                    break;
                case 3:
                    name = "landscape-secondary";
                    rotationDegrees = 90.0d;
                    isLandscape = true;
                    break;
                default:
                    return;
            }
            WritableMap map = Arguments.createMap();
            map.putString("name", name);
            map.putDouble("rotationDegrees", rotationDegrees);
            map.putBoolean("isLandscape", isLandscape);
            sendEvent("namedOrientationDidChange", map);
        }

        private void emitUpdateDimensionsEvent() {
            ((DeviceInfoModule) ReactRootView.this.mReactInstanceManager.getCurrentReactContext().getNativeModule(DeviceInfoModule.class)).emitUpdateDimensionsEvent();
        }

        private void sendEvent(String eventName, @Nullable WritableMap params) {
            if (ReactRootView.this.mReactInstanceManager != null) {
                ((RCTDeviceEventEmitter) ReactRootView.this.mReactInstanceManager.getCurrentReactContext().getJSModule(RCTDeviceEventEmitter.class)).emit(eventName, params);
            }
        }
    }

    public interface ReactRootViewEventListener {
        void onAttachedToReactInstance(ReactRootView reactRootView);
    }

    public ReactRootView(Context context) {
        super(context);
    }

    public ReactRootView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ReactRootView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Systrace.beginSection(0, "ReactRootView.onMeasure");
        try {
            this.mWidthMeasureSpec = widthMeasureSpec;
            this.mHeightMeasureSpec = heightMeasureSpec;
            int width = 0;
            int height = 0;
            int widthMode = MeasureSpec.getMode(widthMeasureSpec);
            if (widthMode == Integer.MIN_VALUE || widthMode == 0) {
                for (int i = 0; i < getChildCount(); i++) {
                    View child = getChildAt(i);
                    width = Math.max(width, child.getLeft() + child.getMeasuredWidth() + child.getPaddingLeft() + child.getPaddingRight());
                }
            } else {
                width = MeasureSpec.getSize(widthMeasureSpec);
            }
            int heightMode = MeasureSpec.getMode(heightMeasureSpec);
            if (heightMode == Integer.MIN_VALUE || heightMode == 0) {
                for (int i2 = 0; i2 < getChildCount(); i2++) {
                    View child2 = getChildAt(i2);
                    height = Math.max(height, child2.getTop() + child2.getMeasuredHeight() + child2.getPaddingTop() + child2.getPaddingBottom());
                }
            } else {
                height = MeasureSpec.getSize(heightMeasureSpec);
            }
            setMeasuredDimension(width, height);
            this.mWasMeasured = true;
            if (this.mReactInstanceManager == null || this.mIsAttachedToInstance) {
                updateRootLayoutSpecs(this.mWidthMeasureSpec, this.mHeightMeasureSpec);
            } else {
                attachToReactInstanceManager();
            }
            enableLayoutCalculation();
        } finally {
            Systrace.endSection(0);
        }
    }

    public int getWidthMeasureSpec() {
        if (this.mWasMeasured || getLayoutParams() == null || getLayoutParams().width <= 0) {
            return this.mWidthMeasureSpec;
        }
        return MeasureSpec.makeMeasureSpec(getLayoutParams().width, 1073741824);
    }

    public int getHeightMeasureSpec() {
        if (this.mWasMeasured || getLayoutParams() == null || getLayoutParams().height <= 0) {
            return this.mHeightMeasureSpec;
        }
        return MeasureSpec.makeMeasureSpec(getLayoutParams().height, 1073741824);
    }

    public void onChildStartedNativeGesture(MotionEvent androidEvent) {
        if (this.mReactInstanceManager == null || !this.mIsAttachedToInstance || this.mReactInstanceManager.getCurrentReactContext() == null) {
            FLog.w(ReactConstants.TAG, "Unable to dispatch touch to JS as the catalyst instance has not been attached");
            return;
        }
        this.mJSTouchDispatcher.onChildStartedNativeGesture(androidEvent, ((UIManagerModule) this.mReactInstanceManager.getCurrentReactContext().getNativeModule(UIManagerModule.class)).getEventDispatcher());
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        dispatchJSTouchEvent(ev);
        return super.onInterceptTouchEvent(ev);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        dispatchJSTouchEvent(ev);
        super.onTouchEvent(ev);
        return true;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        try {
            super.dispatchDraw(canvas);
        } catch (StackOverflowError e) {
            handleException(e);
        }
    }

    private void dispatchJSTouchEvent(MotionEvent event) {
        if (this.mReactInstanceManager == null || !this.mIsAttachedToInstance || this.mReactInstanceManager.getCurrentReactContext() == null) {
            FLog.w(ReactConstants.TAG, "Unable to dispatch touch to JS as the catalyst instance has not been attached");
            return;
        }
        this.mJSTouchDispatcher.handleTouchEvent(event, ((UIManagerModule) this.mReactInstanceManager.getCurrentReactContext().getNativeModule(UIManagerModule.class)).getEventDispatcher());
    }

    public void requestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(disallowIntercept);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mIsAttachedToInstance) {
            getViewTreeObserver().addOnGlobalLayoutListener(getCustomGlobalLayoutListener());
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!this.mIsAttachedToInstance) {
            return;
        }
        if (VERSION.SDK_INT >= 16) {
            getViewTreeObserver().removeOnGlobalLayoutListener(getCustomGlobalLayoutListener());
        } else {
            getViewTreeObserver().removeGlobalOnLayoutListener(getCustomGlobalLayoutListener());
        }
    }

    public void onViewAdded(View child) {
        super.onViewAdded(child);
        if (this.mShouldLogContentAppeared) {
            this.mShouldLogContentAppeared = false;
            if (this.mJSModuleName != null) {
                ReactMarker.logMarker(ReactMarkerConstants.CONTENT_APPEARED, this.mJSModuleName, this.mRootViewTag);
            }
        }
    }

    public void startReactApplication(ReactInstanceManager reactInstanceManager, String moduleName) {
        startReactApplication(reactInstanceManager, moduleName, null);
    }

    public void startReactApplication(ReactInstanceManager reactInstanceManager, String moduleName, @Nullable Bundle initialProperties) {
        Systrace.beginSection(0, "startReactApplication");
        try {
            UiThreadUtil.assertOnUiThread();
            Assertions.assertCondition(this.mReactInstanceManager == null, "This root view has already been attached to a catalyst instance manager");
            this.mReactInstanceManager = reactInstanceManager;
            this.mJSModuleName = moduleName;
            this.mAppProperties = initialProperties;
            if (!this.mReactInstanceManager.hasStartedCreatingInitialContext()) {
                this.mReactInstanceManager.createReactContextInBackground();
            }
            attachToReactInstanceManager();
        } finally {
            Systrace.endSection(0);
        }
    }

    private void enableLayoutCalculation() {
        if (this.mReactInstanceManager == null) {
            FLog.w(ReactConstants.TAG, "Unable to enable layout calculation for uninitialized ReactInstanceManager");
            return;
        }
        ReactContext reactApplicationContext = this.mReactInstanceManager.getCurrentReactContext();
        if (reactApplicationContext != null) {
            ((UIManagerModule) reactApplicationContext.getCatalystInstance().getNativeModule(UIManagerModule.class)).getUIImplementation().enableLayoutCalculationForRootNode(getRootViewTag());
        }
    }

    private void updateRootLayoutSpecs(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.mReactInstanceManager == null) {
            FLog.w(ReactConstants.TAG, "Unable to update root layout specs for uninitialized ReactInstanceManager");
            return;
        }
        ReactContext reactApplicationContext = this.mReactInstanceManager.getCurrentReactContext();
        if (reactApplicationContext != null) {
            final ReactContext reactContext = reactApplicationContext;
            final int i = widthMeasureSpec;
            final int i2 = heightMeasureSpec;
            reactApplicationContext.runOnNativeModulesQueueThread(new GuardedRunnable(reactApplicationContext) {
                public void runGuarded() {
                    ((UIManagerModule) reactContext.getCatalystInstance().getNativeModule(UIManagerModule.class)).updateRootLayoutSpecs(ReactRootView.this.getRootViewTag(), i, i2);
                }
            });
        }
    }

    public void unmountReactApplication() {
        if (this.mReactInstanceManager != null && this.mIsAttachedToInstance) {
            this.mReactInstanceManager.detachRootView(this);
            this.mIsAttachedToInstance = false;
        }
        this.mShouldLogContentAppeared = false;
    }

    public void onAttachedToReactInstance() {
        if (this.mRootViewEventListener != null) {
            this.mRootViewEventListener.onAttachedToReactInstance(this);
        }
    }

    public void setEventListener(ReactRootViewEventListener eventListener) {
        this.mRootViewEventListener = eventListener;
    }

    /* access modifiers changed from: 0000 */
    public String getJSModuleName() {
        return (String) Assertions.assertNotNull(this.mJSModuleName);
    }

    @Nullable
    public Bundle getAppProperties() {
        return this.mAppProperties;
    }

    public void setAppProperties(@Nullable Bundle appProperties) {
        UiThreadUtil.assertOnUiThread();
        this.mAppProperties = appProperties;
        if (getRootViewTag() != 0) {
            invokeJSEntryPoint();
        }
    }

    /* access modifiers changed from: 0000 */
    public void invokeJSEntryPoint() {
        if (this.mJSEntryPoint == null) {
            defaultJSEntryPoint();
        } else {
            this.mJSEntryPoint.run();
        }
    }

    public void setJSEntryPoint(Runnable jsEntryPoint) {
        this.mJSEntryPoint = jsEntryPoint;
    }

    public void invokeDefaultJSEntryPoint(@Nullable Bundle appProperties) {
        UiThreadUtil.assertOnUiThread();
        if (appProperties != null) {
            this.mAppProperties = appProperties;
        }
        defaultJSEntryPoint();
    }

    private void defaultJSEntryPoint() {
        Systrace.beginSection(0, "ReactRootView.runApplication");
        try {
            if (this.mReactInstanceManager != null && this.mIsAttachedToInstance) {
                ReactContext reactContext = this.mReactInstanceManager.getCurrentReactContext();
                if (reactContext == null) {
                    Systrace.endSection(0);
                    return;
                }
                CatalystInstance catalystInstance = reactContext.getCatalystInstance();
                WritableNativeMap appParams = new WritableNativeMap();
                appParams.putDouble("rootTag", (double) getRootViewTag());
                Bundle appProperties = getAppProperties();
                if (appProperties != null) {
                    appParams.putMap("initialProps", Arguments.fromBundle(appProperties));
                }
                this.mShouldLogContentAppeared = true;
                ((AppRegistry) catalystInstance.getJSModule(AppRegistry.class)).runApplication(getJSModuleName(), appParams);
                Systrace.endSection(0);
            }
        } finally {
            Systrace.endSection(0);
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void simulateAttachForTesting() {
        this.mIsAttachedToInstance = true;
    }

    private CustomGlobalLayoutListener getCustomGlobalLayoutListener() {
        if (this.mCustomGlobalLayoutListener == null) {
            this.mCustomGlobalLayoutListener = new CustomGlobalLayoutListener();
        }
        return this.mCustomGlobalLayoutListener;
    }

    private void attachToReactInstanceManager() {
        Systrace.beginSection(0, "attachToReactInstanceManager");
        try {
            if (!this.mIsAttachedToInstance) {
                this.mIsAttachedToInstance = true;
                ((ReactInstanceManager) Assertions.assertNotNull(this.mReactInstanceManager)).attachRootView(this);
                getViewTreeObserver().addOnGlobalLayoutListener(getCustomGlobalLayoutListener());
                Systrace.endSection(0);
            }
        } finally {
            Systrace.endSection(0);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        Assertions.assertCondition(!this.mIsAttachedToInstance, "The application this ReactRootView was rendering was not unmounted before the ReactRootView was garbage collected. This usually means that your application is leaking large amounts of memory. To solve this, make sure to call ReactRootView#unmountReactApplication in the onDestroy() of your hosting Activity or in the onDestroyView() of your hosting Fragment.");
    }

    public int getRootViewTag() {
        return this.mRootViewTag;
    }

    public void setRootViewTag(int rootViewTag) {
        this.mRootViewTag = rootViewTag;
    }

    public void handleException(Throwable t) {
        if (this.mReactInstanceManager == null || this.mReactInstanceManager.getCurrentReactContext() == null) {
            throw new RuntimeException(t);
        }
        Exception e = t instanceof StackOverflowError ? new IllegalViewOperationException("StackOverflowException", this, t) : t instanceof Exception ? (Exception) t : new RuntimeException(t);
        this.mReactInstanceManager.getCurrentReactContext().handleException(e);
    }

    @Nullable
    public ReactInstanceManager getReactInstanceManager() {
        return this.mReactInstanceManager;
    }
}
