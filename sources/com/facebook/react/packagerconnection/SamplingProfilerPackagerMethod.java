package com.facebook.react.packagerconnection;

import android.os.Looper;
import com.facebook.jni.HybridData;
import com.facebook.proguard.annotations.DoNotStrip;
import com.facebook.soloader.SoLoader;
import javax.annotation.Nullable;

public class SamplingProfilerPackagerMethod extends RequestOnlyHandler {
    private SamplingProfilerJniMethod mJniMethod;

    private static final class SamplingProfilerJniMethod {
        @DoNotStrip
        private final HybridData mHybridData;

        @DoNotStrip
        private static native HybridData initHybrid(long j);

        /* access modifiers changed from: private */
        @DoNotStrip
        public native void poke(Responder responder);

        public SamplingProfilerJniMethod(long javaScriptContext) {
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            this.mHybridData = initHybrid(javaScriptContext);
        }
    }

    static {
        SoLoader.loadLibrary("packagerconnectionjnifb");
    }

    public SamplingProfilerPackagerMethod(long javaScriptContext) {
        this.mJniMethod = new SamplingProfilerJniMethod(javaScriptContext);
    }

    public void onRequest(@Nullable Object params, Responder responder) {
        this.mJniMethod.poke(responder);
    }
}
