package com.facebook.react.packagerconnection;

import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import com.facebook.common.logging.FLog;
import com.facebook.common.util.UriUtil;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.annotation.Nullable;
import org.json.JSONObject;

public class FileIoHandler implements Runnable {
    private static final long FILE_TTL = 30000;
    private static final String TAG = JSPackagerClient.class.getSimpleName();
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private int mNextHandle = 1;
    /* access modifiers changed from: private */
    public final Map<Integer, TtlFileInputStream> mOpenFiles = new HashMap();
    private final Map<String, RequestHandler> mRequestHandlers = new HashMap();

    private static class TtlFileInputStream {
        private final FileInputStream mStream;
        private long mTtl = (System.currentTimeMillis() + FileIoHandler.FILE_TTL);

        public TtlFileInputStream(String path) throws FileNotFoundException {
            this.mStream = new FileInputStream(path);
        }

        private void extendTtl() {
            this.mTtl = System.currentTimeMillis() + FileIoHandler.FILE_TTL;
        }

        public boolean expiredTtl() {
            return System.currentTimeMillis() >= this.mTtl;
        }

        public String read(int size) throws IOException {
            extendTtl();
            byte[] buffer = new byte[size];
            return Base64.encodeToString(buffer, 0, this.mStream.read(buffer), 0);
        }

        public void close() throws IOException {
            this.mStream.close();
        }
    }

    public FileIoHandler() {
        this.mRequestHandlers.put("fopen", new RequestOnlyHandler() {
            public void onRequest(@Nullable Object params, Responder responder) {
                synchronized (FileIoHandler.this.mOpenFiles) {
                    try {
                        JSONObject paramsObj = (JSONObject) params;
                        if (paramsObj == null) {
                            throw new Exception("params must be an object { mode: string, filename: string }");
                        }
                        String mode = paramsObj.optString("mode");
                        if (mode == null) {
                            throw new Exception("missing params.mode");
                        }
                        String filename = paramsObj.optString("filename");
                        if (filename == null) {
                            throw new Exception("missing params.filename");
                        } else if (!mode.equals("r")) {
                            throw new IllegalArgumentException("unsupported mode: " + mode);
                        } else {
                            responder.respond(Integer.valueOf(FileIoHandler.this.addOpenFile(filename)));
                        }
                    } catch (Exception e) {
                        responder.error(e.toString());
                    }
                }
            }
        });
        this.mRequestHandlers.put("fclose", new RequestOnlyHandler() {
            public void onRequest(@Nullable Object params, Responder responder) {
                synchronized (FileIoHandler.this.mOpenFiles) {
                    try {
                        if (!(params instanceof Number)) {
                            throw new Exception("params must be a file handle");
                        }
                        TtlFileInputStream stream = (TtlFileInputStream) FileIoHandler.this.mOpenFiles.get(Integer.valueOf(((Integer) params).intValue()));
                        if (stream == null) {
                            throw new Exception("invalid file handle, it might have timed out");
                        }
                        FileIoHandler.this.mOpenFiles.remove(Integer.valueOf(((Integer) params).intValue()));
                        stream.close();
                        responder.respond("");
                    } catch (Exception e) {
                        responder.error(e.toString());
                    }
                }
            }
        });
        this.mRequestHandlers.put("fread", new RequestOnlyHandler() {
            public void onRequest(@Nullable Object params, Responder responder) {
                synchronized (FileIoHandler.this.mOpenFiles) {
                    try {
                        JSONObject paramsObj = (JSONObject) params;
                        if (paramsObj == null) {
                            throw new Exception("params must be an object { file: handle, size: number }");
                        }
                        int file = paramsObj.optInt(UriUtil.LOCAL_FILE_SCHEME);
                        if (file == 0) {
                            throw new Exception("invalid or missing file handle");
                        }
                        int size = paramsObj.optInt("size");
                        if (size == 0) {
                            throw new Exception("invalid or missing read size");
                        }
                        TtlFileInputStream stream = (TtlFileInputStream) FileIoHandler.this.mOpenFiles.get(Integer.valueOf(file));
                        if (stream == null) {
                            throw new Exception("invalid file handle, it might have timed out");
                        }
                        responder.respond(stream.read(size));
                    } catch (Exception e) {
                        responder.error(e.toString());
                    }
                }
            }
        });
    }

    public Map<String, RequestHandler> handlers() {
        return this.mRequestHandlers;
    }

    /* access modifiers changed from: private */
    public int addOpenFile(String filename) throws FileNotFoundException {
        int handle = this.mNextHandle;
        this.mNextHandle = handle + 1;
        this.mOpenFiles.put(Integer.valueOf(handle), new TtlFileInputStream(filename));
        if (this.mOpenFiles.size() == 1) {
            this.mHandler.postDelayed(this, FILE_TTL);
        }
        return handle;
    }

    public void run() {
        synchronized (this.mOpenFiles) {
            Iterator<TtlFileInputStream> i = this.mOpenFiles.values().iterator();
            while (i.hasNext()) {
                TtlFileInputStream stream = (TtlFileInputStream) i.next();
                if (stream.expiredTtl()) {
                    i.remove();
                    try {
                        stream.close();
                    } catch (IOException e) {
                        FLog.e(TAG, "closing expired file failed: " + e.toString());
                    }
                }
            }
            if (!this.mOpenFiles.isEmpty()) {
                this.mHandler.postDelayed(this, FILE_TTL);
            }
        }
    }
}
