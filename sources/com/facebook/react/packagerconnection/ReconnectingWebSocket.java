package com.facebook.react.packagerconnection;

import android.os.Handler;
import android.os.Looper;
import com.facebook.common.logging.FLog;
import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public final class ReconnectingWebSocket extends WebSocketListener {
    private static final int RECONNECT_DELAY_MS = 2000;
    private static final String TAG = ReconnectingWebSocket.class.getSimpleName();
    private boolean mClosed = false;
    @Nullable
    private ConnectionCallback mConnectionCallback;
    private final Handler mHandler;
    @Nullable
    private MessageCallback mMessageCallback;
    private boolean mSuppressConnectionErrors;
    private final String mUrl;
    @Nullable
    private WebSocket mWebSocket;

    public interface ConnectionCallback {
        void onConnected();

        void onDisconnected();
    }

    public interface MessageCallback {
        void onMessage(String str);

        void onMessage(ByteString byteString);
    }

    public ReconnectingWebSocket(String url, MessageCallback messageCallback, ConnectionCallback connectionCallback) {
        this.mUrl = url;
        this.mMessageCallback = messageCallback;
        this.mConnectionCallback = connectionCallback;
        this.mHandler = new Handler(Looper.getMainLooper());
    }

    public void connect() {
        if (this.mClosed) {
            throw new IllegalStateException("Can't connect closed client");
        }
        new Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).readTimeout(0, TimeUnit.MINUTES).build().newWebSocket(new Request.Builder().url(this.mUrl).build(), this);
    }

    /* access modifiers changed from: private */
    public synchronized void delayedReconnect() {
        if (!this.mClosed) {
            connect();
        }
    }

    private void reconnect() {
        if (this.mClosed) {
            throw new IllegalStateException("Can't reconnect closed client");
        }
        if (!this.mSuppressConnectionErrors) {
            FLog.w(TAG, "Couldn't connect to \"" + this.mUrl + "\", will silently retry");
            this.mSuppressConnectionErrors = true;
        }
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                ReconnectingWebSocket.this.delayedReconnect();
            }
        }, 2000);
    }

    public void closeQuietly() {
        this.mClosed = true;
        closeWebSocketQuietly();
        this.mMessageCallback = null;
        if (this.mConnectionCallback != null) {
            this.mConnectionCallback.onDisconnected();
        }
    }

    private void closeWebSocketQuietly() {
        if (this.mWebSocket != null) {
            try {
                this.mWebSocket.close(1000, "End of session");
            } catch (Exception e) {
            }
            this.mWebSocket = null;
        }
    }

    private void abort(String message, Throwable cause) {
        FLog.e(TAG, "Error occurred, shutting down websocket connection: " + message, cause);
        closeWebSocketQuietly();
    }

    public synchronized void onOpen(WebSocket webSocket, Response response) {
        this.mWebSocket = webSocket;
        this.mSuppressConnectionErrors = false;
        if (this.mConnectionCallback != null) {
            this.mConnectionCallback.onConnected();
        }
    }

    public synchronized void onFailure(WebSocket webSocket, Throwable t, Response response) {
        if (this.mWebSocket != null) {
            abort("Websocket exception", t);
        }
        if (!this.mClosed) {
            if (this.mConnectionCallback != null) {
                this.mConnectionCallback.onDisconnected();
            }
            reconnect();
        }
    }

    public synchronized void onMessage(WebSocket webSocket, String text) {
        if (this.mMessageCallback != null) {
            this.mMessageCallback.onMessage(text);
        }
    }

    public synchronized void onMessage(WebSocket webSocket, ByteString bytes) {
        if (this.mMessageCallback != null) {
            this.mMessageCallback.onMessage(bytes);
        }
    }

    public synchronized void onClosed(WebSocket webSocket, int code, String reason) {
        this.mWebSocket = null;
        if (!this.mClosed) {
            if (this.mConnectionCallback != null) {
                this.mConnectionCallback.onDisconnected();
            }
            reconnect();
        }
    }

    public synchronized void sendMessage(String message) throws IOException {
        if (this.mWebSocket != null) {
            this.mWebSocket.send(message);
        } else {
            throw new ClosedChannelException();
        }
    }

    public synchronized void sendMessage(ByteString message) throws IOException {
        if (this.mWebSocket != null) {
            this.mWebSocket.send(message);
        } else {
            throw new ClosedChannelException();
        }
    }
}
