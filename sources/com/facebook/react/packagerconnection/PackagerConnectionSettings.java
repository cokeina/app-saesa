package com.facebook.react.packagerconnection;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.facebook.common.logging.FLog;
import com.facebook.infer.annotation.Assertions;
import com.facebook.react.modules.systeminfo.AndroidInfoHelpers;
import javax.annotation.Nullable;

public class PackagerConnectionSettings {
    private static final String PREFS_DEBUG_SERVER_HOST_KEY = "debug_http_host";
    private static final String TAG = PackagerConnectionSettings.class.getSimpleName();
    private final String mPackageName;
    private final SharedPreferences mPreferences;

    public PackagerConnectionSettings(Context applicationContext) {
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        this.mPackageName = applicationContext.getPackageName();
    }

    public String getDebugServerHost() {
        String hostFromSettings = this.mPreferences.getString(PREFS_DEBUG_SERVER_HOST_KEY, null);
        if (!TextUtils.isEmpty(hostFromSettings)) {
            return (String) Assertions.assertNotNull(hostFromSettings);
        }
        String host = AndroidInfoHelpers.getServerHost();
        if (host.equals(AndroidInfoHelpers.DEVICE_LOCALHOST)) {
            FLog.w(TAG, "You seem to be running on device. Run 'adb reverse tcp:8081 tcp:8081' to forward the debug server's port to the device.");
        }
        return host;
    }

    public String getInspectorServerHost() {
        return AndroidInfoHelpers.getInspectorProxyHost();
    }

    @Nullable
    public String getPackageName() {
        return this.mPackageName;
    }
}
