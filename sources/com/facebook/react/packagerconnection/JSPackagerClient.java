package com.facebook.react.packagerconnection;

import android.net.Uri.Builder;
import com.facebook.common.logging.FLog;
import com.facebook.react.modules.systeminfo.AndroidInfoHelpers;
import com.facebook.react.packagerconnection.ReconnectingWebSocket.ConnectionCallback;
import com.facebook.react.packagerconnection.ReconnectingWebSocket.MessageCallback;
import java.util.Map;
import javax.annotation.Nullable;
import okio.ByteString;
import org.json.JSONObject;

public final class JSPackagerClient implements MessageCallback {
    private static final String PACKAGER_CONNECTION_URL_FORMAT = "ws://%s/message?device=%s&app=%s&context=%s";
    private static final int PROTOCOL_VERSION = 2;
    /* access modifiers changed from: private */
    public static final String TAG = JSPackagerClient.class.getSimpleName();
    private Map<String, RequestHandler> mRequestHandlers;
    /* access modifiers changed from: private */
    public ReconnectingWebSocket mWebSocket;

    private class ResponderImpl implements Responder {
        private Object mId;

        public ResponderImpl(Object id) {
            this.mId = id;
        }

        public void respond(Object result) {
            try {
                JSONObject message = new JSONObject();
                message.put("version", 2);
                message.put("id", this.mId);
                message.put("result", result);
                JSPackagerClient.this.mWebSocket.sendMessage(message.toString());
            } catch (Exception e) {
                FLog.e(JSPackagerClient.TAG, "Responding failed", (Throwable) e);
            }
        }

        public void error(Object error) {
            try {
                JSONObject message = new JSONObject();
                message.put("version", 2);
                message.put("id", this.mId);
                message.put("error", error);
                JSPackagerClient.this.mWebSocket.sendMessage(message.toString());
            } catch (Exception e) {
                FLog.e(JSPackagerClient.TAG, "Responding with error failed", (Throwable) e);
            }
        }
    }

    public JSPackagerClient(String clientId, PackagerConnectionSettings settings, Map<String, RequestHandler> requestHandlers) {
        this(clientId, settings, requestHandlers, null);
    }

    public JSPackagerClient(String clientId, PackagerConnectionSettings settings, Map<String, RequestHandler> requestHandlers, @Nullable ConnectionCallback connectionCallback) {
        Builder builder = new Builder();
        builder.scheme("ws").encodedAuthority(settings.getDebugServerHost()).appendPath("message").appendQueryParameter("device", AndroidInfoHelpers.getFriendlyDeviceName()).appendQueryParameter("app", settings.getPackageName()).appendQueryParameter("clientid", clientId);
        this.mWebSocket = new ReconnectingWebSocket(builder.build().toString(), this, connectionCallback);
        this.mRequestHandlers = requestHandlers;
    }

    public void init() {
        this.mWebSocket.connect();
    }

    public void close() {
        this.mWebSocket.closeQuietly();
    }

    public void onMessage(String text) {
        try {
            JSONObject message = new JSONObject(text);
            int version = message.optInt("version");
            String method = message.optString("method");
            Object id = message.opt("id");
            Object params = message.opt("params");
            if (version != 2) {
                FLog.e(TAG, "Message with incompatible or missing version of protocol received: " + version);
            } else if (method == null) {
                abortOnMessage(id, "No method provided");
            } else {
                RequestHandler handler = (RequestHandler) this.mRequestHandlers.get(method);
                if (handler == null) {
                    abortOnMessage(id, "No request handler for method: " + method);
                } else if (id == null) {
                    handler.onNotification(params);
                } else {
                    handler.onRequest(params, new ResponderImpl(id));
                }
            }
        } catch (Exception e) {
            FLog.e(TAG, "Handling the message failed", (Throwable) e);
        }
    }

    public void onMessage(ByteString bytes) {
        FLog.w(TAG, "Websocket received message with payload of unexpected type binary");
    }

    private void abortOnMessage(Object id, String reason) {
        if (id != null) {
            new ResponderImpl(id).error(reason);
        }
        FLog.e(TAG, "Handling the message failed with reason: " + reason);
    }
}
