package com.facebook.react.views.imagehelper;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.Uri.Builder;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe
public class ResourceDrawableIdHelper {
    private static final String LOCAL_RESOURCE_SCHEME = "res";
    private static volatile ResourceDrawableIdHelper sResourceDrawableIdHelper;
    private Map<String, Integer> mResourceDrawableIdMap = new HashMap();

    private ResourceDrawableIdHelper() {
    }

    public static ResourceDrawableIdHelper getInstance() {
        if (sResourceDrawableIdHelper == null) {
            synchronized (ResourceDrawableIdHelper.class) {
                if (sResourceDrawableIdHelper == null) {
                    sResourceDrawableIdHelper = new ResourceDrawableIdHelper();
                }
            }
        }
        return sResourceDrawableIdHelper;
    }

    public synchronized void clear() {
        this.mResourceDrawableIdMap.clear();
    }

    public int getResourceDrawableId(Context context, @Nullable String name) {
        if (name == null || name.isEmpty()) {
            return 0;
        }
        String name2 = name.toLowerCase().replace("-", "_");
        try {
            return Integer.parseInt(name2);
        } catch (NumberFormatException e) {
            synchronized (this) {
                if (this.mResourceDrawableIdMap.containsKey(name2)) {
                    return ((Integer) this.mResourceDrawableIdMap.get(name2)).intValue();
                }
                int id = context.getResources().getIdentifier(name2, "drawable", context.getPackageName());
                this.mResourceDrawableIdMap.put(name2, Integer.valueOf(id));
                return id;
            }
        }
    }

    @Nullable
    public Drawable getResourceDrawable(Context context, @Nullable String name) {
        int resId = getResourceDrawableId(context, name);
        if (resId > 0) {
            return context.getResources().getDrawable(resId);
        }
        return null;
    }

    public Uri getResourceDrawableUri(Context context, @Nullable String name) {
        int resId = getResourceDrawableId(context, name);
        return resId > 0 ? new Builder().scheme("res").path(String.valueOf(resId)).build() : Uri.EMPTY;
    }
}
