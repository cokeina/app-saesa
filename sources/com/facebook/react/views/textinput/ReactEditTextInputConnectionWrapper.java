package com.facebook.react.views.textinput;

import android.view.KeyEvent;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.uimanager.UIManagerModule;
import com.facebook.react.uimanager.events.EventDispatcher;
import javax.annotation.Nullable;

class ReactEditTextInputConnectionWrapper extends InputConnectionWrapper {
    public static final String BACKSPACE_KEY_VALUE = "Backspace";
    public static final String ENTER_KEY_VALUE = "Enter";
    public static final String NEWLINE_RAW_VALUE = "\n";
    private ReactEditText mEditText;
    private EventDispatcher mEventDispatcher;
    private boolean mIsBatchEdit;
    @Nullable
    private String mKey = null;

    public ReactEditTextInputConnectionWrapper(InputConnection target, ReactContext reactContext, ReactEditText editText) {
        super(target, false);
        this.mEventDispatcher = ((UIManagerModule) reactContext.getNativeModule(UIManagerModule.class)).getEventDispatcher();
        this.mEditText = editText;
    }

    public boolean beginBatchEdit() {
        this.mIsBatchEdit = true;
        return super.beginBatchEdit();
    }

    public boolean endBatchEdit() {
        this.mIsBatchEdit = false;
        if (this.mKey != null) {
            dispatchKeyEvent(this.mKey);
            this.mKey = null;
        }
        return super.endBatchEdit();
    }

    public boolean setComposingText(CharSequence text, int newCursorPosition) {
        boolean noPreviousSelection;
        boolean cursorDidNotMove;
        String key;
        boolean cursorMovedBackwardsOrAtBeginningOfInput = false;
        int previousSelectionStart = this.mEditText.getSelectionStart();
        int previousSelectionEnd = this.mEditText.getSelectionEnd();
        boolean consumed = super.setComposingText(text, newCursorPosition);
        int currentSelectionStart = this.mEditText.getSelectionStart();
        if (previousSelectionStart == previousSelectionEnd) {
            noPreviousSelection = true;
        } else {
            noPreviousSelection = false;
        }
        if (currentSelectionStart == previousSelectionStart) {
            cursorDidNotMove = true;
        } else {
            cursorDidNotMove = false;
        }
        if (currentSelectionStart < previousSelectionStart || currentSelectionStart <= 0) {
            cursorMovedBackwardsOrAtBeginningOfInput = true;
        }
        if (cursorMovedBackwardsOrAtBeginningOfInput || (!noPreviousSelection && cursorDidNotMove)) {
            key = BACKSPACE_KEY_VALUE;
        } else {
            key = String.valueOf(this.mEditText.getText().charAt(currentSelectionStart - 1));
        }
        dispatchKeyEventOrEnqueue(key);
        return consumed;
    }

    public boolean commitText(CharSequence text, int newCursorPosition) {
        String key = text.toString();
        if (key.length() <= 1) {
            if (key.equals("")) {
                key = BACKSPACE_KEY_VALUE;
            }
            dispatchKeyEventOrEnqueue(key);
        }
        return super.commitText(text, newCursorPosition);
    }

    public boolean deleteSurroundingText(int beforeLength, int afterLength) {
        dispatchKeyEvent(BACKSPACE_KEY_VALUE);
        return super.deleteSurroundingText(beforeLength, afterLength);
    }

    public boolean sendKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            if (event.getKeyCode() == 67) {
                dispatchKeyEvent(BACKSPACE_KEY_VALUE);
            } else if (event.getKeyCode() == 66) {
                dispatchKeyEvent(ENTER_KEY_VALUE);
            }
        }
        return super.sendKeyEvent(event);
    }

    private void dispatchKeyEventOrEnqueue(String key) {
        if (this.mIsBatchEdit) {
            this.mKey = key;
        } else {
            dispatchKeyEvent(key);
        }
    }

    private void dispatchKeyEvent(String key) {
        if (key.equals(NEWLINE_RAW_VALUE)) {
            key = ENTER_KEY_VALUE;
        }
        this.mEventDispatcher.dispatchEvent(new ReactTextInputKeyPressEvent(this.mEditText.getId(), key));
    }
}
