package com.facebook.react.views.textinput;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.events.Event;
import com.facebook.react.uimanager.events.RCTEventEmitter;

public class ReactTextChangedEvent extends Event<ReactTextChangedEvent> {
    public static final String EVENT_NAME = "topChange";
    private int mEventCount;
    private String mText;

    public ReactTextChangedEvent(int viewId, String text, int eventCount) {
        super(viewId);
        this.mText = text;
        this.mEventCount = eventCount;
    }

    public String getEventName() {
        return "topChange";
    }

    public void dispatch(RCTEventEmitter rctEventEmitter) {
        rctEventEmitter.receiveEvent(getViewTag(), getEventName(), serializeEventData());
    }

    private WritableMap serializeEventData() {
        WritableMap eventData = Arguments.createMap();
        eventData.putString("text", this.mText);
        eventData.putInt("eventCount", this.mEventCount);
        eventData.putInt("target", getViewTag());
        return eventData;
    }
}
