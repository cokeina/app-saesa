package com.facebook.react.views.checkbox;

import android.content.Context;
import android.widget.CheckBox;

class ReactCheckBox extends CheckBox {
    private boolean mAllowChange = true;

    public ReactCheckBox(Context context) {
        super(context);
    }

    public void setChecked(boolean checked) {
        if (this.mAllowChange) {
            this.mAllowChange = false;
            super.setChecked(checked);
        }
    }

    /* access modifiers changed from: 0000 */
    public void setOn(boolean on) {
        if (isChecked() != on) {
            super.setChecked(on);
        }
        this.mAllowChange = true;
    }
}
