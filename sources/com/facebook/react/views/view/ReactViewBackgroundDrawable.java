package com.facebook.react.views.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.DashPathEffect;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PathEffect;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import com.facebook.react.common.annotations.VisibleForTesting;
import com.facebook.react.modules.i18nmanager.I18nUtil;
import com.facebook.react.uimanager.FloatUtil;
import com.facebook.react.uimanager.Spacing;
import com.facebook.yoga.YogaConstants;
import java.util.Arrays;
import java.util.Locale;
import javax.annotation.Nullable;

public class ReactViewBackgroundDrawable extends Drawable {
    private static final int ALL_BITS_SET = -1;
    private static final int ALL_BITS_UNSET = 0;
    private static final int DEFAULT_BORDER_ALPHA = 255;
    private static final int DEFAULT_BORDER_COLOR = -16777216;
    private static final int DEFAULT_BORDER_RGB = 0;
    private int mAlpha = 255;
    @Nullable
    private Spacing mBorderAlpha;
    @Nullable
    private float[] mBorderCornerRadii;
    @Nullable
    private Spacing mBorderRGB;
    private float mBorderRadius = Float.NaN;
    @Nullable
    private BorderStyle mBorderStyle;
    @Nullable
    private Spacing mBorderWidth;
    private int mColor = 0;
    private final Context mContext;
    @Nullable
    private PointF mInnerBottomLeftCorner;
    @Nullable
    private PointF mInnerBottomRightCorner;
    @Nullable
    private Path mInnerClipPathForBorderRadius;
    @Nullable
    private RectF mInnerClipTempRectForBorderRadius;
    @Nullable
    private PointF mInnerTopLeftCorner;
    @Nullable
    private PointF mInnerTopRightCorner;
    private int mLayoutDirection;
    private boolean mNeedUpdatePathForBorderRadius = false;
    @Nullable
    private Path mOuterClipPathForBorderRadius;
    @Nullable
    private RectF mOuterClipTempRectForBorderRadius;
    private final Paint mPaint = new Paint(1);
    @Nullable
    private PathEffect mPathEffectForBorderStyle;
    @Nullable
    private Path mPathForBorder;
    @Nullable
    private Path mPathForBorderRadiusOutline;
    @Nullable
    private RectF mTempRectForBorderRadiusOutline;

    public enum BorderRadiusLocation {
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_RIGHT,
        BOTTOM_LEFT,
        TOP_START,
        TOP_END,
        BOTTOM_START,
        BOTTOM_END
    }

    private enum BorderStyle {
        SOLID,
        DASHED,
        DOTTED;

        @Nullable
        public PathEffect getPathEffect(float borderWidth) {
            switch (this) {
                case SOLID:
                    return null;
                case DASHED:
                    return new DashPathEffect(new float[]{borderWidth * 3.0f, borderWidth * 3.0f, borderWidth * 3.0f, 3.0f * borderWidth}, 0.0f);
                case DOTTED:
                    return new DashPathEffect(new float[]{borderWidth, borderWidth, borderWidth, borderWidth}, 0.0f);
                default:
                    return null;
            }
        }
    }

    public ReactViewBackgroundDrawable(Context context) {
        this.mContext = context;
    }

    public void draw(Canvas canvas) {
        updatePathEffect();
        if (!hasRoundedBorders()) {
            drawRectangularBackgroundWithBorders(canvas);
        } else {
            drawRoundedBackgroundWithBorders(canvas);
        }
    }

    public boolean hasRoundedBorders() {
        float[] fArr;
        if (!YogaConstants.isUndefined(this.mBorderRadius) && this.mBorderRadius > 0.0f) {
            return true;
        }
        if (this.mBorderCornerRadii != null) {
            for (float borderRadii : this.mBorderCornerRadii) {
                if (!YogaConstants.isUndefined(borderRadii) && borderRadii > 0.0f) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        this.mNeedUpdatePathForBorderRadius = true;
    }

    public void setAlpha(int alpha) {
        if (alpha != this.mAlpha) {
            this.mAlpha = alpha;
            invalidateSelf();
        }
    }

    public int getAlpha() {
        return this.mAlpha;
    }

    public void setColorFilter(ColorFilter cf) {
    }

    public int getOpacity() {
        return ColorUtil.getOpacityFromColor(ColorUtil.multiplyColorAlpha(this.mColor, this.mAlpha));
    }

    public void getOutline(Outline outline) {
        if (VERSION.SDK_INT < 21) {
            super.getOutline(outline);
        } else if ((YogaConstants.isUndefined(this.mBorderRadius) || this.mBorderRadius <= 0.0f) && this.mBorderCornerRadii == null) {
            outline.setRect(getBounds());
        } else {
            updatePath();
            outline.setConvexPath(this.mPathForBorderRadiusOutline);
        }
    }

    public void setBorderWidth(int position, float width) {
        if (this.mBorderWidth == null) {
            this.mBorderWidth = new Spacing();
        }
        if (!FloatUtil.floatsEqual(this.mBorderWidth.getRaw(position), width)) {
            this.mBorderWidth.set(position, width);
            switch (position) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 8:
                    this.mNeedUpdatePathForBorderRadius = true;
                    break;
            }
            invalidateSelf();
        }
    }

    public void setBorderColor(int position, float rgb, float alpha) {
        setBorderRGB(position, rgb);
        setBorderAlpha(position, alpha);
    }

    private void setBorderRGB(int position, float rgb) {
        if (this.mBorderRGB == null) {
            this.mBorderRGB = new Spacing(0.0f);
        }
        if (!FloatUtil.floatsEqual(this.mBorderRGB.getRaw(position), rgb)) {
            this.mBorderRGB.set(position, rgb);
            invalidateSelf();
        }
    }

    private void setBorderAlpha(int position, float alpha) {
        if (this.mBorderAlpha == null) {
            this.mBorderAlpha = new Spacing(255.0f);
        }
        if (!FloatUtil.floatsEqual(this.mBorderAlpha.getRaw(position), alpha)) {
            this.mBorderAlpha.set(position, alpha);
            invalidateSelf();
        }
    }

    public void setBorderStyle(@Nullable String style) {
        BorderStyle borderStyle;
        if (style == null) {
            borderStyle = null;
        } else {
            borderStyle = BorderStyle.valueOf(style.toUpperCase(Locale.US));
        }
        if (this.mBorderStyle != borderStyle) {
            this.mBorderStyle = borderStyle;
            this.mNeedUpdatePathForBorderRadius = true;
            invalidateSelf();
        }
    }

    public void setRadius(float radius) {
        if (!FloatUtil.floatsEqual(this.mBorderRadius, radius)) {
            this.mBorderRadius = radius;
            this.mNeedUpdatePathForBorderRadius = true;
            invalidateSelf();
        }
    }

    public void setRadius(float radius, int position) {
        if (this.mBorderCornerRadii == null) {
            this.mBorderCornerRadii = new float[8];
            Arrays.fill(this.mBorderCornerRadii, Float.NaN);
        }
        if (!FloatUtil.floatsEqual(this.mBorderCornerRadii[position], radius)) {
            this.mBorderCornerRadii[position] = radius;
            this.mNeedUpdatePathForBorderRadius = true;
            invalidateSelf();
        }
    }

    public float getFullBorderRadius() {
        if (YogaConstants.isUndefined(this.mBorderRadius)) {
            return 0.0f;
        }
        return this.mBorderRadius;
    }

    public float getBorderRadius(BorderRadiusLocation location) {
        return getBorderRadiusOrDefaultTo(Float.NaN, location);
    }

    public float getBorderRadiusOrDefaultTo(float defaultValue, BorderRadiusLocation location) {
        if (this.mBorderCornerRadii == null) {
            return defaultValue;
        }
        float radius = this.mBorderCornerRadii[location.ordinal()];
        if (!YogaConstants.isUndefined(radius)) {
            return radius;
        }
        return defaultValue;
    }

    public void setColor(int color) {
        this.mColor = color;
        invalidateSelf();
    }

    public int getResolvedLayoutDirection() {
        return this.mLayoutDirection;
    }

    public boolean setResolvedLayoutDirection(int layoutDirection) {
        if (this.mLayoutDirection == layoutDirection) {
            return false;
        }
        this.mLayoutDirection = layoutDirection;
        return onResolvedLayoutDirectionChanged(layoutDirection);
    }

    public boolean onResolvedLayoutDirectionChanged(int layoutDirection) {
        return false;
    }

    @VisibleForTesting
    public int getColor() {
        return this.mColor;
    }

    private void drawRoundedBackgroundWithBorders(Canvas canvas) {
        int directionAwareColorLeft;
        int directionAwareColorRight;
        boolean isDirectionAwareColorLeftDefined;
        boolean isDirectionAwareColorRightDefined;
        int directionAwareColorLeft2;
        int directionAwareColorRight2;
        updatePath();
        canvas.save();
        int useColor = ColorUtil.multiplyColorAlpha(this.mColor, this.mAlpha);
        if (Color.alpha(useColor) != 0) {
            this.mPaint.setColor(useColor);
            this.mPaint.setStyle(Style.FILL);
            canvas.drawPath(this.mInnerClipPathForBorderRadius, this.mPaint);
        }
        RectF borderWidth = getDirectionAwareBorderInsets();
        if (borderWidth.top > 0.0f || borderWidth.bottom > 0.0f || borderWidth.left > 0.0f || borderWidth.right > 0.0f) {
            this.mPaint.setStyle(Style.FILL);
            canvas.clipPath(this.mOuterClipPathForBorderRadius, Op.INTERSECT);
            canvas.clipPath(this.mInnerClipPathForBorderRadius, Op.DIFFERENCE);
            int colorLeft = getBorderColor(0);
            int colorTop = getBorderColor(1);
            int colorRight = getBorderColor(2);
            int colorBottom = getBorderColor(3);
            if (VERSION.SDK_INT >= 17) {
                boolean isRTL = getResolvedLayoutDirection() == 1;
                int colorStart = getBorderColor(4);
                int colorEnd = getBorderColor(5);
                if (I18nUtil.getInstance().doLeftAndRightSwapInRTL(this.mContext)) {
                    if (!isBorderColorDefined(4)) {
                        colorStart = colorLeft;
                    }
                    if (!isBorderColorDefined(5)) {
                        colorEnd = colorRight;
                    }
                    if (isRTL) {
                        directionAwareColorLeft2 = colorEnd;
                    } else {
                        directionAwareColorLeft2 = colorStart;
                    }
                    if (isRTL) {
                        directionAwareColorRight2 = colorStart;
                    } else {
                        directionAwareColorRight2 = colorEnd;
                    }
                    colorLeft = directionAwareColorLeft2;
                    colorRight = directionAwareColorRight2;
                } else {
                    if (isRTL) {
                        directionAwareColorLeft = colorEnd;
                    } else {
                        directionAwareColorLeft = colorStart;
                    }
                    if (isRTL) {
                        directionAwareColorRight = colorStart;
                    } else {
                        directionAwareColorRight = colorEnd;
                    }
                    boolean isColorStartDefined = isBorderColorDefined(4);
                    boolean isColorEndDefined = isBorderColorDefined(5);
                    if (isRTL) {
                        isDirectionAwareColorLeftDefined = isColorEndDefined;
                    } else {
                        isDirectionAwareColorLeftDefined = isColorStartDefined;
                    }
                    if (isRTL) {
                        isDirectionAwareColorRightDefined = isColorStartDefined;
                    } else {
                        isDirectionAwareColorRightDefined = isColorEndDefined;
                    }
                    if (isDirectionAwareColorLeftDefined) {
                        colorLeft = directionAwareColorLeft;
                    }
                    if (isDirectionAwareColorRightDefined) {
                        colorRight = directionAwareColorRight;
                    }
                }
            }
            float left = this.mOuterClipTempRectForBorderRadius.left;
            float right = this.mOuterClipTempRectForBorderRadius.right;
            float top = this.mOuterClipTempRectForBorderRadius.top;
            float bottom = this.mOuterClipTempRectForBorderRadius.bottom;
            if (borderWidth.left > 0.0f) {
                drawQuadrilateral(canvas, colorLeft, left, top, this.mInnerTopLeftCorner.x, this.mInnerTopLeftCorner.y, this.mInnerBottomLeftCorner.x, this.mInnerBottomLeftCorner.y, left, bottom);
            }
            if (borderWidth.top > 0.0f) {
                drawQuadrilateral(canvas, colorTop, left, top, this.mInnerTopLeftCorner.x, this.mInnerTopLeftCorner.y, this.mInnerTopRightCorner.x, this.mInnerTopRightCorner.y, right, top);
            }
            if (borderWidth.right > 0.0f) {
                drawQuadrilateral(canvas, colorRight, right, top, this.mInnerTopRightCorner.x, this.mInnerTopRightCorner.y, this.mInnerBottomRightCorner.x, this.mInnerBottomRightCorner.y, right, bottom);
            }
            if (borderWidth.bottom > 0.0f) {
                drawQuadrilateral(canvas, colorBottom, left, bottom, this.mInnerBottomLeftCorner.x, this.mInnerBottomLeftCorner.y, this.mInnerBottomRightCorner.x, this.mInnerBottomRightCorner.y, right, bottom);
            }
        }
        canvas.restore();
    }

    private void updatePath() {
        float directionAwareTopLeftRadius;
        float directionAwareTopRightRadius;
        float directionAwareBottomLeftRadius;
        float directionAwareBottomRightRadius;
        float directionAwareTopLeftRadius2;
        float directionAwareTopRightRadius2;
        float directionAwareBottomLeftRadius2;
        float directionAwareBottomRightRadius2;
        if (this.mNeedUpdatePathForBorderRadius) {
            this.mNeedUpdatePathForBorderRadius = false;
            if (this.mInnerClipPathForBorderRadius == null) {
                this.mInnerClipPathForBorderRadius = new Path();
            }
            if (this.mOuterClipPathForBorderRadius == null) {
                this.mOuterClipPathForBorderRadius = new Path();
            }
            if (this.mPathForBorderRadiusOutline == null) {
                this.mPathForBorderRadiusOutline = new Path();
            }
            if (this.mInnerClipTempRectForBorderRadius == null) {
                this.mInnerClipTempRectForBorderRadius = new RectF();
            }
            if (this.mOuterClipTempRectForBorderRadius == null) {
                this.mOuterClipTempRectForBorderRadius = new RectF();
            }
            if (this.mTempRectForBorderRadiusOutline == null) {
                this.mTempRectForBorderRadiusOutline = new RectF();
            }
            this.mInnerClipPathForBorderRadius.reset();
            this.mOuterClipPathForBorderRadius.reset();
            this.mPathForBorderRadiusOutline.reset();
            this.mInnerClipTempRectForBorderRadius.set(getBounds());
            this.mOuterClipTempRectForBorderRadius.set(getBounds());
            this.mTempRectForBorderRadiusOutline.set(getBounds());
            RectF borderWidth = getDirectionAwareBorderInsets();
            this.mInnerClipTempRectForBorderRadius.top += borderWidth.top;
            this.mInnerClipTempRectForBorderRadius.bottom -= borderWidth.bottom;
            this.mInnerClipTempRectForBorderRadius.left += borderWidth.left;
            this.mInnerClipTempRectForBorderRadius.right -= borderWidth.right;
            float borderRadius = getFullBorderRadius();
            float topLeftRadius = getBorderRadiusOrDefaultTo(borderRadius, BorderRadiusLocation.TOP_LEFT);
            float topRightRadius = getBorderRadiusOrDefaultTo(borderRadius, BorderRadiusLocation.TOP_RIGHT);
            float bottomLeftRadius = getBorderRadiusOrDefaultTo(borderRadius, BorderRadiusLocation.BOTTOM_LEFT);
            float bottomRightRadius = getBorderRadiusOrDefaultTo(borderRadius, BorderRadiusLocation.BOTTOM_RIGHT);
            if (VERSION.SDK_INT >= 17) {
                boolean isRTL = getResolvedLayoutDirection() == 1;
                float topStartRadius = getBorderRadius(BorderRadiusLocation.TOP_START);
                float topEndRadius = getBorderRadius(BorderRadiusLocation.TOP_END);
                float bottomStartRadius = getBorderRadius(BorderRadiusLocation.BOTTOM_START);
                float bottomEndRadius = getBorderRadius(BorderRadiusLocation.BOTTOM_END);
                if (I18nUtil.getInstance().doLeftAndRightSwapInRTL(this.mContext)) {
                    if (YogaConstants.isUndefined(topStartRadius)) {
                        topStartRadius = topLeftRadius;
                    }
                    if (YogaConstants.isUndefined(topEndRadius)) {
                        topEndRadius = topRightRadius;
                    }
                    if (YogaConstants.isUndefined(bottomStartRadius)) {
                        bottomStartRadius = bottomLeftRadius;
                    }
                    if (YogaConstants.isUndefined(bottomEndRadius)) {
                        bottomEndRadius = bottomRightRadius;
                    }
                    if (isRTL) {
                        directionAwareTopLeftRadius2 = topEndRadius;
                    } else {
                        directionAwareTopLeftRadius2 = topStartRadius;
                    }
                    if (isRTL) {
                        directionAwareTopRightRadius2 = topStartRadius;
                    } else {
                        directionAwareTopRightRadius2 = topEndRadius;
                    }
                    if (isRTL) {
                        directionAwareBottomLeftRadius2 = bottomEndRadius;
                    } else {
                        directionAwareBottomLeftRadius2 = bottomStartRadius;
                    }
                    if (isRTL) {
                        directionAwareBottomRightRadius2 = bottomStartRadius;
                    } else {
                        directionAwareBottomRightRadius2 = bottomEndRadius;
                    }
                    topLeftRadius = directionAwareTopLeftRadius2;
                    topRightRadius = directionAwareTopRightRadius2;
                    bottomLeftRadius = directionAwareBottomLeftRadius2;
                    bottomRightRadius = directionAwareBottomRightRadius2;
                } else {
                    if (isRTL) {
                        directionAwareTopLeftRadius = topEndRadius;
                    } else {
                        directionAwareTopLeftRadius = topStartRadius;
                    }
                    if (isRTL) {
                        directionAwareTopRightRadius = topStartRadius;
                    } else {
                        directionAwareTopRightRadius = topEndRadius;
                    }
                    if (isRTL) {
                        directionAwareBottomLeftRadius = bottomEndRadius;
                    } else {
                        directionAwareBottomLeftRadius = bottomStartRadius;
                    }
                    if (isRTL) {
                        directionAwareBottomRightRadius = bottomStartRadius;
                    } else {
                        directionAwareBottomRightRadius = bottomEndRadius;
                    }
                    if (!YogaConstants.isUndefined(directionAwareTopLeftRadius)) {
                        topLeftRadius = directionAwareTopLeftRadius;
                    }
                    if (!YogaConstants.isUndefined(directionAwareTopRightRadius)) {
                        topRightRadius = directionAwareTopRightRadius;
                    }
                    if (!YogaConstants.isUndefined(directionAwareBottomLeftRadius)) {
                        bottomLeftRadius = directionAwareBottomLeftRadius;
                    }
                    if (!YogaConstants.isUndefined(directionAwareBottomRightRadius)) {
                        bottomRightRadius = directionAwareBottomRightRadius;
                    }
                }
            }
            float innerTopLeftRadiusX = Math.max(topLeftRadius - borderWidth.left, 0.0f);
            float innerTopLeftRadiusY = Math.max(topLeftRadius - borderWidth.top, 0.0f);
            float innerTopRightRadiusX = Math.max(topRightRadius - borderWidth.right, 0.0f);
            float innerTopRightRadiusY = Math.max(topRightRadius - borderWidth.top, 0.0f);
            float innerBottomRightRadiusX = Math.max(bottomRightRadius - borderWidth.right, 0.0f);
            float innerBottomRightRadiusY = Math.max(bottomRightRadius - borderWidth.bottom, 0.0f);
            float innerBottomLeftRadiusX = Math.max(bottomLeftRadius - borderWidth.left, 0.0f);
            float innerBottomLeftRadiusY = Math.max(bottomLeftRadius - borderWidth.bottom, 0.0f);
            this.mInnerClipPathForBorderRadius.addRoundRect(this.mInnerClipTempRectForBorderRadius, new float[]{innerTopLeftRadiusX, innerTopLeftRadiusY, innerTopRightRadiusX, innerTopRightRadiusY, innerBottomRightRadiusX, innerBottomRightRadiusY, innerBottomLeftRadiusX, innerBottomLeftRadiusY}, Direction.CW);
            this.mOuterClipPathForBorderRadius.addRoundRect(this.mOuterClipTempRectForBorderRadius, new float[]{topLeftRadius, topLeftRadius, topRightRadius, topRightRadius, bottomRightRadius, bottomRightRadius, bottomLeftRadius, bottomLeftRadius}, Direction.CW);
            float extraRadiusForOutline = 0.0f;
            if (this.mBorderWidth != null) {
                extraRadiusForOutline = this.mBorderWidth.get(8) / 2.0f;
            }
            this.mPathForBorderRadiusOutline.addRoundRect(this.mTempRectForBorderRadiusOutline, new float[]{topLeftRadius + extraRadiusForOutline, topLeftRadius + extraRadiusForOutline, topRightRadius + extraRadiusForOutline, topRightRadius + extraRadiusForOutline, bottomRightRadius + extraRadiusForOutline, bottomRightRadius + extraRadiusForOutline, bottomLeftRadius + extraRadiusForOutline, bottomLeftRadius + extraRadiusForOutline}, Direction.CW);
            if (this.mInnerTopLeftCorner == null) {
                this.mInnerTopLeftCorner = new PointF();
            }
            this.mInnerTopLeftCorner.x = this.mInnerClipTempRectForBorderRadius.left;
            this.mInnerTopLeftCorner.y = this.mInnerClipTempRectForBorderRadius.top;
            getEllipseIntersectionWithLine((double) this.mInnerClipTempRectForBorderRadius.left, (double) this.mInnerClipTempRectForBorderRadius.top, (double) (this.mInnerClipTempRectForBorderRadius.left + (2.0f * innerTopLeftRadiusX)), (double) (this.mInnerClipTempRectForBorderRadius.top + (2.0f * innerTopLeftRadiusY)), (double) this.mOuterClipTempRectForBorderRadius.left, (double) this.mOuterClipTempRectForBorderRadius.top, (double) this.mInnerClipTempRectForBorderRadius.left, (double) this.mInnerClipTempRectForBorderRadius.top, this.mInnerTopLeftCorner);
            if (this.mInnerBottomLeftCorner == null) {
                this.mInnerBottomLeftCorner = new PointF();
            }
            this.mInnerBottomLeftCorner.x = this.mInnerClipTempRectForBorderRadius.left;
            this.mInnerBottomLeftCorner.y = this.mInnerClipTempRectForBorderRadius.bottom;
            getEllipseIntersectionWithLine((double) this.mInnerClipTempRectForBorderRadius.left, (double) (this.mInnerClipTempRectForBorderRadius.bottom - (2.0f * innerBottomLeftRadiusY)), (double) (this.mInnerClipTempRectForBorderRadius.left + (2.0f * innerBottomLeftRadiusX)), (double) this.mInnerClipTempRectForBorderRadius.bottom, (double) this.mOuterClipTempRectForBorderRadius.left, (double) this.mOuterClipTempRectForBorderRadius.bottom, (double) this.mInnerClipTempRectForBorderRadius.left, (double) this.mInnerClipTempRectForBorderRadius.bottom, this.mInnerBottomLeftCorner);
            if (this.mInnerTopRightCorner == null) {
                this.mInnerTopRightCorner = new PointF();
            }
            this.mInnerTopRightCorner.x = this.mInnerClipTempRectForBorderRadius.right;
            this.mInnerTopRightCorner.y = this.mInnerClipTempRectForBorderRadius.top;
            getEllipseIntersectionWithLine((double) (this.mInnerClipTempRectForBorderRadius.right - (2.0f * innerTopRightRadiusX)), (double) this.mInnerClipTempRectForBorderRadius.top, (double) this.mInnerClipTempRectForBorderRadius.right, (double) (this.mInnerClipTempRectForBorderRadius.top + (2.0f * innerTopRightRadiusY)), (double) this.mOuterClipTempRectForBorderRadius.right, (double) this.mOuterClipTempRectForBorderRadius.top, (double) this.mInnerClipTempRectForBorderRadius.right, (double) this.mInnerClipTempRectForBorderRadius.top, this.mInnerTopRightCorner);
            if (this.mInnerBottomRightCorner == null) {
                this.mInnerBottomRightCorner = new PointF();
            }
            this.mInnerBottomRightCorner.x = this.mInnerClipTempRectForBorderRadius.right;
            this.mInnerBottomRightCorner.y = this.mInnerClipTempRectForBorderRadius.bottom;
            getEllipseIntersectionWithLine((double) (this.mInnerClipTempRectForBorderRadius.right - (2.0f * innerBottomRightRadiusX)), (double) (this.mInnerClipTempRectForBorderRadius.bottom - (2.0f * innerBottomRightRadiusY)), (double) this.mInnerClipTempRectForBorderRadius.right, (double) this.mInnerClipTempRectForBorderRadius.bottom, (double) this.mOuterClipTempRectForBorderRadius.right, (double) this.mOuterClipTempRectForBorderRadius.bottom, (double) this.mInnerClipTempRectForBorderRadius.right, (double) this.mInnerClipTempRectForBorderRadius.bottom, this.mInnerBottomRightCorner);
        }
    }

    private static void getEllipseIntersectionWithLine(double ellipseBoundsLeft, double ellipseBoundsTop, double ellipseBoundsRight, double ellipseBoundsBottom, double lineStartX, double lineStartY, double lineEndX, double lineEndY, PointF result) {
        double ellipseCenterX = (ellipseBoundsLeft + ellipseBoundsRight) / 2.0d;
        double ellipseCenterY = (ellipseBoundsTop + ellipseBoundsBottom) / 2.0d;
        double lineStartX2 = lineStartX - ellipseCenterX;
        double lineStartY2 = lineStartY - ellipseCenterY;
        double lineEndY2 = lineEndY - ellipseCenterY;
        double a = Math.abs(ellipseBoundsRight - ellipseBoundsLeft) / 2.0d;
        double b = Math.abs(ellipseBoundsBottom - ellipseBoundsTop) / 2.0d;
        double m = (lineEndY2 - lineStartY2) / ((lineEndX - ellipseCenterX) - lineStartX2);
        double c = lineStartY2 - (m * lineStartX2);
        double A = (b * b) + (a * a * m * m);
        double B = 2.0d * a * a * c * m;
        double x2 = ((-B) / (2.0d * A)) - Math.sqrt(((-((a * a) * ((c * c) - (b * b)))) / A) + Math.pow(B / (2.0d * A), 2.0d));
        double x = x2 + ellipseCenterX;
        double y = (m * x2) + c + ellipseCenterY;
        if (!Double.isNaN(x) && !Double.isNaN(y)) {
            result.x = (float) x;
            result.y = (float) y;
        }
    }

    public float getBorderWidthOrDefaultTo(float defaultValue, int spacingType) {
        if (this.mBorderWidth == null) {
            return defaultValue;
        }
        float width = this.mBorderWidth.getRaw(spacingType);
        if (!YogaConstants.isUndefined(width)) {
            return width;
        }
        return defaultValue;
    }

    private void updatePathEffect() {
        this.mPathEffectForBorderStyle = this.mBorderStyle != null ? this.mBorderStyle.getPathEffect(getFullBorderWidth()) : null;
        this.mPaint.setPathEffect(this.mPathEffectForBorderStyle);
    }

    public float getFullBorderWidth() {
        if (this.mBorderWidth == null || YogaConstants.isUndefined(this.mBorderWidth.getRaw(8))) {
            return 0.0f;
        }
        return this.mBorderWidth.getRaw(8);
    }

    private static int fastBorderCompatibleColorOrZero(int borderLeft, int borderTop, int borderRight, int borderBottom, int colorLeft, int colorTop, int colorRight, int colorBottom) {
        int i;
        int i2 = -1;
        if (borderLeft > 0) {
            i = colorLeft;
        } else {
            i = -1;
        }
        int i3 = (borderRight > 0 ? colorRight : -1) & i & (borderTop > 0 ? colorTop : -1);
        if (borderBottom > 0) {
            i2 = colorBottom;
        }
        int andSmear = i3 & i2;
        if (borderLeft <= 0) {
            colorLeft = 0;
        }
        if (borderTop <= 0) {
            colorTop = 0;
        }
        int i4 = colorLeft | colorTop;
        if (borderRight <= 0) {
            colorRight = 0;
        }
        int i5 = i4 | colorRight;
        if (borderBottom <= 0) {
            colorBottom = 0;
        }
        if (andSmear == (i5 | colorBottom)) {
            return andSmear;
        }
        return 0;
    }

    private void drawRectangularBackgroundWithBorders(Canvas canvas) {
        int directionAwareColorLeft;
        int directionAwareColorRight;
        boolean isDirectionAwareColorLeftDefined;
        boolean isDirectionAwareColorRightDefined;
        int directionAwareColorLeft2;
        int directionAwareColorRight2;
        int useColor = ColorUtil.multiplyColorAlpha(this.mColor, this.mAlpha);
        if (Color.alpha(useColor) != 0) {
            this.mPaint.setColor(useColor);
            this.mPaint.setStyle(Style.FILL);
            canvas.drawRect(getBounds(), this.mPaint);
        }
        RectF borderWidth = getDirectionAwareBorderInsets();
        int borderLeft = Math.round(borderWidth.left);
        int borderTop = Math.round(borderWidth.top);
        int borderRight = Math.round(borderWidth.right);
        int borderBottom = Math.round(borderWidth.bottom);
        if (borderLeft > 0 || borderRight > 0 || borderTop > 0 || borderBottom > 0) {
            Rect bounds = getBounds();
            int colorLeft = getBorderColor(0);
            int colorTop = getBorderColor(1);
            int colorRight = getBorderColor(2);
            int colorBottom = getBorderColor(3);
            if (VERSION.SDK_INT >= 17) {
                boolean isRTL = getResolvedLayoutDirection() == 1;
                int colorStart = getBorderColor(4);
                int colorEnd = getBorderColor(5);
                if (I18nUtil.getInstance().doLeftAndRightSwapInRTL(this.mContext)) {
                    if (!isBorderColorDefined(4)) {
                        colorStart = colorLeft;
                    }
                    if (!isBorderColorDefined(5)) {
                        colorEnd = colorRight;
                    }
                    if (isRTL) {
                        directionAwareColorLeft2 = colorEnd;
                    } else {
                        directionAwareColorLeft2 = colorStart;
                    }
                    if (isRTL) {
                        directionAwareColorRight2 = colorStart;
                    } else {
                        directionAwareColorRight2 = colorEnd;
                    }
                    colorLeft = directionAwareColorLeft2;
                    colorRight = directionAwareColorRight2;
                } else {
                    if (isRTL) {
                        directionAwareColorLeft = colorEnd;
                    } else {
                        directionAwareColorLeft = colorStart;
                    }
                    if (isRTL) {
                        directionAwareColorRight = colorStart;
                    } else {
                        directionAwareColorRight = colorEnd;
                    }
                    boolean isColorStartDefined = isBorderColorDefined(4);
                    boolean isColorEndDefined = isBorderColorDefined(5);
                    if (isRTL) {
                        isDirectionAwareColorLeftDefined = isColorEndDefined;
                    } else {
                        isDirectionAwareColorLeftDefined = isColorStartDefined;
                    }
                    if (isRTL) {
                        isDirectionAwareColorRightDefined = isColorStartDefined;
                    } else {
                        isDirectionAwareColorRightDefined = isColorEndDefined;
                    }
                    if (isDirectionAwareColorLeftDefined) {
                        colorLeft = directionAwareColorLeft;
                    }
                    if (isDirectionAwareColorRightDefined) {
                        colorRight = directionAwareColorRight;
                    }
                }
            }
            int left = bounds.left;
            int top = bounds.top;
            int fastBorderColor = fastBorderCompatibleColorOrZero(borderLeft, borderTop, borderRight, borderBottom, colorLeft, colorTop, colorRight, colorBottom);
            if (fastBorderColor == 0) {
                this.mPaint.setAntiAlias(false);
                int width = bounds.width();
                int height = bounds.height();
                if (borderLeft > 0) {
                    drawQuadrilateral(canvas, colorLeft, (float) left, (float) top, (float) (left + borderLeft), (float) (top + borderTop), (float) (left + borderLeft), (float) ((top + height) - borderBottom), (float) left, (float) (top + height));
                }
                if (borderTop > 0) {
                    drawQuadrilateral(canvas, colorTop, (float) left, (float) top, (float) (left + borderLeft), (float) (top + borderTop), (float) ((left + width) - borderRight), (float) (top + borderTop), (float) (left + width), (float) top);
                }
                if (borderRight > 0) {
                    drawQuadrilateral(canvas, colorRight, (float) (left + width), (float) top, (float) (left + width), (float) (top + height), (float) ((left + width) - borderRight), (float) ((top + height) - borderBottom), (float) ((left + width) - borderRight), (float) (top + borderTop));
                }
                if (borderBottom > 0) {
                    drawQuadrilateral(canvas, colorBottom, (float) left, (float) (top + height), (float) (left + width), (float) (top + height), (float) ((left + width) - borderRight), (float) ((top + height) - borderBottom), (float) (left + borderLeft), (float) ((top + height) - borderBottom));
                }
                this.mPaint.setAntiAlias(true);
            } else if (Color.alpha(fastBorderColor) != 0) {
                int right = bounds.right;
                int bottom = bounds.bottom;
                this.mPaint.setColor(fastBorderColor);
                if (borderLeft > 0) {
                    canvas.drawRect((float) left, (float) top, (float) (left + borderLeft), (float) (bottom - borderBottom), this.mPaint);
                }
                if (borderTop > 0) {
                    canvas.drawRect((float) (left + borderLeft), (float) top, (float) right, (float) (top + borderTop), this.mPaint);
                }
                if (borderRight > 0) {
                    canvas.drawRect((float) (right - borderRight), (float) (top + borderTop), (float) right, (float) bottom, this.mPaint);
                }
                if (borderBottom > 0) {
                    canvas.drawRect((float) left, (float) (bottom - borderBottom), (float) (right - borderRight), (float) bottom, this.mPaint);
                }
            }
        }
    }

    private void drawQuadrilateral(Canvas canvas, int fillColor, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
        if (fillColor != 0) {
            if (this.mPathForBorder == null) {
                this.mPathForBorder = new Path();
            }
            this.mPaint.setColor(fillColor);
            this.mPathForBorder.reset();
            this.mPathForBorder.moveTo(x1, y1);
            this.mPathForBorder.lineTo(x2, y2);
            this.mPathForBorder.lineTo(x3, y3);
            this.mPathForBorder.lineTo(x4, y4);
            this.mPathForBorder.lineTo(x1, y1);
            canvas.drawPath(this.mPathForBorder, this.mPaint);
        }
    }

    private int getBorderWidth(int position) {
        if (this.mBorderWidth == null) {
            return 0;
        }
        float width = this.mBorderWidth.get(position);
        if (YogaConstants.isUndefined(width)) {
            return -1;
        }
        return Math.round(width);
    }

    private static int colorFromAlphaAndRGBComponents(float alpha, float rgb) {
        return (16777215 & ((int) rgb)) | (-16777216 & (((int) alpha) << 24));
    }

    private boolean isBorderColorDefined(int position) {
        float rgb;
        float alpha;
        if (this.mBorderRGB != null) {
            rgb = this.mBorderRGB.get(position);
        } else {
            rgb = Float.NaN;
        }
        if (this.mBorderAlpha != null) {
            alpha = this.mBorderAlpha.get(position);
        } else {
            alpha = Float.NaN;
        }
        return !YogaConstants.isUndefined(rgb) && !YogaConstants.isUndefined(alpha);
    }

    private int getBorderColor(int position) {
        return colorFromAlphaAndRGBComponents(this.mBorderAlpha != null ? this.mBorderAlpha.get(position) : 255.0f, this.mBorderRGB != null ? this.mBorderRGB.get(position) : 0.0f);
    }

    public RectF getDirectionAwareBorderInsets() {
        float directionAwareBorderLeftWidth;
        float directionAwareBorderRightWidth;
        float directionAwareBorderLeftWidth2;
        float directionAwareBorderRightWidth2;
        boolean isRTL = true;
        float borderWidth = getBorderWidthOrDefaultTo(0.0f, 8);
        float borderTopWidth = getBorderWidthOrDefaultTo(borderWidth, 1);
        float borderBottomWidth = getBorderWidthOrDefaultTo(borderWidth, 3);
        float borderLeftWidth = getBorderWidthOrDefaultTo(borderWidth, 0);
        float borderRightWidth = getBorderWidthOrDefaultTo(borderWidth, 2);
        if (VERSION.SDK_INT >= 17 && this.mBorderWidth != null) {
            if (getResolvedLayoutDirection() != 1) {
                isRTL = false;
            }
            float borderStartWidth = this.mBorderWidth.getRaw(4);
            float borderEndWidth = this.mBorderWidth.getRaw(5);
            if (I18nUtil.getInstance().doLeftAndRightSwapInRTL(this.mContext)) {
                if (YogaConstants.isUndefined(borderStartWidth)) {
                    borderStartWidth = borderLeftWidth;
                }
                if (YogaConstants.isUndefined(borderEndWidth)) {
                    borderEndWidth = borderRightWidth;
                }
                if (isRTL) {
                    directionAwareBorderLeftWidth2 = borderEndWidth;
                } else {
                    directionAwareBorderLeftWidth2 = borderStartWidth;
                }
                if (isRTL) {
                    directionAwareBorderRightWidth2 = borderStartWidth;
                } else {
                    directionAwareBorderRightWidth2 = borderEndWidth;
                }
                borderLeftWidth = directionAwareBorderLeftWidth2;
                borderRightWidth = directionAwareBorderRightWidth2;
            } else {
                if (isRTL) {
                    directionAwareBorderLeftWidth = borderEndWidth;
                } else {
                    directionAwareBorderLeftWidth = borderStartWidth;
                }
                if (isRTL) {
                    directionAwareBorderRightWidth = borderStartWidth;
                } else {
                    directionAwareBorderRightWidth = borderEndWidth;
                }
                if (!YogaConstants.isUndefined(directionAwareBorderLeftWidth)) {
                    borderLeftWidth = directionAwareBorderLeftWidth;
                }
                if (!YogaConstants.isUndefined(directionAwareBorderRightWidth)) {
                    borderRightWidth = directionAwareBorderRightWidth;
                }
            }
        }
        return new RectF(borderLeftWidth, borderTopWidth, borderRightWidth, borderBottomWidth);
    }
}
