package com.facebook.react.views.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import com.facebook.infer.annotation.Assertions;
import com.facebook.react.common.annotations.VisibleForTesting;
import com.facebook.react.modules.i18nmanager.I18nUtil;
import com.facebook.react.touch.OnInterceptTouchEventListener;
import com.facebook.react.touch.ReactHitSlopView;
import com.facebook.react.touch.ReactInterceptingViewGroup;
import com.facebook.react.uimanager.MeasureSpecAssertions;
import com.facebook.react.uimanager.PointerEvents;
import com.facebook.react.uimanager.ReactClippingViewGroup;
import com.facebook.react.uimanager.ReactClippingViewGroupHelper;
import com.facebook.react.uimanager.ReactPointerEventsView;
import com.facebook.react.uimanager.ReactZIndexedViewGroup;
import com.facebook.react.uimanager.RootView;
import com.facebook.react.uimanager.RootViewUtil;
import com.facebook.react.uimanager.ViewGroupDrawingOrderHelper;
import com.facebook.react.views.view.ReactViewBackgroundDrawable.BorderRadiusLocation;
import com.facebook.yoga.YogaConstants;
import javax.annotation.Nullable;

public class ReactViewGroup extends ViewGroup implements ReactInterceptingViewGroup, ReactClippingViewGroup, ReactPointerEventsView, ReactHitSlopView, ReactZIndexedViewGroup {
    private static final int ARRAY_CAPACITY_INCREMENT = 12;
    private static final int DEFAULT_BACKGROUND_COLOR = 0;
    private static final LayoutParams sDefaultLayoutParam = new LayoutParams(0, 0);
    private static final Rect sHelperRect = new Rect();
    @Nullable
    private View[] mAllChildren = null;
    private int mAllChildrenCount;
    @Nullable
    private ChildrenLayoutChangeListener mChildrenLayoutChangeListener;
    @Nullable
    private Rect mClippingRect;
    private final ViewGroupDrawingOrderHelper mDrawingOrderHelper = new ViewGroupDrawingOrderHelper(this);
    @Nullable
    private Rect mHitSlopRect;
    private int mLayoutDirection;
    private boolean mNeedsOffscreenAlphaCompositing = false;
    @Nullable
    private OnInterceptTouchEventListener mOnInterceptTouchEventListener;
    @Nullable
    private String mOverflow;
    @Nullable
    private Path mPath;
    private PointerEvents mPointerEvents = PointerEvents.AUTO;
    @Nullable
    private ReactViewBackgroundDrawable mReactBackgroundDrawable;
    private boolean mRemoveClippedSubviews = false;

    private static final class ChildrenLayoutChangeListener implements OnLayoutChangeListener {
        private final ReactViewGroup mParent;

        private ChildrenLayoutChangeListener(ReactViewGroup parent) {
            this.mParent = parent;
        }

        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
            if (this.mParent.getRemoveClippedSubviews()) {
                this.mParent.updateSubviewClipStatus(v);
            }
        }
    }

    public ReactViewGroup(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        MeasureSpecAssertions.assertExplicitMeasureSpec(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
    }

    public void onRtlPropertiesChanged(int layoutDirection) {
        if (VERSION.SDK_INT >= 17 && this.mReactBackgroundDrawable != null) {
            this.mReactBackgroundDrawable.setResolvedLayoutDirection(this.mLayoutDirection);
        }
    }

    public void requestLayout() {
    }

    public void setBackgroundColor(int color) {
        if (color != 0 || this.mReactBackgroundDrawable != null) {
            getOrCreateReactViewBackground().setColor(color);
        }
    }

    public void setBackground(Drawable drawable) {
        throw new UnsupportedOperationException("This method is not supported for ReactViewGroup instances");
    }

    public void setTranslucentBackgroundDrawable(@Nullable Drawable background) {
        updateBackgroundDrawable(null);
        if (this.mReactBackgroundDrawable != null && background != null) {
            updateBackgroundDrawable(new LayerDrawable(new Drawable[]{this.mReactBackgroundDrawable, background}));
        } else if (background != null) {
            updateBackgroundDrawable(background);
        }
    }

    public void setOnInterceptTouchEventListener(OnInterceptTouchEventListener listener) {
        this.mOnInterceptTouchEventListener = listener;
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if ((this.mOnInterceptTouchEventListener != null && this.mOnInterceptTouchEventListener.onInterceptTouchEvent(this, ev)) || this.mPointerEvents == PointerEvents.NONE || this.mPointerEvents == PointerEvents.BOX_ONLY) {
            return true;
        }
        return super.onInterceptTouchEvent(ev);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (this.mPointerEvents == PointerEvents.NONE || this.mPointerEvents == PointerEvents.BOX_NONE) {
            return false;
        }
        return true;
    }

    public boolean hasOverlappingRendering() {
        return this.mNeedsOffscreenAlphaCompositing;
    }

    public void setNeedsOffscreenAlphaCompositing(boolean needsOffscreenAlphaCompositing) {
        this.mNeedsOffscreenAlphaCompositing = needsOffscreenAlphaCompositing;
    }

    public void setBorderWidth(int position, float width) {
        getOrCreateReactViewBackground().setBorderWidth(position, width);
    }

    public void setBorderColor(int position, float rgb, float alpha) {
        getOrCreateReactViewBackground().setBorderColor(position, rgb, alpha);
    }

    public void setBorderRadius(float borderRadius) {
        ReactViewBackgroundDrawable backgroundDrawable = getOrCreateReactViewBackground();
        backgroundDrawable.setRadius(borderRadius);
        if (11 < VERSION.SDK_INT && VERSION.SDK_INT < 18) {
            int UPDATED_LAYER_TYPE = backgroundDrawable.hasRoundedBorders() ? 1 : 2;
            if (UPDATED_LAYER_TYPE != getLayerType()) {
                setLayerType(UPDATED_LAYER_TYPE, null);
            }
        }
    }

    public void setBorderRadius(float borderRadius, int position) {
        ReactViewBackgroundDrawable backgroundDrawable = getOrCreateReactViewBackground();
        backgroundDrawable.setRadius(borderRadius, position);
        if (11 < VERSION.SDK_INT && VERSION.SDK_INT < 18) {
            int UPDATED_LAYER_TYPE = backgroundDrawable.hasRoundedBorders() ? 1 : 2;
            if (UPDATED_LAYER_TYPE != getLayerType()) {
                setLayerType(UPDATED_LAYER_TYPE, null);
            }
        }
    }

    public void setBorderStyle(@Nullable String style) {
        getOrCreateReactViewBackground().setBorderStyle(style);
    }

    public void setRemoveClippedSubviews(boolean removeClippedSubviews) {
        if (removeClippedSubviews != this.mRemoveClippedSubviews) {
            this.mRemoveClippedSubviews = removeClippedSubviews;
            if (removeClippedSubviews) {
                this.mClippingRect = new Rect();
                ReactClippingViewGroupHelper.calculateClippingRect(this, this.mClippingRect);
                this.mAllChildrenCount = getChildCount();
                this.mAllChildren = new View[Math.max(12, this.mAllChildrenCount)];
                this.mChildrenLayoutChangeListener = new ChildrenLayoutChangeListener();
                for (int i = 0; i < this.mAllChildrenCount; i++) {
                    View child = getChildAt(i);
                    this.mAllChildren[i] = child;
                    child.addOnLayoutChangeListener(this.mChildrenLayoutChangeListener);
                }
                updateClippingRect();
                return;
            }
            Assertions.assertNotNull(this.mClippingRect);
            Assertions.assertNotNull(this.mAllChildren);
            Assertions.assertNotNull(this.mChildrenLayoutChangeListener);
            for (int i2 = 0; i2 < this.mAllChildrenCount; i2++) {
                this.mAllChildren[i2].removeOnLayoutChangeListener(this.mChildrenLayoutChangeListener);
            }
            getDrawingRect(this.mClippingRect);
            updateClippingToRect(this.mClippingRect);
            this.mAllChildren = null;
            this.mClippingRect = null;
            this.mAllChildrenCount = 0;
            this.mChildrenLayoutChangeListener = null;
        }
    }

    public boolean getRemoveClippedSubviews() {
        return this.mRemoveClippedSubviews;
    }

    public void getClippingRect(Rect outClippingRect) {
        outClippingRect.set(this.mClippingRect);
    }

    public void updateClippingRect() {
        if (this.mRemoveClippedSubviews) {
            Assertions.assertNotNull(this.mClippingRect);
            Assertions.assertNotNull(this.mAllChildren);
            ReactClippingViewGroupHelper.calculateClippingRect(this, this.mClippingRect);
            updateClippingToRect(this.mClippingRect);
        }
    }

    private void updateClippingToRect(Rect clippingRect) {
        Assertions.assertNotNull(this.mAllChildren);
        int clippedSoFar = 0;
        for (int i = 0; i < this.mAllChildrenCount; i++) {
            updateSubviewClipStatus(clippingRect, i, clippedSoFar);
            if (this.mAllChildren[i].getParent() == null) {
                clippedSoFar++;
            }
        }
    }

    private void updateSubviewClipStatus(Rect clippingRect, int idx, int clippedSoFar) {
        View child = ((View[]) Assertions.assertNotNull(this.mAllChildren))[idx];
        sHelperRect.set(child.getLeft(), child.getTop(), child.getRight(), child.getBottom());
        boolean intersects = clippingRect.intersects(sHelperRect.left, sHelperRect.top, sHelperRect.right, sHelperRect.bottom);
        boolean needUpdateClippingRecursive = false;
        Animation animation = child.getAnimation();
        boolean isAnimating = animation != null && !animation.hasEnded();
        if (!intersects && child.getParent() != null && !isAnimating) {
            super.removeViewsInLayout(idx - clippedSoFar, 1);
            needUpdateClippingRecursive = true;
        } else if (intersects && child.getParent() == null) {
            super.addViewInLayout(child, idx - clippedSoFar, sDefaultLayoutParam, true);
            invalidate();
            needUpdateClippingRecursive = true;
        } else if (intersects) {
            needUpdateClippingRecursive = true;
        }
        if (needUpdateClippingRecursive && (child instanceof ReactClippingViewGroup)) {
            ReactClippingViewGroup clippingChild = (ReactClippingViewGroup) child;
            if (clippingChild.getRemoveClippedSubviews()) {
                clippingChild.updateClippingRect();
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateSubviewClipStatus(View subview) {
        if (this.mRemoveClippedSubviews && getParent() != null) {
            Assertions.assertNotNull(this.mClippingRect);
            Assertions.assertNotNull(this.mAllChildren);
            sHelperRect.set(subview.getLeft(), subview.getTop(), subview.getRight(), subview.getBottom());
            if (this.mClippingRect.intersects(sHelperRect.left, sHelperRect.top, sHelperRect.right, sHelperRect.bottom) != (subview.getParent() != null)) {
                int clippedSoFar = 0;
                for (int i = 0; i < this.mAllChildrenCount; i++) {
                    if (this.mAllChildren[i] == subview) {
                        updateSubviewClipStatus(this.mClippingRect, i, clippedSoFar);
                        return;
                    }
                    if (this.mAllChildren[i].getParent() == null) {
                        clippedSoFar++;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (this.mRemoveClippedSubviews) {
            updateClippingRect();
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mRemoveClippedSubviews) {
            updateClippingRect();
        }
    }

    public void addView(View child, int index, LayoutParams params) {
        this.mDrawingOrderHelper.handleAddView(child);
        setChildrenDrawingOrderEnabled(this.mDrawingOrderHelper.shouldEnableCustomDrawingOrder());
        super.addView(child, index, params);
    }

    public void removeView(View view) {
        this.mDrawingOrderHelper.handleRemoveView(view);
        setChildrenDrawingOrderEnabled(this.mDrawingOrderHelper.shouldEnableCustomDrawingOrder());
        super.removeView(view);
    }

    public void removeViewAt(int index) {
        this.mDrawingOrderHelper.handleRemoveView(getChildAt(index));
        setChildrenDrawingOrderEnabled(this.mDrawingOrderHelper.shouldEnableCustomDrawingOrder());
        super.removeViewAt(index);
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int childCount, int index) {
        return this.mDrawingOrderHelper.getChildDrawingOrder(childCount, index);
    }

    public int getZIndexMappedChildIndex(int index) {
        if (this.mDrawingOrderHelper.shouldEnableCustomDrawingOrder()) {
            return this.mDrawingOrderHelper.getChildDrawingOrder(getChildCount(), index);
        }
        return index;
    }

    public void updateDrawingOrder() {
        this.mDrawingOrderHelper.update();
        setChildrenDrawingOrderEnabled(this.mDrawingOrderHelper.shouldEnableCustomDrawingOrder());
        invalidate();
    }

    public PointerEvents getPointerEvents() {
        return this.mPointerEvents;
    }

    /* access modifiers changed from: protected */
    public void dispatchSetPressed(boolean pressed) {
    }

    /* access modifiers changed from: 0000 */
    public void setPointerEvents(PointerEvents pointerEvents) {
        this.mPointerEvents = pointerEvents;
    }

    /* access modifiers changed from: 0000 */
    public int getAllChildrenCount() {
        return this.mAllChildrenCount;
    }

    /* access modifiers changed from: 0000 */
    public View getChildAtWithSubviewClippingEnabled(int index) {
        return ((View[]) Assertions.assertNotNull(this.mAllChildren))[index];
    }

    /* access modifiers changed from: 0000 */
    public void addViewWithSubviewClippingEnabled(View child, int index) {
        addViewWithSubviewClippingEnabled(child, index, sDefaultLayoutParam);
    }

    /* access modifiers changed from: 0000 */
    public void addViewWithSubviewClippingEnabled(View child, int index, LayoutParams params) {
        Assertions.assertCondition(this.mRemoveClippedSubviews);
        Assertions.assertNotNull(this.mClippingRect);
        Assertions.assertNotNull(this.mAllChildren);
        addInArray(child, index);
        int clippedSoFar = 0;
        for (int i = 0; i < index; i++) {
            if (this.mAllChildren[i].getParent() == null) {
                clippedSoFar++;
            }
        }
        updateSubviewClipStatus(this.mClippingRect, index, clippedSoFar);
        child.addOnLayoutChangeListener(this.mChildrenLayoutChangeListener);
    }

    /* access modifiers changed from: 0000 */
    public void removeViewWithSubviewClippingEnabled(View view) {
        Assertions.assertCondition(this.mRemoveClippedSubviews);
        Assertions.assertNotNull(this.mClippingRect);
        Assertions.assertNotNull(this.mAllChildren);
        view.removeOnLayoutChangeListener(this.mChildrenLayoutChangeListener);
        int index = indexOfChildInAllChildren(view);
        if (this.mAllChildren[index].getParent() != null) {
            int clippedSoFar = 0;
            for (int i = 0; i < index; i++) {
                if (this.mAllChildren[i].getParent() == null) {
                    clippedSoFar++;
                }
            }
            super.removeViewsInLayout(index - clippedSoFar, 1);
        }
        removeFromArray(index);
    }

    /* access modifiers changed from: 0000 */
    public void removeAllViewsWithSubviewClippingEnabled() {
        Assertions.assertCondition(this.mRemoveClippedSubviews);
        Assertions.assertNotNull(this.mAllChildren);
        for (int i = 0; i < this.mAllChildrenCount; i++) {
            this.mAllChildren[i].removeOnLayoutChangeListener(this.mChildrenLayoutChangeListener);
        }
        removeAllViewsInLayout();
        this.mAllChildrenCount = 0;
    }

    private int indexOfChildInAllChildren(View child) {
        int count = this.mAllChildrenCount;
        View[] children = (View[]) Assertions.assertNotNull(this.mAllChildren);
        for (int i = 0; i < count; i++) {
            if (children[i] == child) {
                return i;
            }
        }
        return -1;
    }

    private void addInArray(View child, int index) {
        View[] children = (View[]) Assertions.assertNotNull(this.mAllChildren);
        int count = this.mAllChildrenCount;
        int size = children.length;
        if (index == count) {
            if (size == count) {
                this.mAllChildren = new View[(size + 12)];
                System.arraycopy(children, 0, this.mAllChildren, 0, size);
                children = this.mAllChildren;
            }
            int i = this.mAllChildrenCount;
            this.mAllChildrenCount = i + 1;
            children[i] = child;
        } else if (index < count) {
            if (size == count) {
                this.mAllChildren = new View[(size + 12)];
                System.arraycopy(children, 0, this.mAllChildren, 0, index);
                System.arraycopy(children, index, this.mAllChildren, index + 1, count - index);
                children = this.mAllChildren;
            } else {
                System.arraycopy(children, index, children, index + 1, count - index);
            }
            children[index] = child;
            this.mAllChildrenCount++;
        } else {
            throw new IndexOutOfBoundsException("index=" + index + " count=" + count);
        }
    }

    private void removeFromArray(int index) {
        View[] children = (View[]) Assertions.assertNotNull(this.mAllChildren);
        int count = this.mAllChildrenCount;
        if (index == count - 1) {
            int i = this.mAllChildrenCount - 1;
            this.mAllChildrenCount = i;
            children[i] = null;
        } else if (index < 0 || index >= count) {
            throw new IndexOutOfBoundsException();
        } else {
            System.arraycopy(children, index + 1, children, index, (count - index) - 1);
            int i2 = this.mAllChildrenCount - 1;
            this.mAllChildrenCount = i2;
            children[i2] = null;
        }
    }

    @VisibleForTesting
    public int getBackgroundColor() {
        if (getBackground() != null) {
            return ((ReactViewBackgroundDrawable) getBackground()).getColor();
        }
        return 0;
    }

    private ReactViewBackgroundDrawable getOrCreateReactViewBackground() {
        int i = 1;
        if (this.mReactBackgroundDrawable == null) {
            this.mReactBackgroundDrawable = new ReactViewBackgroundDrawable(getContext());
            Drawable backgroundDrawable = getBackground();
            updateBackgroundDrawable(null);
            if (backgroundDrawable == null) {
                updateBackgroundDrawable(this.mReactBackgroundDrawable);
            } else {
                updateBackgroundDrawable(new LayerDrawable(new Drawable[]{this.mReactBackgroundDrawable, backgroundDrawable}));
            }
            if (VERSION.SDK_INT >= 17) {
                if (!I18nUtil.getInstance().isRTL(getContext())) {
                    i = 0;
                }
                this.mLayoutDirection = i;
                this.mReactBackgroundDrawable.setResolvedLayoutDirection(this.mLayoutDirection);
            }
        }
        return this.mReactBackgroundDrawable;
    }

    @Nullable
    public Rect getHitSlopRect() {
        return this.mHitSlopRect;
    }

    public void setHitSlopRect(@Nullable Rect rect) {
        this.mHitSlopRect = rect;
    }

    public void setOverflow(String overflow) {
        this.mOverflow = overflow;
        invalidate();
    }

    private void updateBackgroundDrawable(Drawable drawable) {
        if (VERSION.SDK_INT >= 16) {
            super.setBackground(drawable);
        } else {
            super.setBackgroundDrawable(drawable);
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        try {
            dispatchOverflowDraw(canvas);
            super.dispatchDraw(canvas);
        } catch (StackOverflowError e) {
            RootView rootView = RootViewUtil.getRootView(this);
            if (rootView != null) {
                rootView.handleException(e);
                return;
            }
            throw e;
        }
    }

    private void dispatchOverflowDraw(Canvas canvas) {
        float directionAwareTopLeftRadius;
        float directionAwareTopRightRadius;
        float directionAwareBottomLeftRadius;
        float directionAwareBottomRightRadius;
        float directionAwareTopLeftRadius2;
        float directionAwareTopRightRadius2;
        float directionAwareBottomLeftRadius2;
        float directionAwareBottomRightRadius2;
        if (this.mOverflow != null) {
            String str = this.mOverflow;
            char c = 65535;
            switch (str.hashCode()) {
                case -1217487446:
                    if (str.equals("hidden")) {
                        c = 1;
                        break;
                    }
                    break;
                case 466743410:
                    if (str.equals("visible")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    if (this.mPath != null) {
                        this.mPath.rewind();
                        return;
                    }
                    return;
                case 1:
                    if (this.mReactBackgroundDrawable != null) {
                        float left = 0.0f;
                        float top = 0.0f;
                        float right = (float) getWidth();
                        float bottom = (float) getHeight();
                        RectF borderWidth = this.mReactBackgroundDrawable.getDirectionAwareBorderInsets();
                        if (borderWidth.top > 0.0f || borderWidth.left > 0.0f || borderWidth.bottom > 0.0f || borderWidth.right > 0.0f) {
                            left = 0.0f + borderWidth.left;
                            top = 0.0f + borderWidth.top;
                            right -= borderWidth.right;
                            bottom -= borderWidth.bottom;
                        }
                        float borderRadius = this.mReactBackgroundDrawable.getFullBorderRadius();
                        float topLeftBorderRadius = this.mReactBackgroundDrawable.getBorderRadiusOrDefaultTo(borderRadius, BorderRadiusLocation.TOP_LEFT);
                        float topRightBorderRadius = this.mReactBackgroundDrawable.getBorderRadiusOrDefaultTo(borderRadius, BorderRadiusLocation.TOP_RIGHT);
                        float bottomLeftBorderRadius = this.mReactBackgroundDrawable.getBorderRadiusOrDefaultTo(borderRadius, BorderRadiusLocation.BOTTOM_LEFT);
                        float bottomRightBorderRadius = this.mReactBackgroundDrawable.getBorderRadiusOrDefaultTo(borderRadius, BorderRadiusLocation.BOTTOM_RIGHT);
                        if (VERSION.SDK_INT >= 17) {
                            boolean isRTL = this.mLayoutDirection == 1;
                            float topStartBorderRadius = this.mReactBackgroundDrawable.getBorderRadius(BorderRadiusLocation.TOP_START);
                            float topEndBorderRadius = this.mReactBackgroundDrawable.getBorderRadius(BorderRadiusLocation.TOP_END);
                            float bottomStartBorderRadius = this.mReactBackgroundDrawable.getBorderRadius(BorderRadiusLocation.BOTTOM_START);
                            float bottomEndBorderRadius = this.mReactBackgroundDrawable.getBorderRadius(BorderRadiusLocation.BOTTOM_END);
                            if (I18nUtil.getInstance().doLeftAndRightSwapInRTL(getContext())) {
                                if (YogaConstants.isUndefined(topStartBorderRadius)) {
                                    topStartBorderRadius = topLeftBorderRadius;
                                }
                                if (YogaConstants.isUndefined(topEndBorderRadius)) {
                                    topEndBorderRadius = topRightBorderRadius;
                                }
                                if (YogaConstants.isUndefined(bottomStartBorderRadius)) {
                                    bottomStartBorderRadius = bottomLeftBorderRadius;
                                }
                                if (YogaConstants.isUndefined(bottomEndBorderRadius)) {
                                    bottomEndBorderRadius = bottomRightBorderRadius;
                                }
                                if (isRTL) {
                                    directionAwareTopLeftRadius2 = topEndBorderRadius;
                                } else {
                                    directionAwareTopLeftRadius2 = topStartBorderRadius;
                                }
                                if (isRTL) {
                                    directionAwareTopRightRadius2 = topStartBorderRadius;
                                } else {
                                    directionAwareTopRightRadius2 = topEndBorderRadius;
                                }
                                if (isRTL) {
                                    directionAwareBottomLeftRadius2 = bottomEndBorderRadius;
                                } else {
                                    directionAwareBottomLeftRadius2 = bottomStartBorderRadius;
                                }
                                if (isRTL) {
                                    directionAwareBottomRightRadius2 = bottomStartBorderRadius;
                                } else {
                                    directionAwareBottomRightRadius2 = bottomEndBorderRadius;
                                }
                                topLeftBorderRadius = directionAwareTopLeftRadius2;
                                topRightBorderRadius = directionAwareTopRightRadius2;
                                bottomLeftBorderRadius = directionAwareBottomLeftRadius2;
                                bottomRightBorderRadius = directionAwareBottomRightRadius2;
                            } else {
                                if (isRTL) {
                                    directionAwareTopLeftRadius = topEndBorderRadius;
                                } else {
                                    directionAwareTopLeftRadius = topStartBorderRadius;
                                }
                                if (isRTL) {
                                    directionAwareTopRightRadius = topStartBorderRadius;
                                } else {
                                    directionAwareTopRightRadius = topEndBorderRadius;
                                }
                                if (isRTL) {
                                    directionAwareBottomLeftRadius = bottomEndBorderRadius;
                                } else {
                                    directionAwareBottomLeftRadius = bottomStartBorderRadius;
                                }
                                if (isRTL) {
                                    directionAwareBottomRightRadius = bottomStartBorderRadius;
                                } else {
                                    directionAwareBottomRightRadius = bottomEndBorderRadius;
                                }
                                if (!YogaConstants.isUndefined(directionAwareTopLeftRadius)) {
                                    topLeftBorderRadius = directionAwareTopLeftRadius;
                                }
                                if (!YogaConstants.isUndefined(directionAwareTopRightRadius)) {
                                    topRightBorderRadius = directionAwareTopRightRadius;
                                }
                                if (!YogaConstants.isUndefined(directionAwareBottomLeftRadius)) {
                                    bottomLeftBorderRadius = directionAwareBottomLeftRadius;
                                }
                                if (!YogaConstants.isUndefined(directionAwareBottomRightRadius)) {
                                    bottomRightBorderRadius = directionAwareBottomRightRadius;
                                }
                            }
                        }
                        if (topLeftBorderRadius > 0.0f || topRightBorderRadius > 0.0f || bottomRightBorderRadius > 0.0f || bottomLeftBorderRadius > 0.0f) {
                            if (this.mPath == null) {
                                this.mPath = new Path();
                            }
                            this.mPath.rewind();
                            Path path = this.mPath;
                            RectF rectF = new RectF(left, top, right, bottom);
                            path.addRoundRect(rectF, new float[]{Math.max(topLeftBorderRadius - borderWidth.left, 0.0f), Math.max(topLeftBorderRadius - borderWidth.top, 0.0f), Math.max(topRightBorderRadius - borderWidth.right, 0.0f), Math.max(topRightBorderRadius - borderWidth.top, 0.0f), Math.max(bottomRightBorderRadius - borderWidth.right, 0.0f), Math.max(bottomRightBorderRadius - borderWidth.bottom, 0.0f), Math.max(bottomLeftBorderRadius - borderWidth.left, 0.0f), Math.max(bottomLeftBorderRadius - borderWidth.bottom, 0.0f)}, Direction.CW);
                            canvas.clipPath(this.mPath);
                            return;
                        }
                        RectF rectF2 = new RectF(left, top, right, bottom);
                        canvas.clipRect(rectF2);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
