package com.facebook.react.views.scroll;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import com.facebook.react.modules.i18nmanager.I18nUtil;

public class ReactHorizontalScrollContainerView extends ViewGroup {
    private int mLayoutDirection;

    public ReactHorizontalScrollContainerView(Context context) {
        super(context);
        this.mLayoutDirection = I18nUtil.getInstance().isRTL(context) ? 1 : 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (this.mLayoutDirection == 1) {
            int newRight = 0 + (right - left);
            setLeft(0);
            setRight(newRight);
            HorizontalScrollView parent = (HorizontalScrollView) getParent();
            parent.scrollTo(computeHorizontalScrollRange() - getScrollX(), parent.getScrollY());
        }
    }
}
