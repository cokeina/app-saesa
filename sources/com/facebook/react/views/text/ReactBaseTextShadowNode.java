package com.facebook.react.views.text;

import android.os.Build.VERSION;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.UnderlineSpan;
import com.facebook.react.bridge.JSApplicationIllegalArgumentException;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.facebook.react.uimanager.LayoutShadowNode;
import com.facebook.react.uimanager.PixelUtil;
import com.facebook.react.uimanager.ReactShadowNodeImpl;
import com.facebook.react.uimanager.ViewProps;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.yoga.YogaDirection;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;

public abstract class ReactBaseTextShadowNode extends LayoutShadowNode {
    public static final int DEFAULT_TEXT_SHADOW_COLOR = 1426063360;
    private static final String INLINE_IMAGE_PLACEHOLDER = "I";
    public static final String PROP_SHADOW_COLOR = "textShadowColor";
    public static final String PROP_SHADOW_OFFSET = "textShadowOffset";
    public static final String PROP_SHADOW_OFFSET_HEIGHT = "height";
    public static final String PROP_SHADOW_OFFSET_WIDTH = "width";
    public static final String PROP_SHADOW_RADIUS = "textShadowRadius";
    public static final int UNSET = -1;
    protected boolean mAllowFontScaling = true;
    protected int mBackgroundColor;
    protected int mColor;
    protected boolean mContainsImages;
    @Nullable
    protected String mFontFamily;
    protected int mFontSize = -1;
    protected float mFontSizeInput = -1.0f;
    protected int mFontStyle;
    protected int mFontWeight;
    protected float mHeightOfTallestInlineImage;
    protected boolean mIncludeFontPadding;
    protected boolean mIsBackgroundColorSet = false;
    protected boolean mIsColorSet = false;
    protected boolean mIsLineThroughTextDecorationSet;
    protected boolean mIsUnderlineTextDecorationSet;
    protected float mLineHeight = Float.NaN;
    protected float mLineHeightInput = -1.0f;
    protected int mNumberOfLines = -1;
    protected int mTextAlign = 0;
    protected int mTextBreakStrategy;
    protected int mTextShadowColor;
    protected float mTextShadowOffsetDx;
    protected float mTextShadowOffsetDy;
    protected float mTextShadowRadius;

    private static class SetSpanOperation {
        protected int end;
        protected int start;
        protected Object what;

        SetSpanOperation(int start2, int end2, Object what2) {
            this.start = start2;
            this.end = end2;
            this.what = what2;
        }

        public void execute(SpannableStringBuilder sb, int priority) {
            int spanFlags = 34;
            if (this.start == 0) {
                spanFlags = 18;
            }
            sb.setSpan(this.what, this.start, this.end, (spanFlags & -16711681) | ((priority << 16) & 16711680));
        }
    }

    public ReactBaseTextShadowNode() {
        this.mTextBreakStrategy = VERSION.SDK_INT < 23 ? 0 : 1;
        this.mTextShadowOffsetDx = 0.0f;
        this.mTextShadowOffsetDy = 0.0f;
        this.mTextShadowRadius = 1.0f;
        this.mTextShadowColor = DEFAULT_TEXT_SHADOW_COLOR;
        this.mIsUnderlineTextDecorationSet = false;
        this.mIsLineThroughTextDecorationSet = false;
        this.mIncludeFontPadding = true;
        this.mFontStyle = -1;
        this.mFontWeight = -1;
        this.mFontFamily = null;
        this.mContainsImages = false;
        this.mHeightOfTallestInlineImage = Float.NaN;
    }

    private static void buildSpannedFromShadowNode(ReactBaseTextShadowNode textShadowNode, SpannableStringBuilder sb, List<SetSpanOperation> ops) {
        int start = sb.length();
        int length = textShadowNode.getChildCount();
        for (int i = 0; i < length; i++) {
            ReactShadowNodeImpl childAt = textShadowNode.getChildAt(i);
            if (childAt instanceof ReactRawTextShadowNode) {
                sb.append(((ReactRawTextShadowNode) childAt).getText());
            } else if (childAt instanceof ReactBaseTextShadowNode) {
                buildSpannedFromShadowNode((ReactBaseTextShadowNode) childAt, sb, ops);
            } else if (childAt instanceof ReactTextInlineImageShadowNode) {
                sb.append(INLINE_IMAGE_PLACEHOLDER);
                ops.add(new SetSpanOperation(sb.length() - INLINE_IMAGE_PLACEHOLDER.length(), sb.length(), ((ReactTextInlineImageShadowNode) childAt).buildInlineImageSpan()));
            } else {
                throw new IllegalViewOperationException("Unexpected view type nested under text node: " + childAt.getClass());
            }
            childAt.markUpdateSeen();
        }
        int end = sb.length();
        if (end >= start) {
            if (textShadowNode.mIsColorSet) {
                ops.add(new SetSpanOperation(start, end, new ForegroundColorSpan(textShadowNode.mColor)));
            }
            if (textShadowNode.mIsBackgroundColorSet) {
                ops.add(new SetSpanOperation(start, end, new BackgroundColorSpan(textShadowNode.mBackgroundColor)));
            }
            if (textShadowNode.mFontSize != -1) {
                ops.add(new SetSpanOperation(start, end, new AbsoluteSizeSpan(textShadowNode.mFontSize)));
            }
            if (!(textShadowNode.mFontStyle == -1 && textShadowNode.mFontWeight == -1 && textShadowNode.mFontFamily == null)) {
                ops.add(new SetSpanOperation(start, end, new CustomStyleSpan(textShadowNode.mFontStyle, textShadowNode.mFontWeight, textShadowNode.mFontFamily, textShadowNode.getThemedContext().getAssets())));
            }
            if (textShadowNode.mIsUnderlineTextDecorationSet) {
                ops.add(new SetSpanOperation(start, end, new UnderlineSpan()));
            }
            if (textShadowNode.mIsLineThroughTextDecorationSet) {
                ops.add(new SetSpanOperation(start, end, new StrikethroughSpan()));
            }
            if (!(textShadowNode.mTextShadowOffsetDx == 0.0f && textShadowNode.mTextShadowOffsetDy == 0.0f)) {
                ops.add(new SetSpanOperation(start, end, new ShadowStyleSpan(textShadowNode.mTextShadowOffsetDx, textShadowNode.mTextShadowOffsetDy, textShadowNode.mTextShadowRadius, textShadowNode.mTextShadowColor)));
            }
            if (!Float.isNaN(textShadowNode.getEffectiveLineHeight())) {
                ops.add(new SetSpanOperation(start, end, new CustomLineHeightSpan(textShadowNode.getEffectiveLineHeight())));
            }
            ops.add(new SetSpanOperation(start, end, new ReactTagSpan(textShadowNode.getReactTag())));
        }
    }

    protected static Spannable spannedFromShadowNode(ReactBaseTextShadowNode textShadowNode, String text) {
        int defaultFontSize;
        SpannableStringBuilder sb = new SpannableStringBuilder();
        List<SetSpanOperation> ops = new ArrayList<>();
        buildSpannedFromShadowNode(textShadowNode, sb, ops);
        if (text != null) {
            sb.append(text);
        }
        if (textShadowNode.mFontSize == -1) {
            if (textShadowNode.mAllowFontScaling) {
                defaultFontSize = (int) Math.ceil((double) PixelUtil.toPixelFromSP(14.0f));
            } else {
                defaultFontSize = (int) Math.ceil((double) PixelUtil.toPixelFromDIP(14.0f));
            }
            ops.add(new SetSpanOperation(0, sb.length(), new AbsoluteSizeSpan(defaultFontSize)));
        }
        textShadowNode.mContainsImages = false;
        textShadowNode.mHeightOfTallestInlineImage = Float.NaN;
        int priority = 0;
        for (SetSpanOperation op : ops) {
            if (op.what instanceof TextInlineImageSpan) {
                int height = ((TextInlineImageSpan) op.what).getHeight();
                textShadowNode.mContainsImages = true;
                if (Float.isNaN(textShadowNode.mHeightOfTallestInlineImage) || ((float) height) > textShadowNode.mHeightOfTallestInlineImage) {
                    textShadowNode.mHeightOfTallestInlineImage = (float) height;
                }
            }
            op.execute(sb, priority);
            priority++;
        }
        return sb;
    }

    private static int parseNumericFontWeight(String fontWeightString) {
        if (fontWeightString.length() != 3 || !fontWeightString.endsWith("00") || fontWeightString.charAt(0) > '9' || fontWeightString.charAt(0) < '1') {
            return -1;
        }
        return (fontWeightString.charAt(0) - '0') * 100;
    }

    public float getEffectiveLineHeight() {
        return !Float.isNaN(this.mLineHeight) && !Float.isNaN(this.mHeightOfTallestInlineImage) && (this.mHeightOfTallestInlineImage > this.mLineHeight ? 1 : (this.mHeightOfTallestInlineImage == this.mLineHeight ? 0 : -1)) > 0 ? this.mHeightOfTallestInlineImage : this.mLineHeight;
    }

    private int getTextAlign() {
        int textAlign = this.mTextAlign;
        if (getLayoutDirection() != YogaDirection.RTL) {
            return textAlign;
        }
        if (textAlign == 5) {
            return 3;
        }
        if (textAlign == 3) {
            return 5;
        }
        return textAlign;
    }

    @ReactProp(defaultInt = -1, name = "numberOfLines")
    public void setNumberOfLines(int numberOfLines) {
        if (numberOfLines == 0) {
            numberOfLines = -1;
        }
        this.mNumberOfLines = numberOfLines;
        markUpdated();
    }

    @ReactProp(defaultFloat = -1.0f, name = "lineHeight")
    public void setLineHeight(float lineHeight) {
        float pixelFromDIP;
        this.mLineHeightInput = lineHeight;
        if (lineHeight == -1.0f) {
            this.mLineHeight = Float.NaN;
        } else {
            if (this.mAllowFontScaling) {
                pixelFromDIP = PixelUtil.toPixelFromSP(lineHeight);
            } else {
                pixelFromDIP = PixelUtil.toPixelFromDIP(lineHeight);
            }
            this.mLineHeight = pixelFromDIP;
        }
        markUpdated();
    }

    @ReactProp(defaultBoolean = true, name = "allowFontScaling")
    public void setAllowFontScaling(boolean allowFontScaling) {
        if (allowFontScaling != this.mAllowFontScaling) {
            this.mAllowFontScaling = allowFontScaling;
            setFontSize(this.mFontSizeInput);
            setLineHeight(this.mLineHeightInput);
            markUpdated();
        }
    }

    @ReactProp(name = "textAlign")
    public void setTextAlign(@Nullable String textAlign) {
        if (textAlign == null || "auto".equals(textAlign)) {
            this.mTextAlign = 0;
        } else if (ViewProps.LEFT.equals(textAlign)) {
            this.mTextAlign = 3;
        } else if (ViewProps.RIGHT.equals(textAlign)) {
            this.mTextAlign = 5;
        } else if ("center".equals(textAlign)) {
            this.mTextAlign = 1;
        } else if ("justify".equals(textAlign)) {
            this.mTextAlign = 3;
        } else {
            throw new JSApplicationIllegalArgumentException("Invalid textAlign: " + textAlign);
        }
        markUpdated();
    }

    @ReactProp(defaultFloat = -1.0f, name = "fontSize")
    public void setFontSize(float fontSize) {
        this.mFontSizeInput = fontSize;
        if (fontSize != -1.0f) {
            if (this.mAllowFontScaling) {
                fontSize = (float) Math.ceil((double) PixelUtil.toPixelFromSP(fontSize));
            } else {
                fontSize = (float) Math.ceil((double) PixelUtil.toPixelFromDIP(fontSize));
            }
        }
        this.mFontSize = (int) fontSize;
        markUpdated();
    }

    @ReactProp(name = "color")
    public void setColor(@Nullable Integer color) {
        this.mIsColorSet = color != null;
        if (this.mIsColorSet) {
            this.mColor = color.intValue();
        }
        markUpdated();
    }

    @ReactProp(name = "backgroundColor")
    public void setBackgroundColor(Integer color) {
        if (!isVirtualAnchor()) {
            this.mIsBackgroundColorSet = color != null;
            if (this.mIsBackgroundColorSet) {
                this.mBackgroundColor = color.intValue();
            }
            markUpdated();
        }
    }

    @ReactProp(name = "fontFamily")
    public void setFontFamily(@Nullable String fontFamily) {
        this.mFontFamily = fontFamily;
        markUpdated();
    }

    @ReactProp(name = "fontWeight")
    public void setFontWeight(@Nullable String fontWeightString) {
        int fontWeightNumeric;
        if (fontWeightString != null) {
            fontWeightNumeric = parseNumericFontWeight(fontWeightString);
        } else {
            fontWeightNumeric = -1;
        }
        int fontWeight = -1;
        if (fontWeightNumeric >= 500 || "bold".equals(fontWeightString)) {
            fontWeight = 1;
        } else if ("normal".equals(fontWeightString) || (fontWeightNumeric != -1 && fontWeightNumeric < 500)) {
            fontWeight = 0;
        }
        if (fontWeight != this.mFontWeight) {
            this.mFontWeight = fontWeight;
            markUpdated();
        }
    }

    @ReactProp(name = "fontStyle")
    public void setFontStyle(@Nullable String fontStyleString) {
        int fontStyle = -1;
        if ("italic".equals(fontStyleString)) {
            fontStyle = 2;
        } else if ("normal".equals(fontStyleString)) {
            fontStyle = 0;
        }
        if (fontStyle != this.mFontStyle) {
            this.mFontStyle = fontStyle;
            markUpdated();
        }
    }

    @ReactProp(defaultBoolean = true, name = "includeFontPadding")
    public void setIncludeFontPadding(boolean includepad) {
        this.mIncludeFontPadding = includepad;
    }

    @ReactProp(name = "textDecorationLine")
    public void setTextDecorationLine(@Nullable String textDecorationLineString) {
        String[] split;
        this.mIsUnderlineTextDecorationSet = false;
        this.mIsLineThroughTextDecorationSet = false;
        if (textDecorationLineString != null) {
            for (String textDecorationLineSubString : textDecorationLineString.split(" ")) {
                if ("underline".equals(textDecorationLineSubString)) {
                    this.mIsUnderlineTextDecorationSet = true;
                } else if ("line-through".equals(textDecorationLineSubString)) {
                    this.mIsLineThroughTextDecorationSet = true;
                }
            }
        }
        markUpdated();
    }

    @ReactProp(name = "textBreakStrategy")
    public void setTextBreakStrategy(@Nullable String textBreakStrategy) {
        if (VERSION.SDK_INT >= 23) {
            if (textBreakStrategy == null || "highQuality".equals(textBreakStrategy)) {
                this.mTextBreakStrategy = 1;
            } else if ("simple".equals(textBreakStrategy)) {
                this.mTextBreakStrategy = 0;
            } else if ("balanced".equals(textBreakStrategy)) {
                this.mTextBreakStrategy = 2;
            } else {
                throw new JSApplicationIllegalArgumentException("Invalid textBreakStrategy: " + textBreakStrategy);
            }
            markUpdated();
        }
    }

    @ReactProp(name = "textShadowOffset")
    public void setTextShadowOffset(ReadableMap offsetMap) {
        this.mTextShadowOffsetDx = 0.0f;
        this.mTextShadowOffsetDy = 0.0f;
        if (offsetMap != null) {
            if (offsetMap.hasKey("width") && !offsetMap.isNull("width")) {
                this.mTextShadowOffsetDx = PixelUtil.toPixelFromDIP(offsetMap.getDouble("width"));
            }
            if (offsetMap.hasKey("height") && !offsetMap.isNull("height")) {
                this.mTextShadowOffsetDy = PixelUtil.toPixelFromDIP(offsetMap.getDouble("height"));
            }
        }
        markUpdated();
    }

    @ReactProp(defaultInt = 1, name = "textShadowRadius")
    public void setTextShadowRadius(float textShadowRadius) {
        if (textShadowRadius != this.mTextShadowRadius) {
            this.mTextShadowRadius = textShadowRadius;
            markUpdated();
        }
    }

    @ReactProp(customType = "Color", defaultInt = 1426063360, name = "textShadowColor")
    public void setTextShadowColor(int textShadowColor) {
        if (textShadowColor != this.mTextShadowColor) {
            this.mTextShadowColor = textShadowColor;
            markUpdated();
        }
    }
}
