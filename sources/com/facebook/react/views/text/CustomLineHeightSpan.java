package com.facebook.react.views.text;

import android.graphics.Paint.FontMetricsInt;
import android.text.style.LineHeightSpan;

public class CustomLineHeightSpan implements LineHeightSpan {
    private final int mHeight;

    CustomLineHeightSpan(float height) {
        this.mHeight = (int) Math.ceil((double) height);
    }

    public void chooseHeight(CharSequence text, int start, int end, int spanstartv, int v, FontMetricsInt fm) {
        if (fm.descent > this.mHeight) {
            int min = Math.min(this.mHeight, fm.descent);
            fm.descent = min;
            fm.bottom = min;
            fm.ascent = 0;
            fm.top = 0;
        } else if ((-fm.ascent) + fm.descent > this.mHeight) {
            fm.bottom = fm.descent;
            int i = (-this.mHeight) + fm.descent;
            fm.ascent = i;
            fm.top = i;
        } else if ((-fm.ascent) + fm.bottom > this.mHeight) {
            fm.top = fm.ascent;
            fm.bottom = fm.ascent + this.mHeight;
        } else if ((-fm.top) + fm.bottom > this.mHeight) {
            fm.top = fm.bottom - this.mHeight;
        } else {
            int additional = this.mHeight - ((-fm.top) + fm.bottom);
            fm.top -= additional / 2;
            fm.ascent -= additional / 2;
            fm.descent += additional / 2;
            fm.bottom += additional / 2;
        }
    }
}
