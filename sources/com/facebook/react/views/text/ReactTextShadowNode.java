package com.facebook.react.views.text;

import android.text.Spannable;
import android.text.TextPaint;
import com.facebook.react.uimanager.UIViewOperationQueue;
import com.facebook.yoga.YogaDirection;
import com.facebook.yoga.YogaMeasureFunction;
import javax.annotation.Nullable;

public class ReactTextShadowNode extends ReactBaseTextShadowNode {
    /* access modifiers changed from: private */
    public static final TextPaint sTextPaintInstance = new TextPaint(1);
    /* access modifiers changed from: private */
    @Nullable
    public Spannable mPreparedSpannableText;
    private final YogaMeasureFunction mTextMeasureFunction = new YogaMeasureFunction() {
        /* JADX WARNING: type inference failed for: r2v2, types: [android.text.BoringLayout] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public long measure(com.facebook.yoga.YogaNode r24, float r25, com.facebook.yoga.YogaMeasureMode r26, float r27, com.facebook.yoga.YogaMeasureMode r28) {
            /*
                r23 = this;
                android.text.TextPaint r4 = com.facebook.react.views.text.ReactTextShadowNode.sTextPaintInstance
                r0 = r23
                com.facebook.react.views.text.ReactTextShadowNode r6 = com.facebook.react.views.text.ReactTextShadowNode.this
                android.text.Spannable r6 = r6.mPreparedSpannableText
                java.lang.String r7 = "Spannable element has not been prepared in onBeforeLayout"
                java.lang.Object r3 = com.facebook.infer.annotation.Assertions.assertNotNull(r6, r7)
                android.text.Spanned r3 = (android.text.Spanned) r3
                android.text.BoringLayout$Metrics r12 = android.text.BoringLayout.isBoring(r3, r4)
                if (r12 != 0) goto L_0x0081
                float r21 = android.text.Layout.getDesiredWidth(r3, r4)
            L_0x001e:
                com.facebook.yoga.YogaMeasureMode r6 = com.facebook.yoga.YogaMeasureMode.UNDEFINED
                r0 = r26
                if (r0 == r6) goto L_0x0029
                r6 = 0
                int r6 = (r25 > r6 ? 1 : (r25 == r6 ? 0 : -1))
                if (r6 >= 0) goto L_0x0084
            L_0x0029:
                r22 = 1
            L_0x002b:
                if (r12 != 0) goto L_0x00bb
                if (r22 != 0) goto L_0x0039
                boolean r6 = com.facebook.yoga.YogaConstants.isUndefined(r21)
                if (r6 != 0) goto L_0x00bb
                int r6 = (r21 > r25 ? 1 : (r21 == r25 ? 0 : -1))
                if (r6 > 0) goto L_0x00bb
            L_0x0039:
                r0 = r21
                double r6 = (double) r0
                double r6 = java.lang.Math.ceil(r6)
                int r5 = (int) r6
                int r6 = android.os.Build.VERSION.SDK_INT
                r7 = 23
                if (r6 >= r7) goto L_0x0087
                android.text.StaticLayout r2 = new android.text.StaticLayout
                android.text.Layout$Alignment r6 = android.text.Layout.Alignment.ALIGN_NORMAL
                r7 = 1065353216(0x3f800000, float:1.0)
                r8 = 0
                r0 = r23
                com.facebook.react.views.text.ReactTextShadowNode r9 = com.facebook.react.views.text.ReactTextShadowNode.this
                boolean r9 = r9.mIncludeFontPadding
                r2.<init>(r3, r4, r5, r6, r7, r8, r9)
            L_0x0057:
                r0 = r23
                com.facebook.react.views.text.ReactTextShadowNode r6 = com.facebook.react.views.text.ReactTextShadowNode.this
                int r6 = r6.mNumberOfLines
                r7 = -1
                if (r6 == r7) goto L_0x0136
                r0 = r23
                com.facebook.react.views.text.ReactTextShadowNode r6 = com.facebook.react.views.text.ReactTextShadowNode.this
                int r6 = r6.mNumberOfLines
                int r7 = r2.getLineCount()
                if (r6 >= r7) goto L_0x0136
                int r6 = r2.getWidth()
                r0 = r23
                com.facebook.react.views.text.ReactTextShadowNode r7 = com.facebook.react.views.text.ReactTextShadowNode.this
                int r7 = r7.mNumberOfLines
                int r7 = r7 + -1
                int r7 = r2.getLineBottom(r7)
                long r6 = com.facebook.yoga.YogaMeasureOutput.make(r6, r7)
            L_0x0080:
                return r6
            L_0x0081:
                r21 = 2143289344(0x7fc00000, float:NaN)
                goto L_0x001e
            L_0x0084:
                r22 = 0
                goto L_0x002b
            L_0x0087:
                r6 = 0
                int r7 = r3.length()
                android.text.StaticLayout$Builder r6 = android.text.StaticLayout.Builder.obtain(r3, r6, r7, r4, r5)
                android.text.Layout$Alignment r7 = android.text.Layout.Alignment.ALIGN_NORMAL
                android.text.StaticLayout$Builder r6 = r6.setAlignment(r7)
                r7 = 0
                r8 = 1065353216(0x3f800000, float:1.0)
                android.text.StaticLayout$Builder r6 = r6.setLineSpacing(r7, r8)
                r0 = r23
                com.facebook.react.views.text.ReactTextShadowNode r7 = com.facebook.react.views.text.ReactTextShadowNode.this
                boolean r7 = r7.mIncludeFontPadding
                android.text.StaticLayout$Builder r6 = r6.setIncludePad(r7)
                r0 = r23
                com.facebook.react.views.text.ReactTextShadowNode r7 = com.facebook.react.views.text.ReactTextShadowNode.this
                int r7 = r7.mTextBreakStrategy
                android.text.StaticLayout$Builder r6 = r6.setBreakStrategy(r7)
                r7 = 1
                android.text.StaticLayout$Builder r6 = r6.setHyphenationFrequency(r7)
                android.text.StaticLayout r2 = r6.build()
                goto L_0x0057
            L_0x00bb:
                if (r12 == 0) goto L_0x00db
                if (r22 != 0) goto L_0x00c6
                int r6 = r12.width
                float r6 = (float) r6
                int r6 = (r6 > r25 ? 1 : (r6 == r25 ? 0 : -1))
                if (r6 > 0) goto L_0x00db
            L_0x00c6:
                int r8 = r12.width
                android.text.Layout$Alignment r9 = android.text.Layout.Alignment.ALIGN_NORMAL
                r10 = 1065353216(0x3f800000, float:1.0)
                r11 = 0
                r0 = r23
                com.facebook.react.views.text.ReactTextShadowNode r6 = com.facebook.react.views.text.ReactTextShadowNode.this
                boolean r13 = r6.mIncludeFontPadding
                r6 = r3
                r7 = r4
                android.text.BoringLayout r2 = android.text.BoringLayout.make(r6, r7, r8, r9, r10, r11, r12, r13)
                goto L_0x0057
            L_0x00db:
                int r6 = android.os.Build.VERSION.SDK_INT
                r7 = 23
                if (r6 >= r7) goto L_0x00fe
                android.text.StaticLayout r2 = new android.text.StaticLayout
                r0 = r25
                int r0 = (int) r0
                r16 = r0
                android.text.Layout$Alignment r17 = android.text.Layout.Alignment.ALIGN_NORMAL
                r18 = 1065353216(0x3f800000, float:1.0)
                r19 = 0
                r0 = r23
                com.facebook.react.views.text.ReactTextShadowNode r6 = com.facebook.react.views.text.ReactTextShadowNode.this
                boolean r0 = r6.mIncludeFontPadding
                r20 = r0
                r13 = r2
                r14 = r3
                r15 = r4
                r13.<init>(r14, r15, r16, r17, r18, r19, r20)
                goto L_0x0057
            L_0x00fe:
                r6 = 0
                int r7 = r3.length()
                r0 = r25
                int r8 = (int) r0
                android.text.StaticLayout$Builder r6 = android.text.StaticLayout.Builder.obtain(r3, r6, r7, r4, r8)
                android.text.Layout$Alignment r7 = android.text.Layout.Alignment.ALIGN_NORMAL
                android.text.StaticLayout$Builder r6 = r6.setAlignment(r7)
                r7 = 0
                r8 = 1065353216(0x3f800000, float:1.0)
                android.text.StaticLayout$Builder r6 = r6.setLineSpacing(r7, r8)
                r0 = r23
                com.facebook.react.views.text.ReactTextShadowNode r7 = com.facebook.react.views.text.ReactTextShadowNode.this
                boolean r7 = r7.mIncludeFontPadding
                android.text.StaticLayout$Builder r6 = r6.setIncludePad(r7)
                r0 = r23
                com.facebook.react.views.text.ReactTextShadowNode r7 = com.facebook.react.views.text.ReactTextShadowNode.this
                int r7 = r7.mTextBreakStrategy
                android.text.StaticLayout$Builder r6 = r6.setBreakStrategy(r7)
                r7 = 1
                android.text.StaticLayout$Builder r6 = r6.setHyphenationFrequency(r7)
                android.text.StaticLayout r2 = r6.build()
                goto L_0x0057
            L_0x0136:
                int r6 = r2.getWidth()
                int r7 = r2.getHeight()
                long r6 = com.facebook.yoga.YogaMeasureOutput.make(r6, r7)
                goto L_0x0080
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.views.text.ReactTextShadowNode.AnonymousClass1.measure(com.facebook.yoga.YogaNode, float, com.facebook.yoga.YogaMeasureMode, float, com.facebook.yoga.YogaMeasureMode):long");
        }
    };

    public ReactTextShadowNode() {
        if (!isVirtual()) {
            setMeasureFunction(this.mTextMeasureFunction);
        }
    }

    private int getTextAlign() {
        int textAlign = this.mTextAlign;
        if (getLayoutDirection() != YogaDirection.RTL) {
            return textAlign;
        }
        if (textAlign == 5) {
            return 3;
        }
        if (textAlign == 3) {
            return 5;
        }
        return textAlign;
    }

    public void onBeforeLayout() {
        this.mPreparedSpannableText = spannedFromShadowNode(this, null);
        markUpdated();
    }

    public boolean isVirtualAnchor() {
        return true;
    }

    public void markUpdated() {
        super.markUpdated();
        super.dirty();
    }

    public void onCollectExtraUpdates(UIViewOperationQueue uiViewOperationQueue) {
        super.onCollectExtraUpdates(uiViewOperationQueue);
        if (this.mPreparedSpannableText != null) {
            uiViewOperationQueue.enqueueUpdateExtraData(getReactTag(), new ReactTextUpdate(this.mPreparedSpannableText, -1, this.mContainsImages, getPadding(4), getPadding(1), getPadding(5), getPadding(3), getTextAlign(), this.mTextBreakStrategy));
        }
    }
}
