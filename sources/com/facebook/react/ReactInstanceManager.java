package com.facebook.react;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Process;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import com.facebook.common.logging.FLog;
import com.facebook.debug.holder.PrinterHolder;
import com.facebook.debug.tags.ReactDebugOverlayTags;
import com.facebook.infer.annotation.Assertions;
import com.facebook.infer.annotation.ThreadConfined;
import com.facebook.infer.annotation.ThreadSafe;
import com.facebook.react.bridge.CatalystInstance;
import com.facebook.react.bridge.CatalystInstanceImpl.Builder;
import com.facebook.react.bridge.JSBundleLoader;
import com.facebook.react.bridge.JavaJSExecutor.Factory;
import com.facebook.react.bridge.JavaScriptExecutor;
import com.facebook.react.bridge.JavaScriptExecutorFactory;
import com.facebook.react.bridge.NativeModuleCallExceptionHandler;
import com.facebook.react.bridge.NativeModuleRegistry;
import com.facebook.react.bridge.NotThreadSafeBridgeIdleDebugListener;
import com.facebook.react.bridge.ProxyJavaScriptExecutor;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactMarker;
import com.facebook.react.bridge.ReactMarkerConstants;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.bridge.queue.ReactQueueConfigurationSpec;
import com.facebook.react.common.LifecycleState;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.common.annotations.VisibleForTesting;
import com.facebook.react.devsupport.DevSupportManagerFactory;
import com.facebook.react.devsupport.ReactInstanceManagerDevHelper;
import com.facebook.react.devsupport.RedBoxHandler;
import com.facebook.react.devsupport.interfaces.DevBundleDownloadListener;
import com.facebook.react.devsupport.interfaces.DevSupportManager;
import com.facebook.react.devsupport.interfaces.PackagerStatusCallback;
import com.facebook.react.modules.appregistry.AppRegistry;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;
import com.facebook.react.modules.core.ReactChoreographer;
import com.facebook.react.modules.debug.interfaces.DeveloperSettings;
import com.facebook.react.uimanager.DisplayMetricsHolder;
import com.facebook.react.uimanager.UIImplementationProvider;
import com.facebook.react.uimanager.UIManagerModule;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.views.imagehelper.ResourceDrawableIdHelper;
import com.facebook.soloader.SoLoader;
import com.facebook.systrace.Systrace;
import com.facebook.systrace.SystraceMessage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;

@ThreadSafe
public class ReactInstanceManager {
    private static final String TAG = ReactInstanceManager.class.getSimpleName();
    private final Context mApplicationContext;
    private final List<ReactRootView> mAttachedRootViews = Collections.synchronizedList(new ArrayList());
    @Nullable
    private final NotThreadSafeBridgeIdleDebugListener mBridgeIdleDebugListener;
    @Nullable
    private final JSBundleLoader mBundleLoader;
    /* access modifiers changed from: private */
    @Nullable
    public volatile Thread mCreateReactContextThread;
    /* access modifiers changed from: private */
    @Nullable
    public Activity mCurrentActivity;
    @Nullable
    private volatile ReactContext mCurrentReactContext;
    @ThreadConfined("UI")
    @Nullable
    private DefaultHardwareBackBtnHandler mDefaultBackButtonImpl;
    private final boolean mDelayViewManagerClassLoadsEnabled;
    /* access modifiers changed from: private */
    public final DevSupportManager mDevSupportManager;
    /* access modifiers changed from: private */
    public volatile boolean mHasStartedCreatingInitialContext = false;
    /* access modifiers changed from: private */
    public volatile Boolean mHasStartedDestroying = Boolean.valueOf(false);
    @Nullable
    private final String mJSMainModulePath;
    private final JavaScriptExecutorFactory mJavaScriptExecutorFactory;
    private final boolean mLazyNativeModulesEnabled;
    private volatile LifecycleState mLifecycleState;
    private final MemoryPressureRouter mMemoryPressureRouter;
    @Nullable
    private final NativeModuleCallExceptionHandler mNativeModuleCallExceptionHandler;
    private final List<ReactPackage> mPackages;
    /* access modifiers changed from: private */
    @ThreadConfined("UI")
    @Nullable
    public ReactContextInitParams mPendingReactContextInitParams;
    private final Object mReactContextLock = new Object();
    private final Collection<ReactInstanceEventListener> mReactInstanceEventListeners = Collections.synchronizedSet(new HashSet());
    private final boolean mUseDeveloperSupport;

    private class ReactContextInitParams {
        private final JSBundleLoader mJsBundleLoader;
        private final JavaScriptExecutorFactory mJsExecutorFactory;

        public ReactContextInitParams(JavaScriptExecutorFactory jsExecutorFactory, JSBundleLoader jsBundleLoader) {
            this.mJsExecutorFactory = (JavaScriptExecutorFactory) Assertions.assertNotNull(jsExecutorFactory);
            this.mJsBundleLoader = (JSBundleLoader) Assertions.assertNotNull(jsBundleLoader);
        }

        public JavaScriptExecutorFactory getJsExecutorFactory() {
            return this.mJsExecutorFactory;
        }

        public JSBundleLoader getJsBundleLoader() {
            return this.mJsBundleLoader;
        }
    }

    public interface ReactInstanceEventListener {
        void onReactContextInitialized(ReactContext reactContext);
    }

    public static ReactInstanceManagerBuilder builder() {
        return new ReactInstanceManagerBuilder();
    }

    ReactInstanceManager(Context applicationContext, @Nullable Activity currentActivity, @Nullable DefaultHardwareBackBtnHandler defaultHardwareBackBtnHandler, JavaScriptExecutorFactory javaScriptExecutorFactory, @Nullable JSBundleLoader bundleLoader, @Nullable String jsMainModulePath, List<ReactPackage> packages, boolean useDeveloperSupport, @Nullable NotThreadSafeBridgeIdleDebugListener bridgeIdleDebugListener, LifecycleState initialLifecycleState, UIImplementationProvider uiImplementationProvider, NativeModuleCallExceptionHandler nativeModuleCallExceptionHandler, @Nullable RedBoxHandler redBoxHandler, boolean lazyNativeModulesEnabled, boolean lazyViewManagersEnabled, boolean delayViewManagerClassLoadsEnabled, @Nullable DevBundleDownloadListener devBundleDownloadListener, int minNumShakes, int minTimeLeftInFrameForNonBatchedOperationMs) {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.ctor()");
        initializeSoLoaderIfNecessary(applicationContext);
        DisplayMetricsHolder.initDisplayMetricsIfNotInitialized(applicationContext);
        this.mApplicationContext = applicationContext;
        this.mCurrentActivity = currentActivity;
        this.mDefaultBackButtonImpl = defaultHardwareBackBtnHandler;
        this.mJavaScriptExecutorFactory = javaScriptExecutorFactory;
        this.mBundleLoader = bundleLoader;
        this.mJSMainModulePath = jsMainModulePath;
        this.mPackages = new ArrayList();
        this.mUseDeveloperSupport = useDeveloperSupport;
        this.mDevSupportManager = DevSupportManagerFactory.create(applicationContext, createDevHelperInterface(), this.mJSMainModulePath, useDeveloperSupport, redBoxHandler, devBundleDownloadListener, minNumShakes);
        this.mBridgeIdleDebugListener = bridgeIdleDebugListener;
        this.mLifecycleState = initialLifecycleState;
        this.mMemoryPressureRouter = new MemoryPressureRouter(applicationContext);
        this.mNativeModuleCallExceptionHandler = nativeModuleCallExceptionHandler;
        this.mLazyNativeModulesEnabled = lazyNativeModulesEnabled;
        this.mDelayViewManagerClassLoadsEnabled = delayViewManagerClassLoadsEnabled;
        synchronized (this.mPackages) {
            PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.RN_CORE, "RNCore: Use Split Packages");
            this.mPackages.add(new CoreModulesPackage(this, new DefaultHardwareBackBtnHandler() {
                public void invokeDefaultOnBackPressed() {
                    ReactInstanceManager.this.invokeDefaultOnBackPressed();
                }
            }, uiImplementationProvider, lazyViewManagersEnabled, minTimeLeftInFrameForNonBatchedOperationMs));
            if (this.mUseDeveloperSupport) {
                this.mPackages.add(new DebugCorePackage());
            }
            this.mPackages.addAll(packages);
        }
        ReactChoreographer.initialize();
        if (this.mUseDeveloperSupport) {
            this.mDevSupportManager.startInspector();
        }
    }

    private ReactInstanceManagerDevHelper createDevHelperInterface() {
        return new ReactInstanceManagerDevHelper() {
            public void onReloadWithJSDebugger(Factory jsExecutorFactory) {
                ReactInstanceManager.this.onReloadWithJSDebugger(jsExecutorFactory);
            }

            public void onJSBundleLoadedFromServer() {
                ReactInstanceManager.this.onJSBundleLoadedFromServer();
            }

            public void toggleElementInspector() {
                ReactInstanceManager.this.toggleElementInspector();
            }

            @Nullable
            public Activity getCurrentActivity() {
                return ReactInstanceManager.this.mCurrentActivity;
            }
        };
    }

    public DevSupportManager getDevSupportManager() {
        return this.mDevSupportManager;
    }

    public MemoryPressureRouter getMemoryPressureRouter() {
        return this.mMemoryPressureRouter;
    }

    private static void initializeSoLoaderIfNecessary(Context applicationContext) {
        SoLoader.init(applicationContext, false);
    }

    @ThreadConfined("UI")
    public void createReactContextInBackground() {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.createReactContextInBackground()");
        Assertions.assertCondition(!this.mHasStartedCreatingInitialContext, "createReactContextInBackground should only be called when creating the react application for the first time. When reloading JS, e.g. from a new file, explicitlyuse recreateReactContextInBackground");
        this.mHasStartedCreatingInitialContext = true;
        recreateReactContextInBackgroundInner();
    }

    @ThreadConfined("UI")
    public void registerAdditionalPackages(List<ReactPackage> packages) {
        if (packages != null && !packages.isEmpty()) {
            if (!hasStartedCreatingInitialContext()) {
                synchronized (this.mPackages) {
                    for (ReactPackage p : packages) {
                        if (!this.mPackages.contains(p)) {
                            this.mPackages.add(p);
                        }
                    }
                }
                return;
            }
            ReactContext context = getCurrentReactContext();
            CatalystInstance catalystInstance = context != null ? context.getCatalystInstance() : null;
            Assertions.assertNotNull(catalystInstance, "CatalystInstance null after hasStartedCreatingInitialContext true.");
            catalystInstance.extendNativeModules(processPackages(new ReactApplicationContext(this.mApplicationContext), packages, true));
        }
    }

    @ThreadConfined("UI")
    public void recreateReactContextInBackground() {
        Assertions.assertCondition(this.mHasStartedCreatingInitialContext, "recreateReactContextInBackground should only be called after the initial createReactContextInBackground call.");
        recreateReactContextInBackgroundInner();
    }

    @ThreadConfined("UI")
    private void recreateReactContextInBackgroundInner() {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.recreateReactContextInBackgroundInner()");
        PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.RN_CORE, "RNCore: recreateReactContextInBackground");
        UiThreadUtil.assertOnUiThread();
        if (!this.mUseDeveloperSupport || this.mJSMainModulePath == null || Systrace.isTracing(0)) {
            recreateReactContextInBackgroundFromBundleLoader();
            return;
        }
        final DeveloperSettings devSettings = this.mDevSupportManager.getDevSettings();
        if (this.mDevSupportManager.hasUpToDateJSBundleInCache() && !devSettings.isRemoteJSDebugEnabled()) {
            onJSBundleLoadedFromServer();
        } else if (this.mBundleLoader == null) {
            this.mDevSupportManager.handleReloadJS();
        } else {
            this.mDevSupportManager.isPackagerRunning(new PackagerStatusCallback() {
                public void onPackagerStatusFetched(final boolean packagerIsRunning) {
                    UiThreadUtil.runOnUiThread(new Runnable() {
                        public void run() {
                            if (packagerIsRunning) {
                                ReactInstanceManager.this.mDevSupportManager.handleReloadJS();
                                return;
                            }
                            devSettings.setRemoteJSDebugEnabled(false);
                            ReactInstanceManager.this.recreateReactContextInBackgroundFromBundleLoader();
                        }
                    });
                }
            });
        }
    }

    /* access modifiers changed from: private */
    @ThreadConfined("UI")
    public void recreateReactContextInBackgroundFromBundleLoader() {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.recreateReactContextInBackgroundFromBundleLoader()");
        PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.RN_CORE, "RNCore: load from BundleLoader");
        recreateReactContextInBackground(this.mJavaScriptExecutorFactory, this.mBundleLoader);
    }

    public boolean hasStartedCreatingInitialContext() {
        return this.mHasStartedCreatingInitialContext;
    }

    public void onBackPressed() {
        UiThreadUtil.assertOnUiThread();
        ReactContext reactContext = this.mCurrentReactContext;
        if (reactContext == null) {
            FLog.w(ReactConstants.TAG, "Instance detached from instance manager");
            invokeDefaultOnBackPressed();
            return;
        }
        ((DeviceEventManagerModule) reactContext.getNativeModule(DeviceEventManagerModule.class)).emitHardwareBackPressed();
    }

    /* access modifiers changed from: private */
    public void invokeDefaultOnBackPressed() {
        UiThreadUtil.assertOnUiThread();
        if (this.mDefaultBackButtonImpl != null) {
            this.mDefaultBackButtonImpl.invokeDefaultOnBackPressed();
        }
    }

    @ThreadConfined("UI")
    public void onNewIntent(Intent intent) {
        UiThreadUtil.assertOnUiThread();
        ReactContext currentContext = getCurrentReactContext();
        if (currentContext == null) {
            FLog.w(ReactConstants.TAG, "Instance detached from instance manager");
            return;
        }
        String action = intent.getAction();
        Uri uri = intent.getData();
        if ("android.intent.action.VIEW".equals(action) && uri != null) {
            ((DeviceEventManagerModule) currentContext.getNativeModule(DeviceEventManagerModule.class)).emitNewIntentReceived(uri);
        }
        currentContext.onNewIntent(this.mCurrentActivity, intent);
    }

    /* access modifiers changed from: private */
    public void toggleElementInspector() {
        ReactContext currentContext = getCurrentReactContext();
        if (currentContext != null) {
            ((RCTDeviceEventEmitter) currentContext.getJSModule(RCTDeviceEventEmitter.class)).emit("toggleElementInspector", null);
        }
    }

    @ThreadConfined("UI")
    public void onHostPause() {
        UiThreadUtil.assertOnUiThread();
        this.mDefaultBackButtonImpl = null;
        if (this.mUseDeveloperSupport) {
            this.mDevSupportManager.setDevSupportEnabled(false);
        }
        moveToBeforeResumeLifecycleState();
    }

    @ThreadConfined("UI")
    public void onHostPause(Activity activity) {
        Assertions.assertNotNull(this.mCurrentActivity);
        Assertions.assertCondition(activity == this.mCurrentActivity, "Pausing an activity that is not the current activity, this is incorrect! Current activity: " + this.mCurrentActivity.getClass().getSimpleName() + " Paused activity: " + activity.getClass().getSimpleName());
        onHostPause();
    }

    @ThreadConfined("UI")
    public void onHostResume(Activity activity, DefaultHardwareBackBtnHandler defaultBackButtonImpl) {
        UiThreadUtil.assertOnUiThread();
        this.mDefaultBackButtonImpl = defaultBackButtonImpl;
        this.mCurrentActivity = activity;
        if (this.mUseDeveloperSupport) {
            final View decorView = this.mCurrentActivity.getWindow().getDecorView();
            if (!ViewCompat.isAttachedToWindow(decorView)) {
                decorView.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
                    public void onViewAttachedToWindow(View v) {
                        decorView.removeOnAttachStateChangeListener(this);
                        ReactInstanceManager.this.mDevSupportManager.setDevSupportEnabled(true);
                    }

                    public void onViewDetachedFromWindow(View v) {
                    }
                });
            } else {
                this.mDevSupportManager.setDevSupportEnabled(true);
            }
        }
        moveToResumedLifecycleState(false);
    }

    @ThreadConfined("UI")
    public void onHostDestroy() {
        UiThreadUtil.assertOnUiThread();
        if (this.mUseDeveloperSupport) {
            this.mDevSupportManager.setDevSupportEnabled(false);
        }
        moveToBeforeCreateLifecycleState();
        this.mCurrentActivity = null;
    }

    @ThreadConfined("UI")
    public void onHostDestroy(Activity activity) {
        if (activity == this.mCurrentActivity) {
            onHostDestroy();
        }
    }

    @ThreadConfined("UI")
    public void destroy() {
        UiThreadUtil.assertOnUiThread();
        PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.RN_CORE, "RNCore: Destroy");
        this.mHasStartedDestroying = Boolean.valueOf(true);
        if (this.mUseDeveloperSupport) {
            this.mDevSupportManager.setDevSupportEnabled(false);
            this.mDevSupportManager.stopInspector();
        }
        moveToBeforeCreateLifecycleState();
        if (this.mCreateReactContextThread != null) {
            this.mCreateReactContextThread = null;
        }
        this.mMemoryPressureRouter.destroy(this.mApplicationContext);
        synchronized (this.mReactContextLock) {
            if (this.mCurrentReactContext != null) {
                this.mCurrentReactContext.destroy();
                this.mCurrentReactContext = null;
            }
        }
        this.mHasStartedCreatingInitialContext = false;
        this.mCurrentActivity = null;
        ResourceDrawableIdHelper.getInstance().clear();
        this.mHasStartedDestroying = Boolean.valueOf(false);
        synchronized (this.mHasStartedDestroying) {
            this.mHasStartedDestroying.notifyAll();
        }
    }

    private synchronized void moveToResumedLifecycleState(boolean force) {
        ReactContext currentContext = getCurrentReactContext();
        if (currentContext != null && (force || this.mLifecycleState == LifecycleState.BEFORE_RESUME || this.mLifecycleState == LifecycleState.BEFORE_CREATE)) {
            currentContext.onHostResume(this.mCurrentActivity);
        }
        this.mLifecycleState = LifecycleState.RESUMED;
    }

    private synchronized void moveToBeforeResumeLifecycleState() {
        ReactContext currentContext = getCurrentReactContext();
        if (currentContext != null) {
            if (this.mLifecycleState == LifecycleState.BEFORE_CREATE) {
                currentContext.onHostResume(this.mCurrentActivity);
                currentContext.onHostPause();
            } else if (this.mLifecycleState == LifecycleState.RESUMED) {
                currentContext.onHostPause();
            }
        }
        this.mLifecycleState = LifecycleState.BEFORE_RESUME;
    }

    private synchronized void moveToBeforeCreateLifecycleState() {
        ReactContext currentContext = getCurrentReactContext();
        if (currentContext != null) {
            if (this.mLifecycleState == LifecycleState.RESUMED) {
                currentContext.onHostPause();
                this.mLifecycleState = LifecycleState.BEFORE_RESUME;
            }
            if (this.mLifecycleState == LifecycleState.BEFORE_RESUME) {
                currentContext.onHostDestroy();
            }
        }
        this.mLifecycleState = LifecycleState.BEFORE_CREATE;
    }

    private synchronized void moveReactContextToCurrentLifecycleState() {
        if (this.mLifecycleState == LifecycleState.RESUMED) {
            moveToResumedLifecycleState(true);
        }
    }

    @ThreadConfined("UI")
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        ReactContext currentContext = getCurrentReactContext();
        if (currentContext != null) {
            currentContext.onActivityResult(activity, requestCode, resultCode, data);
        }
    }

    @ThreadConfined("UI")
    public void showDevOptionsDialog() {
        UiThreadUtil.assertOnUiThread();
        this.mDevSupportManager.showDevOptionsDialog();
    }

    @ThreadConfined("UI")
    public void attachRootView(ReactRootView rootView) {
        UiThreadUtil.assertOnUiThread();
        this.mAttachedRootViews.add(rootView);
        rootView.removeAllViews();
        rootView.setId(-1);
        ReactContext currentContext = getCurrentReactContext();
        if (this.mCreateReactContextThread == null && currentContext != null) {
            attachRootViewToInstance(rootView, currentContext.getCatalystInstance());
        }
    }

    @ThreadConfined("UI")
    public void detachRootView(ReactRootView rootView) {
        UiThreadUtil.assertOnUiThread();
        if (this.mAttachedRootViews.remove(rootView)) {
            ReactContext currentContext = getCurrentReactContext();
            if (currentContext != null && currentContext.hasActiveCatalystInstance()) {
                detachViewFromInstance(rootView, currentContext.getCatalystInstance());
            }
        }
    }

    public List<ViewManager> createAllViewManagers(ReactApplicationContext catalystApplicationContext) {
        List<ViewManager> allViewManagers;
        ReactMarker.logMarker(ReactMarkerConstants.CREATE_VIEW_MANAGERS_START);
        Systrace.beginSection(0, "createAllViewManagers");
        try {
            synchronized (this.mPackages) {
                allViewManagers = new ArrayList<>();
                for (ReactPackage reactPackage : this.mPackages) {
                    allViewManagers.addAll(reactPackage.createViewManagers(catalystApplicationContext));
                }
            }
            return allViewManagers;
        } finally {
            Systrace.endSection(0);
            ReactMarker.logMarker(ReactMarkerConstants.CREATE_VIEW_MANAGERS_END);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        r5 = r7.mPackages;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        monitor-enter(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r6 = r7.mPackages.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0023, code lost:
        if (r6.hasNext() == false) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0025, code lost:
        r1 = (com.facebook.react.ReactPackage) r6.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002d, code lost:
        if ((r1 instanceof com.facebook.react.ViewManagerOnDemandReactPackage) == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002f, code lost:
        r1 = (com.facebook.react.ViewManagerOnDemandReactPackage) r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0033, code lost:
        if (r7.mDelayViewManagerClassLoadsEnabled != false) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0036, code lost:
        r2 = r1.createViewManager(r0, r8, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003a, code lost:
        if (r2 == null) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003c, code lost:
        monitor-exit(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0044, code lost:
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        monitor-exit(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return null;
     */
    @javax.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.react.uimanager.ViewManager createViewManager(java.lang.String r8) {
        /*
            r7 = this;
            r3 = 0
            java.lang.Object r4 = r7.mReactContextLock
            monitor-enter(r4)
            com.facebook.react.bridge.ReactContext r0 = r7.getCurrentReactContext()     // Catch:{ all -> 0x0041 }
            com.facebook.react.bridge.ReactApplicationContext r0 = (com.facebook.react.bridge.ReactApplicationContext) r0     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0012
            boolean r5 = r0.hasActiveCatalystInstance()     // Catch:{ all -> 0x0041 }
            if (r5 != 0) goto L_0x0015
        L_0x0012:
            monitor-exit(r4)     // Catch:{ all -> 0x0041 }
            r2 = r3
        L_0x0014:
            return r2
        L_0x0015:
            monitor-exit(r4)     // Catch:{ all -> 0x0041 }
            java.util.List<com.facebook.react.ReactPackage> r5 = r7.mPackages
            monitor-enter(r5)
            java.util.List<com.facebook.react.ReactPackage> r4 = r7.mPackages     // Catch:{ all -> 0x003e }
            java.util.Iterator r6 = r4.iterator()     // Catch:{ all -> 0x003e }
        L_0x001f:
            boolean r4 = r6.hasNext()     // Catch:{ all -> 0x003e }
            if (r4 == 0) goto L_0x0046
            java.lang.Object r1 = r6.next()     // Catch:{ all -> 0x003e }
            com.facebook.react.ReactPackage r1 = (com.facebook.react.ReactPackage) r1     // Catch:{ all -> 0x003e }
            boolean r4 = r1 instanceof com.facebook.react.ViewManagerOnDemandReactPackage     // Catch:{ all -> 0x003e }
            if (r4 == 0) goto L_0x001f
            com.facebook.react.ViewManagerOnDemandReactPackage r1 = (com.facebook.react.ViewManagerOnDemandReactPackage) r1     // Catch:{ all -> 0x003e }
            boolean r4 = r7.mDelayViewManagerClassLoadsEnabled     // Catch:{ all -> 0x003e }
            if (r4 != 0) goto L_0x0044
            r4 = 1
        L_0x0036:
            com.facebook.react.uimanager.ViewManager r2 = r1.createViewManager(r0, r8, r4)     // Catch:{ all -> 0x003e }
            if (r2 == 0) goto L_0x001f
            monitor-exit(r5)     // Catch:{ all -> 0x003e }
            goto L_0x0014
        L_0x003e:
            r3 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x003e }
            throw r3
        L_0x0041:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0041 }
            throw r3
        L_0x0044:
            r4 = 0
            goto L_0x0036
        L_0x0046:
            monitor-exit(r5)     // Catch:{ all -> 0x003e }
            r2 = r3
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.ReactInstanceManager.createViewManager(java.lang.String):com.facebook.react.uimanager.ViewManager");
    }

    @Nullable
    public List<String> getViewManagerNames() {
        ArrayList arrayList;
        synchronized (this.mReactContextLock) {
            ReactApplicationContext context = (ReactApplicationContext) getCurrentReactContext();
            if (context == null || !context.hasActiveCatalystInstance()) {
                arrayList = null;
            } else {
                synchronized (this.mPackages) {
                    Set<String> uniqueNames = new HashSet<>();
                    for (ReactPackage reactPackage : this.mPackages) {
                        if (reactPackage instanceof ViewManagerOnDemandReactPackage) {
                            List<String> names = ((ViewManagerOnDemandReactPackage) reactPackage).getViewManagerNames(context, !this.mDelayViewManagerClassLoadsEnabled);
                            if (names != null) {
                                uniqueNames.addAll(names);
                            }
                        }
                    }
                    arrayList = new ArrayList(uniqueNames);
                }
            }
        }
        return arrayList;
    }

    public void addReactInstanceEventListener(ReactInstanceEventListener listener) {
        this.mReactInstanceEventListeners.add(listener);
    }

    public void removeReactInstanceEventListener(ReactInstanceEventListener listener) {
        this.mReactInstanceEventListeners.remove(listener);
    }

    @VisibleForTesting
    @Nullable
    public ReactContext getCurrentReactContext() {
        ReactContext reactContext;
        synchronized (this.mReactContextLock) {
            reactContext = this.mCurrentReactContext;
        }
        return reactContext;
    }

    public LifecycleState getLifecycleState() {
        return this.mLifecycleState;
    }

    /* access modifiers changed from: private */
    @ThreadConfined("UI")
    public void onReloadWithJSDebugger(Factory jsExecutorFactory) {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.onReloadWithJSDebugger()");
        recreateReactContextInBackground(new ProxyJavaScriptExecutor.Factory(jsExecutorFactory), JSBundleLoader.createRemoteDebuggerBundleLoader(this.mDevSupportManager.getJSBundleURLForRemoteDebugging(), this.mDevSupportManager.getSourceUrl()));
    }

    /* access modifiers changed from: private */
    @ThreadConfined("UI")
    public void onJSBundleLoadedFromServer() {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.onJSBundleLoadedFromServer()");
        recreateReactContextInBackground(this.mJavaScriptExecutorFactory, JSBundleLoader.createCachedBundleFromNetworkLoader(this.mDevSupportManager.getSourceUrl(), this.mDevSupportManager.getDownloadedJSBundleFile()));
    }

    @ThreadConfined("UI")
    private void recreateReactContextInBackground(JavaScriptExecutorFactory jsExecutorFactory, JSBundleLoader jsBundleLoader) {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.recreateReactContextInBackground()");
        UiThreadUtil.assertOnUiThread();
        ReactContextInitParams initParams = new ReactContextInitParams(jsExecutorFactory, jsBundleLoader);
        if (this.mCreateReactContextThread == null) {
            runCreateReactContextOnNewThread(initParams);
        } else {
            this.mPendingReactContextInitParams = initParams;
        }
    }

    /* access modifiers changed from: private */
    @ThreadConfined("UI")
    public void runCreateReactContextOnNewThread(final ReactContextInitParams initParams) {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.runCreateReactContextOnNewThread()");
        UiThreadUtil.assertOnUiThread();
        synchronized (this.mReactContextLock) {
            if (this.mCurrentReactContext != null) {
                tearDownReactContext(this.mCurrentReactContext);
                this.mCurrentReactContext = null;
            }
        }
        this.mCreateReactContextThread = new Thread(new Runnable() {
            public void run() {
                ReactMarker.logMarker(ReactMarkerConstants.REACT_CONTEXT_THREAD_END);
                synchronized (ReactInstanceManager.this.mHasStartedDestroying) {
                    while (ReactInstanceManager.this.mHasStartedDestroying.booleanValue()) {
                        try {
                            ReactInstanceManager.this.mHasStartedDestroying.wait();
                        } catch (InterruptedException e) {
                        }
                    }
                }
                ReactInstanceManager.this.mHasStartedCreatingInitialContext = true;
                try {
                    Process.setThreadPriority(-4);
                    final ReactApplicationContext reactApplicationContext = ReactInstanceManager.this.createReactContext(initParams.getJsExecutorFactory().create(), initParams.getJsBundleLoader());
                    ReactInstanceManager.this.mCreateReactContextThread = null;
                    ReactMarker.logMarker(ReactMarkerConstants.PRE_SETUP_REACT_CONTEXT_START);
                    Runnable maybeRecreateReactContextRunnable = new Runnable() {
                        public void run() {
                            if (ReactInstanceManager.this.mPendingReactContextInitParams != null) {
                                ReactInstanceManager.this.runCreateReactContextOnNewThread(ReactInstanceManager.this.mPendingReactContextInitParams);
                                ReactInstanceManager.this.mPendingReactContextInitParams = null;
                            }
                        }
                    };
                    reactApplicationContext.runOnNativeModulesQueueThread(new Runnable() {
                        public void run() {
                            try {
                                ReactInstanceManager.this.setupReactContext(reactApplicationContext);
                            } catch (Exception e) {
                                ReactInstanceManager.this.mDevSupportManager.handleException(e);
                            }
                        }
                    });
                    UiThreadUtil.runOnUiThread(maybeRecreateReactContextRunnable);
                } catch (Exception e2) {
                    ReactInstanceManager.this.mDevSupportManager.handleException(e2);
                }
            }
        });
        ReactMarker.logMarker(ReactMarkerConstants.REACT_CONTEXT_THREAD_START);
        this.mCreateReactContextThread.start();
    }

    /* access modifiers changed from: private */
    public void setupReactContext(final ReactApplicationContext reactContext) {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.setupReactContext()");
        ReactMarker.logMarker(ReactMarkerConstants.PRE_SETUP_REACT_CONTEXT_END);
        ReactMarker.logMarker(ReactMarkerConstants.SETUP_REACT_CONTEXT_START);
        Systrace.beginSection(0, "setupReactContext");
        synchronized (this.mReactContextLock) {
            this.mCurrentReactContext = (ReactContext) Assertions.assertNotNull(reactContext);
        }
        CatalystInstance catalystInstance = (CatalystInstance) Assertions.assertNotNull(reactContext.getCatalystInstance());
        catalystInstance.initialize();
        this.mDevSupportManager.onNewReactContextCreated(reactContext);
        this.mMemoryPressureRouter.addMemoryPressureListener(catalystInstance);
        moveReactContextToCurrentLifecycleState();
        ReactMarker.logMarker(ReactMarkerConstants.ATTACH_MEASURED_ROOT_VIEWS_START);
        synchronized (this.mAttachedRootViews) {
            for (ReactRootView rootView : this.mAttachedRootViews) {
                attachRootViewToInstance(rootView, catalystInstance);
            }
        }
        ReactMarker.logMarker(ReactMarkerConstants.ATTACH_MEASURED_ROOT_VIEWS_END);
        final ReactInstanceEventListener[] finalListeners = (ReactInstanceEventListener[]) this.mReactInstanceEventListeners.toArray(new ReactInstanceEventListener[this.mReactInstanceEventListeners.size()]);
        UiThreadUtil.runOnUiThread(new Runnable() {
            public void run() {
                for (ReactInstanceEventListener listener : finalListeners) {
                    listener.onReactContextInitialized(reactContext);
                }
            }
        });
        Systrace.endSection(0);
        ReactMarker.logMarker(ReactMarkerConstants.SETUP_REACT_CONTEXT_END);
        reactContext.runOnJSQueueThread(new Runnable() {
            public void run() {
                Process.setThreadPriority(0);
            }
        });
        reactContext.runOnNativeModulesQueueThread(new Runnable() {
            public void run() {
                Process.setThreadPriority(0);
            }
        });
    }

    private void attachRootViewToInstance(final ReactRootView rootView, CatalystInstance catalystInstance) {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.attachRootViewToInstance()");
        Systrace.beginSection(0, "attachRootViewToInstance");
        final int rootTag = ((UIManagerModule) catalystInstance.getNativeModule(UIManagerModule.class)).addRootView(rootView);
        rootView.setRootViewTag(rootTag);
        rootView.invokeJSEntryPoint();
        Systrace.beginAsyncSection(0, "pre_rootView.onAttachedToReactInstance", rootTag);
        UiThreadUtil.runOnUiThread(new Runnable() {
            public void run() {
                Systrace.endAsyncSection(0, "pre_rootView.onAttachedToReactInstance", rootTag);
                rootView.onAttachedToReactInstance();
            }
        });
        Systrace.endSection(0);
    }

    private void detachViewFromInstance(ReactRootView rootView, CatalystInstance catalystInstance) {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.detachViewFromInstance()");
        UiThreadUtil.assertOnUiThread();
        ((AppRegistry) catalystInstance.getJSModule(AppRegistry.class)).unmountApplicationComponentAtRootTag(rootView.getId());
    }

    private void tearDownReactContext(ReactContext reactContext) {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.tearDownReactContext()");
        UiThreadUtil.assertOnUiThread();
        if (this.mLifecycleState == LifecycleState.RESUMED) {
            reactContext.onHostPause();
        }
        synchronized (this.mAttachedRootViews) {
            for (ReactRootView rootView : this.mAttachedRootViews) {
                rootView.removeAllViews();
                rootView.setId(-1);
            }
        }
        reactContext.destroy();
        this.mDevSupportManager.onReactInstanceDestroyed(reactContext);
        this.mMemoryPressureRouter.removeMemoryPressureListener(reactContext.getCatalystInstance());
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public ReactApplicationContext createReactContext(JavaScriptExecutor jsExecutor, JSBundleLoader jsBundleLoader) {
        Log.d(ReactConstants.TAG, "ReactInstanceManager.createReactContext()");
        ReactMarker.logMarker(ReactMarkerConstants.CREATE_REACT_CONTEXT_START);
        ReactApplicationContext reactContext = new ReactApplicationContext(this.mApplicationContext);
        if (this.mUseDeveloperSupport) {
            reactContext.setNativeModuleCallExceptionHandler(this.mDevSupportManager);
        }
        Builder catalystInstanceBuilder = new Builder().setReactQueueConfigurationSpec(ReactQueueConfigurationSpec.createDefault()).setJSExecutor(jsExecutor).setRegistry(processPackages(reactContext, this.mPackages, false)).setJSBundleLoader(jsBundleLoader).setNativeModuleCallExceptionHandler(this.mNativeModuleCallExceptionHandler != null ? this.mNativeModuleCallExceptionHandler : this.mDevSupportManager);
        ReactMarker.logMarker(ReactMarkerConstants.CREATE_CATALYST_INSTANCE_START);
        Systrace.beginSection(0, "createCatalystInstance");
        try {
            CatalystInstance catalystInstance = catalystInstanceBuilder.build();
            Systrace.endSection(0);
            ReactMarker.logMarker(ReactMarkerConstants.CREATE_CATALYST_INSTANCE_END);
            if (this.mBridgeIdleDebugListener != null) {
                catalystInstance.addBridgeIdleDebugListener(this.mBridgeIdleDebugListener);
            }
            if (Systrace.isTracing(0)) {
                catalystInstance.setGlobalVariable("__RCTProfileIsProfiling", "true");
            }
            ReactMarker.logMarker(ReactMarkerConstants.PRE_RUN_JS_BUNDLE_START);
            catalystInstance.runJSBundle();
            reactContext.initializeWithInstance(catalystInstance);
            return reactContext;
        } catch (Throwable th) {
            Systrace.endSection(0);
            ReactMarker.logMarker(ReactMarkerConstants.CREATE_CATALYST_INSTANCE_END);
            throw th;
        }
    }

    private NativeModuleRegistry processPackages(ReactApplicationContext reactContext, List<ReactPackage> packages, boolean checkAndUpdatePackageMembership) {
        NativeModuleRegistryBuilder nativeModuleRegistryBuilder = new NativeModuleRegistryBuilder(reactContext, this, this.mLazyNativeModulesEnabled);
        ReactMarker.logMarker(ReactMarkerConstants.PROCESS_PACKAGES_START);
        synchronized (this.mPackages) {
            for (ReactPackage reactPackage : packages) {
                if (!checkAndUpdatePackageMembership || !this.mPackages.contains(reactPackage)) {
                    Systrace.beginSection(0, "createAndProcessCustomReactPackage");
                    if (checkAndUpdatePackageMembership) {
                        try {
                            this.mPackages.add(reactPackage);
                        } catch (Throwable th) {
                            Systrace.endSection(0);
                            throw th;
                        }
                    }
                    processPackage(reactPackage, nativeModuleRegistryBuilder);
                    Systrace.endSection(0);
                }
            }
        }
        ReactMarker.logMarker(ReactMarkerConstants.PROCESS_PACKAGES_END);
        ReactMarker.logMarker(ReactMarkerConstants.BUILD_NATIVE_MODULE_REGISTRY_START);
        Systrace.beginSection(0, "buildNativeModuleRegistry");
        try {
            return nativeModuleRegistryBuilder.build();
        } finally {
            Systrace.endSection(0);
            ReactMarker.logMarker(ReactMarkerConstants.BUILD_NATIVE_MODULE_REGISTRY_END);
        }
    }

    private void processPackage(ReactPackage reactPackage, NativeModuleRegistryBuilder nativeModuleRegistryBuilder) {
        SystraceMessage.beginSection(0, "processPackage").arg("className", (Object) reactPackage.getClass().getSimpleName()).flush();
        if (reactPackage instanceof ReactPackageLogger) {
            ((ReactPackageLogger) reactPackage).startProcessPackage();
        }
        nativeModuleRegistryBuilder.processPackage(reactPackage);
        if (reactPackage instanceof ReactPackageLogger) {
            ((ReactPackageLogger) reactPackage).endProcessPackage();
        }
        SystraceMessage.endSection(0).flush();
    }
}
