package com.facebook.react;

import com.facebook.common.logging.FLog;
import com.facebook.react.bridge.BaseJavaModule;
import com.facebook.react.bridge.ModuleHolder;
import com.facebook.react.bridge.ModuleSpec;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.NativeModuleRegistry;
import com.facebook.react.bridge.OnBatchCompleteListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMarker;
import com.facebook.react.bridge.ReactMarkerConstants;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.module.model.ReactModuleInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class NativeModuleRegistryBuilder {
    private final boolean mLazyNativeModulesEnabled;
    private final Map<Class<? extends NativeModule>, ModuleHolder> mModules = new HashMap();
    private final ReactApplicationContext mReactApplicationContext;
    private final ReactInstanceManager mReactInstanceManager;
    private final Map<String, Class<? extends NativeModule>> namesToType = new HashMap();

    public NativeModuleRegistryBuilder(ReactApplicationContext reactApplicationContext, ReactInstanceManager reactInstanceManager, boolean lazyNativeModulesEnabled) {
        this.mReactApplicationContext = reactApplicationContext;
        this.mReactInstanceManager = reactInstanceManager;
        this.mLazyNativeModulesEnabled = lazyNativeModulesEnabled;
    }

    /* JADX INFO: finally extract failed */
    public void processPackage(ReactPackage reactPackage) {
        List<NativeModule> nativeModules;
        ModuleHolder moduleHolder;
        if (!this.mLazyNativeModulesEnabled) {
            FLog.d(ReactConstants.TAG, reactPackage.getClass().getSimpleName() + " is not a LazyReactPackage, falling back to old version.");
            if (reactPackage instanceof ReactInstancePackage) {
                nativeModules = ((ReactInstancePackage) reactPackage).createNativeModules(this.mReactApplicationContext, this.mReactInstanceManager);
            } else {
                nativeModules = reactPackage.createNativeModules(this.mReactApplicationContext);
            }
            for (NativeModule nativeModule : nativeModules) {
                addNativeModule(nativeModule);
            }
        } else if (!(reactPackage instanceof LazyReactPackage)) {
            throw new IllegalStateException("Lazy native modules requires all ReactPackage to inherit from LazyReactPackage");
        } else {
            LazyReactPackage lazyReactPackage = (LazyReactPackage) reactPackage;
            List<ModuleSpec> moduleSpecs = lazyReactPackage.getNativeModules(this.mReactApplicationContext);
            Map<Class, ReactModuleInfo> reactModuleInfoMap = lazyReactPackage.getReactModuleInfoProvider().getReactModuleInfos();
            for (ModuleSpec moduleSpec : moduleSpecs) {
                Class<? extends NativeModule> type = moduleSpec.getType();
                ReactModuleInfo reactModuleInfo = (ReactModuleInfo) reactModuleInfoMap.get(type);
                if (reactModuleInfo != null) {
                    moduleHolder = new ModuleHolder(reactModuleInfo, moduleSpec.getProvider());
                } else if (BaseJavaModule.class.isAssignableFrom(type)) {
                    throw new IllegalStateException("Native Java module " + type.getSimpleName() + " should be annotated with @ReactModule and added to a @ReactModuleList.");
                } else {
                    ReactMarker.logMarker(ReactMarkerConstants.CREATE_MODULE_START, moduleSpec.getType().getName());
                    try {
                        NativeModule module = (NativeModule) moduleSpec.getProvider().get();
                        ReactMarker.logMarker(ReactMarkerConstants.CREATE_MODULE_END);
                        moduleHolder = new ModuleHolder(module);
                    } catch (Throwable th) {
                        ReactMarker.logMarker(ReactMarkerConstants.CREATE_MODULE_END);
                        throw th;
                    }
                }
                String name = moduleHolder.getName();
                if (this.namesToType.containsKey(name)) {
                    Class<? extends NativeModule> existingNativeModule = (Class) this.namesToType.get(name);
                    if (!moduleHolder.getCanOverrideExistingModule()) {
                        throw new IllegalStateException("Native module " + type.getSimpleName() + " tried to override " + existingNativeModule.getSimpleName() + " for module name " + name + ". If this was your intention, set canOverrideExistingModule=true");
                    }
                    this.mModules.remove(existingNativeModule);
                }
                this.namesToType.put(name, type);
                this.mModules.put(type, moduleHolder);
            }
        }
    }

    public void addNativeModule(NativeModule nativeModule) {
        String name = nativeModule.getName();
        Class<? extends NativeModule> type = nativeModule.getClass();
        if (this.namesToType.containsKey(name)) {
            Class<? extends NativeModule> existingModule = (Class) this.namesToType.get(name);
            if (!nativeModule.canOverrideExistingModule()) {
                throw new IllegalStateException("Native module " + type.getSimpleName() + " tried to override " + existingModule.getSimpleName() + " for module name " + name + ". If this was your intention, set canOverrideExistingModule=true");
            }
            this.mModules.remove(existingModule);
        }
        this.namesToType.put(name, type);
        this.mModules.put(type, new ModuleHolder(nativeModule));
    }

    public NativeModuleRegistry build() {
        ArrayList<ModuleHolder> batchCompleteListenerModules = new ArrayList<>();
        for (Entry<Class<? extends NativeModule>, ModuleHolder> entry : this.mModules.entrySet()) {
            if (OnBatchCompleteListener.class.isAssignableFrom((Class) entry.getKey())) {
                batchCompleteListenerModules.add(entry.getValue());
            }
        }
        return new NativeModuleRegistry(this.mReactApplicationContext, this.mModules, batchCompleteListenerModules);
    }
}
