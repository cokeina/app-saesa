package com.facebook.react.bridge;

import com.facebook.react.common.ReactConstants;

public class JSCJavaScriptExecutorFactory implements JavaScriptExecutorFactory {
    private final String mAppName;
    private final String mDeviceName;

    public JSCJavaScriptExecutorFactory(String appName, String deviceName) {
        this.mAppName = appName;
        this.mDeviceName = deviceName;
    }

    public JavaScriptExecutor create() throws Exception {
        WritableNativeMap jscConfig = new WritableNativeMap();
        jscConfig.putString("OwnerIdentity", ReactConstants.TAG);
        jscConfig.putString("AppIdentity", this.mAppName);
        jscConfig.putString("DeviceIdentity", this.mDeviceName);
        return new JSCJavaScriptExecutor(jscConfig);
    }
}
