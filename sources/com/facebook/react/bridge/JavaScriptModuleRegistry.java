package com.facebook.react.bridge;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import javax.annotation.Nullable;

public final class JavaScriptModuleRegistry {
    private final HashMap<Class<? extends JavaScriptModule>, JavaScriptModule> mModuleInstances = new HashMap<>();

    private static class JavaScriptModuleInvocationHandler implements InvocationHandler {
        private final CatalystInstance mCatalystInstance;
        private final Class<? extends JavaScriptModule> mModuleInterface;
        @Nullable
        private String mName;

        public JavaScriptModuleInvocationHandler(CatalystInstance catalystInstance, Class<? extends JavaScriptModule> moduleInterface) {
            this.mCatalystInstance = catalystInstance;
            this.mModuleInterface = moduleInterface;
        }

        private String getJSModuleName() {
            if (this.mName == null) {
                String name = this.mModuleInterface.getSimpleName();
                int dollarSignIndex = name.lastIndexOf(36);
                if (dollarSignIndex != -1) {
                    name = name.substring(dollarSignIndex + 1);
                }
                this.mName = name;
            }
            return this.mName;
        }

        @Nullable
        public Object invoke(Object proxy, Method method, @Nullable Object[] args) throws Throwable {
            this.mCatalystInstance.callFunction(getJSModuleName(), method.getName(), args != null ? Arguments.fromJavaArgs(args) : new WritableNativeArray());
            return null;
        }
    }

    public synchronized <T extends JavaScriptModule> T getJavaScriptModule(CatalystInstance instance, Class<T> moduleInterface) {
        JavaScriptModule module;
        module = (JavaScriptModule) this.mModuleInstances.get(moduleInterface);
        if (module == null) {
            JavaScriptModule interfaceProxy = (JavaScriptModule) Proxy.newProxyInstance(moduleInterface.getClassLoader(), new Class[]{moduleInterface}, new JavaScriptModuleInvocationHandler(instance, moduleInterface));
            this.mModuleInstances.put(moduleInterface, interfaceProxy);
            module = interfaceProxy;
        }
        return module;
    }
}
