package com.facebook.react.bridge;

import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.util.Log;
import com.facebook.common.logging.FLog;
import com.facebook.infer.annotation.Assertions;
import com.facebook.jni.HybridData;
import com.facebook.proguard.annotations.DoNotStrip;
import com.facebook.react.bridge.queue.MessageQueueThread;
import com.facebook.react.bridge.queue.QueueThreadExceptionHandler;
import com.facebook.react.bridge.queue.ReactQueueConfiguration;
import com.facebook.react.bridge.queue.ReactQueueConfigurationImpl;
import com.facebook.react.bridge.queue.ReactQueueConfigurationSpec;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.common.annotations.VisibleForTesting;
import com.facebook.systrace.Systrace;
import com.facebook.systrace.TraceListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.Nullable;

@DoNotStrip
public class CatalystInstanceImpl implements CatalystInstance {
    private static final AtomicInteger sNextInstanceIdForTrace = new AtomicInteger(1);
    private volatile boolean mAcceptCalls;
    /* access modifiers changed from: private */
    public final CopyOnWriteArrayList<NotThreadSafeBridgeIdleDebugListener> mBridgeIdleListeners;
    private volatile boolean mDestroyed;
    /* access modifiers changed from: private */
    public final HybridData mHybridData;
    private boolean mInitialized;
    private boolean mJSBundleHasLoaded;
    private final JSBundleLoader mJSBundleLoader;
    private final ArrayList<PendingJSCall> mJSCallsPendingInit;
    private final Object mJSCallsPendingInitLock;
    private final JavaScriptModuleRegistry mJSModuleRegistry;
    /* access modifiers changed from: private */
    public JavaScriptContextHolder mJavaScriptContextHolder;
    private final String mJsPendingCallsTitleForTrace;
    private final NativeModuleCallExceptionHandler mNativeModuleCallExceptionHandler;
    /* access modifiers changed from: private */
    public final NativeModuleRegistry mNativeModuleRegistry;
    private final MessageQueueThread mNativeModulesQueueThread;
    /* access modifiers changed from: private */
    public final AtomicInteger mPendingJSCalls;
    private final ReactQueueConfigurationImpl mReactQueueConfiguration;
    @Nullable
    private String mSourceURL;
    private final TraceListener mTraceListener;

    private static class BridgeCallback implements ReactCallback {
        private final WeakReference<CatalystInstanceImpl> mOuter;

        public BridgeCallback(CatalystInstanceImpl outer) {
            this.mOuter = new WeakReference<>(outer);
        }

        public void onBatchComplete() {
            CatalystInstanceImpl impl = (CatalystInstanceImpl) this.mOuter.get();
            if (impl != null) {
                impl.mNativeModuleRegistry.onBatchComplete();
            }
        }

        public void incrementPendingJSCalls() {
            CatalystInstanceImpl impl = (CatalystInstanceImpl) this.mOuter.get();
            if (impl != null) {
                impl.incrementPendingJSCalls();
            }
        }

        public void decrementPendingJSCalls() {
            CatalystInstanceImpl impl = (CatalystInstanceImpl) this.mOuter.get();
            if (impl != null) {
                impl.decrementPendingJSCalls();
            }
        }
    }

    public static class Builder {
        @Nullable
        private JSBundleLoader mJSBundleLoader;
        @Nullable
        private JavaScriptExecutor mJSExecutor;
        @Nullable
        private NativeModuleCallExceptionHandler mNativeModuleCallExceptionHandler;
        @Nullable
        private ReactQueueConfigurationSpec mReactQueueConfigurationSpec;
        @Nullable
        private NativeModuleRegistry mRegistry;

        public Builder setReactQueueConfigurationSpec(ReactQueueConfigurationSpec ReactQueueConfigurationSpec) {
            this.mReactQueueConfigurationSpec = ReactQueueConfigurationSpec;
            return this;
        }

        public Builder setRegistry(NativeModuleRegistry registry) {
            this.mRegistry = registry;
            return this;
        }

        public Builder setJSBundleLoader(JSBundleLoader jsBundleLoader) {
            this.mJSBundleLoader = jsBundleLoader;
            return this;
        }

        public Builder setJSExecutor(JavaScriptExecutor jsExecutor) {
            this.mJSExecutor = jsExecutor;
            return this;
        }

        public Builder setNativeModuleCallExceptionHandler(NativeModuleCallExceptionHandler handler) {
            this.mNativeModuleCallExceptionHandler = handler;
            return this;
        }

        public CatalystInstanceImpl build() {
            return new CatalystInstanceImpl((ReactQueueConfigurationSpec) Assertions.assertNotNull(this.mReactQueueConfigurationSpec), (JavaScriptExecutor) Assertions.assertNotNull(this.mJSExecutor), (NativeModuleRegistry) Assertions.assertNotNull(this.mRegistry), (JSBundleLoader) Assertions.assertNotNull(this.mJSBundleLoader), (NativeModuleCallExceptionHandler) Assertions.assertNotNull(this.mNativeModuleCallExceptionHandler));
        }
    }

    private static class JSProfilerTraceListener implements TraceListener {
        private final WeakReference<CatalystInstanceImpl> mOuter;

        public JSProfilerTraceListener(CatalystInstanceImpl outer) {
            this.mOuter = new WeakReference<>(outer);
        }

        public void onTraceStarted() {
            CatalystInstanceImpl impl = (CatalystInstanceImpl) this.mOuter.get();
            if (impl != null) {
                ((Systrace) impl.getJSModule(Systrace.class)).setEnabled(true);
            }
        }

        public void onTraceStopped() {
            CatalystInstanceImpl impl = (CatalystInstanceImpl) this.mOuter.get();
            if (impl != null) {
                ((Systrace) impl.getJSModule(Systrace.class)).setEnabled(false);
            }
        }
    }

    private class NativeExceptionHandler implements QueueThreadExceptionHandler {
        private NativeExceptionHandler() {
        }

        public void handleException(Exception e) {
            CatalystInstanceImpl.this.onNativeException(e);
        }
    }

    public static class PendingJSCall {
        @Nullable
        public NativeArray mArguments;
        public String mMethod;
        public String mModule;

        public PendingJSCall(String module, String method, @Nullable NativeArray arguments) {
            this.mModule = module;
            this.mMethod = method;
            this.mArguments = arguments;
        }

        /* access modifiers changed from: 0000 */
        public void call(CatalystInstanceImpl catalystInstance) {
            catalystInstance.jniCallJSFunction(this.mModule, this.mMethod, this.mArguments != null ? this.mArguments : new WritableNativeArray());
        }

        public String toString() {
            String nativeArray;
            StringBuilder append = new StringBuilder().append(this.mModule).append(".").append(this.mMethod).append("(");
            if (this.mArguments == null) {
                nativeArray = "";
            } else {
                nativeArray = this.mArguments.toString();
            }
            return append.append(nativeArray).append(")").toString();
        }
    }

    private native long getJavaScriptContext();

    private static native HybridData initHybrid();

    private native void initializeBridge(ReactCallback reactCallback, JavaScriptExecutor javaScriptExecutor, MessageQueueThread messageQueueThread, MessageQueueThread messageQueueThread2, Collection<JavaModuleWrapper> collection, Collection<ModuleHolder> collection2);

    private native void jniCallJSCallback(int i, NativeArray nativeArray);

    /* access modifiers changed from: private */
    public native void jniCallJSFunction(String str, String str2, NativeArray nativeArray);

    private native void jniExtendNativeModules(Collection<JavaModuleWrapper> collection, Collection<ModuleHolder> collection2);

    private native void jniHandleMemoryPressure(int i);

    private native void jniLoadScriptFromAssets(AssetManager assetManager, String str, boolean z);

    private native void jniLoadScriptFromFile(String str, String str2, boolean z);

    private native void jniRegisterSegment(int i, String str);

    private native void jniSetSourceURL(String str);

    public native void setGlobalVariable(String str, String str2);

    static {
        ReactBridge.staticInit();
    }

    private CatalystInstanceImpl(ReactQueueConfigurationSpec reactQueueConfigurationSpec, JavaScriptExecutor jsExecutor, NativeModuleRegistry nativeModuleRegistry, JSBundleLoader jsBundleLoader, NativeModuleCallExceptionHandler nativeModuleCallExceptionHandler) {
        this.mPendingJSCalls = new AtomicInteger(0);
        this.mJsPendingCallsTitleForTrace = "pending_js_calls_instance" + sNextInstanceIdForTrace.getAndIncrement();
        this.mDestroyed = false;
        this.mJSCallsPendingInit = new ArrayList<>();
        this.mJSCallsPendingInitLock = new Object();
        this.mInitialized = false;
        this.mAcceptCalls = false;
        Log.d(ReactConstants.TAG, "Initializing React Xplat Bridge.");
        this.mHybridData = initHybrid();
        this.mReactQueueConfiguration = ReactQueueConfigurationImpl.create(reactQueueConfigurationSpec, new NativeExceptionHandler());
        this.mBridgeIdleListeners = new CopyOnWriteArrayList<>();
        this.mNativeModuleRegistry = nativeModuleRegistry;
        this.mJSModuleRegistry = new JavaScriptModuleRegistry();
        this.mJSBundleLoader = jsBundleLoader;
        this.mNativeModuleCallExceptionHandler = nativeModuleCallExceptionHandler;
        this.mNativeModulesQueueThread = this.mReactQueueConfiguration.getNativeModulesQueueThread();
        this.mTraceListener = new JSProfilerTraceListener(this);
        Log.d(ReactConstants.TAG, "Initializing React Xplat Bridge before initializeBridge");
        initializeBridge(new BridgeCallback(this), jsExecutor, this.mReactQueueConfiguration.getJSQueueThread(), this.mNativeModulesQueueThread, this.mNativeModuleRegistry.getJavaModules(this), this.mNativeModuleRegistry.getCxxModules());
        Log.d(ReactConstants.TAG, "Initializing React Xplat Bridge after initializeBridge");
        this.mJavaScriptContextHolder = new JavaScriptContextHolder(getJavaScriptContext());
    }

    public void extendNativeModules(NativeModuleRegistry modules) {
        this.mNativeModuleRegistry.registerModules(modules);
        jniExtendNativeModules(modules.getJavaModules(this), modules.getCxxModules());
    }

    /* access modifiers changed from: 0000 */
    public void setSourceURLs(String deviceURL, String remoteURL) {
        this.mSourceURL = deviceURL;
        jniSetSourceURL(remoteURL);
    }

    public void registerSegment(int segmentId, String path) {
        jniRegisterSegment(segmentId, path);
    }

    /* access modifiers changed from: 0000 */
    public void loadScriptFromAssets(AssetManager assetManager, String assetURL, boolean loadSynchronously) {
        this.mSourceURL = assetURL;
        jniLoadScriptFromAssets(assetManager, assetURL, loadSynchronously);
    }

    /* access modifiers changed from: 0000 */
    public void loadScriptFromFile(String fileName, String sourceURL, boolean loadSynchronously) {
        this.mSourceURL = sourceURL;
        jniLoadScriptFromFile(fileName, sourceURL, loadSynchronously);
    }

    public void runJSBundle() {
        boolean z = true;
        Log.d(ReactConstants.TAG, "CatalystInstanceImpl.runJSBundle()");
        if (this.mJSBundleHasLoaded) {
            z = false;
        }
        Assertions.assertCondition(z, "JS bundle was already loaded!");
        this.mJSBundleLoader.loadScript(this);
        synchronized (this.mJSCallsPendingInitLock) {
            this.mAcceptCalls = true;
            Iterator it = this.mJSCallsPendingInit.iterator();
            while (it.hasNext()) {
                ((PendingJSCall) it.next()).call(this);
            }
            this.mJSCallsPendingInit.clear();
            this.mJSBundleHasLoaded = true;
        }
        Systrace.registerListener(this.mTraceListener);
    }

    public boolean hasRunJSBundle() {
        boolean z;
        synchronized (this.mJSCallsPendingInitLock) {
            z = this.mJSBundleHasLoaded && this.mAcceptCalls;
        }
        return z;
    }

    @Nullable
    public String getSourceURL() {
        return this.mSourceURL;
    }

    public void callFunction(String module, String method, NativeArray arguments) {
        callFunction(new PendingJSCall(module, method, arguments));
    }

    public void callFunction(PendingJSCall function) {
        if (this.mDestroyed) {
            FLog.w(ReactConstants.TAG, "Calling JS function after bridge has been destroyed: " + function.toString());
            return;
        }
        if (!this.mAcceptCalls) {
            synchronized (this.mJSCallsPendingInitLock) {
                if (!this.mAcceptCalls) {
                    this.mJSCallsPendingInit.add(function);
                    return;
                }
            }
        }
        function.call(this);
    }

    public void invokeCallback(int callbackID, NativeArray arguments) {
        if (this.mDestroyed) {
            FLog.w(ReactConstants.TAG, "Invoking JS callback after bridge has been destroyed.");
        } else {
            jniCallJSCallback(callbackID, arguments);
        }
    }

    public void destroy() {
        Log.d(ReactConstants.TAG, "CatalystInstanceImpl.destroy() start");
        UiThreadUtil.assertOnUiThread();
        if (!this.mDestroyed) {
            ReactMarker.logMarker(ReactMarkerConstants.DESTROY_CATALYST_INSTANCE_START);
            this.mDestroyed = true;
            this.mNativeModulesQueueThread.runOnQueue(new Runnable() {
                public void run() {
                    boolean wasIdle = false;
                    CatalystInstanceImpl.this.mNativeModuleRegistry.notifyJSInstanceDestroy();
                    if (CatalystInstanceImpl.this.mPendingJSCalls.getAndSet(0) == 0) {
                        wasIdle = true;
                    }
                    if (!wasIdle && !CatalystInstanceImpl.this.mBridgeIdleListeners.isEmpty()) {
                        Iterator it = CatalystInstanceImpl.this.mBridgeIdleListeners.iterator();
                        while (it.hasNext()) {
                            ((NotThreadSafeBridgeIdleDebugListener) it.next()).onTransitionToBridgeIdle();
                        }
                    }
                    AsyncTask.execute(new Runnable() {
                        public void run() {
                            CatalystInstanceImpl.this.mJavaScriptContextHolder.clear();
                            CatalystInstanceImpl.this.mHybridData.resetNative();
                            CatalystInstanceImpl.this.getReactQueueConfiguration().destroy();
                            Log.d(ReactConstants.TAG, "CatalystInstanceImpl.destroy() end");
                            ReactMarker.logMarker(ReactMarkerConstants.DESTROY_CATALYST_INSTANCE_END);
                        }
                    });
                }
            });
            Systrace.unregisterListener(this.mTraceListener);
        }
    }

    public boolean isDestroyed() {
        return this.mDestroyed;
    }

    @VisibleForTesting
    public void initialize() {
        Log.d(ReactConstants.TAG, "CatalystInstanceImpl.initialize()");
        Assertions.assertCondition(!this.mInitialized, "This catalyst instance has already been initialized");
        Assertions.assertCondition(this.mAcceptCalls, "RunJSBundle hasn't completed.");
        this.mInitialized = true;
        this.mNativeModulesQueueThread.runOnQueue(new Runnable() {
            public void run() {
                CatalystInstanceImpl.this.mNativeModuleRegistry.notifyJSInstanceInitialized();
            }
        });
    }

    public ReactQueueConfiguration getReactQueueConfiguration() {
        return this.mReactQueueConfiguration;
    }

    public <T extends JavaScriptModule> T getJSModule(Class<T> jsInterface) {
        return this.mJSModuleRegistry.getJavaScriptModule(this, jsInterface);
    }

    public <T extends NativeModule> boolean hasNativeModule(Class<T> nativeModuleInterface) {
        return this.mNativeModuleRegistry.hasModule(nativeModuleInterface);
    }

    public <T extends NativeModule> T getNativeModule(Class<T> nativeModuleInterface) {
        return this.mNativeModuleRegistry.getModule(nativeModuleInterface);
    }

    public Collection<NativeModule> getNativeModules() {
        return this.mNativeModuleRegistry.getAllModules();
    }

    public void handleMemoryPressure(int level) {
        if (!this.mDestroyed) {
            jniHandleMemoryPressure(level);
        }
    }

    public void addBridgeIdleDebugListener(NotThreadSafeBridgeIdleDebugListener listener) {
        this.mBridgeIdleListeners.add(listener);
    }

    public void removeBridgeIdleDebugListener(NotThreadSafeBridgeIdleDebugListener listener) {
        this.mBridgeIdleListeners.remove(listener);
    }

    public JavaScriptContextHolder getJavaScriptContextHolder() {
        return this.mJavaScriptContextHolder;
    }

    /* access modifiers changed from: private */
    public void incrementPendingJSCalls() {
        int oldPendingCalls = this.mPendingJSCalls.getAndIncrement();
        boolean wasIdle = oldPendingCalls == 0;
        Systrace.traceCounter(0, this.mJsPendingCallsTitleForTrace, oldPendingCalls + 1);
        if (wasIdle && !this.mBridgeIdleListeners.isEmpty()) {
            this.mNativeModulesQueueThread.runOnQueue(new Runnable() {
                public void run() {
                    Iterator it = CatalystInstanceImpl.this.mBridgeIdleListeners.iterator();
                    while (it.hasNext()) {
                        ((NotThreadSafeBridgeIdleDebugListener) it.next()).onTransitionToBridgeBusy();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void decrementPendingJSCalls() {
        int newPendingCalls = this.mPendingJSCalls.decrementAndGet();
        boolean isNowIdle = newPendingCalls == 0;
        Systrace.traceCounter(0, this.mJsPendingCallsTitleForTrace, newPendingCalls);
        if (isNowIdle && !this.mBridgeIdleListeners.isEmpty()) {
            this.mNativeModulesQueueThread.runOnQueue(new Runnable() {
                public void run() {
                    Iterator it = CatalystInstanceImpl.this.mBridgeIdleListeners.iterator();
                    while (it.hasNext()) {
                        ((NotThreadSafeBridgeIdleDebugListener) it.next()).onTransitionToBridgeIdle();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void onNativeException(Exception e) {
        this.mNativeModuleCallExceptionHandler.handleException(e);
        this.mReactQueueConfiguration.getUIQueueThread().runOnQueue(new Runnable() {
            public void run() {
                CatalystInstanceImpl.this.destroy();
            }
        });
    }
}
