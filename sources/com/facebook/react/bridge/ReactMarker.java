package com.facebook.react.bridge;

import com.facebook.proguard.annotations.DoNotStrip;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;

@DoNotStrip
public class ReactMarker {
    private static final List<MarkerListener> sListeners = new ArrayList();

    public interface MarkerListener {
        void logMarker(ReactMarkerConstants reactMarkerConstants, @Nullable String str, int i);
    }

    @DoNotStrip
    public static void addListener(MarkerListener listener) {
        synchronized (sListeners) {
            if (!sListeners.contains(listener)) {
                sListeners.add(listener);
            }
        }
    }

    @DoNotStrip
    public static void removeListener(MarkerListener listener) {
        synchronized (sListeners) {
            sListeners.remove(listener);
        }
    }

    @DoNotStrip
    public static void clearMarkerListeners() {
        synchronized (sListeners) {
            sListeners.clear();
        }
    }

    @DoNotStrip
    public static void logMarker(String name) {
        logMarker(name, (String) null);
    }

    @DoNotStrip
    public static void logMarker(String name, int instanceKey) {
        logMarker(name, (String) null, instanceKey);
    }

    @DoNotStrip
    public static void logMarker(String name, @Nullable String tag) {
        logMarker(name, tag, 0);
    }

    @DoNotStrip
    public static void logMarker(String name, @Nullable String tag, int instanceKey) {
        logMarker(ReactMarkerConstants.valueOf(name), tag, instanceKey);
    }

    @DoNotStrip
    public static void logMarker(ReactMarkerConstants name) {
        logMarker(name, (String) null, 0);
    }

    @DoNotStrip
    public static void logMarker(ReactMarkerConstants name, int instanceKey) {
        logMarker(name, (String) null, instanceKey);
    }

    @DoNotStrip
    public static void logMarker(ReactMarkerConstants name, @Nullable String tag) {
        logMarker(name, tag, 0);
    }

    @DoNotStrip
    public static void logMarker(ReactMarkerConstants name, @Nullable String tag, int instanceKey) {
        synchronized (sListeners) {
            for (MarkerListener listener : sListeners) {
                listener.logMarker(name, tag, instanceKey);
            }
        }
    }
}
