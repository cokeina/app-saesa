package com.facebook.react.bridge;

import com.facebook.jni.HybridData;
import com.facebook.proguard.annotations.DoNotStrip;

@DoNotStrip
public class CxxModuleWrapperBase implements NativeModule {
    @DoNotStrip
    private HybridData mHybridData;

    public native String getName();

    static {
        ReactBridge.staticInit();
    }

    public void initialize() {
    }

    public boolean canOverrideExistingModule() {
        return false;
    }

    public void onCatalystInstanceDestroy() {
        this.mHybridData.resetNative();
    }

    protected CxxModuleWrapperBase(HybridData hd) {
        this.mHybridData = hd;
    }

    /* access modifiers changed from: protected */
    public void resetModule(HybridData hd) {
        if (hd != this.mHybridData) {
            this.mHybridData.resetNative();
            this.mHybridData = hd;
        }
    }
}
