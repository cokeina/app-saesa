package com.facebook.react.bridge;

import com.facebook.infer.annotation.Assertions;
import com.facebook.systrace.Systrace;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class NativeModuleRegistry {
    private final ArrayList<ModuleHolder> mBatchCompleteListenerModules;
    private final Map<Class<? extends NativeModule>, ModuleHolder> mModules;
    private final ReactApplicationContext mReactApplicationContext;

    public NativeModuleRegistry(ReactApplicationContext reactApplicationContext, Map<Class<? extends NativeModule>, ModuleHolder> modules, ArrayList<ModuleHolder> batchCompleteListenerModules) {
        this.mReactApplicationContext = reactApplicationContext;
        this.mModules = modules;
        this.mBatchCompleteListenerModules = batchCompleteListenerModules;
    }

    private Map<Class<? extends NativeModule>, ModuleHolder> getModuleMap() {
        return this.mModules;
    }

    private ReactApplicationContext getReactApplicationContext() {
        return this.mReactApplicationContext;
    }

    private ArrayList<ModuleHolder> getBatchCompleteListenerModules() {
        return this.mBatchCompleteListenerModules;
    }

    /* access modifiers changed from: 0000 */
    public Collection<JavaModuleWrapper> getJavaModules(JSInstance jsInstance) {
        ArrayList<JavaModuleWrapper> javaModules = new ArrayList<>();
        for (Entry<Class<? extends NativeModule>, ModuleHolder> entry : this.mModules.entrySet()) {
            Class<? extends NativeModule> type = (Class) entry.getKey();
            if (!CxxModuleWrapperBase.class.isAssignableFrom(type)) {
                javaModules.add(new JavaModuleWrapper(jsInstance, type, (ModuleHolder) entry.getValue()));
            }
        }
        return javaModules;
    }

    /* access modifiers changed from: 0000 */
    public Collection<ModuleHolder> getCxxModules() {
        ArrayList<ModuleHolder> cxxModules = new ArrayList<>();
        for (Entry<Class<? extends NativeModule>, ModuleHolder> entry : this.mModules.entrySet()) {
            if (CxxModuleWrapperBase.class.isAssignableFrom((Class) entry.getKey())) {
                cxxModules.add(entry.getValue());
            }
        }
        return cxxModules;
    }

    /* access modifiers changed from: 0000 */
    public void registerModules(NativeModuleRegistry newRegister) {
        Assertions.assertCondition(this.mReactApplicationContext.equals(newRegister.getReactApplicationContext()), "Extending native modules with non-matching application contexts.");
        Map<Class<? extends NativeModule>, ModuleHolder> newModules = newRegister.getModuleMap();
        ArrayList<ModuleHolder> batchCompleteListeners = newRegister.getBatchCompleteListenerModules();
        for (Entry<Class<? extends NativeModule>, ModuleHolder> entry : newModules.entrySet()) {
            Class<? extends NativeModule> key = (Class) entry.getKey();
            if (!this.mModules.containsKey(key)) {
                ModuleHolder value = (ModuleHolder) entry.getValue();
                if (batchCompleteListeners.contains(value)) {
                    this.mBatchCompleteListenerModules.add(value);
                }
                this.mModules.put(key, value);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void notifyJSInstanceDestroy() {
        this.mReactApplicationContext.assertOnNativeModulesQueueThread();
        Systrace.beginSection(0, "NativeModuleRegistry_notifyJSInstanceDestroy");
        try {
            for (ModuleHolder module : this.mModules.values()) {
                module.destroy();
            }
        } finally {
            Systrace.endSection(0);
        }
    }

    /* access modifiers changed from: 0000 */
    public void notifyJSInstanceInitialized() {
        this.mReactApplicationContext.assertOnNativeModulesQueueThread("From version React Native v0.44, native modules are explicitly not initialized on the UI thread. See https://github.com/facebook/react-native/wiki/Breaking-Changes#d4611211-reactnativeandroidbreaking-move-nativemodule-initialization-off-ui-thread---aaachiuuu  for more details.");
        ReactMarker.logMarker(ReactMarkerConstants.NATIVE_MODULE_INITIALIZE_START);
        Systrace.beginSection(0, "NativeModuleRegistry_notifyJSInstanceInitialized");
        try {
            for (ModuleHolder module : this.mModules.values()) {
                module.markInitializable();
            }
        } finally {
            Systrace.endSection(0);
            ReactMarker.logMarker(ReactMarkerConstants.NATIVE_MODULE_INITIALIZE_END);
        }
    }

    public void onBatchComplete() {
        Iterator it = this.mBatchCompleteListenerModules.iterator();
        while (it.hasNext()) {
            ModuleHolder moduleHolder = (ModuleHolder) it.next();
            if (moduleHolder.hasInstance()) {
                ((OnBatchCompleteListener) moduleHolder.getModule()).onBatchComplete();
            }
        }
    }

    public <T extends NativeModule> boolean hasModule(Class<T> moduleInterface) {
        return this.mModules.containsKey(moduleInterface);
    }

    public <T extends NativeModule> T getModule(Class<T> moduleInterface) {
        return ((ModuleHolder) Assertions.assertNotNull(this.mModules.get(moduleInterface))).getModule();
    }

    public List<NativeModule> getAllModules() {
        List<NativeModule> modules = new ArrayList<>();
        for (ModuleHolder module : this.mModules.values()) {
            modules.add(module.getModule());
        }
        return modules;
    }
}
