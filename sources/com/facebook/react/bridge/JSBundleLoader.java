package com.facebook.react.bridge;

import android.content.Context;
import com.facebook.react.common.DebugServerException;

public abstract class JSBundleLoader {
    public abstract String loadScript(CatalystInstanceImpl catalystInstanceImpl);

    public static JSBundleLoader createAssetLoader(final Context context, final String assetUrl, final boolean loadSynchronously) {
        return new JSBundleLoader() {
            public String loadScript(CatalystInstanceImpl instance) {
                instance.loadScriptFromAssets(context.getAssets(), assetUrl, loadSynchronously);
                return assetUrl;
            }
        };
    }

    public static JSBundleLoader createFileLoader(String fileName) {
        return createFileLoader(fileName, fileName, false);
    }

    public static JSBundleLoader createFileLoader(final String fileName, final String assetUrl, final boolean loadSynchronously) {
        return new JSBundleLoader() {
            public String loadScript(CatalystInstanceImpl instance) {
                instance.loadScriptFromFile(fileName, assetUrl, loadSynchronously);
                return fileName;
            }
        };
    }

    public static JSBundleLoader createCachedBundleFromNetworkLoader(final String sourceURL, final String cachedFileLocation) {
        return new JSBundleLoader() {
            public String loadScript(CatalystInstanceImpl instance) {
                try {
                    instance.loadScriptFromFile(cachedFileLocation, sourceURL, false);
                    return sourceURL;
                } catch (Exception e) {
                    throw DebugServerException.makeGeneric(e.getMessage(), e);
                }
            }
        };
    }

    public static JSBundleLoader createRemoteDebuggerBundleLoader(final String proxySourceURL, final String realSourceURL) {
        return new JSBundleLoader() {
            public String loadScript(CatalystInstanceImpl instance) {
                instance.setSourceURLs(realSourceURL, proxySourceURL);
                return realSourceURL;
            }
        };
    }
}
