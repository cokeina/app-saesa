package com.facebook.react.bridge;

import com.facebook.proguard.annotations.DoNotStrip;
import com.facebook.react.bridge.NativeModule.NativeMethod;
import com.facebook.systrace.Systrace;
import com.facebook.systrace.SystraceMessage;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;

@DoNotStrip
public class JavaModuleWrapper {
    private final ArrayList<MethodDescriptor> mDescs = new ArrayList<>();
    private final JSInstance mJSInstance;
    private final ArrayList<NativeMethod> mMethods = new ArrayList<>();
    private final Class<? extends NativeModule> mModuleClass;
    private final ModuleHolder mModuleHolder;

    @DoNotStrip
    public class MethodDescriptor {
        @DoNotStrip
        Method method;
        @DoNotStrip
        String name;
        @DoNotStrip
        String signature;
        @DoNotStrip
        String type;

        public MethodDescriptor() {
        }
    }

    public JavaModuleWrapper(JSInstance jsInstance, Class<? extends NativeModule> moduleClass, ModuleHolder moduleHolder) {
        this.mJSInstance = jsInstance;
        this.mModuleHolder = moduleHolder;
        this.mModuleClass = moduleClass;
    }

    @DoNotStrip
    public BaseJavaModule getModule() {
        return (BaseJavaModule) this.mModuleHolder.getModule();
    }

    @DoNotStrip
    public String getName() {
        return this.mModuleHolder.getName();
    }

    @DoNotStrip
    private void findMethods() {
        Method[] targetMethods;
        Systrace.beginSection(0, "findMethods");
        Set<String> methodNames = new HashSet<>();
        Class<? extends NativeModule> classForMethods = this.mModuleClass;
        Class<? extends NativeModule> superClass = this.mModuleClass.getSuperclass();
        if (ReactModuleWithSpec.class.isAssignableFrom(superClass)) {
            classForMethods = superClass;
        }
        for (Method targetMethod : classForMethods.getDeclaredMethods()) {
            ReactMethod annotation = (ReactMethod) targetMethod.getAnnotation(ReactMethod.class);
            if (annotation != null) {
                String methodName = targetMethod.getName();
                if (methodNames.contains(methodName)) {
                    throw new IllegalArgumentException("Java Module " + getName() + " method name already registered: " + methodName);
                }
                MethodDescriptor md = new MethodDescriptor();
                JavaMethodWrapper method = new JavaMethodWrapper(this, targetMethod, annotation.isBlockingSynchronousMethod());
                md.name = methodName;
                md.type = method.getType();
                if (md.type == BaseJavaModule.METHOD_TYPE_SYNC) {
                    md.signature = method.getSignature();
                    md.method = targetMethod;
                }
                this.mMethods.add(method);
                this.mDescs.add(md);
            }
        }
        Systrace.endSection(0);
    }

    @DoNotStrip
    public List<MethodDescriptor> getMethodDescriptors() {
        if (this.mDescs.isEmpty()) {
            findMethods();
        }
        return this.mDescs;
    }

    @DoNotStrip
    @Nullable
    public NativeMap getConstants() {
        if (!this.mModuleHolder.getHasConstants()) {
            return null;
        }
        String moduleName = getName();
        SystraceMessage.beginSection(0, "JavaModuleWrapper.getConstants").arg("moduleName", (Object) moduleName).flush();
        ReactMarker.logMarker(ReactMarkerConstants.GET_CONSTANTS_START, moduleName);
        BaseJavaModule baseJavaModule = getModule();
        Systrace.beginSection(0, "module.getConstants");
        Map<String, Object> map = baseJavaModule.getConstants();
        Systrace.endSection(0);
        Systrace.beginSection(0, "create WritableNativeMap");
        ReactMarker.logMarker(ReactMarkerConstants.CONVERT_CONSTANTS_START, moduleName);
        try {
            return Arguments.makeNativeMap(map);
        } finally {
            ReactMarker.logMarker(ReactMarkerConstants.CONVERT_CONSTANTS_END);
            Systrace.endSection(0);
            ReactMarker.logMarker(ReactMarkerConstants.GET_CONSTANTS_END);
            SystraceMessage.endSection(0).flush();
        }
    }

    @DoNotStrip
    public void invoke(int methodId, ReadableNativeArray parameters) {
        if (this.mMethods != null && methodId < this.mMethods.size()) {
            ((NativeMethod) this.mMethods.get(methodId)).invoke(this.mJSInstance, parameters);
        }
    }
}
