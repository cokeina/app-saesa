package com.facebook.react.bridge;

import com.facebook.debug.holder.PrinterHolder;
import com.facebook.debug.tags.ReactDebugOverlayTags;
import com.facebook.infer.annotation.Assertions;
import com.facebook.react.bridge.NativeModule.NativeMethod;
import com.facebook.systrace.SystraceMessage;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.annotation.Nullable;

public class JavaMethodWrapper implements NativeMethod {
    private static final ArgumentExtractor<ReadableNativeArray> ARGUMENT_EXTRACTOR_ARRAY = new ArgumentExtractor<ReadableNativeArray>() {
        public ReadableNativeArray extractArgument(JSInstance jsInstance, ReadableNativeArray jsArguments, int atIndex) {
            return jsArguments.getArray(atIndex);
        }
    };
    private static final ArgumentExtractor<Boolean> ARGUMENT_EXTRACTOR_BOOLEAN = new ArgumentExtractor<Boolean>() {
        public Boolean extractArgument(JSInstance jsInstance, ReadableNativeArray jsArguments, int atIndex) {
            return Boolean.valueOf(jsArguments.getBoolean(atIndex));
        }
    };
    /* access modifiers changed from: private */
    public static final ArgumentExtractor<Callback> ARGUMENT_EXTRACTOR_CALLBACK = new ArgumentExtractor<Callback>() {
        @Nullable
        public Callback extractArgument(JSInstance jsInstance, ReadableNativeArray jsArguments, int atIndex) {
            if (jsArguments.isNull(atIndex)) {
                return null;
            }
            return new CallbackImpl(jsInstance, (int) jsArguments.getDouble(atIndex));
        }
    };
    private static final ArgumentExtractor<Double> ARGUMENT_EXTRACTOR_DOUBLE = new ArgumentExtractor<Double>() {
        public Double extractArgument(JSInstance jsInstance, ReadableNativeArray jsArguments, int atIndex) {
            return Double.valueOf(jsArguments.getDouble(atIndex));
        }
    };
    private static final ArgumentExtractor<Dynamic> ARGUMENT_EXTRACTOR_DYNAMIC = new ArgumentExtractor<Dynamic>() {
        public Dynamic extractArgument(JSInstance jsInstance, ReadableNativeArray jsArguments, int atIndex) {
            return DynamicFromArray.create(jsArguments, atIndex);
        }
    };
    private static final ArgumentExtractor<Float> ARGUMENT_EXTRACTOR_FLOAT = new ArgumentExtractor<Float>() {
        public Float extractArgument(JSInstance jsInstance, ReadableNativeArray jsArguments, int atIndex) {
            return Float.valueOf((float) jsArguments.getDouble(atIndex));
        }
    };
    private static final ArgumentExtractor<Integer> ARGUMENT_EXTRACTOR_INTEGER = new ArgumentExtractor<Integer>() {
        public Integer extractArgument(JSInstance jsInstance, ReadableNativeArray jsArguments, int atIndex) {
            return Integer.valueOf((int) jsArguments.getDouble(atIndex));
        }
    };
    private static final ArgumentExtractor<ReadableMap> ARGUMENT_EXTRACTOR_MAP = new ArgumentExtractor<ReadableMap>() {
        public ReadableMap extractArgument(JSInstance jsInstance, ReadableNativeArray jsArguments, int atIndex) {
            return jsArguments.getMap(atIndex);
        }
    };
    private static final ArgumentExtractor<Promise> ARGUMENT_EXTRACTOR_PROMISE = new ArgumentExtractor<Promise>() {
        public int getJSArgumentsNeeded() {
            return 2;
        }

        public Promise extractArgument(JSInstance jsInstance, ReadableNativeArray jsArguments, int atIndex) {
            return new PromiseImpl((Callback) JavaMethodWrapper.ARGUMENT_EXTRACTOR_CALLBACK.extractArgument(jsInstance, jsArguments, atIndex), (Callback) JavaMethodWrapper.ARGUMENT_EXTRACTOR_CALLBACK.extractArgument(jsInstance, jsArguments, atIndex + 1));
        }
    };
    private static final ArgumentExtractor<String> ARGUMENT_EXTRACTOR_STRING = new ArgumentExtractor<String>() {
        public String extractArgument(JSInstance jsInstance, ReadableNativeArray jsArguments, int atIndex) {
            return jsArguments.getString(atIndex);
        }
    };
    private static final boolean DEBUG = PrinterHolder.getPrinter().shouldDisplayLogMessage(ReactDebugOverlayTags.BRIDGE_CALLS);
    @Nullable
    private ArgumentExtractor[] mArgumentExtractors;
    @Nullable
    private Object[] mArguments;
    private boolean mArgumentsProcessed = false;
    @Nullable
    private int mJSArgumentsNeeded;
    private final Method mMethod;
    private final JavaModuleWrapper mModuleWrapper;
    private final int mParamLength;
    private final Class[] mParameterTypes;
    @Nullable
    private String mSignature;
    private String mType = BaseJavaModule.METHOD_TYPE_ASYNC;

    private static abstract class ArgumentExtractor<T> {
        @Nullable
        public abstract T extractArgument(JSInstance jSInstance, ReadableNativeArray readableNativeArray, int i);

        private ArgumentExtractor() {
        }

        public int getJSArgumentsNeeded() {
            return 1;
        }
    }

    private static char paramTypeToChar(Class paramClass) {
        char tryCommon = commonTypeToChar(paramClass);
        if (tryCommon != 0) {
            return tryCommon;
        }
        if (paramClass == Callback.class) {
            return 'X';
        }
        if (paramClass == Promise.class) {
            return 'P';
        }
        if (paramClass == ReadableMap.class) {
            return 'M';
        }
        if (paramClass == ReadableArray.class) {
            return 'A';
        }
        if (paramClass == Dynamic.class) {
            return 'Y';
        }
        throw new RuntimeException("Got unknown param class: " + paramClass.getSimpleName());
    }

    private static char returnTypeToChar(Class returnClass) {
        char tryCommon = commonTypeToChar(returnClass);
        if (tryCommon != 0) {
            return tryCommon;
        }
        if (returnClass == Void.TYPE) {
            return 'v';
        }
        if (returnClass == WritableMap.class) {
            return 'M';
        }
        if (returnClass == WritableArray.class) {
            return 'A';
        }
        throw new RuntimeException("Got unknown return class: " + returnClass.getSimpleName());
    }

    private static char commonTypeToChar(Class typeClass) {
        if (typeClass == Boolean.TYPE) {
            return 'z';
        }
        if (typeClass == Boolean.class) {
            return 'Z';
        }
        if (typeClass == Integer.TYPE) {
            return 'i';
        }
        if (typeClass == Integer.class) {
            return 'I';
        }
        if (typeClass == Double.TYPE) {
            return 'd';
        }
        if (typeClass == Double.class) {
            return 'D';
        }
        if (typeClass == Float.TYPE) {
            return 'f';
        }
        if (typeClass == Float.class) {
            return 'F';
        }
        if (typeClass == String.class) {
            return 'S';
        }
        return 0;
    }

    public JavaMethodWrapper(JavaModuleWrapper module, Method method, boolean isSync) {
        this.mModuleWrapper = module;
        this.mMethod = method;
        this.mMethod.setAccessible(true);
        this.mParameterTypes = this.mMethod.getParameterTypes();
        this.mParamLength = this.mParameterTypes.length;
        if (isSync) {
            this.mType = BaseJavaModule.METHOD_TYPE_SYNC;
        } else if (this.mParamLength > 0 && this.mParameterTypes[this.mParamLength - 1] == Promise.class) {
            this.mType = BaseJavaModule.METHOD_TYPE_PROMISE;
        }
    }

    private void processArguments() {
        if (!this.mArgumentsProcessed) {
            SystraceMessage.beginSection(0, "processArguments").arg("method", (Object) this.mModuleWrapper.getName() + "." + this.mMethod.getName()).flush();
            try {
                this.mArgumentsProcessed = true;
                this.mArgumentExtractors = buildArgumentExtractors(this.mParameterTypes);
                this.mSignature = buildSignature(this.mMethod, this.mParameterTypes, this.mType.equals(BaseJavaModule.METHOD_TYPE_SYNC));
                this.mArguments = new Object[this.mParameterTypes.length];
                this.mJSArgumentsNeeded = calculateJSArgumentsNeeded();
            } finally {
                SystraceMessage.endSection(0).flush();
            }
        }
    }

    public Method getMethod() {
        return this.mMethod;
    }

    public String getSignature() {
        if (!this.mArgumentsProcessed) {
            processArguments();
        }
        return (String) Assertions.assertNotNull(this.mSignature);
    }

    private String buildSignature(Method method, Class[] paramTypes, boolean isSync) {
        StringBuilder builder = new StringBuilder(paramTypes.length + 2);
        if (isSync) {
            builder.append(returnTypeToChar(method.getReturnType()));
            builder.append('.');
        } else {
            builder.append("v.");
        }
        int i = 0;
        while (i < paramTypes.length) {
            Class paramClass = paramTypes[i];
            if (paramClass == Promise.class) {
                Assertions.assertCondition(i == paramTypes.length + -1, "Promise must be used as last parameter only");
            }
            builder.append(paramTypeToChar(paramClass));
            i++;
        }
        return builder.toString();
    }

    private ArgumentExtractor[] buildArgumentExtractors(Class[] paramTypes) {
        ArgumentExtractor[] argumentExtractors = new ArgumentExtractor[paramTypes.length];
        int i = 0;
        while (i < paramTypes.length) {
            Class argumentClass = paramTypes[i];
            if (argumentClass == Boolean.class || argumentClass == Boolean.TYPE) {
                argumentExtractors[i] = ARGUMENT_EXTRACTOR_BOOLEAN;
            } else if (argumentClass == Integer.class || argumentClass == Integer.TYPE) {
                argumentExtractors[i] = ARGUMENT_EXTRACTOR_INTEGER;
            } else if (argumentClass == Double.class || argumentClass == Double.TYPE) {
                argumentExtractors[i] = ARGUMENT_EXTRACTOR_DOUBLE;
            } else if (argumentClass == Float.class || argumentClass == Float.TYPE) {
                argumentExtractors[i] = ARGUMENT_EXTRACTOR_FLOAT;
            } else if (argumentClass == String.class) {
                argumentExtractors[i] = ARGUMENT_EXTRACTOR_STRING;
            } else if (argumentClass == Callback.class) {
                argumentExtractors[i] = ARGUMENT_EXTRACTOR_CALLBACK;
            } else if (argumentClass == Promise.class) {
                argumentExtractors[i] = ARGUMENT_EXTRACTOR_PROMISE;
                Assertions.assertCondition(i == paramTypes.length + -1, "Promise must be used as last parameter only");
            } else if (argumentClass == ReadableMap.class) {
                argumentExtractors[i] = ARGUMENT_EXTRACTOR_MAP;
            } else if (argumentClass == ReadableArray.class) {
                argumentExtractors[i] = ARGUMENT_EXTRACTOR_ARRAY;
            } else if (argumentClass == Dynamic.class) {
                argumentExtractors[i] = ARGUMENT_EXTRACTOR_DYNAMIC;
            } else {
                throw new RuntimeException("Got unknown argument class: " + argumentClass.getSimpleName());
            }
            i += argumentExtractors[i].getJSArgumentsNeeded();
        }
        return argumentExtractors;
    }

    private int calculateJSArgumentsNeeded() {
        int n = 0;
        for (ArgumentExtractor extractor : (ArgumentExtractor[]) Assertions.assertNotNull(this.mArgumentExtractors)) {
            n += extractor.getJSArgumentsNeeded();
        }
        return n;
    }

    private String getAffectedRange(int startIndex, int jsArgumentsNeeded) {
        return jsArgumentsNeeded > 1 ? "" + startIndex + "-" + ((startIndex + jsArgumentsNeeded) - 1) : "" + startIndex;
    }

    public void invoke(JSInstance jsInstance, ReadableNativeArray parameters) {
        int i;
        int jsArgumentsConsumed;
        String traceName = this.mModuleWrapper.getName() + "." + this.mMethod.getName();
        SystraceMessage.beginSection(0, "callJavaModuleMethod").arg("method", (Object) traceName).flush();
        if (DEBUG) {
            PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.BRIDGE_CALLS, "JS->Java: %s.%s()", this.mModuleWrapper.getName(), this.mMethod.getName());
        }
        try {
            if (!this.mArgumentsProcessed) {
                processArguments();
            }
            if (this.mArguments == null || this.mArgumentExtractors == null) {
                throw new Error("processArguments failed");
            } else if (this.mJSArgumentsNeeded != parameters.size()) {
                throw new NativeArgumentsParseException(traceName + " got " + parameters.size() + " arguments, expected " + this.mJSArgumentsNeeded);
            } else {
                i = 0;
                jsArgumentsConsumed = 0;
                while (i < this.mArgumentExtractors.length) {
                    this.mArguments[i] = this.mArgumentExtractors[i].extractArgument(jsInstance, parameters, jsArgumentsConsumed);
                    jsArgumentsConsumed += this.mArgumentExtractors[i].getJSArgumentsNeeded();
                    i++;
                }
                this.mMethod.invoke(this.mModuleWrapper.getModule(), this.mArguments);
                SystraceMessage.endSection(0).flush();
            }
        } catch (IllegalArgumentException ie) {
            throw new RuntimeException("Could not invoke " + traceName, ie);
        } catch (IllegalAccessException iae) {
            throw new RuntimeException("Could not invoke " + traceName, iae);
        } catch (InvocationTargetException ite) {
            if (ite.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) ite.getCause());
            }
            throw new RuntimeException("Could not invoke " + traceName, ite);
        } catch (UnexpectedNativeTypeException e) {
            throw new NativeArgumentsParseException(e.getMessage() + " (constructing arguments for " + traceName + " at argument index " + getAffectedRange(jsArgumentsConsumed, this.mArgumentExtractors[i].getJSArgumentsNeeded()) + ")", e);
        } catch (Throwable th) {
            SystraceMessage.endSection(0).flush();
            throw th;
        }
    }

    public String getType() {
        return this.mType;
    }
}
