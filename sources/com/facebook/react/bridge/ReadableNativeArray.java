package com.facebook.react.bridge;

import com.facebook.infer.annotation.Assertions;
import com.facebook.jni.HybridData;
import com.facebook.proguard.annotations.DoNotStrip;
import java.util.ArrayList;
import java.util.Arrays;
import javax.annotation.Nullable;

@DoNotStrip
public class ReadableNativeArray extends NativeArray implements ReadableArray {
    private static int jniPassCounter = 0;
    private static boolean mUseNativeAccessor = false;
    @Nullable
    private Object[] mLocalArray;
    @Nullable
    private ReadableType[] mLocalTypeArray;

    private native ReadableNativeArray getArrayNative(int i);

    private native boolean getBooleanNative(int i);

    private native double getDoubleNative(int i);

    private native int getIntNative(int i);

    private native ReadableNativeMap getMapNative(int i);

    private native String getStringNative(int i);

    private native ReadableType getTypeNative(int i);

    private native Object[] importArray();

    private native Object[] importTypeArray();

    private native boolean isNullNative(int i);

    private native int sizeNative();

    static {
        ReactBridge.staticInit();
    }

    protected ReadableNativeArray(HybridData hybridData) {
        super(hybridData);
    }

    public static void setUseNativeAccessor(boolean useNativeAccessor) {
        mUseNativeAccessor = useNativeAccessor;
    }

    public static int getJNIPassCounter() {
        return jniPassCounter;
    }

    private Object[] getLocalArray() {
        if (this.mLocalArray != null) {
            return this.mLocalArray;
        }
        synchronized (this) {
            if (this.mLocalArray == null) {
                jniPassCounter++;
                this.mLocalArray = (Object[]) Assertions.assertNotNull(importArray());
            }
        }
        return this.mLocalArray;
    }

    private ReadableType[] getLocalTypeArray() {
        if (this.mLocalTypeArray != null) {
            return this.mLocalTypeArray;
        }
        synchronized (this) {
            if (this.mLocalTypeArray == null) {
                jniPassCounter++;
                Object[] tempArray = (Object[]) Assertions.assertNotNull(importTypeArray());
                this.mLocalTypeArray = (ReadableType[]) Arrays.copyOf(tempArray, tempArray.length, ReadableType[].class);
            }
        }
        return this.mLocalTypeArray;
    }

    public int size() {
        if (!mUseNativeAccessor) {
            return getLocalArray().length;
        }
        jniPassCounter++;
        return sizeNative();
    }

    public boolean isNull(int index) {
        if (!mUseNativeAccessor) {
            return getLocalArray()[index] == null;
        }
        jniPassCounter++;
        return isNullNative(index);
    }

    public boolean getBoolean(int index) {
        if (!mUseNativeAccessor) {
            return ((Boolean) getLocalArray()[index]).booleanValue();
        }
        jniPassCounter++;
        return getBooleanNative(index);
    }

    public double getDouble(int index) {
        if (!mUseNativeAccessor) {
            return ((Double) getLocalArray()[index]).doubleValue();
        }
        jniPassCounter++;
        return getDoubleNative(index);
    }

    public int getInt(int index) {
        if (!mUseNativeAccessor) {
            return ((Double) getLocalArray()[index]).intValue();
        }
        jniPassCounter++;
        return getIntNative(index);
    }

    public String getString(int index) {
        if (!mUseNativeAccessor) {
            return (String) getLocalArray()[index];
        }
        jniPassCounter++;
        return getStringNative(index);
    }

    public ReadableNativeArray getArray(int index) {
        if (!mUseNativeAccessor) {
            return (ReadableNativeArray) getLocalArray()[index];
        }
        jniPassCounter++;
        return getArrayNative(index);
    }

    public ReadableNativeMap getMap(int index) {
        if (!mUseNativeAccessor) {
            return (ReadableNativeMap) getLocalArray()[index];
        }
        jniPassCounter++;
        return getMapNative(index);
    }

    public ReadableType getType(int index) {
        if (!mUseNativeAccessor) {
            return getLocalTypeArray()[index];
        }
        jniPassCounter++;
        return getTypeNative(index);
    }

    public Dynamic getDynamic(int index) {
        return DynamicFromArray.create(this, index);
    }

    public ArrayList<Object> toArrayList() {
        ArrayList<Object> arrayList = new ArrayList<>();
        for (int i = 0; i < size(); i++) {
            switch (getType(i)) {
                case Null:
                    arrayList.add(null);
                    break;
                case Boolean:
                    arrayList.add(Boolean.valueOf(getBoolean(i)));
                    break;
                case Number:
                    arrayList.add(Double.valueOf(getDouble(i)));
                    break;
                case String:
                    arrayList.add(getString(i));
                    break;
                case Map:
                    arrayList.add(getMap(i).toHashMap());
                    break;
                case Array:
                    arrayList.add(getArray(i).toArrayList());
                    break;
                default:
                    throw new IllegalArgumentException("Could not convert object at index: " + i + ".");
            }
        }
        return arrayList;
    }
}
