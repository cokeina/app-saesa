package com.facebook.react.bridge;

import android.os.Bundle;
import java.lang.reflect.Array;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Nullable;

public class Arguments {
    private static Object makeNativeObject(Object object) {
        if (object == null) {
            return null;
        }
        if ((object instanceof Float) || (object instanceof Long) || (object instanceof Byte) || (object instanceof Short)) {
            return new Double(((Number) object).doubleValue());
        }
        if (object.getClass().isArray()) {
            return makeNativeArray(object);
        }
        if (object instanceof List) {
            return makeNativeArray((List) object);
        }
        if (object instanceof Map) {
            return makeNativeMap((Map) object);
        }
        if (object instanceof Bundle) {
            return makeNativeMap((Bundle) object);
        }
        return object;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.Object>, for r6v0, types: [java.util.List, java.util.List<java.lang.Object>] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.react.bridge.WritableNativeArray makeNativeArray(java.util.List<java.lang.Object> r6) {
        /*
            com.facebook.react.bridge.WritableNativeArray r1 = new com.facebook.react.bridge.WritableNativeArray
            r1.<init>()
            if (r6 != 0) goto L_0x0008
        L_0x0007:
            return r1
        L_0x0008:
            java.util.Iterator r2 = r6.iterator()
        L_0x000c:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0007
            java.lang.Object r0 = r2.next()
            java.lang.Object r0 = makeNativeObject(r0)
            if (r0 != 0) goto L_0x0020
            r1.pushNull()
            goto L_0x000c
        L_0x0020:
            boolean r3 = r0 instanceof java.lang.Boolean
            if (r3 == 0) goto L_0x002e
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r3 = r0.booleanValue()
            r1.pushBoolean(r3)
            goto L_0x000c
        L_0x002e:
            boolean r3 = r0 instanceof java.lang.Integer
            if (r3 == 0) goto L_0x003c
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r3 = r0.intValue()
            r1.pushInt(r3)
            goto L_0x000c
        L_0x003c:
            boolean r3 = r0 instanceof java.lang.Double
            if (r3 == 0) goto L_0x004a
            java.lang.Double r0 = (java.lang.Double) r0
            double r4 = r0.doubleValue()
            r1.pushDouble(r4)
            goto L_0x000c
        L_0x004a:
            boolean r3 = r0 instanceof java.lang.String
            if (r3 == 0) goto L_0x0054
            java.lang.String r0 = (java.lang.String) r0
            r1.pushString(r0)
            goto L_0x000c
        L_0x0054:
            boolean r3 = r0 instanceof com.facebook.react.bridge.WritableNativeArray
            if (r3 == 0) goto L_0x005e
            com.facebook.react.bridge.WritableNativeArray r0 = (com.facebook.react.bridge.WritableNativeArray) r0
            r1.pushArray(r0)
            goto L_0x000c
        L_0x005e:
            boolean r3 = r0 instanceof com.facebook.react.bridge.WritableNativeMap
            if (r3 == 0) goto L_0x0068
            com.facebook.react.bridge.WritableNativeMap r0 = (com.facebook.react.bridge.WritableNativeMap) r0
            r1.pushMap(r0)
            goto L_0x000c
        L_0x0068:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Could not convert "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.Class r4 = r0.getClass()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.bridge.Arguments.makeNativeArray(java.util.List):com.facebook.react.bridge.WritableNativeArray");
    }

    public static <T> WritableNativeArray makeNativeArray(final Object objects) {
        if (objects == null) {
            return new WritableNativeArray();
        }
        return makeNativeArray((List) new AbstractList() {
            public int size() {
                return Array.getLength(objects);
            }

            public Object get(int index) {
                return Array.get(objects, index);
            }
        });
    }

    private static void addEntry(WritableNativeMap nativeMap, String key, Object value) {
        Object value2 = makeNativeObject(value);
        if (value2 == null) {
            nativeMap.putNull(key);
        } else if (value2 instanceof Boolean) {
            nativeMap.putBoolean(key, ((Boolean) value2).booleanValue());
        } else if (value2 instanceof Integer) {
            nativeMap.putInt(key, ((Integer) value2).intValue());
        } else if (value2 instanceof Number) {
            nativeMap.putDouble(key, ((Number) value2).doubleValue());
        } else if (value2 instanceof String) {
            nativeMap.putString(key, (String) value2);
        } else if (value2 instanceof WritableNativeArray) {
            nativeMap.putArray(key, (WritableNativeArray) value2);
        } else if (value2 instanceof WritableNativeMap) {
            nativeMap.putMap(key, (WritableNativeMap) value2);
        } else {
            throw new IllegalArgumentException("Could not convert " + value2.getClass());
        }
    }

    public static WritableNativeMap makeNativeMap(Map<String, Object> objects) {
        WritableNativeMap nativeMap = new WritableNativeMap();
        if (objects != null) {
            for (Entry<String, Object> entry : objects.entrySet()) {
                addEntry(nativeMap, (String) entry.getKey(), entry.getValue());
            }
        }
        return nativeMap;
    }

    public static WritableNativeMap makeNativeMap(Bundle bundle) {
        WritableNativeMap nativeMap = new WritableNativeMap();
        if (bundle != null) {
            for (String key : bundle.keySet()) {
                addEntry(nativeMap, key, bundle.get(key));
            }
        }
        return nativeMap;
    }

    public static WritableArray createArray() {
        return new WritableNativeArray();
    }

    public static WritableMap createMap() {
        return new WritableNativeMap();
    }

    public static WritableNativeArray fromJavaArgs(Object[] args) {
        WritableNativeArray arguments = new WritableNativeArray();
        for (Object argument : args) {
            if (argument == null) {
                arguments.pushNull();
            } else {
                Class argumentClass = argument.getClass();
                if (argumentClass == Boolean.class) {
                    arguments.pushBoolean(((Boolean) argument).booleanValue());
                } else if (argumentClass == Integer.class) {
                    arguments.pushDouble(((Integer) argument).doubleValue());
                } else if (argumentClass == Double.class) {
                    arguments.pushDouble(((Double) argument).doubleValue());
                } else if (argumentClass == Float.class) {
                    arguments.pushDouble(((Float) argument).doubleValue());
                } else if (argumentClass == String.class) {
                    arguments.pushString(argument.toString());
                } else if (argumentClass == WritableNativeMap.class) {
                    arguments.pushMap((WritableNativeMap) argument);
                } else if (argumentClass == WritableNativeArray.class) {
                    arguments.pushArray((WritableNativeArray) argument);
                } else {
                    throw new RuntimeException("Cannot convert argument of type " + argumentClass);
                }
            }
        }
        return arguments;
    }

    public static WritableArray fromArray(Object array) {
        int i = 0;
        WritableArray catalystArray = createArray();
        if (array instanceof String[]) {
            String[] strArr = (String[]) array;
            int length = strArr.length;
            while (i < length) {
                catalystArray.pushString(strArr[i]);
                i++;
            }
        } else if (array instanceof Bundle[]) {
            Bundle[] bundleArr = (Bundle[]) array;
            int length2 = bundleArr.length;
            while (i < length2) {
                catalystArray.pushMap(fromBundle(bundleArr[i]));
                i++;
            }
        } else if (array instanceof int[]) {
            int[] iArr = (int[]) array;
            int length3 = iArr.length;
            while (i < length3) {
                catalystArray.pushInt(iArr[i]);
                i++;
            }
        } else if (array instanceof float[]) {
            float[] fArr = (float[]) array;
            int length4 = fArr.length;
            while (i < length4) {
                catalystArray.pushDouble((double) fArr[i]);
                i++;
            }
        } else if (array instanceof double[]) {
            double[] dArr = (double[]) array;
            int length5 = dArr.length;
            while (i < length5) {
                catalystArray.pushDouble(dArr[i]);
                i++;
            }
        } else if (array instanceof boolean[]) {
            boolean[] array2 = (boolean[]) array;
            int length6 = array2.length;
            while (i < length6) {
                catalystArray.pushBoolean(array2[i]);
                i++;
            }
        } else {
            throw new IllegalArgumentException("Unknown array type " + array.getClass());
        }
        return catalystArray;
    }

    public static WritableArray fromList(List list) {
        WritableArray catalystArray = createArray();
        for (Object obj : list) {
            if (obj == null) {
                catalystArray.pushNull();
            } else if (obj.getClass().isArray()) {
                catalystArray.pushArray(fromArray(obj));
            } else if (obj instanceof Bundle) {
                catalystArray.pushMap(fromBundle((Bundle) obj));
            } else if (obj instanceof List) {
                catalystArray.pushArray(fromList((List) obj));
            } else if (obj instanceof String) {
                catalystArray.pushString((String) obj);
            } else if (obj instanceof Integer) {
                catalystArray.pushInt(((Integer) obj).intValue());
            } else if (obj instanceof Number) {
                catalystArray.pushDouble(((Number) obj).doubleValue());
            } else if (obj instanceof Boolean) {
                catalystArray.pushBoolean(((Boolean) obj).booleanValue());
            } else {
                throw new IllegalArgumentException("Unknown value type " + obj.getClass());
            }
        }
        return catalystArray;
    }

    public static WritableMap fromBundle(Bundle bundle) {
        WritableMap map = createMap();
        for (String key : bundle.keySet()) {
            Object value = bundle.get(key);
            if (value == null) {
                map.putNull(key);
            } else if (value.getClass().isArray()) {
                map.putArray(key, fromArray(value));
            } else if (value instanceof String) {
                map.putString(key, (String) value);
            } else if (value instanceof Number) {
                if (value instanceof Integer) {
                    map.putInt(key, ((Integer) value).intValue());
                } else {
                    map.putDouble(key, ((Number) value).doubleValue());
                }
            } else if (value instanceof Boolean) {
                map.putBoolean(key, ((Boolean) value).booleanValue());
            } else if (value instanceof Bundle) {
                map.putMap(key, fromBundle((Bundle) value));
            } else if (value instanceof List) {
                map.putArray(key, fromList((List) value));
            } else {
                throw new IllegalArgumentException("Could not convert " + value.getClass());
            }
        }
        return map;
    }

    @Nullable
    public static ArrayList toList(@Nullable ReadableArray readableArray) {
        if (readableArray == null) {
            return null;
        }
        ArrayList list = new ArrayList();
        for (int i = 0; i < readableArray.size(); i++) {
            switch (readableArray.getType(i)) {
                case Null:
                    list.add(null);
                    break;
                case Boolean:
                    list.add(Boolean.valueOf(readableArray.getBoolean(i)));
                    break;
                case Number:
                    double number = readableArray.getDouble(i);
                    if (number != Math.rint(number)) {
                        list.add(Double.valueOf(number));
                        break;
                    } else {
                        list.add(Integer.valueOf((int) number));
                        break;
                    }
                case String:
                    list.add(readableArray.getString(i));
                    break;
                case Map:
                    list.add(toBundle(readableArray.getMap(i)));
                    break;
                case Array:
                    list.add(toList(readableArray.getArray(i)));
                    break;
                default:
                    throw new IllegalArgumentException("Could not convert object in array.");
            }
        }
        return list;
    }

    @Nullable
    public static Bundle toBundle(@Nullable ReadableMap readableMap) {
        if (readableMap == null) {
            return null;
        }
        ReadableMapKeySetIterator iterator = readableMap.keySetIterator();
        Bundle bundle = new Bundle();
        while (iterator.hasNextKey()) {
            String key = iterator.nextKey();
            switch (readableMap.getType(key)) {
                case Null:
                    bundle.putString(key, null);
                    break;
                case Boolean:
                    bundle.putBoolean(key, readableMap.getBoolean(key));
                    break;
                case Number:
                    bundle.putDouble(key, readableMap.getDouble(key));
                    break;
                case String:
                    bundle.putString(key, readableMap.getString(key));
                    break;
                case Map:
                    bundle.putBundle(key, toBundle(readableMap.getMap(key)));
                    break;
                case Array:
                    bundle.putSerializable(key, toList(readableMap.getArray(key)));
                    break;
                default:
                    throw new IllegalArgumentException("Could not convert object with key: " + key + ".");
            }
        }
        return bundle;
    }
}
