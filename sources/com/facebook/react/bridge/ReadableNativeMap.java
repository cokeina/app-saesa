package com.facebook.react.bridge;

import com.facebook.infer.annotation.Assertions;
import com.facebook.jni.HybridData;
import com.facebook.proguard.annotations.DoNotStrip;
import java.util.HashMap;
import javax.annotation.Nullable;

@DoNotStrip
public class ReadableNativeMap extends NativeMap implements ReadableMap {
    private static int mJniCallCounter;
    private static boolean mUseNativeAccessor;
    @Nullable
    private String[] mKeys;
    @Nullable
    private HashMap<String, Object> mLocalMap;
    @Nullable
    private HashMap<String, ReadableType> mLocalTypeMap;

    @DoNotStrip
    private static class ReadableNativeMapKeySetIterator implements ReadableMapKeySetIterator {
        @DoNotStrip
        private final HybridData mHybridData;
        @DoNotStrip
        private final ReadableNativeMap mMap;

        private static native HybridData initHybrid(ReadableNativeMap readableNativeMap);

        public native boolean hasNextKey();

        public native String nextKey();

        public ReadableNativeMapKeySetIterator(ReadableNativeMap readableNativeMap) {
            this.mMap = readableNativeMap;
            this.mHybridData = initHybrid(readableNativeMap);
        }
    }

    private native ReadableNativeArray getArrayNative(String str);

    private native boolean getBooleanNative(String str);

    private native double getDoubleNative(String str);

    private native int getIntNative(String str);

    private native ReadableNativeMap getMapNative(String str);

    private native String getStringNative(String str);

    private native ReadableType getTypeNative(String str);

    private native boolean hasKeyNative(String str);

    private native String[] importKeys();

    private native Object[] importTypes();

    private native Object[] importValues();

    private native boolean isNullNative(String str);

    static {
        ReactBridge.staticInit();
    }

    protected ReadableNativeMap(HybridData hybridData) {
        super(hybridData);
    }

    public static void setUseNativeAccessor(boolean useNativeAccessor) {
        mUseNativeAccessor = useNativeAccessor;
    }

    public static int getJNIPassCounter() {
        return mJniCallCounter;
    }

    private HashMap<String, Object> getLocalMap() {
        if (this.mLocalMap != null) {
            return this.mLocalMap;
        }
        synchronized (this) {
            if (this.mKeys == null) {
                this.mKeys = (String[]) Assertions.assertNotNull(importKeys());
                mJniCallCounter++;
            }
            if (this.mLocalMap == null) {
                Object[] values = (Object[]) Assertions.assertNotNull(importValues());
                mJniCallCounter++;
                this.mLocalMap = new HashMap<>();
                for (int i = 0; i < this.mKeys.length; i++) {
                    this.mLocalMap.put(this.mKeys[i], values[i]);
                }
            }
        }
        return this.mLocalMap;
    }

    private HashMap<String, ReadableType> getLocalTypeMap() {
        if (this.mLocalTypeMap != null) {
            return this.mLocalTypeMap;
        }
        synchronized (this) {
            if (this.mKeys == null) {
                this.mKeys = (String[]) Assertions.assertNotNull(importKeys());
                mJniCallCounter++;
            }
            if (this.mLocalTypeMap == null) {
                Object[] types = (Object[]) Assertions.assertNotNull(importTypes());
                mJniCallCounter++;
                this.mLocalTypeMap = new HashMap<>();
                for (int i = 0; i < this.mKeys.length; i++) {
                    this.mLocalTypeMap.put(this.mKeys[i], (ReadableType) types[i]);
                }
            }
        }
        return this.mLocalTypeMap;
    }

    public boolean hasKey(String name) {
        if (!mUseNativeAccessor) {
            return getLocalMap().containsKey(name);
        }
        mJniCallCounter++;
        return hasKeyNative(name);
    }

    public boolean isNull(String name) {
        if (mUseNativeAccessor) {
            mJniCallCounter++;
            return isNullNative(name);
        } else if (getLocalMap().containsKey(name)) {
            return getLocalMap().get(name) == null;
        } else {
            throw new NoSuchKeyException(name);
        }
    }

    private Object getValue(String name) {
        if (hasKey(name) && !isNull(name)) {
            return Assertions.assertNotNull(getLocalMap().get(name));
        }
        throw new NoSuchKeyException(name);
    }

    @Nullable
    private Object getNullableValue(String name) {
        if (hasKey(name)) {
            return getLocalMap().get(name);
        }
        throw new NoSuchKeyException(name);
    }

    public boolean getBoolean(String name) {
        if (!mUseNativeAccessor) {
            return ((Boolean) getValue(name)).booleanValue();
        }
        mJniCallCounter++;
        return getBooleanNative(name);
    }

    public double getDouble(String name) {
        if (!mUseNativeAccessor) {
            return ((Double) getValue(name)).doubleValue();
        }
        mJniCallCounter++;
        return getDoubleNative(name);
    }

    public int getInt(String name) {
        if (!mUseNativeAccessor) {
            return ((Double) getValue(name)).intValue();
        }
        mJniCallCounter++;
        return getIntNative(name);
    }

    @Nullable
    public String getString(String name) {
        if (!mUseNativeAccessor) {
            return (String) getNullableValue(name);
        }
        mJniCallCounter++;
        return getStringNative(name);
    }

    @Nullable
    public ReadableArray getArray(String name) {
        if (!mUseNativeAccessor) {
            return (ReadableArray) getNullableValue(name);
        }
        mJniCallCounter++;
        return getArrayNative(name);
    }

    @Nullable
    public ReadableNativeMap getMap(String name) {
        if (!mUseNativeAccessor) {
            return (ReadableNativeMap) getNullableValue(name);
        }
        mJniCallCounter++;
        return getMapNative(name);
    }

    public ReadableType getType(String name) {
        if (mUseNativeAccessor) {
            mJniCallCounter++;
            return getTypeNative(name);
        } else if (getLocalTypeMap().containsKey(name)) {
            return (ReadableType) Assertions.assertNotNull(getLocalTypeMap().get(name));
        } else {
            throw new NoSuchKeyException(name);
        }
    }

    public Dynamic getDynamic(String name) {
        return DynamicFromMap.create(this, name);
    }

    public ReadableMapKeySetIterator keySetIterator() {
        return new ReadableNativeMapKeySetIterator(this);
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> hashMap;
        if (mUseNativeAccessor) {
            ReadableMapKeySetIterator iterator = keySetIterator();
            hashMap = new HashMap<>();
            while (iterator.hasNextKey()) {
                mJniCallCounter++;
                String key = iterator.nextKey();
                mJniCallCounter++;
                switch (getType(key)) {
                    case Null:
                        hashMap.put(key, null);
                        break;
                    case Boolean:
                        hashMap.put(key, Boolean.valueOf(getBoolean(key)));
                        break;
                    case Number:
                        hashMap.put(key, Double.valueOf(getDouble(key)));
                        break;
                    case String:
                        hashMap.put(key, getString(key));
                        break;
                    case Map:
                        hashMap.put(key, ((ReadableNativeMap) Assertions.assertNotNull(getMap(key))).toHashMap());
                        break;
                    case Array:
                        hashMap.put(key, ((ReadableArray) Assertions.assertNotNull(getArray(key))).toArrayList());
                        break;
                    default:
                        throw new IllegalArgumentException("Could not convert object with key: " + key + ".");
                }
            }
        } else {
            hashMap = new HashMap<>(getLocalMap());
            for (String key2 : hashMap.keySet()) {
                switch (getType(key2)) {
                    case Null:
                    case Boolean:
                    case Number:
                    case String:
                        break;
                    case Map:
                        hashMap.put(key2, ((ReadableNativeMap) Assertions.assertNotNull(getMap(key2))).toHashMap());
                        break;
                    case Array:
                        hashMap.put(key2, ((ReadableArray) Assertions.assertNotNull(getArray(key2))).toArrayList());
                        break;
                    default:
                        throw new IllegalArgumentException("Could not convert object with key: " + key2 + ".");
                }
            }
        }
        return hashMap;
    }
}
