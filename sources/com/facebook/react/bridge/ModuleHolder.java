package com.facebook.react.bridge;

import com.facebook.debug.holder.PrinterHolder;
import com.facebook.debug.tags.ReactDebugOverlayTags;
import com.facebook.infer.annotation.Assertions;
import com.facebook.proguard.annotations.DoNotStrip;
import com.facebook.react.module.model.ReactModuleInfo;
import com.facebook.systrace.SystraceMessage;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.inject.Provider;

@DoNotStrip
public class ModuleHolder {
    private static final AtomicInteger sInstanceKeyCounter = new AtomicInteger(1);
    private final boolean mCanOverrideExistingModule;
    private final boolean mHasConstants;
    @GuardedBy("this")
    private boolean mInitializable;
    private final int mInstanceKey = sInstanceKeyCounter.getAndIncrement();
    @GuardedBy("this")
    private boolean mIsCreating;
    @GuardedBy("this")
    private boolean mIsInitializing;
    @GuardedBy("this")
    @Nullable
    private NativeModule mModule;
    private final String mName;
    @Nullable
    private Provider<? extends NativeModule> mProvider;

    public ModuleHolder(ReactModuleInfo moduleInfo, Provider<? extends NativeModule> provider) {
        this.mName = moduleInfo.name();
        this.mCanOverrideExistingModule = moduleInfo.canOverrideExistingModule();
        this.mHasConstants = moduleInfo.hasConstants();
        this.mProvider = provider;
        if (moduleInfo.needsEagerInit()) {
            this.mModule = create();
        }
    }

    public ModuleHolder(NativeModule nativeModule) {
        this.mName = nativeModule.getName();
        this.mCanOverrideExistingModule = nativeModule.canOverrideExistingModule();
        this.mHasConstants = true;
        this.mModule = nativeModule;
        PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.NATIVE_MODULE, "NativeModule init: %s", this.mName);
    }

    /* access modifiers changed from: 0000 */
    public void markInitializable() {
        boolean z = true;
        boolean shouldInitializeNow = false;
        NativeModule module = null;
        synchronized (this) {
            this.mInitializable = true;
            if (this.mModule != null) {
                if (this.mIsInitializing) {
                    z = false;
                }
                Assertions.assertCondition(z);
                shouldInitializeNow = true;
                module = this.mModule;
            }
        }
        if (shouldInitializeNow) {
            doInitialize(module);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean hasInstance() {
        return this.mModule != null;
    }

    public synchronized void destroy() {
        if (this.mModule != null) {
            this.mModule.onCatalystInstanceDestroy();
        }
    }

    @DoNotStrip
    public String getName() {
        return this.mName;
    }

    public boolean getCanOverrideExistingModule() {
        return this.mCanOverrideExistingModule;
    }

    public boolean getHasConstants() {
        return this.mHasConstants;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0013, code lost:
        if (r2 == false) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0015, code lost:
        r1 = create();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0019, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r4.mIsCreating = false;
        notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0020, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0028, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x002b, code lost:
        if (r4.mModule != null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x002f, code lost:
        if (r4.mIsCreating == false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        wait();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        r3 = (com.facebook.react.bridge.NativeModule) com.facebook.infer.annotation.Assertions.assertNotNull(r4.mModule);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x003f, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        return r3;
     */
    @com.facebook.proguard.annotations.DoNotStrip
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.react.bridge.NativeModule getModule() {
        /*
            r4 = this;
            r2 = 0
            monitor-enter(r4)
            com.facebook.react.bridge.NativeModule r3 = r4.mModule     // Catch:{ all -> 0x0025 }
            if (r3 == 0) goto L_0x000a
            com.facebook.react.bridge.NativeModule r1 = r4.mModule     // Catch:{ all -> 0x0025 }
            monitor-exit(r4)     // Catch:{ all -> 0x0025 }
        L_0x0009:
            return r1
        L_0x000a:
            boolean r3 = r4.mIsCreating     // Catch:{ all -> 0x0025 }
            if (r3 != 0) goto L_0x0012
            r2 = 1
            r3 = 1
            r4.mIsCreating = r3     // Catch:{ all -> 0x0025 }
        L_0x0012:
            monitor-exit(r4)     // Catch:{ all -> 0x0025 }
            if (r2 == 0) goto L_0x0028
            com.facebook.react.bridge.NativeModule r1 = r4.create()
            monitor-enter(r4)
            r3 = 0
            r4.mIsCreating = r3     // Catch:{ all -> 0x0022 }
            r4.notifyAll()     // Catch:{ all -> 0x0022 }
            monitor-exit(r4)     // Catch:{ all -> 0x0022 }
            goto L_0x0009
        L_0x0022:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0022 }
            throw r3
        L_0x0025:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0025 }
            throw r3
        L_0x0028:
            monitor-enter(r4)
        L_0x0029:
            com.facebook.react.bridge.NativeModule r3 = r4.mModule     // Catch:{ all -> 0x0042 }
            if (r3 != 0) goto L_0x0037
            boolean r3 = r4.mIsCreating     // Catch:{ all -> 0x0042 }
            if (r3 == 0) goto L_0x0037
            r4.wait()     // Catch:{ InterruptedException -> 0x0035 }
            goto L_0x0029
        L_0x0035:
            r0 = move-exception
            goto L_0x0029
        L_0x0037:
            com.facebook.react.bridge.NativeModule r3 = r4.mModule     // Catch:{ all -> 0x0042 }
            java.lang.Object r3 = com.facebook.infer.annotation.Assertions.assertNotNull(r3)     // Catch:{ all -> 0x0042 }
            com.facebook.react.bridge.NativeModule r3 = (com.facebook.react.bridge.NativeModule) r3     // Catch:{ all -> 0x0042 }
            monitor-exit(r4)     // Catch:{ all -> 0x0042 }
            r1 = r3
            goto L_0x0009
        L_0x0042:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0042 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.bridge.ModuleHolder.getModule():com.facebook.react.bridge.NativeModule");
    }

    private NativeModule create() {
        boolean z;
        if (this.mModule == null) {
            z = true;
        } else {
            z = false;
        }
        SoftAssertions.assertCondition(z, "Creating an already created module.");
        ReactMarker.logMarker(ReactMarkerConstants.CREATE_MODULE_START, this.mName, this.mInstanceKey);
        SystraceMessage.beginSection(0, "ModuleHolder.createModule").arg("name", (Object) this.mName).flush();
        PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.NATIVE_MODULE, "NativeModule init: %s", this.mName);
        try {
            NativeModule module = (NativeModule) ((Provider) Assertions.assertNotNull(this.mProvider)).get();
            this.mProvider = null;
            boolean shouldInitializeNow = false;
            synchronized (this) {
                this.mModule = module;
                if (this.mInitializable && !this.mIsInitializing) {
                    shouldInitializeNow = true;
                }
            }
            if (shouldInitializeNow) {
                doInitialize(module);
            }
            return module;
        } finally {
            ReactMarker.logMarker(ReactMarkerConstants.CREATE_MODULE_END, this.mInstanceKey);
            SystraceMessage.endSection(0).flush();
        }
    }

    private void doInitialize(NativeModule module) {
        SystraceMessage.beginSection(0, "ModuleHolder.initialize").arg("name", (Object) this.mName).flush();
        ReactMarker.logMarker(ReactMarkerConstants.INITIALIZE_MODULE_START, this.mName, this.mInstanceKey);
        boolean shouldInitialize = false;
        try {
            synchronized (this) {
                if (this.mInitializable && !this.mIsInitializing) {
                    shouldInitialize = true;
                    this.mIsInitializing = true;
                }
            }
            if (shouldInitialize) {
                module.initialize();
                synchronized (this) {
                    this.mIsInitializing = false;
                }
            }
            ReactMarker.logMarker(ReactMarkerConstants.INITIALIZE_MODULE_END, this.mInstanceKey);
            SystraceMessage.endSection(0).flush();
        } catch (Throwable th) {
            ReactMarker.logMarker(ReactMarkerConstants.INITIALIZE_MODULE_END, this.mInstanceKey);
            SystraceMessage.endSection(0).flush();
            throw th;
        }
    }
}
