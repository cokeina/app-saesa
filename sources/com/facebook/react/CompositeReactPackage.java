package com.facebook.react;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;

public class CompositeReactPackage extends ReactInstancePackage implements ViewManagerOnDemandReactPackage {
    private final List<ReactPackage> mChildReactPackages = new ArrayList();

    public CompositeReactPackage(ReactPackage arg1, ReactPackage arg2, ReactPackage... args) {
        this.mChildReactPackages.add(arg1);
        this.mChildReactPackages.add(arg2);
        Collections.addAll(this.mChildReactPackages, args);
    }

    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        Map<String, NativeModule> moduleMap = new HashMap<>();
        for (ReactPackage reactPackage : this.mChildReactPackages) {
            for (NativeModule nativeModule : reactPackage.createNativeModules(reactContext)) {
                moduleMap.put(nativeModule.getName(), nativeModule);
            }
        }
        return new ArrayList(moduleMap.values());
    }

    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext, ReactInstanceManager reactInstanceManager) {
        List<NativeModule> nativeModules;
        Map<String, NativeModule> moduleMap = new HashMap<>();
        for (ReactPackage reactPackage : this.mChildReactPackages) {
            if (reactPackage instanceof ReactInstancePackage) {
                nativeModules = ((ReactInstancePackage) reactPackage).createNativeModules(reactContext, reactInstanceManager);
            } else {
                nativeModules = reactPackage.createNativeModules(reactContext);
            }
            for (NativeModule nativeModule : nativeModules) {
                moduleMap.put(nativeModule.getName(), nativeModule);
            }
        }
        return new ArrayList(moduleMap.values());
    }

    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        Map<String, ViewManager> viewManagerMap = new HashMap<>();
        for (ReactPackage reactPackage : this.mChildReactPackages) {
            for (ViewManager viewManager : reactPackage.createViewManagers(reactContext)) {
                viewManagerMap.put(viewManager.getName(), viewManager);
            }
        }
        return new ArrayList(viewManagerMap.values());
    }

    public List<String> getViewManagerNames(ReactApplicationContext reactContext, boolean loadClasses) {
        Set<String> uniqueNames = new HashSet<>();
        for (ReactPackage reactPackage : this.mChildReactPackages) {
            if (reactPackage instanceof ViewManagerOnDemandReactPackage) {
                List<String> names = ((ViewManagerOnDemandReactPackage) reactPackage).getViewManagerNames(reactContext, loadClasses);
                if (names != null) {
                    uniqueNames.addAll(names);
                }
            }
        }
        return new ArrayList(uniqueNames);
    }

    @Nullable
    public ViewManager createViewManager(ReactApplicationContext reactContext, String viewManagerName, boolean loadClasses) {
        ListIterator<ReactPackage> iterator = this.mChildReactPackages.listIterator(this.mChildReactPackages.size());
        while (iterator.hasPrevious()) {
            ReactPackage reactPackage = (ReactPackage) iterator.previous();
            if (reactPackage instanceof ViewManagerOnDemandReactPackage) {
                ViewManager viewManager = ((ViewManagerOnDemandReactPackage) reactPackage).createViewManager(reactContext, viewManagerName, loadClasses);
                if (viewManager != null) {
                    return viewManager;
                }
            }
        }
        return null;
    }
}
