package com.facebook.react.modules.core;

import android.util.SparseArray;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.common.SystemClock;
import com.facebook.react.devsupport.interfaces.DevSupportManager;
import com.facebook.react.jstasks.HeadlessJsTaskContext;
import com.facebook.react.jstasks.HeadlessJsTaskEventListener;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.ChoreographerCompat.FrameCallback;
import com.facebook.react.modules.core.ReactChoreographer.CallbackType;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Nullable;

@ReactModule(name = "Timing")
public final class Timing extends ReactContextBaseJavaModule implements LifecycleEventListener, HeadlessJsTaskEventListener {
    private static final float FRAME_DURATION_MS = 16.666666f;
    private static final float IDLE_CALLBACK_FRAME_DEADLINE_MS = 1.0f;
    protected static final String NAME = "Timing";
    /* access modifiers changed from: private */
    public final AtomicBoolean isPaused = new AtomicBoolean(true);
    /* access modifiers changed from: private */
    public final AtomicBoolean isRunningTasks = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    @Nullable
    public IdleCallbackRunnable mCurrentIdleCallbackRunnable;
    private final DevSupportManager mDevSupportManager;
    private boolean mFrameCallbackPosted = false;
    private boolean mFrameIdleCallbackPosted = false;
    /* access modifiers changed from: private */
    public final Object mIdleCallbackGuard = new Object();
    private final IdleFrameCallback mIdleFrameCallback = new IdleFrameCallback();
    /* access modifiers changed from: private */
    public final ReactChoreographer mReactChoreographer;
    /* access modifiers changed from: private */
    public boolean mSendIdleEvents = false;
    private final TimerFrameCallback mTimerFrameCallback = new TimerFrameCallback();
    /* access modifiers changed from: private */
    public final Object mTimerGuard = new Object();
    /* access modifiers changed from: private */
    public final SparseArray<Timer> mTimerIdsToTimers;
    /* access modifiers changed from: private */
    public final PriorityQueue<Timer> mTimers;

    private class IdleCallbackRunnable implements Runnable {
        private volatile boolean mCancelled = false;
        private final long mFrameStartTime;

        public IdleCallbackRunnable(long frameStartTime) {
            this.mFrameStartTime = frameStartTime;
        }

        public void run() {
            boolean sendIdleEvents;
            if (!this.mCancelled) {
                long frameTimeElapsed = SystemClock.uptimeMillis() - (this.mFrameStartTime / 1000000);
                long absoluteFrameStartTime = SystemClock.currentTimeMillis() - frameTimeElapsed;
                if (Timing.FRAME_DURATION_MS - ((float) frameTimeElapsed) >= Timing.IDLE_CALLBACK_FRAME_DEADLINE_MS) {
                    synchronized (Timing.this.mIdleCallbackGuard) {
                        sendIdleEvents = Timing.this.mSendIdleEvents;
                    }
                    if (sendIdleEvents) {
                        ((JSTimers) Timing.this.getReactApplicationContext().getJSModule(JSTimers.class)).callIdleCallbacks((double) absoluteFrameStartTime);
                    }
                    Timing.this.mCurrentIdleCallbackRunnable = null;
                }
            }
        }

        public void cancel() {
            this.mCancelled = true;
        }
    }

    private class IdleFrameCallback extends FrameCallback {
        private IdleFrameCallback() {
        }

        public void doFrame(long frameTimeNanos) {
            if (!Timing.this.isPaused.get() || Timing.this.isRunningTasks.get()) {
                if (Timing.this.mCurrentIdleCallbackRunnable != null) {
                    Timing.this.mCurrentIdleCallbackRunnable.cancel();
                }
                Timing.this.mCurrentIdleCallbackRunnable = new IdleCallbackRunnable(frameTimeNanos);
                Timing.this.getReactApplicationContext().runOnJSQueueThread(Timing.this.mCurrentIdleCallbackRunnable);
                Timing.this.mReactChoreographer.postFrameCallback(CallbackType.IDLE_EVENT, this);
            }
        }
    }

    private static class Timer {
        /* access modifiers changed from: private */
        public final int mCallbackID;
        /* access modifiers changed from: private */
        public final int mInterval;
        /* access modifiers changed from: private */
        public final boolean mRepeat;
        /* access modifiers changed from: private */
        public long mTargetTime;

        private Timer(int callbackID, long initialTargetTime, int duration, boolean repeat) {
            this.mCallbackID = callbackID;
            this.mTargetTime = initialTargetTime;
            this.mInterval = duration;
            this.mRepeat = repeat;
        }
    }

    private class TimerFrameCallback extends FrameCallback {
        @Nullable
        private WritableArray mTimersToCall;

        private TimerFrameCallback() {
            this.mTimersToCall = null;
        }

        public void doFrame(long frameTimeNanos) {
            if (!Timing.this.isPaused.get() || Timing.this.isRunningTasks.get()) {
                long frameTimeMillis = frameTimeNanos / 1000000;
                synchronized (Timing.this.mTimerGuard) {
                    while (!Timing.this.mTimers.isEmpty() && ((Timer) Timing.this.mTimers.peek()).mTargetTime < frameTimeMillis) {
                        Timer timer = (Timer) Timing.this.mTimers.poll();
                        if (this.mTimersToCall == null) {
                            this.mTimersToCall = Arguments.createArray();
                        }
                        this.mTimersToCall.pushInt(timer.mCallbackID);
                        if (timer.mRepeat) {
                            timer.mTargetTime = ((long) timer.mInterval) + frameTimeMillis;
                            Timing.this.mTimers.add(timer);
                        } else {
                            Timing.this.mTimerIdsToTimers.remove(timer.mCallbackID);
                        }
                    }
                }
                if (this.mTimersToCall != null) {
                    ((JSTimers) Timing.this.getReactApplicationContext().getJSModule(JSTimers.class)).callTimers(this.mTimersToCall);
                    this.mTimersToCall = null;
                }
                Timing.this.mReactChoreographer.postFrameCallback(CallbackType.TIMERS_EVENTS, this);
            }
        }
    }

    public Timing(ReactApplicationContext reactContext, DevSupportManager devSupportManager) {
        super(reactContext);
        this.mDevSupportManager = devSupportManager;
        this.mTimers = new PriorityQueue<>(11, new Comparator<Timer>() {
            public int compare(Timer lhs, Timer rhs) {
                long diff = lhs.mTargetTime - rhs.mTargetTime;
                if (diff == 0) {
                    return 0;
                }
                if (diff < 0) {
                    return -1;
                }
                return 1;
            }
        });
        this.mTimerIdsToTimers = new SparseArray<>();
        this.mReactChoreographer = ReactChoreographer.getInstance();
    }

    public void initialize() {
        getReactApplicationContext().addLifecycleEventListener(this);
        HeadlessJsTaskContext.getInstance(getReactApplicationContext()).addTaskEventListener(this);
    }

    public void onHostPause() {
        this.isPaused.set(true);
        clearFrameCallback();
        maybeIdleCallback();
    }

    public void onHostDestroy() {
        clearFrameCallback();
        maybeIdleCallback();
    }

    public void onHostResume() {
        this.isPaused.set(false);
        setChoreographerCallback();
        maybeSetChoreographerIdleCallback();
    }

    public void onHeadlessJsTaskStart(int taskId) {
        if (!this.isRunningTasks.getAndSet(true)) {
            setChoreographerCallback();
            maybeSetChoreographerIdleCallback();
        }
    }

    public void onHeadlessJsTaskFinish(int taskId) {
        if (!HeadlessJsTaskContext.getInstance(getReactApplicationContext()).hasActiveTasks()) {
            this.isRunningTasks.set(false);
            clearFrameCallback();
            maybeIdleCallback();
        }
    }

    public void onCatalystInstanceDestroy() {
        clearFrameCallback();
        clearChoreographerIdleCallback();
        HeadlessJsTaskContext.getInstance(getReactApplicationContext()).removeTaskEventListener(this);
    }

    private void maybeSetChoreographerIdleCallback() {
        synchronized (this.mIdleCallbackGuard) {
            if (this.mSendIdleEvents) {
                setChoreographerIdleCallback();
            }
        }
    }

    private void maybeIdleCallback() {
        if (this.isPaused.get() && !this.isRunningTasks.get()) {
            clearFrameCallback();
        }
    }

    private void setChoreographerCallback() {
        if (!this.mFrameCallbackPosted) {
            this.mReactChoreographer.postFrameCallback(CallbackType.TIMERS_EVENTS, this.mTimerFrameCallback);
            this.mFrameCallbackPosted = true;
        }
    }

    private void clearFrameCallback() {
        HeadlessJsTaskContext headlessJsTaskContext = HeadlessJsTaskContext.getInstance(getReactApplicationContext());
        if (this.mFrameCallbackPosted && this.isPaused.get() && !headlessJsTaskContext.hasActiveTasks()) {
            this.mReactChoreographer.removeFrameCallback(CallbackType.TIMERS_EVENTS, this.mTimerFrameCallback);
            this.mFrameCallbackPosted = false;
        }
    }

    /* access modifiers changed from: private */
    public void setChoreographerIdleCallback() {
        if (!this.mFrameIdleCallbackPosted) {
            this.mReactChoreographer.postFrameCallback(CallbackType.IDLE_EVENT, this.mIdleFrameCallback);
            this.mFrameIdleCallbackPosted = true;
        }
    }

    /* access modifiers changed from: private */
    public void clearChoreographerIdleCallback() {
        if (this.mFrameIdleCallbackPosted) {
            this.mReactChoreographer.removeFrameCallback(CallbackType.IDLE_EVENT, this.mIdleFrameCallback);
            this.mFrameIdleCallbackPosted = false;
        }
    }

    public String getName() {
        return NAME;
    }

    @ReactMethod
    public void createTimer(int callbackID, int duration, double jsSchedulingTime, boolean repeat) {
        long deviceTime = SystemClock.currentTimeMillis();
        long remoteTime = (long) jsSchedulingTime;
        if (this.mDevSupportManager.getDevSupportEnabled() && Math.abs(remoteTime - deviceTime) > 60000) {
            ((JSTimers) getReactApplicationContext().getJSModule(JSTimers.class)).emitTimeDriftWarning("Debugger and device times have drifted by more than 60s. Please correct this by running adb shell \"date `date +%m%d%H%M%Y.%S`\" on your debugger machine.");
        }
        long adjustedDuration = Math.max(0, (remoteTime - deviceTime) + ((long) duration));
        if (duration != 0 || repeat) {
            Timer timer = new Timer(callbackID, (SystemClock.nanoTime() / 1000000) + adjustedDuration, duration, repeat);
            synchronized (this.mTimerGuard) {
                this.mTimers.add(timer);
                this.mTimerIdsToTimers.put(callbackID, timer);
            }
            return;
        }
        WritableArray timerToCall = Arguments.createArray();
        timerToCall.pushInt(callbackID);
        ((JSTimers) getReactApplicationContext().getJSModule(JSTimers.class)).callTimers(timerToCall);
    }

    @ReactMethod
    public void deleteTimer(int timerId) {
        synchronized (this.mTimerGuard) {
            Timer timer = (Timer) this.mTimerIdsToTimers.get(timerId);
            if (timer != null) {
                this.mTimerIdsToTimers.remove(timerId);
                this.mTimers.remove(timer);
            }
        }
    }

    @ReactMethod
    public void setSendIdleEvents(final boolean sendIdleEvents) {
        synchronized (this.mIdleCallbackGuard) {
            this.mSendIdleEvents = sendIdleEvents;
        }
        UiThreadUtil.runOnUiThread(new Runnable() {
            public void run() {
                synchronized (Timing.this.mIdleCallbackGuard) {
                    if (sendIdleEvents) {
                        Timing.this.setChoreographerIdleCallback();
                    } else {
                        Timing.this.clearChoreographerIdleCallback();
                    }
                }
            }
        });
    }
}
