package com.facebook.react.modules.i18nmanager;

import android.content.Context;
import com.facebook.react.bridge.ContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.module.annotations.ReactModule;
import java.util.Locale;
import java.util.Map;

@ReactModule(name = "I18nManager")
public class I18nManagerModule extends ContextBaseJavaModule {
    private final I18nUtil sharedI18nUtilInstance = I18nUtil.getInstance();

    public I18nManagerModule(Context context) {
        super(context);
    }

    public String getName() {
        return "I18nManager";
    }

    public Map<String, Object> getConstants() {
        Context context = getContext();
        Locale locale = context.getResources().getConfiguration().locale;
        Map<String, Object> constants = MapBuilder.newHashMap();
        constants.put("isRTL", Boolean.valueOf(this.sharedI18nUtilInstance.isRTL(context)));
        constants.put("doLeftAndRightSwapInRTL", Boolean.valueOf(this.sharedI18nUtilInstance.doLeftAndRightSwapInRTL(context)));
        constants.put("localeIdentifier", locale.toString());
        return constants;
    }

    @ReactMethod
    public void allowRTL(boolean value) {
        this.sharedI18nUtilInstance.allowRTL(getContext(), value);
    }

    @ReactMethod
    public void forceRTL(boolean value) {
        this.sharedI18nUtilInstance.forceRTL(getContext(), value);
    }

    @ReactMethod
    public void swapLeftAndRightInRTL(boolean value) {
        this.sharedI18nUtilInstance.swapLeftAndRightInRTL(getContext(), value);
    }
}
