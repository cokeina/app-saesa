package com.facebook.react.modules.websocket;

import com.facebook.common.logging.FLog;
import com.facebook.common.util.UriUtil;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;
import com.facebook.react.modules.network.ForwardingCookieHandler;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import okhttp3.WebSocket;
import okio.ByteString;

@ReactModule(hasConstants = false, name = "WebSocketModule")
public final class WebSocketModule extends ReactContextBaseJavaModule {
    /* access modifiers changed from: private */
    public final Map<Integer, ContentHandler> mContentHandlers = new ConcurrentHashMap();
    private ForwardingCookieHandler mCookieHandler;
    private ReactContext mReactContext;
    /* access modifiers changed from: private */
    public final Map<Integer, WebSocket> mWebSocketConnections = new ConcurrentHashMap();

    public interface ContentHandler {
        void onMessage(String str, WritableMap writableMap);

        void onMessage(ByteString byteString, WritableMap writableMap);
    }

    public WebSocketModule(ReactApplicationContext context) {
        super(context);
        this.mReactContext = context;
        this.mCookieHandler = new ForwardingCookieHandler(context);
    }

    /* access modifiers changed from: private */
    public void sendEvent(String eventName, WritableMap params) {
        ((RCTDeviceEventEmitter) this.mReactContext.getJSModule(RCTDeviceEventEmitter.class)).emit(eventName, params);
    }

    public String getName() {
        return "WebSocketModule";
    }

    public void setContentHandler(int id, ContentHandler contentHandler) {
        if (contentHandler != null) {
            this.mContentHandlers.put(Integer.valueOf(id), contentHandler);
        } else {
            this.mContentHandlers.remove(Integer.valueOf(id));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00fd  */
    @com.facebook.react.bridge.ReactMethod
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void connect(java.lang.String r16, @javax.annotation.Nullable com.facebook.react.bridge.ReadableArray r17, @javax.annotation.Nullable com.facebook.react.bridge.ReadableMap r18, int r19) {
        /*
            r15 = this;
            okhttp3.OkHttpClient$Builder r11 = new okhttp3.OkHttpClient$Builder
            r11.<init>()
            r12 = 10
            java.util.concurrent.TimeUnit r14 = java.util.concurrent.TimeUnit.SECONDS
            okhttp3.OkHttpClient$Builder r11 = r11.connectTimeout(r12, r14)
            r12 = 10
            java.util.concurrent.TimeUnit r14 = java.util.concurrent.TimeUnit.SECONDS
            okhttp3.OkHttpClient$Builder r11 = r11.writeTimeout(r12, r14)
            r12 = 0
            java.util.concurrent.TimeUnit r14 = java.util.concurrent.TimeUnit.MINUTES
            okhttp3.OkHttpClient$Builder r11 = r11.readTimeout(r12, r14)
            okhttp3.OkHttpClient r3 = r11.build()
            okhttp3.Request$Builder r11 = new okhttp3.Request$Builder
            r11.<init>()
            java.lang.Integer r12 = java.lang.Integer.valueOf(r19)
            okhttp3.Request$Builder r11 = r11.tag(r12)
            r0 = r16
            okhttp3.Request$Builder r2 = r11.url(r0)
            java.lang.String r4 = r15.getCookie(r16)
            if (r4 == 0) goto L_0x003f
            java.lang.String r11 = "Cookie"
            r2.addHeader(r11, r4)
        L_0x003f:
            if (r18 == 0) goto L_0x00b5
            java.lang.String r11 = "headers"
            r0 = r18
            boolean r11 = r0.hasKey(r11)
            if (r11 == 0) goto L_0x00b5
            java.lang.String r11 = "headers"
            r0 = r18
            com.facebook.react.bridge.ReadableType r11 = r0.getType(r11)
            com.facebook.react.bridge.ReadableType r12 = com.facebook.react.bridge.ReadableType.Map
            boolean r11 = r11.equals(r12)
            if (r11 == 0) goto L_0x00b5
            java.lang.String r11 = "headers"
            r0 = r18
            com.facebook.react.bridge.ReadableMap r5 = r0.getMap(r11)
            com.facebook.react.bridge.ReadableMapKeySetIterator r7 = r5.keySetIterator()
            java.lang.String r11 = "origin"
            boolean r11 = r5.hasKey(r11)
            if (r11 != 0) goto L_0x0078
            java.lang.String r11 = "origin"
            java.lang.String r12 = getDefaultOrigin(r16)
            r2.addHeader(r11, r12)
        L_0x0078:
            boolean r11 = r7.hasNextKey()
            if (r11 == 0) goto L_0x00be
            java.lang.String r8 = r7.nextKey()
            com.facebook.react.bridge.ReadableType r11 = com.facebook.react.bridge.ReadableType.String
            com.facebook.react.bridge.ReadableType r12 = r5.getType(r8)
            boolean r11 = r11.equals(r12)
            if (r11 == 0) goto L_0x0096
            java.lang.String r11 = r5.getString(r8)
            r2.addHeader(r8, r11)
            goto L_0x0078
        L_0x0096:
            java.lang.String r11 = "ReactNative"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "Ignoring: requested "
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.StringBuilder r12 = r12.append(r8)
            java.lang.String r13 = ", value not a string"
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r12 = r12.toString()
            com.facebook.common.logging.FLog.w(r11, r12)
            goto L_0x0078
        L_0x00b5:
            java.lang.String r11 = "origin"
            java.lang.String r12 = getDefaultOrigin(r16)
            r2.addHeader(r11, r12)
        L_0x00be:
            if (r17 == 0) goto L_0x0115
            int r11 = r17.size()
            if (r11 <= 0) goto L_0x0115
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r11 = ""
            r9.<init>(r11)
            r6 = 0
        L_0x00ce:
            int r11 = r17.size()
            if (r6 >= r11) goto L_0x00f7
            r0 = r17
            java.lang.String r11 = r0.getString(r6)
            java.lang.String r10 = r11.trim()
            boolean r11 = r10.isEmpty()
            if (r11 != 0) goto L_0x00f4
            java.lang.String r11 = ","
            boolean r11 = r10.contains(r11)
            if (r11 != 0) goto L_0x00f4
            r9.append(r10)
            java.lang.String r11 = ","
            r9.append(r11)
        L_0x00f4:
            int r6 = r6 + 1
            goto L_0x00ce
        L_0x00f7:
            int r11 = r9.length()
            if (r11 <= 0) goto L_0x0115
            int r11 = r9.length()
            int r11 = r11 + -1
            int r12 = r9.length()
            java.lang.String r13 = ""
            r9.replace(r11, r12, r13)
            java.lang.String r11 = "Sec-WebSocket-Protocol"
            java.lang.String r12 = r9.toString()
            r2.addHeader(r11, r12)
        L_0x0115:
            okhttp3.Request r11 = r2.build()
            com.facebook.react.modules.websocket.WebSocketModule$1 r12 = new com.facebook.react.modules.websocket.WebSocketModule$1
            r0 = r19
            r12.<init>(r0)
            r3.newWebSocket(r11, r12)
            okhttp3.Dispatcher r11 = r3.dispatcher()
            java.util.concurrent.ExecutorService r11 = r11.executorService()
            r11.shutdown()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.modules.websocket.WebSocketModule.connect(java.lang.String, com.facebook.react.bridge.ReadableArray, com.facebook.react.bridge.ReadableMap, int):void");
    }

    @ReactMethod
    public void close(int code, String reason, int id) {
        WebSocket client = (WebSocket) this.mWebSocketConnections.get(Integer.valueOf(id));
        if (client != null) {
            try {
                client.close(code, reason);
                this.mWebSocketConnections.remove(Integer.valueOf(id));
                this.mContentHandlers.remove(Integer.valueOf(id));
            } catch (Exception e) {
                FLog.e(ReactConstants.TAG, "Could not close WebSocket connection for id " + id, (Throwable) e);
            }
        }
    }

    @ReactMethod
    public void send(String message, int id) {
        WebSocket client = (WebSocket) this.mWebSocketConnections.get(Integer.valueOf(id));
        if (client == null) {
            WritableMap params = Arguments.createMap();
            params.putInt("id", id);
            params.putString("message", "client is null");
            sendEvent("websocketFailed", params);
            WritableMap params2 = Arguments.createMap();
            params2.putInt("id", id);
            params2.putInt("code", 0);
            params2.putString("reason", "client is null");
            sendEvent("websocketClosed", params2);
            this.mWebSocketConnections.remove(Integer.valueOf(id));
            this.mContentHandlers.remove(Integer.valueOf(id));
            return;
        }
        try {
            client.send(message);
        } catch (Exception e) {
            notifyWebSocketFailed(id, e.getMessage());
        }
    }

    @ReactMethod
    public void sendBinary(String base64String, int id) {
        WebSocket client = (WebSocket) this.mWebSocketConnections.get(Integer.valueOf(id));
        if (client == null) {
            WritableMap params = Arguments.createMap();
            params.putInt("id", id);
            params.putString("message", "client is null");
            sendEvent("websocketFailed", params);
            WritableMap params2 = Arguments.createMap();
            params2.putInt("id", id);
            params2.putInt("code", 0);
            params2.putString("reason", "client is null");
            sendEvent("websocketClosed", params2);
            this.mWebSocketConnections.remove(Integer.valueOf(id));
            this.mContentHandlers.remove(Integer.valueOf(id));
            return;
        }
        try {
            client.send(ByteString.decodeBase64(base64String));
        } catch (Exception e) {
            notifyWebSocketFailed(id, e.getMessage());
        }
    }

    public void sendBinary(ByteString byteString, int id) {
        WebSocket client = (WebSocket) this.mWebSocketConnections.get(Integer.valueOf(id));
        if (client == null) {
            WritableMap params = Arguments.createMap();
            params.putInt("id", id);
            params.putString("message", "client is null");
            sendEvent("websocketFailed", params);
            WritableMap params2 = Arguments.createMap();
            params2.putInt("id", id);
            params2.putInt("code", 0);
            params2.putString("reason", "client is null");
            sendEvent("websocketClosed", params2);
            this.mWebSocketConnections.remove(Integer.valueOf(id));
            this.mContentHandlers.remove(Integer.valueOf(id));
            return;
        }
        try {
            client.send(byteString);
        } catch (Exception e) {
            notifyWebSocketFailed(id, e.getMessage());
        }
    }

    @ReactMethod
    public void ping(int id) {
        WebSocket client = (WebSocket) this.mWebSocketConnections.get(Integer.valueOf(id));
        if (client == null) {
            WritableMap params = Arguments.createMap();
            params.putInt("id", id);
            params.putString("message", "client is null");
            sendEvent("websocketFailed", params);
            WritableMap params2 = Arguments.createMap();
            params2.putInt("id", id);
            params2.putInt("code", 0);
            params2.putString("reason", "client is null");
            sendEvent("websocketClosed", params2);
            this.mWebSocketConnections.remove(Integer.valueOf(id));
            this.mContentHandlers.remove(Integer.valueOf(id));
            return;
        }
        try {
            client.send(ByteString.EMPTY);
        } catch (Exception e) {
            notifyWebSocketFailed(id, e.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void notifyWebSocketFailed(int id, String message) {
        WritableMap params = Arguments.createMap();
        params.putInt("id", id);
        params.putString("message", message);
        sendEvent("websocketFailed", params);
    }

    private static String getDefaultOrigin(String uri) {
        String scheme = "";
        try {
            URI requestURI = new URI(uri);
            if (requestURI.getScheme().equals("wss")) {
                scheme = scheme + UriUtil.HTTPS_SCHEME;
            } else if (requestURI.getScheme().equals("ws")) {
                scheme = scheme + UriUtil.HTTP_SCHEME;
            } else if (requestURI.getScheme().equals(UriUtil.HTTP_SCHEME) || requestURI.getScheme().equals(UriUtil.HTTPS_SCHEME)) {
                scheme = scheme + requestURI.getScheme();
            }
            if (requestURI.getPort() != -1) {
                return String.format("%s://%s:%s", new Object[]{scheme, requestURI.getHost(), Integer.valueOf(requestURI.getPort())});
            }
            return String.format("%s://%s/", new Object[]{scheme, requestURI.getHost()});
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Unable to set " + uri + " as default origin header");
        }
    }

    private String getCookie(String uri) {
        try {
            List<String> cookieList = (List) this.mCookieHandler.get(new URI(getDefaultOrigin(uri)), new HashMap()).get("Cookie");
            if (cookieList == null || cookieList.isEmpty()) {
                return null;
            }
            return (String) cookieList.get(0);
        } catch (IOException | URISyntaxException e) {
            throw new IllegalArgumentException("Unable to get cookie from " + uri);
        }
    }
}
