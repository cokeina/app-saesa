package com.facebook.react.modules.blob;

import android.util.Base64;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.module.annotations.ReactModule;

@ReactModule(name = "FileReaderModule")
public class FileReaderModule extends ReactContextBaseJavaModule {
    private static final String ERROR_INVALID_BLOB = "ERROR_INVALID_BLOB";
    protected static final String NAME = "FileReaderModule";

    public FileReaderModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    public String getName() {
        return NAME;
    }

    private BlobModule getBlobModule() {
        return (BlobModule) getReactApplicationContext().getNativeModule(BlobModule.class);
    }

    @ReactMethod
    public void readAsText(ReadableMap blob, String encoding, Promise promise) {
        byte[] bytes = getBlobModule().resolve(blob.getString("blobId"), blob.getInt("offset"), blob.getInt("size"));
        if (bytes == null) {
            promise.reject(ERROR_INVALID_BLOB, "The specified blob is invalid");
            return;
        }
        try {
            promise.resolve(new String(bytes, encoding));
        } catch (Exception e) {
            promise.reject((Throwable) e);
        }
    }

    @ReactMethod
    public void readAsDataURL(ReadableMap blob, Promise promise) {
        byte[] bytes = getBlobModule().resolve(blob.getString("blobId"), blob.getInt("offset"), blob.getInt("size"));
        if (bytes == null) {
            promise.reject(ERROR_INVALID_BLOB, "The specified blob is invalid");
            return;
        }
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("data:");
            if (!blob.hasKey("type") || blob.getString("type").isEmpty()) {
                sb.append("application/octet-stream");
            } else {
                sb.append(blob.getString("type"));
            }
            sb.append(";base64,");
            sb.append(Base64.encodeToString(bytes, 2));
            promise.resolve(sb.toString());
        } catch (Exception e) {
            promise.reject((Throwable) e);
        }
    }
}
