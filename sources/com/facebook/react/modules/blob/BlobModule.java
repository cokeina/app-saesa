package com.facebook.react.modules.blob;

import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.webkit.MimeTypeMap;
import com.facebook.common.util.UriUtil;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.network.NetworkingModule;
import com.facebook.react.modules.network.NetworkingModule.RequestBodyHandler;
import com.facebook.react.modules.network.NetworkingModule.ResponseHandler;
import com.facebook.react.modules.network.NetworkingModule.UriHandler;
import com.facebook.react.modules.websocket.WebSocketModule;
import com.facebook.react.modules.websocket.WebSocketModule.ContentHandler;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.ByteString;

@ReactModule(name = "BlobModule")
public class BlobModule extends ReactContextBaseJavaModule {
    protected static final String NAME = "BlobModule";
    private final Map<String, byte[]> mBlobs = new HashMap();
    private final RequestBodyHandler mNetworkingRequestBodyHandler = new RequestBodyHandler() {
        public boolean supports(ReadableMap data) {
            return data.hasKey("blob");
        }

        public RequestBody toRequestBody(ReadableMap data, String contentType) {
            String type = contentType;
            if (data.hasKey("type") && !data.getString("type").isEmpty()) {
                type = data.getString("type");
            }
            if (type == null) {
                type = "application/octet-stream";
            }
            ReadableMap blob = data.getMap("blob");
            return RequestBody.create(MediaType.parse(type), BlobModule.this.resolve(blob.getString("blobId"), blob.getInt("offset"), blob.getInt("size")));
        }
    };
    private final ResponseHandler mNetworkingResponseHandler = new ResponseHandler() {
        public boolean supports(String responseType) {
            return responseType.equals("blob");
        }

        public WritableMap toResponseData(ResponseBody body) throws IOException {
            byte[] data = body.bytes();
            WritableMap blob = Arguments.createMap();
            blob.putString("blobId", BlobModule.this.store(data));
            blob.putInt("offset", 0);
            blob.putInt("size", data.length);
            return blob;
        }
    };
    private final UriHandler mNetworkingUriHandler = new UriHandler() {
        public boolean supports(Uri uri, String responseType) {
            boolean isRemote;
            String scheme = uri.getScheme();
            if (scheme.equals(UriUtil.HTTP_SCHEME) || scheme.equals(UriUtil.HTTPS_SCHEME)) {
                isRemote = true;
            } else {
                isRemote = false;
            }
            if (isRemote || !responseType.equals("blob")) {
                return false;
            }
            return true;
        }

        public WritableMap fetch(Uri uri) throws IOException {
            byte[] data = BlobModule.this.getBytesFromUri(uri);
            WritableMap blob = Arguments.createMap();
            blob.putString("blobId", BlobModule.this.store(data));
            blob.putInt("offset", 0);
            blob.putInt("size", data.length);
            blob.putString("type", BlobModule.this.getMimeTypeFromUri(uri));
            blob.putString("name", BlobModule.this.getNameFromUri(uri));
            blob.putDouble("lastModified", (double) BlobModule.this.getLastModifiedFromUri(uri));
            return blob;
        }
    };
    private final ContentHandler mWebSocketContentHandler = new ContentHandler() {
        public void onMessage(String text, WritableMap params) {
            params.putString(UriUtil.DATA_SCHEME, text);
        }

        public void onMessage(ByteString bytes, WritableMap params) {
            byte[] data = bytes.toByteArray();
            WritableMap blob = Arguments.createMap();
            blob.putString("blobId", BlobModule.this.store(data));
            blob.putInt("offset", 0);
            blob.putInt("size", data.length);
            params.putMap(UriUtil.DATA_SCHEME, blob);
            params.putString("type", "blob");
        }
    };

    public BlobModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    public String getName() {
        return NAME;
    }

    @Nullable
    public Map<String, Object> getConstants() {
        Resources resources = getReactApplicationContext().getResources();
        int resourceId = resources.getIdentifier("blob_provider_authority", "string", getReactApplicationContext().getPackageName());
        if (resourceId == 0) {
            return null;
        }
        return MapBuilder.of("BLOB_URI_SCHEME", UriUtil.LOCAL_CONTENT_SCHEME, "BLOB_URI_HOST", resources.getString(resourceId));
    }

    public String store(byte[] data) {
        String blobId = UUID.randomUUID().toString();
        store(data, blobId);
        return blobId;
    }

    public void store(byte[] data, String blobId) {
        this.mBlobs.put(blobId, data);
    }

    public void remove(String blobId) {
        this.mBlobs.remove(blobId);
    }

    @Nullable
    public byte[] resolve(Uri uri) {
        String blobId = uri.getLastPathSegment();
        int offset = 0;
        int size = -1;
        String offsetParam = uri.getQueryParameter("offset");
        if (offsetParam != null) {
            offset = Integer.parseInt(offsetParam, 10);
        }
        String sizeParam = uri.getQueryParameter("size");
        if (sizeParam != null) {
            size = Integer.parseInt(sizeParam, 10);
        }
        return resolve(blobId, offset, size);
    }

    @Nullable
    public byte[] resolve(String blobId, int offset, int size) {
        byte[] data = (byte[]) this.mBlobs.get(blobId);
        if (data == null) {
            return null;
        }
        if (size == -1) {
            size = data.length - offset;
        }
        if (offset > 0 || size != data.length) {
            data = Arrays.copyOfRange(data, offset, offset + size);
        }
        return data;
    }

    @Nullable
    public byte[] resolve(ReadableMap blob) {
        return resolve(blob.getString("blobId"), blob.getInt("offset"), blob.getInt("size"));
    }

    /* access modifiers changed from: private */
    public byte[] getBytesFromUri(Uri contentUri) throws IOException {
        InputStream is = getReactApplicationContext().getContentResolver().openInputStream(contentUri);
        if (is == null) {
            throw new FileNotFoundException("File not found for " + contentUri);
        }
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        while (true) {
            int len = is.read(buffer);
            if (len == -1) {
                return byteBuffer.toByteArray();
            }
            byteBuffer.write(buffer, 0, len);
        }
    }

    /* access modifiers changed from: private */
    public String getNameFromUri(Uri contentUri) {
        if (contentUri.getScheme().equals(UriUtil.LOCAL_FILE_SCHEME)) {
            return contentUri.getLastPathSegment();
        }
        Cursor metaCursor = getReactApplicationContext().getContentResolver().query(contentUri, new String[]{"_display_name"}, null, null, null);
        if (metaCursor != null) {
            try {
                if (metaCursor.moveToFirst()) {
                    return metaCursor.getString(0);
                }
                metaCursor.close();
            } finally {
                metaCursor.close();
            }
        }
        return contentUri.getLastPathSegment();
    }

    /* access modifiers changed from: private */
    public long getLastModifiedFromUri(Uri contentUri) {
        if (contentUri.getScheme().equals(UriUtil.LOCAL_FILE_SCHEME)) {
            return new File(contentUri.toString()).lastModified();
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public String getMimeTypeFromUri(Uri contentUri) {
        String type = getReactApplicationContext().getContentResolver().getType(contentUri);
        if (type == null) {
            String ext = MimeTypeMap.getFileExtensionFromUrl(contentUri.getPath());
            if (ext != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(ext);
            }
        }
        if (type == null) {
            return "";
        }
        return type;
    }

    private WebSocketModule getWebSocketModule() {
        return (WebSocketModule) getReactApplicationContext().getNativeModule(WebSocketModule.class);
    }

    @ReactMethod
    public void addNetworkingHandler() {
        NetworkingModule networkingModule = (NetworkingModule) getReactApplicationContext().getNativeModule(NetworkingModule.class);
        networkingModule.addUriHandler(this.mNetworkingUriHandler);
        networkingModule.addRequestBodyHandler(this.mNetworkingRequestBodyHandler);
        networkingModule.addResponseHandler(this.mNetworkingResponseHandler);
    }

    @ReactMethod
    public void addWebSocketHandler(int id) {
        getWebSocketModule().setContentHandler(id, this.mWebSocketContentHandler);
    }

    @ReactMethod
    public void removeWebSocketHandler(int id) {
        getWebSocketModule().setContentHandler(id, null);
    }

    @ReactMethod
    public void sendOverSocket(ReadableMap blob, int id) {
        byte[] data = resolve(blob.getString("blobId"), blob.getInt("offset"), blob.getInt("size"));
        if (data != null) {
            getWebSocketModule().sendBinary(ByteString.of(data), id);
        } else {
            getWebSocketModule().sendBinary((ByteString) null, id);
        }
    }

    @ReactMethod
    public void createFromParts(ReadableArray parts, String blobId) {
        int totalBlobSize = 0;
        ArrayList<byte[]> partList = new ArrayList<>(parts.size());
        for (int i = 0; i < parts.size(); i++) {
            ReadableMap part = parts.getMap(i);
            String string = part.getString("type");
            char c = 65535;
            switch (string.hashCode()) {
                case -891985903:
                    if (string.equals("string")) {
                        c = 1;
                        break;
                    }
                    break;
                case 3026845:
                    if (string.equals("blob")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    ReadableMap blob = part.getMap(UriUtil.DATA_SCHEME);
                    totalBlobSize += blob.getInt("size");
                    partList.add(i, resolve(blob));
                    break;
                case 1:
                    byte[] bytes = part.getString(UriUtil.DATA_SCHEME).getBytes(Charset.forName("UTF-8"));
                    totalBlobSize += bytes.length;
                    partList.add(i, bytes);
                    break;
                default:
                    throw new IllegalArgumentException("Invalid type for blob: " + part.getString("type"));
            }
        }
        ByteBuffer buffer = ByteBuffer.allocate(totalBlobSize);
        Iterator it = partList.iterator();
        while (it.hasNext()) {
            buffer.put((byte[]) it.next());
        }
        store(buffer.array(), blobId);
    }

    @ReactMethod
    public void release(String blobId) {
        remove(blobId);
    }
}
