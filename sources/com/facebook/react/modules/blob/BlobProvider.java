package com.facebook.react.modules.blob;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.os.ParcelFileDescriptor.AutoCloseOutputStream;
import android.support.annotation.Nullable;
import com.facebook.react.ReactApplication;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

public final class BlobProvider extends ContentProvider {
    public boolean onCreate() {
        return true;
    }

    @Nullable
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    @Nullable
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        if (!mode.equals("r")) {
            throw new FileNotFoundException("Cannot open " + uri.toString() + " in mode '" + mode + "'");
        }
        BlobModule blobModule = null;
        Context context = getContext().getApplicationContext();
        if (context instanceof ReactApplication) {
            blobModule = (BlobModule) ((ReactApplication) context).getReactNativeHost().getReactInstanceManager().getCurrentReactContext().getNativeModule(BlobModule.class);
        }
        if (blobModule == null) {
            throw new RuntimeException("No blob module associated with BlobProvider");
        }
        byte[] data = blobModule.resolve(uri);
        if (data == null) {
            throw new FileNotFoundException("Cannot open " + uri.toString() + ", blob not found.");
        }
        try {
            ParcelFileDescriptor[] pipe = ParcelFileDescriptor.createPipe();
            ParcelFileDescriptor readSide = pipe[0];
            OutputStream outputStream = new AutoCloseOutputStream(pipe[1]);
            try {
                outputStream.write(data);
                outputStream.close();
                return readSide;
            } catch (IOException e) {
                return null;
            }
        } catch (IOException e2) {
            return null;
        }
    }
}
