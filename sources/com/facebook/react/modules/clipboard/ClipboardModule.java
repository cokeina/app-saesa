package com.facebook.react.modules.clipboard;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build.VERSION;
import com.facebook.react.bridge.ContextBaseJavaModule;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;

@ReactModule(name = "Clipboard")
public class ClipboardModule extends ContextBaseJavaModule {
    public ClipboardModule(Context context) {
        super(context);
    }

    public String getName() {
        return "Clipboard";
    }

    private ClipboardManager getClipboardService() {
        Context context = getContext();
        getContext();
        return (ClipboardManager) context.getSystemService("clipboard");
    }

    @ReactMethod
    public void getString(Promise promise) {
        try {
            ClipboardManager clipboard = getClipboardService();
            ClipData clipData = clipboard.getPrimaryClip();
            if (clipData == null) {
                promise.resolve("");
            } else if (clipData.getItemCount() >= 1) {
                promise.resolve("" + clipboard.getPrimaryClip().getItemAt(0).getText());
            } else {
                promise.resolve("");
            }
        } catch (Exception e) {
            promise.reject((Throwable) e);
        }
    }

    @SuppressLint({"DeprecatedMethod"})
    @ReactMethod
    public void setString(String text) {
        if (VERSION.SDK_INT >= 11) {
            getClipboardService().setPrimaryClip(ClipData.newPlainText(null, text));
            return;
        }
        getClipboardService().setText(text);
    }
}
