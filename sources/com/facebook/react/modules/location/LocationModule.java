package com.facebook.react.modules.location;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import com.facebook.common.logging.FLog;
import com.facebook.common.time.Clock;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.common.SystemClock;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;
import javax.annotation.Nullable;

@ReactModule(name = "LocationObserver")
public class LocationModule extends ReactContextBaseJavaModule {
    private static final float RCT_DEFAULT_LOCATION_ACCURACY = 100.0f;
    private final LocationListener mLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            ((RCTDeviceEventEmitter) LocationModule.this.getReactApplicationContext().getJSModule(RCTDeviceEventEmitter.class)).emit("geolocationDidChange", LocationModule.locationToMap(location));
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (status == 0) {
                LocationModule.this.emitError(PositionError.POSITION_UNAVAILABLE, "Provider " + provider + " is out of service.");
            } else if (status == 1) {
                LocationModule.this.emitError(PositionError.TIMEOUT, "Provider " + provider + " is temporarily unavailable.");
            }
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    @Nullable
    private String mWatchedProvider;

    private static class LocationOptions {
        /* access modifiers changed from: private */
        public final float distanceFilter;
        /* access modifiers changed from: private */
        public final boolean highAccuracy;
        /* access modifiers changed from: private */
        public final double maximumAge;
        /* access modifiers changed from: private */
        public final long timeout;

        private LocationOptions(long timeout2, double maximumAge2, boolean highAccuracy2, float distanceFilter2) {
            this.timeout = timeout2;
            this.maximumAge = maximumAge2;
            this.highAccuracy = highAccuracy2;
            this.distanceFilter = distanceFilter2;
        }

        /* access modifiers changed from: private */
        public static LocationOptions fromReactMap(ReadableMap map) {
            return new LocationOptions(map.hasKey("timeout") ? (long) map.getDouble("timeout") : Clock.MAX_TIME, map.hasKey("maximumAge") ? map.getDouble("maximumAge") : Double.POSITIVE_INFINITY, map.hasKey("enableHighAccuracy") && map.getBoolean("enableHighAccuracy"), map.hasKey("distanceFilter") ? (float) map.getDouble("distanceFilter") : LocationModule.RCT_DEFAULT_LOCATION_ACCURACY);
        }
    }

    private static class SingleUpdateRequest {
        private static final int TWO_MINUTES = 120000;
        /* access modifiers changed from: private */
        public final Callback mError;
        /* access modifiers changed from: private */
        public final Handler mHandler;
        /* access modifiers changed from: private */
        public final LocationListener mLocationListener;
        /* access modifiers changed from: private */
        public final LocationManager mLocationManager;
        /* access modifiers changed from: private */
        public Location mOldLocation;
        private final String mProvider;
        /* access modifiers changed from: private */
        public final Callback mSuccess;
        private final long mTimeout;
        /* access modifiers changed from: private */
        public final Runnable mTimeoutRunnable;
        /* access modifiers changed from: private */
        public boolean mTriggered;

        private SingleUpdateRequest(LocationManager locationManager, String provider, long timeout, Callback success, Callback error) {
            this.mHandler = new Handler();
            this.mTimeoutRunnable = new Runnable() {
                public void run() {
                    synchronized (SingleUpdateRequest.this) {
                        if (!SingleUpdateRequest.this.mTriggered) {
                            SingleUpdateRequest.this.mError.invoke(PositionError.buildError(PositionError.TIMEOUT, "Location request timed out"));
                            SingleUpdateRequest.this.mLocationManager.removeUpdates(SingleUpdateRequest.this.mLocationListener);
                            FLog.i(ReactConstants.TAG, "LocationModule: Location request timed out");
                            SingleUpdateRequest.this.mTriggered = true;
                        }
                    }
                }
            };
            this.mLocationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    synchronized (SingleUpdateRequest.this) {
                        if (!SingleUpdateRequest.this.mTriggered && SingleUpdateRequest.this.isBetterLocation(location, SingleUpdateRequest.this.mOldLocation)) {
                            SingleUpdateRequest.this.mSuccess.invoke(LocationModule.locationToMap(location));
                            SingleUpdateRequest.this.mHandler.removeCallbacks(SingleUpdateRequest.this.mTimeoutRunnable);
                            SingleUpdateRequest.this.mTriggered = true;
                            SingleUpdateRequest.this.mLocationManager.removeUpdates(SingleUpdateRequest.this.mLocationListener);
                        }
                        SingleUpdateRequest.this.mOldLocation = location;
                    }
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };
            this.mLocationManager = locationManager;
            this.mProvider = provider;
            this.mTimeout = timeout;
            this.mSuccess = success;
            this.mError = error;
        }

        public void invoke(Location location) {
            this.mOldLocation = location;
            this.mLocationManager.requestLocationUpdates(this.mProvider, 100, 1.0f, this.mLocationListener);
            this.mHandler.postDelayed(this.mTimeoutRunnable, this.mTimeout);
        }

        /* access modifiers changed from: private */
        public boolean isBetterLocation(Location location, Location currentBestLocation) {
            if (currentBestLocation == null) {
                return true;
            }
            long timeDelta = location.getTime() - currentBestLocation.getTime();
            boolean isSignificantlyNewer = timeDelta > 120000;
            boolean isSignificantlyOlder = timeDelta < -120000;
            boolean isNewer = timeDelta > 0;
            if (isSignificantlyNewer) {
                return true;
            }
            if (isSignificantlyOlder) {
                return false;
            }
            int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
            boolean isLessAccurate = accuracyDelta > 0;
            boolean isMoreAccurate = accuracyDelta < 0;
            boolean isSignificantlyLessAccurate = accuracyDelta > 200;
            boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());
            if (isMoreAccurate) {
                return true;
            }
            if (isNewer && !isLessAccurate) {
                return true;
            }
            if (!isNewer || isSignificantlyLessAccurate || !isFromSameProvider) {
                return false;
            }
            return true;
        }

        private boolean isSameProvider(String provider1, String provider2) {
            if (provider1 == null) {
                return provider2 == null;
            }
            return provider1.equals(provider2);
        }
    }

    public LocationModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    public String getName() {
        return "LocationObserver";
    }

    @ReactMethod
    public void getCurrentPosition(ReadableMap options, Callback success, Callback error) {
        LocationOptions locationOptions = LocationOptions.fromReactMap(options);
        try {
            LocationManager locationManager = (LocationManager) getReactApplicationContext().getSystemService("location");
            String provider = getValidProvider(locationManager, locationOptions.highAccuracy);
            if (provider == null) {
                error.invoke(PositionError.buildError(PositionError.POSITION_UNAVAILABLE, "No location provider available."));
                return;
            }
            Location location = locationManager.getLastKnownLocation(provider);
            if (location == null || ((double) (SystemClock.currentTimeMillis() - location.getTime())) >= locationOptions.maximumAge) {
                new SingleUpdateRequest(locationManager, provider, locationOptions.timeout, success, error).invoke(location);
                return;
            }
            success.invoke(locationToMap(location));
        } catch (SecurityException e) {
            throwLocationPermissionMissing(e);
        }
    }

    @ReactMethod
    public void startObserving(ReadableMap options) {
        if (!"gps".equals(this.mWatchedProvider)) {
            LocationOptions locationOptions = LocationOptions.fromReactMap(options);
            try {
                LocationManager locationManager = (LocationManager) getReactApplicationContext().getSystemService("location");
                String provider = getValidProvider(locationManager, locationOptions.highAccuracy);
                if (provider == null) {
                    emitError(PositionError.POSITION_UNAVAILABLE, "No location provider available.");
                    return;
                }
                if (!provider.equals(this.mWatchedProvider)) {
                    locationManager.removeUpdates(this.mLocationListener);
                    locationManager.requestLocationUpdates(provider, 1000, locationOptions.distanceFilter, this.mLocationListener);
                }
                this.mWatchedProvider = provider;
            } catch (SecurityException e) {
                throwLocationPermissionMissing(e);
            }
        }
    }

    @ReactMethod
    public void stopObserving() {
        ((LocationManager) getReactApplicationContext().getSystemService("location")).removeUpdates(this.mLocationListener);
        this.mWatchedProvider = null;
    }

    @Nullable
    private static String getValidProvider(LocationManager locationManager, boolean highAccuracy) {
        String provider = highAccuracy ? "gps" : "network";
        if (!locationManager.isProviderEnabled(provider)) {
            provider = provider.equals("gps") ? "network" : "gps";
            if (!locationManager.isProviderEnabled(provider)) {
                return null;
            }
        }
        return provider;
    }

    /* access modifiers changed from: private */
    public static WritableMap locationToMap(Location location) {
        WritableMap map = Arguments.createMap();
        WritableMap coords = Arguments.createMap();
        coords.putDouble("latitude", location.getLatitude());
        coords.putDouble("longitude", location.getLongitude());
        coords.putDouble("altitude", location.getAltitude());
        coords.putDouble("accuracy", (double) location.getAccuracy());
        coords.putDouble("heading", (double) location.getBearing());
        coords.putDouble("speed", (double) location.getSpeed());
        map.putMap("coords", coords);
        map.putDouble("timestamp", (double) location.getTime());
        if (VERSION.SDK_INT >= 18) {
            map.putBoolean("mocked", location.isFromMockProvider());
        }
        return map;
    }

    /* access modifiers changed from: private */
    public void emitError(int code, String message) {
        ((RCTDeviceEventEmitter) getReactApplicationContext().getJSModule(RCTDeviceEventEmitter.class)).emit("geolocationError", PositionError.buildError(code, message));
    }

    private static void throwLocationPermissionMissing(SecurityException e) {
        throw new SecurityException("Looks like the app doesn't have the permission to access location.\nAdd the following line to your app's AndroidManifest.xml:\n<uses-permission android:name=\"android.permission.ACCESS_FINE_LOCATION\" />", e);
    }
}
