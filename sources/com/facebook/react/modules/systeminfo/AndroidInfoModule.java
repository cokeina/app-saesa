package com.facebook.react.modules.systeminfo;

import android.os.Build;
import android.os.Build.VERSION;
import com.facebook.react.bridge.BaseJavaModule;
import com.facebook.react.module.annotations.ReactModule;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nullable;

@ReactModule(name = "PlatformConstants")
public class AndroidInfoModule extends BaseJavaModule {
    private static final String IS_TESTING = "IS_TESTING";

    public String getName() {
        return "PlatformConstants";
    }

    @Nullable
    public Map<String, Object> getConstants() {
        HashMap<String, Object> constants = new HashMap<>();
        constants.put("Version", Integer.valueOf(VERSION.SDK_INT));
        constants.put("Release", VERSION.RELEASE);
        constants.put("Serial", Build.SERIAL);
        constants.put("Fingerprint", Build.FINGERPRINT);
        constants.put("Model", Build.MODEL);
        constants.put("ServerHost", AndroidInfoHelpers.getServerHost());
        constants.put("isTesting", Boolean.valueOf("true".equals(System.getProperty(IS_TESTING))));
        constants.put("reactNativeVersion", ReactNativeVersion.VERSION);
        return constants;
    }
}
