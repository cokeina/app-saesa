package com.facebook.react.modules.fresco;

import android.support.annotation.Nullable;
import com.facebook.common.logging.FLog;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.core.ImagePipelineConfig.Builder;
import com.facebook.imagepipeline.listener.RequestListener;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.common.ModuleDataCleaner.Cleanable;
import com.facebook.react.modules.network.CookieJarContainer;
import com.facebook.react.modules.network.ForwardingCookieHandler;
import com.facebook.react.modules.network.OkHttpClientProvider;
import java.util.HashSet;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;

@ReactModule(name = "FrescoModule")
public class FrescoModule extends ReactContextBaseJavaModule implements Cleanable, LifecycleEventListener {
    private static boolean sHasBeenInitialized = false;
    private final boolean mClearOnDestroy;
    @Nullable
    private ImagePipelineConfig mConfig;

    public FrescoModule(ReactApplicationContext reactContext) {
        this(reactContext, true, null);
    }

    public FrescoModule(ReactApplicationContext reactContext, boolean clearOnDestroy) {
        this(reactContext, clearOnDestroy, null);
    }

    public FrescoModule(ReactApplicationContext reactContext, boolean clearOnDestroy, @Nullable ImagePipelineConfig config) {
        super(reactContext);
        this.mClearOnDestroy = clearOnDestroy;
        this.mConfig = config;
    }

    public void initialize() {
        super.initialize();
        getReactApplicationContext().addLifecycleEventListener(this);
        if (!hasBeenInitialized()) {
            if (this.mConfig == null) {
                this.mConfig = getDefaultConfig(getReactApplicationContext());
            }
            Fresco.initialize(getReactApplicationContext().getApplicationContext(), this.mConfig);
            sHasBeenInitialized = true;
        } else if (this.mConfig != null) {
            FLog.w(ReactConstants.TAG, "Fresco has already been initialized with a different config. The new Fresco configuration will be ignored!");
        }
        this.mConfig = null;
    }

    public String getName() {
        return "FrescoModule";
    }

    public void clearSensitiveData() {
        Fresco.getImagePipeline().clearCaches();
    }

    public static boolean hasBeenInitialized() {
        return sHasBeenInitialized;
    }

    private static ImagePipelineConfig getDefaultConfig(ReactContext context) {
        return getDefaultConfigBuilder(context).build();
    }

    public static Builder getDefaultConfigBuilder(ReactContext context) {
        HashSet<RequestListener> requestListeners = new HashSet<>();
        requestListeners.add(new SystraceRequestListener());
        OkHttpClient client = OkHttpClientProvider.createClient();
        ((CookieJarContainer) client.cookieJar()).setCookieJar(new JavaNetCookieJar(new ForwardingCookieHandler(context)));
        return OkHttpImagePipelineConfigFactory.newBuilder(context.getApplicationContext(), client).setNetworkFetcher(new ReactOkHttpNetworkFetcher(client)).setDownsampleEnabled(false).setRequestListeners(requestListeners);
    }

    public void onHostResume() {
    }

    public void onHostPause() {
    }

    public void onHostDestroy() {
        if (hasBeenInitialized() && this.mClearOnDestroy) {
            Fresco.getImagePipeline().clearMemoryCaches();
        }
    }
}
