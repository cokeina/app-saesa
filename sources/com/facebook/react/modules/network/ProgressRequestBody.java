package com.facebook.react.modules.network;

import java.io.IOException;
import java.io.OutputStream;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
import okio.Okio;
import okio.Sink;

public class ProgressRequestBody extends RequestBody {
    private BufferedSink mBufferedSink;
    private long mContentLength = 0;
    /* access modifiers changed from: private */
    public final ProgressListener mProgressListener;
    private final RequestBody mRequestBody;

    public ProgressRequestBody(RequestBody requestBody, ProgressListener progressListener) {
        this.mRequestBody = requestBody;
        this.mProgressListener = progressListener;
    }

    public MediaType contentType() {
        return this.mRequestBody.contentType();
    }

    public long contentLength() throws IOException {
        if (this.mContentLength == 0) {
            this.mContentLength = this.mRequestBody.contentLength();
        }
        return this.mContentLength;
    }

    public void writeTo(BufferedSink sink) throws IOException {
        if (this.mBufferedSink == null) {
            this.mBufferedSink = Okio.buffer(outputStreamSink(sink));
        }
        contentLength();
        this.mRequestBody.writeTo(this.mBufferedSink);
        this.mBufferedSink.flush();
    }

    private Sink outputStreamSink(BufferedSink sink) {
        return Okio.sink((OutputStream) new CountingOutputStream(sink.outputStream()) {
            public void write(byte[] data, int offset, int byteCount) throws IOException {
                super.write(data, offset, byteCount);
                sendProgressUpdate();
            }

            public void write(int data) throws IOException {
                super.write(data);
                sendProgressUpdate();
            }

            private void sendProgressUpdate() throws IOException {
                long bytesWritten = getCount();
                long contentLength = ProgressRequestBody.this.contentLength();
                ProgressRequestBody.this.mProgressListener.onProgress(bytesWritten, contentLength, bytesWritten == contentLength);
            }
        });
    }
}
