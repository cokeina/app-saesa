package com.facebook.react.modules.network;

import android.content.Context;
import android.net.Uri;
import com.facebook.common.logging.FLog;
import com.facebook.common.time.Clock;
import com.facebook.common.util.UriUtil;
import com.facebook.react.common.ReactConstants;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.zip.GZIPOutputStream;
import javax.annotation.Nullable;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.ByteString;
import okio.Okio;
import okio.Source;

class RequestBodyUtil {
    private static final String CONTENT_ENCODING_GZIP = "gzip";
    private static final String NAME = "RequestBodyUtil";
    private static final String TEMP_FILE_SUFFIX = "temp";

    RequestBodyUtil() {
    }

    public static boolean isGzipEncoding(@Nullable String encodingType) {
        return CONTENT_ENCODING_GZIP.equalsIgnoreCase(encodingType);
    }

    @Nullable
    public static InputStream getFileInputStream(Context context, String fileContentUriStr) {
        try {
            Uri fileContentUri = Uri.parse(fileContentUriStr);
            if (fileContentUri.getScheme().startsWith(UriUtil.HTTP_SCHEME)) {
                return getDownloadFileInputStream(context, fileContentUri);
            }
            return context.getContentResolver().openInputStream(fileContentUri);
        } catch (Exception e) {
            FLog.e(ReactConstants.TAG, "Could not retrieve file for contentUri " + fileContentUriStr, (Throwable) e);
            return null;
        }
    }

    private static InputStream getDownloadFileInputStream(Context context, Uri uri) throws IOException {
        FileOutputStream stream;
        File file = File.createTempFile(NAME, TEMP_FILE_SUFFIX, context.getApplicationContext().getCacheDir());
        file.deleteOnExit();
        InputStream is = new URL(uri.toString()).openStream();
        try {
            ReadableByteChannel channel = Channels.newChannel(is);
            try {
                stream = new FileOutputStream(file);
                stream.getChannel().transferFrom(channel, 0, Clock.MAX_TIME);
                FileInputStream fileInputStream = new FileInputStream(file);
                stream.close();
                channel.close();
                return fileInputStream;
            } catch (Throwable th) {
                channel.close();
                throw th;
            }
        } finally {
            is.close();
        }
    }

    @Nullable
    public static RequestBody createGzip(MediaType mediaType, String body) {
        ByteArrayOutputStream gzipByteArrayOutputStream = new ByteArrayOutputStream();
        try {
            OutputStream gzipOutputStream = new GZIPOutputStream(gzipByteArrayOutputStream);
            gzipOutputStream.write(body.getBytes());
            gzipOutputStream.close();
            return RequestBody.create(mediaType, gzipByteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            return null;
        }
    }

    public static RequestBody create(final MediaType mediaType, final InputStream inputStream) {
        return new RequestBody() {
            public MediaType contentType() {
                return mediaType;
            }

            public long contentLength() {
                try {
                    return (long) inputStream.available();
                } catch (IOException e) {
                    return 0;
                }
            }

            public void writeTo(BufferedSink sink) throws IOException {
                Source source = null;
                try {
                    source = Okio.source(inputStream);
                    sink.writeAll(source);
                } finally {
                    Util.closeQuietly((Closeable) source);
                }
            }
        };
    }

    public static ProgressRequestBody createProgressRequest(RequestBody requestBody, ProgressListener listener) {
        return new ProgressRequestBody(requestBody, listener);
    }

    public static RequestBody getEmptyBody(String method) {
        if (method.equals("POST") || method.equals("PUT") || method.equals("PATCH")) {
            return RequestBody.create((MediaType) null, ByteString.EMPTY);
        }
        return null;
    }
}
