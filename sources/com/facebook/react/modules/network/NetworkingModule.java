package com.facebook.react.modules.network;

import android.net.Uri;
import android.util.Base64;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.GuardedAsyncTask;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.StandardCharsets;
import com.facebook.react.common.network.OkHttpCallUtil;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.CookieJar;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.JavaNetCookieJar;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.ByteString;

@ReactModule(name = "Networking")
public final class NetworkingModule extends ReactContextBaseJavaModule {
    private static final int CHUNK_TIMEOUT_NS = 100000000;
    private static final String CONTENT_ENCODING_HEADER_NAME = "content-encoding";
    private static final String CONTENT_TYPE_HEADER_NAME = "content-type";
    private static final int MAX_CHUNK_SIZE_BETWEEN_FLUSHES = 8192;
    protected static final String NAME = "Networking";
    private static final String REQUEST_BODY_KEY_BASE64 = "base64";
    private static final String REQUEST_BODY_KEY_FORMDATA = "formData";
    private static final String REQUEST_BODY_KEY_STRING = "string";
    private static final String REQUEST_BODY_KEY_URI = "uri";
    private static final String USER_AGENT_HEADER_NAME = "user-agent";
    /* access modifiers changed from: private */
    public final OkHttpClient mClient;
    private final ForwardingCookieHandler mCookieHandler;
    private final CookieJarContainer mCookieJarContainer;
    @Nullable
    private final String mDefaultUserAgent;
    private final List<RequestBodyHandler> mRequestBodyHandlers;
    private final Set<Integer> mRequestIds;
    /* access modifiers changed from: private */
    public final List<ResponseHandler> mResponseHandlers;
    /* access modifiers changed from: private */
    public boolean mShuttingDown;
    private final List<UriHandler> mUriHandlers;

    public interface RequestBodyHandler {
        boolean supports(ReadableMap readableMap);

        RequestBody toRequestBody(ReadableMap readableMap, String str);
    }

    public interface ResponseHandler {
        boolean supports(String str);

        WritableMap toResponseData(ResponseBody responseBody) throws IOException;
    }

    public interface UriHandler {
        WritableMap fetch(Uri uri) throws IOException;

        boolean supports(Uri uri, String str);
    }

    NetworkingModule(ReactApplicationContext reactContext, @Nullable String defaultUserAgent, OkHttpClient client, @Nullable List<NetworkInterceptorCreator> networkInterceptorCreators) {
        super(reactContext);
        this.mRequestBodyHandlers = new ArrayList();
        this.mUriHandlers = new ArrayList();
        this.mResponseHandlers = new ArrayList();
        if (networkInterceptorCreators != null) {
            Builder clientBuilder = client.newBuilder();
            for (NetworkInterceptorCreator networkInterceptorCreator : networkInterceptorCreators) {
                clientBuilder.addNetworkInterceptor(networkInterceptorCreator.create());
            }
            client = clientBuilder.build();
        }
        this.mClient = client;
        this.mCookieHandler = new ForwardingCookieHandler(reactContext);
        this.mCookieJarContainer = (CookieJarContainer) this.mClient.cookieJar();
        this.mShuttingDown = false;
        this.mDefaultUserAgent = defaultUserAgent;
        this.mRequestIds = new HashSet();
    }

    NetworkingModule(ReactApplicationContext context, @Nullable String defaultUserAgent, OkHttpClient client) {
        this(context, defaultUserAgent, client, null);
    }

    public NetworkingModule(ReactApplicationContext context) {
        this(context, null, OkHttpClientProvider.createClient(), null);
    }

    public NetworkingModule(ReactApplicationContext context, List<NetworkInterceptorCreator> networkInterceptorCreators) {
        this(context, null, OkHttpClientProvider.createClient(), networkInterceptorCreators);
    }

    public NetworkingModule(ReactApplicationContext context, String defaultUserAgent) {
        this(context, defaultUserAgent, OkHttpClientProvider.createClient(), null);
    }

    public void initialize() {
        this.mCookieJarContainer.setCookieJar(new JavaNetCookieJar(this.mCookieHandler));
    }

    public String getName() {
        return NAME;
    }

    public void onCatalystInstanceDestroy() {
        this.mShuttingDown = true;
        cancelAllRequests();
        this.mCookieHandler.destroy();
        this.mCookieJarContainer.removeCookieJar();
        this.mRequestBodyHandlers.clear();
        this.mResponseHandlers.clear();
        this.mUriHandlers.clear();
    }

    public void addUriHandler(UriHandler handler) {
        this.mUriHandlers.add(handler);
    }

    public void addRequestBodyHandler(RequestBodyHandler handler) {
        this.mRequestBodyHandlers.add(handler);
    }

    public void addResponseHandler(ResponseHandler handler) {
        this.mResponseHandlers.add(handler);
    }

    public void removeUriHandler(UriHandler handler) {
        this.mUriHandlers.remove(handler);
    }

    public void removeRequestBodyHandler(RequestBodyHandler handler) {
        this.mRequestBodyHandlers.remove(handler);
    }

    public void removeResponseHandler(ResponseHandler handler) {
        this.mResponseHandlers.remove(handler);
    }

    @ReactMethod
    public void sendRequest(String method, String url, int requestId, ReadableArray headers, ReadableMap data, String responseType, boolean useIncrementalUpdates, int timeout, boolean withCredentials) {
        RequestBody requestBody;
        final RCTDeviceEventEmitter eventEmitter = getEventEmitter();
        try {
            Uri uri = Uri.parse(url);
            for (UriHandler handler : this.mUriHandlers) {
                if (handler.supports(uri, responseType)) {
                    ResponseUtil.onDataReceived(eventEmitter, requestId, handler.fetch(uri));
                    ResponseUtil.onRequestSuccess(eventEmitter, requestId);
                    return;
                }
            }
            Request.Builder requestBuilder = new Request.Builder().url(url);
            if (requestId != 0) {
                requestBuilder.tag(Integer.valueOf(requestId));
            }
            Builder clientBuilder = this.mClient.newBuilder();
            if (!withCredentials) {
                clientBuilder.cookieJar(CookieJar.NO_COOKIES);
            }
            if (useIncrementalUpdates) {
                final String str = responseType;
                final int i = requestId;
                clientBuilder.addNetworkInterceptor(new Interceptor() {
                    public Response intercept(Chain chain) throws IOException {
                        Response originalResponse = chain.proceed(chain.request());
                        return originalResponse.newBuilder().body(new ProgressResponseBody(originalResponse.body(), new ProgressListener() {
                            long last = System.nanoTime();

                            public void onProgress(long bytesWritten, long contentLength, boolean done) {
                                long now = System.nanoTime();
                                if ((done || NetworkingModule.shouldDispatch(now, this.last)) && !str.equals("text")) {
                                    ResponseUtil.onDataReceivedProgress(eventEmitter, i, bytesWritten, contentLength);
                                    this.last = now;
                                }
                            }
                        })).build();
                    }
                });
            }
            if (timeout != this.mClient.connectTimeoutMillis()) {
                clientBuilder.readTimeout((long) timeout, TimeUnit.MILLISECONDS);
            }
            OkHttpClient client = clientBuilder.build();
            Headers requestHeaders = extractHeaders(headers, data);
            if (requestHeaders == null) {
                ResponseUtil.onRequestError(eventEmitter, requestId, "Unrecognized headers format", null);
                return;
            }
            String contentType = requestHeaders.get(CONTENT_TYPE_HEADER_NAME);
            String contentEncoding = requestHeaders.get(CONTENT_ENCODING_HEADER_NAME);
            requestBuilder.headers(requestHeaders);
            RequestBodyHandler handler2 = null;
            if (data != null) {
                Iterator it = this.mRequestBodyHandlers.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    RequestBodyHandler curHandler = (RequestBodyHandler) it.next();
                    if (curHandler.supports(data)) {
                        handler2 = curHandler;
                        break;
                    }
                }
            }
            if (data == null || method.toLowerCase().equals("get") || method.toLowerCase().equals("head")) {
                requestBody = RequestBodyUtil.getEmptyBody(method);
            } else if (handler2 != null) {
                requestBody = handler2.toRequestBody(data, contentType);
            } else {
                if (!data.hasKey(REQUEST_BODY_KEY_STRING)) {
                    if (!data.hasKey("base64")) {
                        if (!data.hasKey("uri")) {
                            if (data.hasKey(REQUEST_BODY_KEY_FORMDATA)) {
                                if (contentType == null) {
                                    contentType = "multipart/form-data";
                                }
                                MultipartBody.Builder multipartBuilder = constructMultipartBody(data.getArray(REQUEST_BODY_KEY_FORMDATA), contentType, requestId);
                                if (multipartBuilder != null) {
                                    requestBody = multipartBuilder.build();
                                } else {
                                    return;
                                }
                            } else {
                                requestBody = RequestBodyUtil.getEmptyBody(method);
                            }
                        } else if (contentType == null) {
                            ResponseUtil.onRequestError(eventEmitter, requestId, "Payload is set but no content-type header specified", null);
                            return;
                        } else {
                            String uri2 = data.getString("uri");
                            InputStream fileInputStream = RequestBodyUtil.getFileInputStream(getReactApplicationContext(), uri2);
                            if (fileInputStream == null) {
                                ResponseUtil.onRequestError(eventEmitter, requestId, "Could not retrieve file for uri " + uri2, null);
                                return;
                            }
                            requestBody = RequestBodyUtil.create(MediaType.parse(contentType), fileInputStream);
                        }
                    } else if (contentType == null) {
                        ResponseUtil.onRequestError(eventEmitter, requestId, "Payload is set but no content-type header specified", null);
                        return;
                    } else {
                        requestBody = RequestBody.create(MediaType.parse(contentType), ByteString.decodeBase64(data.getString("base64")));
                    }
                } else if (contentType == null) {
                    ResponseUtil.onRequestError(eventEmitter, requestId, "Payload is set but no content-type header specified", null);
                    return;
                } else {
                    String body = data.getString(REQUEST_BODY_KEY_STRING);
                    MediaType contentMediaType = MediaType.parse(contentType);
                    if (RequestBodyUtil.isGzipEncoding(contentEncoding)) {
                        requestBody = RequestBodyUtil.createGzip(contentMediaType, body);
                        if (requestBody == null) {
                            ResponseUtil.onRequestError(eventEmitter, requestId, "Failed to gzip request body", null);
                            return;
                        }
                    } else {
                        requestBody = RequestBody.create(contentMediaType, body);
                    }
                }
            }
            requestBuilder.method(method, wrapRequestBodyWithProgressEmitter(requestBody, eventEmitter, requestId));
            addRequest(requestId);
            final int i2 = requestId;
            final String str2 = responseType;
            final boolean z = useIncrementalUpdates;
            client.newCall(requestBuilder.build()).enqueue(new Callback() {
                public void onFailure(Call call, IOException e) {
                    String errorMessage;
                    if (!NetworkingModule.this.mShuttingDown) {
                        NetworkingModule.this.removeRequest(i2);
                        if (e.getMessage() != null) {
                            errorMessage = e.getMessage();
                        } else {
                            errorMessage = "Error while executing request: " + e.getClass().getSimpleName();
                        }
                        ResponseUtil.onRequestError(eventEmitter, i2, errorMessage, e);
                    }
                }

                public void onResponse(Call call, Response response) throws IOException {
                    if (!NetworkingModule.this.mShuttingDown) {
                        NetworkingModule.this.removeRequest(i2);
                        ResponseUtil.onResponseReceived(eventEmitter, i2, response.code(), NetworkingModule.translateHeaders(response.headers()), response.request().url().toString());
                        ResponseBody responseBody = response.body();
                        try {
                            for (ResponseHandler handler : NetworkingModule.this.mResponseHandlers) {
                                if (handler.supports(str2)) {
                                    ResponseUtil.onDataReceived(eventEmitter, i2, handler.toResponseData(responseBody));
                                    ResponseUtil.onRequestSuccess(eventEmitter, i2);
                                    return;
                                }
                            }
                            if (!z || !str2.equals("text")) {
                                String responseString = "";
                                if (str2.equals("text")) {
                                    try {
                                        responseString = responseBody.string();
                                    } catch (IOException e) {
                                        if (!response.request().method().equalsIgnoreCase("HEAD")) {
                                            ResponseUtil.onRequestError(eventEmitter, i2, e.getMessage(), e);
                                        }
                                    }
                                } else if (str2.equals("base64")) {
                                    responseString = Base64.encodeToString(responseBody.bytes(), 2);
                                }
                                ResponseUtil.onDataReceived(eventEmitter, i2, responseString);
                                ResponseUtil.onRequestSuccess(eventEmitter, i2);
                                return;
                            }
                            NetworkingModule.this.readWithProgress(eventEmitter, i2, responseBody);
                            ResponseUtil.onRequestSuccess(eventEmitter, i2);
                        } catch (IOException e2) {
                            ResponseUtil.onRequestError(eventEmitter, i2, e2.getMessage(), e2);
                        }
                    }
                }
            });
        } catch (IOException e) {
            ResponseUtil.onRequestError(eventEmitter, requestId, e.getMessage(), e);
        }
    }

    private RequestBody wrapRequestBodyWithProgressEmitter(RequestBody requestBody, final RCTDeviceEventEmitter eventEmitter, final int requestId) {
        if (requestBody == null) {
            return null;
        }
        return RequestBodyUtil.createProgressRequest(requestBody, new ProgressListener() {
            long last = System.nanoTime();

            public void onProgress(long bytesWritten, long contentLength, boolean done) {
                long now = System.nanoTime();
                if (done || NetworkingModule.shouldDispatch(now, this.last)) {
                    ResponseUtil.onDataSend(eventEmitter, requestId, bytesWritten, contentLength);
                    this.last = now;
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void readWithProgress(RCTDeviceEventEmitter eventEmitter, int requestId, ResponseBody responseBody) throws IOException {
        Charset charset;
        long totalBytesRead = -1;
        long contentLength = -1;
        try {
            ProgressResponseBody progressResponseBody = (ProgressResponseBody) responseBody;
            totalBytesRead = progressResponseBody.totalBytesRead();
            contentLength = progressResponseBody.contentLength();
        } catch (ClassCastException e) {
        }
        if (responseBody.contentType() == null) {
            charset = StandardCharsets.UTF_8;
        } else {
            charset = responseBody.contentType().charset(StandardCharsets.UTF_8);
        }
        ProgressiveStringDecoder streamDecoder = new ProgressiveStringDecoder(charset);
        InputStream inputStream = responseBody.byteStream();
        try {
            byte[] buffer = new byte[8192];
            while (true) {
                int read = inputStream.read(buffer);
                if (read != -1) {
                    ResponseUtil.onIncrementalDataReceived(eventEmitter, requestId, streamDecoder.decodeNext(buffer, read), totalBytesRead, contentLength);
                } else {
                    return;
                }
            }
        } finally {
            inputStream.close();
        }
    }

    /* access modifiers changed from: private */
    public static boolean shouldDispatch(long now, long last) {
        return 100000000 + last < now;
    }

    private synchronized void addRequest(int requestId) {
        this.mRequestIds.add(Integer.valueOf(requestId));
    }

    /* access modifiers changed from: private */
    public synchronized void removeRequest(int requestId) {
        this.mRequestIds.remove(Integer.valueOf(requestId));
    }

    private synchronized void cancelAllRequests() {
        for (Integer requestId : this.mRequestIds) {
            cancelRequest(requestId.intValue());
        }
        this.mRequestIds.clear();
    }

    /* access modifiers changed from: private */
    public static WritableMap translateHeaders(Headers headers) {
        WritableMap responseHeaders = Arguments.createMap();
        for (int i = 0; i < headers.size(); i++) {
            String headerName = headers.name(i);
            if (responseHeaders.hasKey(headerName)) {
                responseHeaders.putString(headerName, responseHeaders.getString(headerName) + ", " + headers.value(i));
            } else {
                responseHeaders.putString(headerName, headers.value(i));
            }
        }
        return responseHeaders;
    }

    @ReactMethod
    public void abortRequest(int requestId) {
        cancelRequest(requestId);
        removeRequest(requestId);
    }

    private void cancelRequest(final int requestId) {
        new GuardedAsyncTask<Void, Void>(getReactApplicationContext()) {
            /* access modifiers changed from: protected */
            public void doInBackgroundGuarded(Void... params) {
                OkHttpCallUtil.cancelTag(NetworkingModule.this.mClient, Integer.valueOf(requestId));
            }
        }.execute(new Void[0]);
    }

    @ReactMethod
    public void clearCookies(com.facebook.react.bridge.Callback callback) {
        this.mCookieHandler.clearCookies(callback);
    }

    @Nullable
    private MultipartBody.Builder constructMultipartBody(ReadableArray body, String contentType, int requestId) {
        RCTDeviceEventEmitter eventEmitter = getEventEmitter();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder();
        multipartBuilder.setType(MediaType.parse(contentType));
        int size = body.size();
        for (int i = 0; i < size; i++) {
            ReadableMap bodyPart = body.getMap(i);
            Headers headers = extractHeaders(bodyPart.getArray("headers"), null);
            if (headers == null) {
                ResponseUtil.onRequestError(eventEmitter, requestId, "Missing or invalid header format for FormData part.", null);
                return null;
            }
            MediaType partContentType = null;
            String partContentTypeStr = headers.get(CONTENT_TYPE_HEADER_NAME);
            if (partContentTypeStr != null) {
                partContentType = MediaType.parse(partContentTypeStr);
                headers = headers.newBuilder().removeAll(CONTENT_TYPE_HEADER_NAME).build();
            }
            if (bodyPart.hasKey(REQUEST_BODY_KEY_STRING)) {
                multipartBuilder.addPart(headers, RequestBody.create(partContentType, bodyPart.getString(REQUEST_BODY_KEY_STRING)));
            } else if (!bodyPart.hasKey("uri")) {
                ResponseUtil.onRequestError(eventEmitter, requestId, "Unrecognized FormData part.", null);
            } else if (partContentType == null) {
                ResponseUtil.onRequestError(eventEmitter, requestId, "Binary FormData part needs a content-type header.", null);
                return null;
            } else {
                String fileContentUriStr = bodyPart.getString("uri");
                InputStream fileInputStream = RequestBodyUtil.getFileInputStream(getReactApplicationContext(), fileContentUriStr);
                if (fileInputStream == null) {
                    ResponseUtil.onRequestError(eventEmitter, requestId, "Could not retrieve file for uri " + fileContentUriStr, null);
                    return null;
                }
                multipartBuilder.addPart(headers, RequestBodyUtil.create(partContentType, fileInputStream));
            }
        }
        return multipartBuilder;
    }

    @Nullable
    private Headers extractHeaders(@Nullable ReadableArray headersArray, @Nullable ReadableMap requestData) {
        boolean isGzipSupported = true;
        if (headersArray == null) {
            return null;
        }
        Headers.Builder headersBuilder = new Headers.Builder();
        int size = headersArray.size();
        for (int headersIdx = 0; headersIdx < size; headersIdx++) {
            ReadableArray header = headersArray.getArray(headersIdx);
            if (header == null || header.size() != 2) {
                return null;
            }
            String headerName = header.getString(0);
            String headerValue = header.getString(1);
            if (headerName == null || headerValue == null) {
                return null;
            }
            headersBuilder.add(headerName, headerValue);
        }
        if (headersBuilder.get(USER_AGENT_HEADER_NAME) == null && this.mDefaultUserAgent != null) {
            headersBuilder.add(USER_AGENT_HEADER_NAME, this.mDefaultUserAgent);
        }
        if (requestData == null || !requestData.hasKey(REQUEST_BODY_KEY_STRING)) {
            isGzipSupported = false;
        }
        if (!isGzipSupported) {
            headersBuilder.removeAll(CONTENT_ENCODING_HEADER_NAME);
        }
        return headersBuilder.build();
    }

    private RCTDeviceEventEmitter getEventEmitter() {
        return (RCTDeviceEventEmitter) getReactApplicationContext().getJSModule(RCTDeviceEventEmitter.class);
    }
}
