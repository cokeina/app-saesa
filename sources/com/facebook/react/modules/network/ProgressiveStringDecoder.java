package com.facebook.react.modules.network;

import com.facebook.common.logging.FLog;
import com.facebook.react.common.ReactConstants;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

public class ProgressiveStringDecoder {
    private static final String EMPTY_STRING = "";
    private final CharsetDecoder mDecoder;
    private byte[] remainder = null;

    public ProgressiveStringDecoder(Charset charset) {
        this.mDecoder = charset.newDecoder();
    }

    public String decodeNext(byte[] data, int length) {
        byte[] decodeData;
        boolean hasRemainder;
        if (this.remainder != null) {
            decodeData = new byte[(this.remainder.length + length)];
            System.arraycopy(this.remainder, 0, decodeData, 0, this.remainder.length);
            System.arraycopy(data, 0, decodeData, this.remainder.length, length);
            length += this.remainder.length;
        } else {
            decodeData = data;
        }
        ByteBuffer decodeBuffer = ByteBuffer.wrap(decodeData, 0, length);
        CharBuffer result = null;
        boolean decoded = false;
        int remainderLenght = 0;
        while (!decoded && remainderLenght < 4) {
            try {
                result = this.mDecoder.decode(decodeBuffer);
                decoded = true;
            } catch (CharacterCodingException e) {
                remainderLenght++;
                decodeBuffer = ByteBuffer.wrap(decodeData, 0, length - remainderLenght);
            }
        }
        if (!decoded || remainderLenght <= 0) {
            hasRemainder = false;
        } else {
            hasRemainder = true;
        }
        if (hasRemainder) {
            this.remainder = new byte[remainderLenght];
            System.arraycopy(decodeData, length - remainderLenght, this.remainder, 0, remainderLenght);
        } else {
            this.remainder = null;
        }
        if (decoded) {
            return new String(result.array(), 0, result.length());
        }
        FLog.w(ReactConstants.TAG, "failed to decode string from byte array");
        return "";
    }
}
