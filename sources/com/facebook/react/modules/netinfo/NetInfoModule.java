package com.facebook.react.modules.netinfo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.net.ConnectivityManagerCompat;
import android.support.v4.os.EnvironmentCompat;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;

@ReactModule(name = "NetInfo")
public class NetInfoModule extends ReactContextBaseJavaModule implements LifecycleEventListener {
    private static final String CONNECTION_TYPE_BLUETOOTH = "bluetooth";
    private static final String CONNECTION_TYPE_CELLULAR = "cellular";
    private static final String CONNECTION_TYPE_ETHERNET = "ethernet";
    private static final String CONNECTION_TYPE_NONE = "none";
    private static final String CONNECTION_TYPE_NONE_DEPRECATED = "NONE";
    private static final String CONNECTION_TYPE_UNKNOWN = "unknown";
    private static final String CONNECTION_TYPE_UNKNOWN_DEPRECATED = "UNKNOWN";
    private static final String CONNECTION_TYPE_WIFI = "wifi";
    private static final String CONNECTION_TYPE_WIMAX = "wimax";
    private static final String EFFECTIVE_CONNECTION_TYPE_2G = "2g";
    private static final String EFFECTIVE_CONNECTION_TYPE_3G = "3g";
    private static final String EFFECTIVE_CONNECTION_TYPE_4G = "4g";
    private static final String EFFECTIVE_CONNECTION_TYPE_UNKNOWN = "unknown";
    private static final String ERROR_MISSING_PERMISSION = "E_MISSING_PERMISSION";
    private static final String MISSING_PERMISSION_MESSAGE = "To use NetInfo on Android, add the following to your AndroidManifest.xml:\n<uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\" />";
    private String mConnectionType = EnvironmentCompat.MEDIA_UNKNOWN;
    private final ConnectivityBroadcastReceiver mConnectivityBroadcastReceiver;
    private String mConnectivityDeprecated = CONNECTION_TYPE_UNKNOWN_DEPRECATED;
    private final ConnectivityManager mConnectivityManager;
    private String mEffectiveConnectionType = EnvironmentCompat.MEDIA_UNKNOWN;
    private boolean mNoNetworkPermission = false;

    private class ConnectivityBroadcastReceiver extends BroadcastReceiver {
        private boolean isRegistered;

        private ConnectivityBroadcastReceiver() {
            this.isRegistered = false;
        }

        public void setRegistered(boolean registered) {
            this.isRegistered = registered;
        }

        public boolean isRegistered() {
            return this.isRegistered;
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                NetInfoModule.this.updateAndSendConnectionType();
            }
        }
    }

    public NetInfoModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.mConnectivityManager = (ConnectivityManager) reactContext.getSystemService("connectivity");
        this.mConnectivityBroadcastReceiver = new ConnectivityBroadcastReceiver();
    }

    public void onHostResume() {
        registerReceiver();
    }

    public void onHostPause() {
        unregisterReceiver();
    }

    public void onHostDestroy() {
    }

    public void initialize() {
        getReactApplicationContext().addLifecycleEventListener(this);
    }

    public String getName() {
        return "NetInfo";
    }

    @ReactMethod
    public void getCurrentConnectivity(Promise promise) {
        if (this.mNoNetworkPermission) {
            promise.reject(ERROR_MISSING_PERMISSION, MISSING_PERMISSION_MESSAGE, null);
        } else {
            promise.resolve(createConnectivityEventMap());
        }
    }

    @ReactMethod
    public void isConnectionMetered(Promise promise) {
        if (this.mNoNetworkPermission) {
            promise.reject(ERROR_MISSING_PERMISSION, MISSING_PERMISSION_MESSAGE, null);
        } else {
            promise.resolve(Boolean.valueOf(ConnectivityManagerCompat.isActiveNetworkMetered(this.mConnectivityManager)));
        }
    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        getReactApplicationContext().registerReceiver(this.mConnectivityBroadcastReceiver, filter);
        this.mConnectivityBroadcastReceiver.setRegistered(true);
        updateAndSendConnectionType();
    }

    private void unregisterReceiver() {
        if (this.mConnectivityBroadcastReceiver.isRegistered()) {
            getReactApplicationContext().unregisterReceiver(this.mConnectivityBroadcastReceiver);
            this.mConnectivityBroadcastReceiver.setRegistered(false);
        }
    }

    /* access modifiers changed from: private */
    public void updateAndSendConnectionType() {
        String connectionType;
        String currentConnectivity;
        String str = EnvironmentCompat.MEDIA_UNKNOWN;
        String effectiveConnectionType = EnvironmentCompat.MEDIA_UNKNOWN;
        try {
            NetworkInfo networkInfo = this.mConnectivityManager.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()) {
                connectionType = "none";
                currentConnectivity = getCurrentConnectionType();
                if (connectionType.equalsIgnoreCase(this.mConnectionType) || !effectiveConnectionType.equalsIgnoreCase(this.mEffectiveConnectionType) || !currentConnectivity.equalsIgnoreCase(this.mConnectivityDeprecated)) {
                    this.mConnectionType = connectionType;
                    this.mEffectiveConnectionType = effectiveConnectionType;
                    this.mConnectivityDeprecated = currentConnectivity;
                    sendConnectivityChangedEvent();
                }
                return;
            }
            switch (networkInfo.getType()) {
                case 0:
                case 4:
                    connectionType = CONNECTION_TYPE_CELLULAR;
                    effectiveConnectionType = getEffectiveConnectionType(networkInfo);
                    break;
                case 1:
                    connectionType = CONNECTION_TYPE_WIFI;
                    break;
                case 6:
                    connectionType = CONNECTION_TYPE_WIMAX;
                    break;
                case 7:
                    connectionType = CONNECTION_TYPE_BLUETOOTH;
                    break;
                case 9:
                    connectionType = CONNECTION_TYPE_ETHERNET;
                    break;
                default:
                    connectionType = EnvironmentCompat.MEDIA_UNKNOWN;
                    break;
            }
            currentConnectivity = getCurrentConnectionType();
            if (connectionType.equalsIgnoreCase(this.mConnectionType)) {
            }
            this.mConnectionType = connectionType;
            this.mEffectiveConnectionType = effectiveConnectionType;
            this.mConnectivityDeprecated = currentConnectivity;
            sendConnectivityChangedEvent();
        } catch (SecurityException e) {
            this.mNoNetworkPermission = true;
            connectionType = EnvironmentCompat.MEDIA_UNKNOWN;
        }
    }

    private String getCurrentConnectionType() {
        try {
            NetworkInfo networkInfo = this.mConnectivityManager.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()) {
                return CONNECTION_TYPE_NONE_DEPRECATED;
            }
            if (ConnectivityManager.isNetworkTypeValid(networkInfo.getType())) {
                return networkInfo.getTypeName().toUpperCase();
            }
            return CONNECTION_TYPE_UNKNOWN_DEPRECATED;
        } catch (SecurityException e) {
            this.mNoNetworkPermission = true;
            return CONNECTION_TYPE_UNKNOWN_DEPRECATED;
        }
    }

    private String getEffectiveConnectionType(NetworkInfo networkInfo) {
        switch (networkInfo.getSubtype()) {
            case 1:
            case 2:
            case 4:
            case 7:
            case 11:
                return EFFECTIVE_CONNECTION_TYPE_2G;
            case 3:
            case 5:
            case 6:
            case 8:
            case 9:
            case 10:
            case 12:
            case 14:
                return EFFECTIVE_CONNECTION_TYPE_3G;
            case 13:
            case 15:
                return EFFECTIVE_CONNECTION_TYPE_4G;
            default:
                return EnvironmentCompat.MEDIA_UNKNOWN;
        }
    }

    private void sendConnectivityChangedEvent() {
        ((RCTDeviceEventEmitter) getReactApplicationContext().getJSModule(RCTDeviceEventEmitter.class)).emit("networkStatusDidChange", createConnectivityEventMap());
    }

    private WritableMap createConnectivityEventMap() {
        WritableMap event = new WritableNativeMap();
        event.putString("network_info", this.mConnectivityDeprecated);
        event.putString("connectionType", this.mConnectionType);
        event.putString("effectiveConnectionType", this.mEffectiveConnectionType);
        return event;
    }
}
