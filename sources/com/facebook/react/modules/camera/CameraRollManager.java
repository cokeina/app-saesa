package com.facebook.react.modules.camera;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Environment;
import com.facebook.common.logging.FLog;
import com.facebook.react.bridge.GuardedAsyncTask;
import com.facebook.react.bridge.JSApplicationIllegalArgumentException;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.module.annotations.ReactModule;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import javax.annotation.Nullable;

@ReactModule(name = "CameraRollManager")
public class CameraRollManager extends ReactContextBaseJavaModule {
    private static final String ERROR_UNABLE_TO_LOAD = "E_UNABLE_TO_LOAD";
    private static final String ERROR_UNABLE_TO_LOAD_PERMISSION = "E_UNABLE_TO_LOAD_PERMISSION";
    private static final String ERROR_UNABLE_TO_SAVE = "E_UNABLE_TO_SAVE";
    public static final boolean IS_JELLY_BEAN_OR_LATER = (VERSION.SDK_INT >= 16);
    protected static final String NAME = "CameraRollManager";
    /* access modifiers changed from: private */
    public static final String[] PROJECTION;
    private static final String SELECTION_BUCKET = "bucket_display_name = ?";
    private static final String SELECTION_DATE_TAKEN = "datetaken < ?";

    private static class GetPhotosTask extends GuardedAsyncTask<Void, Void> {
        @Nullable
        private final String mAfter;
        @Nullable
        private final String mAssetType;
        private final Context mContext;
        private final int mFirst;
        @Nullable
        private final String mGroupName;
        @Nullable
        private final ReadableArray mMimeTypes;
        private final Promise mPromise;

        private GetPhotosTask(ReactContext context, int first, @Nullable String after, @Nullable String groupName, @Nullable ReadableArray mimeTypes, @Nullable String assetType, Promise promise) {
            super(context);
            this.mContext = context;
            this.mFirst = first;
            this.mAfter = after;
            this.mGroupName = groupName;
            this.mMimeTypes = mimeTypes;
            this.mPromise = promise;
            this.mAssetType = assetType;
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void doInBackgroundGuarded(java.lang.Void... r14) {
            /*
                r13 = this;
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                java.lang.String r2 = "1"
                r10.<init>(r2)
                java.util.ArrayList r11 = new java.util.ArrayList
                r11.<init>()
                java.lang.String r2 = r13.mAfter
                boolean r2 = android.text.TextUtils.isEmpty(r2)
                if (r2 != 0) goto L_0x001e
                java.lang.String r2 = " AND datetaken < ?"
                r10.append(r2)
                java.lang.String r2 = r13.mAfter
                r11.add(r2)
            L_0x001e:
                java.lang.String r2 = r13.mGroupName
                boolean r2 = android.text.TextUtils.isEmpty(r2)
                if (r2 != 0) goto L_0x0030
                java.lang.String r2 = " AND bucket_display_name = ?"
                r10.append(r2)
                java.lang.String r2 = r13.mGroupName
                r11.add(r2)
            L_0x0030:
                com.facebook.react.bridge.ReadableArray r2 = r13.mMimeTypes
                if (r2 == 0) goto L_0x006a
                com.facebook.react.bridge.ReadableArray r2 = r13.mMimeTypes
                int r2 = r2.size()
                if (r2 <= 0) goto L_0x006a
                java.lang.String r2 = " AND mime_type IN ("
                r10.append(r2)
                r7 = 0
            L_0x0042:
                com.facebook.react.bridge.ReadableArray r2 = r13.mMimeTypes
                int r2 = r2.size()
                if (r7 >= r2) goto L_0x005b
                java.lang.String r2 = "?,"
                r10.append(r2)
                com.facebook.react.bridge.ReadableArray r2 = r13.mMimeTypes
                java.lang.String r2 = r2.getString(r7)
                r11.add(r2)
                int r7 = r7 + 1
                goto L_0x0042
            L_0x005b:
                int r2 = r10.length()
                int r2 = r2 + -1
                int r3 = r10.length()
                java.lang.String r4 = ")"
                r10.replace(r2, r3, r4)
            L_0x006a:
                com.facebook.react.bridge.WritableNativeMap r9 = new com.facebook.react.bridge.WritableNativeMap
                r9.<init>()
                android.content.Context r2 = r13.mContext
                android.content.ContentResolver r0 = r2.getContentResolver()
                java.lang.String r2 = r13.mAssetType     // Catch:{ SecurityException -> 0x00d8 }
                if (r2 == 0) goto L_0x00c0
                java.lang.String r2 = r13.mAssetType     // Catch:{ SecurityException -> 0x00d8 }
                java.lang.String r3 = "Videos"
                boolean r2 = r2.equals(r3)     // Catch:{ SecurityException -> 0x00d8 }
                if (r2 == 0) goto L_0x00c0
                android.net.Uri r1 = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI     // Catch:{ SecurityException -> 0x00d8 }
            L_0x0085:
                java.lang.String[] r2 = com.facebook.react.modules.camera.CameraRollManager.PROJECTION     // Catch:{ SecurityException -> 0x00d8 }
                java.lang.String r3 = r10.toString()     // Catch:{ SecurityException -> 0x00d8 }
                int r4 = r11.size()     // Catch:{ SecurityException -> 0x00d8 }
                java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SecurityException -> 0x00d8 }
                java.lang.Object[] r4 = r11.toArray(r4)     // Catch:{ SecurityException -> 0x00d8 }
                java.lang.String[] r4 = (java.lang.String[]) r4     // Catch:{ SecurityException -> 0x00d8 }
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x00d8 }
                r5.<init>()     // Catch:{ SecurityException -> 0x00d8 }
                java.lang.String r12 = "datetaken DESC, date_modified DESC LIMIT "
                java.lang.StringBuilder r5 = r5.append(r12)     // Catch:{ SecurityException -> 0x00d8 }
                int r12 = r13.mFirst     // Catch:{ SecurityException -> 0x00d8 }
                int r12 = r12 + 1
                java.lang.StringBuilder r5 = r5.append(r12)     // Catch:{ SecurityException -> 0x00d8 }
                java.lang.String r5 = r5.toString()     // Catch:{ SecurityException -> 0x00d8 }
                android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ SecurityException -> 0x00d8 }
                if (r8 != 0) goto L_0x00c3
                com.facebook.react.bridge.Promise r2 = r13.mPromise     // Catch:{ SecurityException -> 0x00d8 }
                java.lang.String r3 = "E_UNABLE_TO_LOAD"
                java.lang.String r4 = "Could not get photos"
                r2.reject(r3, r4)     // Catch:{ SecurityException -> 0x00d8 }
            L_0x00bf:
                return
            L_0x00c0:
                android.net.Uri r1 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI     // Catch:{ SecurityException -> 0x00d8 }
                goto L_0x0085
            L_0x00c3:
                int r2 = r13.mFirst     // Catch:{ all -> 0x00e3 }
                java.lang.String r3 = r13.mAssetType     // Catch:{ all -> 0x00e3 }
                com.facebook.react.modules.camera.CameraRollManager.putEdges(r0, r8, r9, r2, r3)     // Catch:{ all -> 0x00e3 }
                int r2 = r13.mFirst     // Catch:{ all -> 0x00e3 }
                com.facebook.react.modules.camera.CameraRollManager.putPageInfo(r8, r9, r2)     // Catch:{ all -> 0x00e3 }
                r8.close()     // Catch:{ SecurityException -> 0x00d8 }
                com.facebook.react.bridge.Promise r2 = r13.mPromise     // Catch:{ SecurityException -> 0x00d8 }
                r2.resolve(r9)     // Catch:{ SecurityException -> 0x00d8 }
                goto L_0x00bf
            L_0x00d8:
                r6 = move-exception
                com.facebook.react.bridge.Promise r2 = r13.mPromise
                java.lang.String r3 = "E_UNABLE_TO_LOAD_PERMISSION"
                java.lang.String r4 = "Could not get photos: need READ_EXTERNAL_STORAGE permission"
                r2.reject(r3, r4, r6)
                goto L_0x00bf
            L_0x00e3:
                r2 = move-exception
                r8.close()     // Catch:{ SecurityException -> 0x00d8 }
                com.facebook.react.bridge.Promise r3 = r13.mPromise     // Catch:{ SecurityException -> 0x00d8 }
                r3.resolve(r9)     // Catch:{ SecurityException -> 0x00d8 }
                throw r2     // Catch:{ SecurityException -> 0x00d8 }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.modules.camera.CameraRollManager.GetPhotosTask.doInBackgroundGuarded(java.lang.Void[]):void");
        }
    }

    private static class SaveToCameraRoll extends GuardedAsyncTask<Void, Void> {
        private final Context mContext;
        /* access modifiers changed from: private */
        public final Promise mPromise;
        private final Uri mUri;

        public SaveToCameraRoll(ReactContext context, Uri uri, Promise promise) {
            super(context);
            this.mContext = context;
            this.mUri = uri;
            this.mPromise = promise;
        }

        /* access modifiers changed from: protected */
        public void doInBackgroundGuarded(Void... params) {
            String sourceName;
            String sourceExt;
            int n;
            File source = new File(this.mUri.getPath());
            FileChannel input = null;
            FileChannel output = null;
            try {
                File exportDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                exportDir.mkdirs();
                if (!exportDir.isDirectory()) {
                    this.mPromise.reject(CameraRollManager.ERROR_UNABLE_TO_LOAD, "External media storage directory not available");
                    if (input != null && input.isOpen()) {
                        try {
                            input.close();
                        } catch (IOException e) {
                            FLog.e(ReactConstants.TAG, "Could not close input channel", (Throwable) e);
                        }
                    }
                    if (output != null && output.isOpen()) {
                        try {
                            output.close();
                        } catch (IOException e2) {
                            FLog.e(ReactConstants.TAG, "Could not close output channel", (Throwable) e2);
                        }
                    }
                } else {
                    File dest = new File(exportDir, source.getName());
                    String fullSourceName = source.getName();
                    if (fullSourceName.indexOf(46) >= 0) {
                        sourceName = fullSourceName.substring(0, fullSourceName.lastIndexOf(46));
                        sourceExt = fullSourceName.substring(fullSourceName.lastIndexOf(46));
                        n = 0;
                    } else {
                        sourceName = fullSourceName;
                        sourceExt = "";
                        n = 0;
                    }
                    while (!dest.createNewFile()) {
                        int n2 = n + 1;
                        dest = new File(exportDir, sourceName + "_" + n + sourceExt);
                        n = n2;
                    }
                    FileChannel input2 = new FileInputStream(source).getChannel();
                    FileChannel output2 = new FileOutputStream(dest).getChannel();
                    output2.transferFrom(input2, 0, input2.size());
                    input2.close();
                    output2.close();
                    MediaScannerConnection.scanFile(this.mContext, new String[]{dest.getAbsolutePath()}, null, new OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            if (uri != null) {
                                SaveToCameraRoll.this.mPromise.resolve(uri.toString());
                            } else {
                                SaveToCameraRoll.this.mPromise.reject(CameraRollManager.ERROR_UNABLE_TO_SAVE, "Could not add image to gallery");
                            }
                        }
                    });
                    if (input2 != null && input2.isOpen()) {
                        try {
                            input2.close();
                        } catch (IOException e3) {
                            FLog.e(ReactConstants.TAG, "Could not close input channel", (Throwable) e3);
                        }
                    }
                    if (output2 != null && output2.isOpen()) {
                        try {
                            output2.close();
                        } catch (IOException e4) {
                            FLog.e(ReactConstants.TAG, "Could not close output channel", (Throwable) e4);
                        }
                    }
                }
            } catch (IOException e5) {
                this.mPromise.reject((Throwable) e5);
                if (input != null && input.isOpen()) {
                    try {
                        input.close();
                    } catch (IOException e6) {
                        FLog.e(ReactConstants.TAG, "Could not close input channel", (Throwable) e6);
                    }
                }
                if (output != null && output.isOpen()) {
                    try {
                        output.close();
                    } catch (IOException e7) {
                        FLog.e(ReactConstants.TAG, "Could not close output channel", (Throwable) e7);
                    }
                }
            } catch (Throwable th) {
                if (input != null && input.isOpen()) {
                    try {
                        input.close();
                    } catch (IOException e8) {
                        FLog.e(ReactConstants.TAG, "Could not close input channel", (Throwable) e8);
                    }
                }
                if (output != null && output.isOpen()) {
                    try {
                        output.close();
                    } catch (IOException e9) {
                        FLog.e(ReactConstants.TAG, "Could not close output channel", (Throwable) e9);
                    }
                }
                throw th;
            }
        }
    }

    static {
        if (IS_JELLY_BEAN_OR_LATER) {
            PROJECTION = new String[]{"_id", "mime_type", "bucket_display_name", "datetaken", "width", "height", "longitude", "latitude"};
        } else {
            PROJECTION = new String[]{"_id", "mime_type", "bucket_display_name", "datetaken", "longitude", "latitude"};
        }
    }

    public CameraRollManager(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    public String getName() {
        return NAME;
    }

    @ReactMethod
    public void saveToCameraRoll(String uri, String type, Promise promise) {
        new SaveToCameraRoll(getReactApplicationContext(), Uri.parse(uri), promise).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    @ReactMethod
    public void getPhotos(ReadableMap params, Promise promise) {
        String after;
        String groupName;
        String assetType;
        ReadableArray mimeTypes;
        int first = params.getInt("first");
        if (params.hasKey("after")) {
            after = params.getString("after");
        } else {
            after = null;
        }
        if (params.hasKey("groupName")) {
            groupName = params.getString("groupName");
        } else {
            groupName = null;
        }
        if (params.hasKey("assetType")) {
            assetType = params.getString("assetType");
        } else {
            assetType = null;
        }
        if (params.hasKey("mimeTypes")) {
            mimeTypes = params.getArray("mimeTypes");
        } else {
            mimeTypes = null;
        }
        if (params.hasKey("groupTypes")) {
            throw new JSApplicationIllegalArgumentException("groupTypes is not supported on Android");
        }
        new GetPhotosTask(getReactApplicationContext(), first, after, groupName, mimeTypes, assetType, promise).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    /* access modifiers changed from: private */
    public static void putPageInfo(Cursor photos, WritableMap response, int limit) {
        WritableMap pageInfo = new WritableNativeMap();
        pageInfo.putBoolean("has_next_page", limit < photos.getCount());
        if (limit < photos.getCount()) {
            photos.moveToPosition(limit - 1);
            pageInfo.putString("end_cursor", photos.getString(photos.getColumnIndex("datetaken")));
        }
        response.putMap("page_info", pageInfo);
    }

    /* access modifiers changed from: private */
    public static void putEdges(ContentResolver resolver, Cursor photos, WritableMap response, int limit, @Nullable String assetType) {
        int widthIndex;
        int heightIndex;
        WritableArray edges = new WritableNativeArray();
        photos.moveToFirst();
        int idIndex = photos.getColumnIndex("_id");
        int mimeTypeIndex = photos.getColumnIndex("mime_type");
        int groupNameIndex = photos.getColumnIndex("bucket_display_name");
        int dateTakenIndex = photos.getColumnIndex("datetaken");
        if (IS_JELLY_BEAN_OR_LATER) {
            widthIndex = photos.getColumnIndex("width");
        } else {
            widthIndex = -1;
        }
        if (IS_JELLY_BEAN_OR_LATER) {
            heightIndex = photos.getColumnIndex("height");
        } else {
            heightIndex = -1;
        }
        int longitudeIndex = photos.getColumnIndex("longitude");
        int latitudeIndex = photos.getColumnIndex("latitude");
        int i = 0;
        while (i < limit && !photos.isAfterLast()) {
            WritableMap edge = new WritableNativeMap();
            WritableMap node = new WritableNativeMap();
            if (putImageInfo(resolver, photos, node, idIndex, widthIndex, heightIndex, assetType)) {
                putBasicNodeInfo(photos, node, mimeTypeIndex, groupNameIndex, dateTakenIndex);
                putLocationInfo(photos, node, longitudeIndex, latitudeIndex);
                edge.putMap("node", node);
                edges.pushMap(edge);
            } else {
                i--;
            }
            photos.moveToNext();
            i++;
        }
        response.putArray("edges", edges);
    }

    private static void putBasicNodeInfo(Cursor photos, WritableMap node, int mimeTypeIndex, int groupNameIndex, int dateTakenIndex) {
        node.putString("type", photos.getString(mimeTypeIndex));
        node.putString("group_name", photos.getString(groupNameIndex));
        node.putDouble("timestamp", ((double) photos.getLong(dateTakenIndex)) / 1000.0d);
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean putImageInfo(android.content.ContentResolver r16, android.database.Cursor r17, com.facebook.react.bridge.WritableMap r18, int r19, int r20, int r21, @javax.annotation.Nullable java.lang.String r22) {
        /*
            com.facebook.react.bridge.WritableNativeMap r4 = new com.facebook.react.bridge.WritableNativeMap
            r4.<init>()
            if (r22 == 0) goto L_0x00df
            java.lang.String r12 = "Videos"
            r0 = r22
            boolean r12 = r0.equals(r12)
            if (r12 == 0) goto L_0x00df
            android.net.Uri r12 = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            r0 = r17
            r1 = r19
            java.lang.String r13 = r0.getString(r1)
            android.net.Uri r7 = android.net.Uri.withAppendedPath(r12, r13)
        L_0x001f:
            java.lang.String r12 = "uri"
            java.lang.String r13 = r7.toString()
            r4.putString(r12, r13)
            r11 = -1082130432(0xffffffffbf800000, float:-1.0)
            r3 = -1082130432(0xffffffffbf800000, float:-1.0)
            boolean r12 = IS_JELLY_BEAN_OR_LATER
            if (r12 == 0) goto L_0x0042
            r0 = r17
            r1 = r20
            int r12 = r0.getInt(r1)
            float r11 = (float) r12
            r0 = r17
            r1 = r21
            int r12 = r0.getInt(r1)
            float r3 = (float) r12
        L_0x0042:
            if (r22 == 0) goto L_0x009f
            java.lang.String r12 = "Videos"
            r0 = r22
            boolean r12 = r0.equals(r12)
            if (r12 == 0) goto L_0x009f
            int r12 = android.os.Build.VERSION.SDK_INT
            r13 = 10
            if (r12 < r13) goto L_0x009f
            java.lang.String r12 = "r"
            r0 = r16
            android.content.res.AssetFileDescriptor r6 = r0.openAssetFileDescriptor(r7, r12)     // Catch:{ IOException -> 0x0114 }
            android.media.MediaMetadataRetriever r9 = new android.media.MediaMetadataRetriever     // Catch:{ IOException -> 0x0114 }
            r9.<init>()     // Catch:{ IOException -> 0x0114 }
            java.io.FileDescriptor r12 = r6.getFileDescriptor()     // Catch:{ IOException -> 0x0114 }
            r9.setDataSource(r12)     // Catch:{ IOException -> 0x0114 }
            r12 = 0
            int r12 = (r11 > r12 ? 1 : (r11 == r12 ? 0 : -1))
            if (r12 <= 0) goto L_0x0072
            r12 = 0
            int r12 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r12 > 0) goto L_0x0088
        L_0x0072:
            r12 = 18
            java.lang.String r12 = r9.extractMetadata(r12)     // Catch:{ NumberFormatException -> 0x00ef }
            int r12 = java.lang.Integer.parseInt(r12)     // Catch:{ NumberFormatException -> 0x00ef }
            float r11 = (float) r12     // Catch:{ NumberFormatException -> 0x00ef }
            r12 = 19
            java.lang.String r12 = r9.extractMetadata(r12)     // Catch:{ NumberFormatException -> 0x00ef }
            int r12 = java.lang.Integer.parseInt(r12)     // Catch:{ NumberFormatException -> 0x00ef }
            float r3 = (float) r12     // Catch:{ NumberFormatException -> 0x00ef }
        L_0x0088:
            r12 = 9
            java.lang.String r12 = r9.extractMetadata(r12)     // Catch:{ NumberFormatException -> 0x00ef }
            int r10 = java.lang.Integer.parseInt(r12)     // Catch:{ NumberFormatException -> 0x00ef }
            int r8 = r10 / 1000
            java.lang.String r12 = "playableDuration"
            r4.putInt(r12, r8)     // Catch:{ NumberFormatException -> 0x00ef }
            r9.release()     // Catch:{ IOException -> 0x0114 }
            r6.close()     // Catch:{ IOException -> 0x0114 }
        L_0x009f:
            r12 = 0
            int r12 = (r11 > r12 ? 1 : (r11 == r12 ? 0 : -1))
            if (r12 <= 0) goto L_0x00a9
            r12 = 0
            int r12 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r12 > 0) goto L_0x00ca
        L_0x00a9:
            java.lang.String r12 = "r"
            r0 = r16
            android.content.res.AssetFileDescriptor r6 = r0.openAssetFileDescriptor(r7, r12)     // Catch:{ IOException -> 0x013b }
            android.graphics.BitmapFactory$Options r5 = new android.graphics.BitmapFactory$Options     // Catch:{ IOException -> 0x013b }
            r5.<init>()     // Catch:{ IOException -> 0x013b }
            r12 = 1
            r5.inJustDecodeBounds = r12     // Catch:{ IOException -> 0x013b }
            java.io.FileDescriptor r12 = r6.getFileDescriptor()     // Catch:{ IOException -> 0x013b }
            r13 = 0
            android.graphics.BitmapFactory.decodeFileDescriptor(r12, r13, r5)     // Catch:{ IOException -> 0x013b }
            int r12 = r5.outWidth     // Catch:{ IOException -> 0x013b }
            float r11 = (float) r12     // Catch:{ IOException -> 0x013b }
            int r12 = r5.outHeight     // Catch:{ IOException -> 0x013b }
            float r3 = (float) r12     // Catch:{ IOException -> 0x013b }
            r6.close()     // Catch:{ IOException -> 0x013b }
        L_0x00ca:
            java.lang.String r12 = "width"
            double r14 = (double) r11
            r4.putDouble(r12, r14)
            java.lang.String r12 = "height"
            double r14 = (double) r3
            r4.putDouble(r12, r14)
            java.lang.String r12 = "image"
            r0 = r18
            r0.putMap(r12, r4)
            r12 = 1
        L_0x00de:
            return r12
        L_0x00df:
            android.net.Uri r12 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            r0 = r17
            r1 = r19
            java.lang.String r13 = r0.getString(r1)
            android.net.Uri r7 = android.net.Uri.withAppendedPath(r12, r13)
            goto L_0x001f
        L_0x00ef:
            r2 = move-exception
            java.lang.String r12 = "ReactNative"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x0133 }
            r13.<init>()     // Catch:{ all -> 0x0133 }
            java.lang.String r14 = "Number format exception occurred while trying to fetch video metadata for "
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x0133 }
            java.lang.String r14 = r7.toString()     // Catch:{ all -> 0x0133 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x0133 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0133 }
            com.facebook.common.logging.FLog.e(r12, r13, r2)     // Catch:{ all -> 0x0133 }
            r12 = 0
            r9.release()     // Catch:{ IOException -> 0x0114 }
            r6.close()     // Catch:{ IOException -> 0x0114 }
            goto L_0x00de
        L_0x0114:
            r2 = move-exception
            java.lang.String r12 = "ReactNative"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "Could not get video metadata for "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String r14 = r7.toString()
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String r13 = r13.toString()
            com.facebook.common.logging.FLog.e(r12, r13, r2)
            r12 = 0
            goto L_0x00de
        L_0x0133:
            r12 = move-exception
            r9.release()     // Catch:{ IOException -> 0x0114 }
            r6.close()     // Catch:{ IOException -> 0x0114 }
            throw r12     // Catch:{ IOException -> 0x0114 }
        L_0x013b:
            r2 = move-exception
            java.lang.String r12 = "ReactNative"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "Could not get width/height for "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String r14 = r7.toString()
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String r13 = r13.toString()
            com.facebook.common.logging.FLog.e(r12, r13, r2)
            r12 = 0
            goto L_0x00de
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.modules.camera.CameraRollManager.putImageInfo(android.content.ContentResolver, android.database.Cursor, com.facebook.react.bridge.WritableMap, int, int, int, java.lang.String):boolean");
    }

    private static void putLocationInfo(Cursor photos, WritableMap node, int longitudeIndex, int latitudeIndex) {
        double longitude = photos.getDouble(longitudeIndex);
        double latitude = photos.getDouble(latitudeIndex);
        if (longitude > 0.0d || latitude > 0.0d) {
            WritableMap location = new WritableNativeMap();
            location.putDouble("longitude", longitude);
            location.putDouble("latitude", latitude);
            node.putMap("location", location);
        }
    }
}
