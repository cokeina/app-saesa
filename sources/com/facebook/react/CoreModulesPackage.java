package com.facebook.react;

import com.facebook.react.bridge.ModuleSpec;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMarker;
import com.facebook.react.bridge.ReactMarkerConstants;
import com.facebook.react.module.model.ReactModuleInfoProvider;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.modules.core.ExceptionsManagerModule;
import com.facebook.react.modules.core.HeadlessJsTaskSupportModule;
import com.facebook.react.modules.core.Timing;
import com.facebook.react.modules.debug.SourceCodeModule;
import com.facebook.react.modules.deviceinfo.DeviceInfoModule;
import com.facebook.react.modules.systeminfo.AndroidInfoModule;
import com.facebook.react.uimanager.UIImplementationProvider;
import com.facebook.react.uimanager.UIManagerModule;
import com.facebook.react.uimanager.UIManagerModule.ViewManagerResolver;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.systrace.Systrace;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Nullable;
import javax.inject.Provider;

class CoreModulesPackage extends LazyReactPackage implements ReactPackageLogger {
    /* access modifiers changed from: private */
    public final DefaultHardwareBackBtnHandler mHardwareBackBtnHandler;
    private final boolean mLazyViewManagersEnabled;
    private final int mMinTimeLeftInFrameForNonBatchedOperationMs;
    /* access modifiers changed from: private */
    public final ReactInstanceManager mReactInstanceManager;
    private final UIImplementationProvider mUIImplementationProvider;

    CoreModulesPackage(ReactInstanceManager reactInstanceManager, DefaultHardwareBackBtnHandler hardwareBackBtnHandler, UIImplementationProvider uiImplementationProvider, boolean lazyViewManagersEnabled, int minTimeLeftInFrameForNonBatchedOperationMs) {
        this.mReactInstanceManager = reactInstanceManager;
        this.mHardwareBackBtnHandler = hardwareBackBtnHandler;
        this.mUIImplementationProvider = uiImplementationProvider;
        this.mLazyViewManagersEnabled = lazyViewManagersEnabled;
        this.mMinTimeLeftInFrameForNonBatchedOperationMs = minTimeLeftInFrameForNonBatchedOperationMs;
    }

    public List<ModuleSpec> getNativeModules(final ReactApplicationContext reactContext) {
        return Arrays.asList(new ModuleSpec[]{ModuleSpec.nativeModuleSpec(AndroidInfoModule.class, new Provider<NativeModule>() {
            public NativeModule get() {
                return new AndroidInfoModule();
            }
        }), ModuleSpec.nativeModuleSpec(DeviceEventManagerModule.class, new Provider<NativeModule>() {
            public NativeModule get() {
                return new DeviceEventManagerModule(reactContext, CoreModulesPackage.this.mHardwareBackBtnHandler);
            }
        }), ModuleSpec.nativeModuleSpec(ExceptionsManagerModule.class, new Provider<NativeModule>() {
            public NativeModule get() {
                return new ExceptionsManagerModule(CoreModulesPackage.this.mReactInstanceManager.getDevSupportManager());
            }
        }), ModuleSpec.nativeModuleSpec(HeadlessJsTaskSupportModule.class, new Provider<NativeModule>() {
            public NativeModule get() {
                return new HeadlessJsTaskSupportModule(reactContext);
            }
        }), ModuleSpec.nativeModuleSpec(SourceCodeModule.class, new Provider<NativeModule>() {
            public NativeModule get() {
                return new SourceCodeModule(reactContext);
            }
        }), ModuleSpec.nativeModuleSpec(Timing.class, new Provider<NativeModule>() {
            public NativeModule get() {
                return new Timing(reactContext, CoreModulesPackage.this.mReactInstanceManager.getDevSupportManager());
            }
        }), ModuleSpec.nativeModuleSpec(UIManagerModule.class, new Provider<NativeModule>() {
            public NativeModule get() {
                return CoreModulesPackage.this.createUIManager(reactContext);
            }
        }), ModuleSpec.nativeModuleSpec(DeviceInfoModule.class, new Provider<NativeModule>() {
            public NativeModule get() {
                return new DeviceInfoModule(reactContext);
            }
        })});
    }

    public ReactModuleInfoProvider getReactModuleInfoProvider() {
        return LazyReactPackage.getReactModuleInfoProviderViaReflection(this);
    }

    /* access modifiers changed from: private */
    public UIManagerModule createUIManager(ReactApplicationContext reactContext) {
        UIManagerModule uIManagerModule;
        ReactMarker.logMarker(ReactMarkerConstants.CREATE_UI_MANAGER_MODULE_START);
        Systrace.beginSection(0, "createUIManagerModule");
        try {
            if (this.mLazyViewManagersEnabled) {
                uIManagerModule = new UIManagerModule(reactContext, new ViewManagerResolver() {
                    @Nullable
                    public ViewManager getViewManager(String viewManagerName) {
                        return CoreModulesPackage.this.mReactInstanceManager.createViewManager(viewManagerName);
                    }

                    public List<String> getViewManagerNames() {
                        return CoreModulesPackage.this.mReactInstanceManager.getViewManagerNames();
                    }
                }, this.mUIImplementationProvider, this.mMinTimeLeftInFrameForNonBatchedOperationMs);
            } else {
                uIManagerModule = new UIManagerModule(reactContext, this.mReactInstanceManager.createAllViewManagers(reactContext), this.mUIImplementationProvider, this.mMinTimeLeftInFrameForNonBatchedOperationMs);
                Systrace.endSection(0);
                ReactMarker.logMarker(ReactMarkerConstants.CREATE_UI_MANAGER_MODULE_END);
            }
            return uIManagerModule;
        } finally {
            Systrace.endSection(0);
            ReactMarker.logMarker(ReactMarkerConstants.CREATE_UI_MANAGER_MODULE_END);
        }
    }

    public void startProcessPackage() {
        ReactMarker.logMarker(ReactMarkerConstants.PROCESS_CORE_REACT_PACKAGE_START);
    }

    public void endProcessPackage() {
        ReactMarker.logMarker(ReactMarkerConstants.PROCESS_CORE_REACT_PACKAGE_END);
    }
}
