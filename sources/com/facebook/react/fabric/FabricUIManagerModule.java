package com.facebook.react.fabric;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.module.annotations.ReactModule;

@ReactModule(name = "FabricUIManager")
public class FabricUIManagerModule extends ReactContextBaseJavaModule {
    static final String NAME = "FabricUIManager";

    public FabricUIManagerModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public int createNode(int reactTag, String viewName, int rootTag, ReadableMap props, int instanceHandle) {
        return -1;
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public int cloneNode(int node) {
        return -1;
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public int cloneNodeWithNewChildren(int node) {
        return -1;
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public int cloneNodeWithNewProps(int node, ReadableMap newProps) {
        return -1;
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public int cloneNodeWithNewChildrenAndProps(int node, ReadableMap newProps) {
        return -1;
    }

    @ReactMethod
    public void appendChild(int parent, int child) {
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public int createChildSet() {
        return -1;
    }

    @ReactMethod
    public void appendChildToSet(int childSet, int child) {
    }

    @ReactMethod
    public void completeRoot(int rootTag, int childSet) {
    }

    public String getName() {
        return NAME;
    }
}
