package com.facebook.react.common;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.facebook.infer.annotation.Assertions;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

public class ShakeDetector implements SensorEventListener {
    private static final long MIN_TIME_BETWEEN_SAMPLES_NS = TimeUnit.NANOSECONDS.convert(20, TimeUnit.MILLISECONDS);
    private static final float REQUIRED_FORCE = 13.042845f;
    private static final float SHAKING_WINDOW_NS = ((float) TimeUnit.NANOSECONDS.convert(3, TimeUnit.SECONDS));
    private float mAccelerationX;
    private float mAccelerationY;
    private float mAccelerationZ;
    private long mLastShakeTimestamp;
    private long mLastTimestamp;
    private int mMinNumShakes;
    private int mNumShakes;
    @Nullable
    private SensorManager mSensorManager;
    private final ShakeListener mShakeListener;

    public interface ShakeListener {
        void onShake();
    }

    public ShakeDetector(ShakeListener listener) {
        this(listener, 1);
    }

    public ShakeDetector(ShakeListener listener, int minNumShakes) {
        this.mShakeListener = listener;
        this.mMinNumShakes = minNumShakes;
    }

    public void start(SensorManager manager) {
        Assertions.assertNotNull(manager);
        Sensor accelerometer = manager.getDefaultSensor(1);
        if (accelerometer != null) {
            this.mSensorManager = manager;
            this.mLastTimestamp = -1;
            this.mSensorManager.registerListener(this, accelerometer, 2);
            this.mLastShakeTimestamp = MIN_TIME_BETWEEN_SAMPLES_NS;
            reset();
        }
    }

    public void stop() {
        if (this.mSensorManager != null) {
            this.mSensorManager.unregisterListener(this);
            this.mSensorManager = null;
        }
    }

    private void reset() {
        this.mNumShakes = 0;
        this.mAccelerationX = 0.0f;
        this.mAccelerationY = 0.0f;
        this.mAccelerationZ = 0.0f;
    }

    private boolean atLeastRequiredForce(float a) {
        return Math.abs(a) > REQUIRED_FORCE;
    }

    private void recordShake(long timestamp) {
        this.mLastShakeTimestamp = timestamp;
        this.mNumShakes++;
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.timestamp - this.mLastTimestamp >= MIN_TIME_BETWEEN_SAMPLES_NS) {
            float ax = sensorEvent.values[0];
            float ay = sensorEvent.values[1];
            float az = sensorEvent.values[2] - 9.80665f;
            this.mLastTimestamp = sensorEvent.timestamp;
            if (atLeastRequiredForce(ax) && this.mAccelerationX * ax <= 0.0f) {
                recordShake(sensorEvent.timestamp);
                this.mAccelerationX = ax;
            } else if (atLeastRequiredForce(ay) && this.mAccelerationY * ay <= 0.0f) {
                recordShake(sensorEvent.timestamp);
                this.mAccelerationY = ay;
            } else if (atLeastRequiredForce(az) && this.mAccelerationZ * az <= 0.0f) {
                recordShake(sensorEvent.timestamp);
                this.mAccelerationZ = az;
            }
            maybeDispatchShake(sensorEvent.timestamp);
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    private void maybeDispatchShake(long currentTimestamp) {
        if (this.mNumShakes >= this.mMinNumShakes * 8) {
            reset();
            this.mShakeListener.onShake();
        }
        if (((float) (currentTimestamp - this.mLastShakeTimestamp)) > SHAKING_WINDOW_NS) {
            reset();
        }
    }
}
