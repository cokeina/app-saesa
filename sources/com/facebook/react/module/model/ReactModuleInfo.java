package com.facebook.react.module.model;

public class ReactModuleInfo {
    private final boolean mCanOverrideExistingModule;
    private final boolean mHasConstants;
    private final String mName;
    private final boolean mNeedsEagerInit;

    public ReactModuleInfo(String name, boolean canOverrideExistingModule, boolean needsEagerInit, boolean hasConstants) {
        this.mName = name;
        this.mCanOverrideExistingModule = canOverrideExistingModule;
        this.mNeedsEagerInit = needsEagerInit;
        this.mHasConstants = hasConstants;
    }

    public String name() {
        return this.mName;
    }

    public boolean canOverrideExistingModule() {
        return this.mCanOverrideExistingModule;
    }

    public boolean needsEagerInit() {
        return this.mNeedsEagerInit;
    }

    public boolean hasConstants() {
        return this.mHasConstants;
    }
}
