package com.facebook.react.animated;

import android.util.SparseArray;
import com.facebook.common.logging.FLog;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.JSApplicationIllegalArgumentException;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.facebook.react.uimanager.UIImplementation;
import com.facebook.react.uimanager.UIManagerModule;
import com.facebook.react.uimanager.UIManagerModule.CustomEventNamesResolver;
import com.facebook.react.uimanager.events.Event;
import com.facebook.react.uimanager.events.EventDispatcherListener;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Queue;
import javax.annotation.Nullable;

class NativeAnimatedNodesManager implements EventDispatcherListener {
    private final SparseArray<AnimationDriver> mActiveAnimations = new SparseArray<>();
    private int mAnimatedGraphBFSColor = 0;
    private final SparseArray<AnimatedNode> mAnimatedNodes = new SparseArray<>();
    private final CustomEventNamesResolver mCustomEventNamesResolver;
    private final Map<String, List<EventAnimationDriver>> mEventDrivers = new HashMap();
    private final List<AnimatedNode> mRunUpdateNodeList = new LinkedList();
    private final UIImplementation mUIImplementation;
    private final SparseArray<AnimatedNode> mUpdatedNodes = new SparseArray<>();

    public NativeAnimatedNodesManager(UIManagerModule uiManager) {
        this.mUIImplementation = uiManager.getUIImplementation();
        uiManager.getEventDispatcher().addListener(this);
        this.mCustomEventNamesResolver = uiManager.getDirectEventNamesResolver();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public AnimatedNode getNodeById(int id) {
        return (AnimatedNode) this.mAnimatedNodes.get(id);
    }

    public boolean hasActiveAnimations() {
        return this.mActiveAnimations.size() > 0 || this.mUpdatedNodes.size() > 0;
    }

    public void createAnimatedNode(int tag, ReadableMap config) {
        AnimatedNode node;
        if (this.mAnimatedNodes.get(tag) != null) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + tag + " already exists");
        }
        String type = config.getString("type");
        if ("style".equals(type)) {
            node = new StyleAnimatedNode(config, this);
        } else if ("value".equals(type)) {
            node = new ValueAnimatedNode(config);
        } else if ("props".equals(type)) {
            node = new PropsAnimatedNode(config, this, this.mUIImplementation);
        } else if ("interpolation".equals(type)) {
            node = new InterpolationAnimatedNode(config);
        } else if ("addition".equals(type)) {
            node = new AdditionAnimatedNode(config, this);
        } else if ("division".equals(type)) {
            node = new DivisionAnimatedNode(config, this);
        } else if ("multiplication".equals(type)) {
            node = new MultiplicationAnimatedNode(config, this);
        } else if ("modulus".equals(type)) {
            node = new ModulusAnimatedNode(config, this);
        } else if ("diffclamp".equals(type)) {
            node = new DiffClampAnimatedNode(config, this);
        } else if ("transform".equals(type)) {
            node = new TransformAnimatedNode(config, this);
        } else {
            throw new JSApplicationIllegalArgumentException("Unsupported node type: " + type);
        }
        node.mTag = tag;
        this.mAnimatedNodes.put(tag, node);
        this.mUpdatedNodes.put(tag, node);
    }

    public void dropAnimatedNode(int tag) {
        this.mAnimatedNodes.remove(tag);
        this.mUpdatedNodes.remove(tag);
    }

    public void startListeningToAnimatedNodeValue(int tag, AnimatedNodeValueListener listener) {
        AnimatedNode node = (AnimatedNode) this.mAnimatedNodes.get(tag);
        if (node == null || !(node instanceof ValueAnimatedNode)) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + tag + " does not exists or is not a 'value' node");
        }
        ((ValueAnimatedNode) node).setValueListener(listener);
    }

    public void stopListeningToAnimatedNodeValue(int tag) {
        AnimatedNode node = (AnimatedNode) this.mAnimatedNodes.get(tag);
        if (node == null || !(node instanceof ValueAnimatedNode)) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + tag + " does not exists or is not a 'value' node");
        }
        ((ValueAnimatedNode) node).setValueListener(null);
    }

    public void setAnimatedNodeValue(int tag, double value) {
        AnimatedNode node = (AnimatedNode) this.mAnimatedNodes.get(tag);
        if (node == null || !(node instanceof ValueAnimatedNode)) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + tag + " does not exists or is not a 'value' node");
        }
        stopAnimationsForNode(node);
        ((ValueAnimatedNode) node).mValue = value;
        this.mUpdatedNodes.put(tag, node);
    }

    public void setAnimatedNodeOffset(int tag, double offset) {
        AnimatedNode node = (AnimatedNode) this.mAnimatedNodes.get(tag);
        if (node == null || !(node instanceof ValueAnimatedNode)) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + tag + " does not exists or is not a 'value' node");
        }
        ((ValueAnimatedNode) node).mOffset = offset;
        this.mUpdatedNodes.put(tag, node);
    }

    public void flattenAnimatedNodeOffset(int tag) {
        AnimatedNode node = (AnimatedNode) this.mAnimatedNodes.get(tag);
        if (node == null || !(node instanceof ValueAnimatedNode)) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + tag + " does not exists or is not a 'value' node");
        }
        ((ValueAnimatedNode) node).flattenOffset();
    }

    public void extractAnimatedNodeOffset(int tag) {
        AnimatedNode node = (AnimatedNode) this.mAnimatedNodes.get(tag);
        if (node == null || !(node instanceof ValueAnimatedNode)) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + tag + " does not exists or is not a 'value' node");
        }
        ((ValueAnimatedNode) node).extractOffset();
    }

    public void startAnimatingNode(int animationId, int animatedNodeTag, ReadableMap animationConfig, Callback endCallback) {
        AnimationDriver animation;
        AnimatedNode node = (AnimatedNode) this.mAnimatedNodes.get(animatedNodeTag);
        if (node == null) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + animatedNodeTag + " does not exists");
        } else if (!(node instanceof ValueAnimatedNode)) {
            throw new JSApplicationIllegalArgumentException("Animated node should be of type " + ValueAnimatedNode.class.getName());
        } else {
            String type = animationConfig.getString("type");
            if ("frames".equals(type)) {
                animation = new FrameBasedAnimationDriver(animationConfig);
            } else if ("spring".equals(type)) {
                animation = new SpringAnimation(animationConfig);
            } else if ("decay".equals(type)) {
                animation = new DecayAnimation(animationConfig);
            } else {
                throw new JSApplicationIllegalArgumentException("Unsupported animation type: " + type);
            }
            animation.mId = animationId;
            animation.mEndCallback = endCallback;
            animation.mAnimatedValue = (ValueAnimatedNode) node;
            this.mActiveAnimations.put(animationId, animation);
        }
    }

    private void stopAnimationsForNode(AnimatedNode animatedNode) {
        int i = 0;
        while (i < this.mActiveAnimations.size()) {
            AnimationDriver animation = (AnimationDriver) this.mActiveAnimations.valueAt(i);
            if (animatedNode.equals(animation.mAnimatedValue)) {
                WritableMap endCallbackResponse = Arguments.createMap();
                endCallbackResponse.putBoolean("finished", false);
                animation.mEndCallback.invoke(endCallbackResponse);
                this.mActiveAnimations.removeAt(i);
                i--;
            }
            i++;
        }
    }

    public void stopAnimation(int animationId) {
        for (int i = 0; i < this.mActiveAnimations.size(); i++) {
            AnimationDriver animation = (AnimationDriver) this.mActiveAnimations.valueAt(i);
            if (animation.mId == animationId) {
                WritableMap endCallbackResponse = Arguments.createMap();
                endCallbackResponse.putBoolean("finished", false);
                animation.mEndCallback.invoke(endCallbackResponse);
                this.mActiveAnimations.removeAt(i);
                return;
            }
        }
    }

    public void connectAnimatedNodes(int parentNodeTag, int childNodeTag) {
        AnimatedNode parentNode = (AnimatedNode) this.mAnimatedNodes.get(parentNodeTag);
        if (parentNode == null) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + parentNodeTag + " does not exists");
        }
        AnimatedNode childNode = (AnimatedNode) this.mAnimatedNodes.get(childNodeTag);
        if (childNode == null) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + childNodeTag + " does not exists");
        }
        parentNode.addChild(childNode);
        this.mUpdatedNodes.put(childNodeTag, childNode);
    }

    public void disconnectAnimatedNodes(int parentNodeTag, int childNodeTag) {
        AnimatedNode parentNode = (AnimatedNode) this.mAnimatedNodes.get(parentNodeTag);
        if (parentNode == null) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + parentNodeTag + " does not exists");
        }
        AnimatedNode childNode = (AnimatedNode) this.mAnimatedNodes.get(childNodeTag);
        if (childNode == null) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + childNodeTag + " does not exists");
        }
        parentNode.removeChild(childNode);
        this.mUpdatedNodes.put(childNodeTag, childNode);
    }

    public void connectAnimatedNodeToView(int animatedNodeTag, int viewTag) {
        AnimatedNode node = (AnimatedNode) this.mAnimatedNodes.get(animatedNodeTag);
        if (node == null) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + animatedNodeTag + " does not exists");
        } else if (!(node instanceof PropsAnimatedNode)) {
            throw new JSApplicationIllegalArgumentException("Animated node connected to view should beof type " + PropsAnimatedNode.class.getName());
        } else {
            ((PropsAnimatedNode) node).connectToView(viewTag);
            this.mUpdatedNodes.put(animatedNodeTag, node);
        }
    }

    public void disconnectAnimatedNodeFromView(int animatedNodeTag, int viewTag) {
        AnimatedNode node = (AnimatedNode) this.mAnimatedNodes.get(animatedNodeTag);
        if (node == null) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + animatedNodeTag + " does not exists");
        } else if (!(node instanceof PropsAnimatedNode)) {
            throw new JSApplicationIllegalArgumentException("Animated node connected to view should beof type " + PropsAnimatedNode.class.getName());
        } else {
            ((PropsAnimatedNode) node).disconnectFromView(viewTag);
        }
    }

    public void restoreDefaultValues(int animatedNodeTag, int viewTag) {
        AnimatedNode node = (AnimatedNode) this.mAnimatedNodes.get(animatedNodeTag);
        if (node != null) {
            if (!(node instanceof PropsAnimatedNode)) {
                throw new JSApplicationIllegalArgumentException("Animated node connected to view should beof type " + PropsAnimatedNode.class.getName());
            }
            ((PropsAnimatedNode) node).restoreDefaultValues();
        }
    }

    public void addAnimatedEventToView(int viewTag, String eventName, ReadableMap eventMapping) {
        int nodeTag = eventMapping.getInt("animatedValueTag");
        AnimatedNode node = (AnimatedNode) this.mAnimatedNodes.get(nodeTag);
        if (node == null) {
            throw new JSApplicationIllegalArgumentException("Animated node with tag " + nodeTag + " does not exists");
        } else if (!(node instanceof ValueAnimatedNode)) {
            throw new JSApplicationIllegalArgumentException("Animated node connected to event should beof type " + ValueAnimatedNode.class.getName());
        } else {
            ReadableArray path = eventMapping.getArray("nativeEventPath");
            List<String> pathList = new ArrayList<>(path.size());
            for (int i = 0; i < path.size(); i++) {
                pathList.add(path.getString(i));
            }
            EventAnimationDriver event = new EventAnimationDriver(pathList, (ValueAnimatedNode) node);
            String key = viewTag + eventName;
            if (this.mEventDrivers.containsKey(key)) {
                ((List) this.mEventDrivers.get(key)).add(event);
                return;
            }
            List<EventAnimationDriver> drivers = new ArrayList<>(1);
            drivers.add(event);
            this.mEventDrivers.put(key, drivers);
        }
    }

    public void removeAnimatedEventFromView(int viewTag, String eventName, int animatedValueTag) {
        String key = viewTag + eventName;
        if (this.mEventDrivers.containsKey(key)) {
            List<EventAnimationDriver> driversForKey = (List) this.mEventDrivers.get(key);
            if (driversForKey.size() == 1) {
                this.mEventDrivers.remove(viewTag + eventName);
                return;
            }
            ListIterator<EventAnimationDriver> it = driversForKey.listIterator();
            while (it.hasNext()) {
                if (((EventAnimationDriver) it.next()).mValueNode.mTag == animatedValueTag) {
                    it.remove();
                    return;
                }
            }
        }
    }

    public void onEventDispatch(final Event event) {
        if (UiThreadUtil.isOnUiThread()) {
            handleEvent(event);
        } else {
            UiThreadUtil.runOnUiThread(new Runnable() {
                public void run() {
                    NativeAnimatedNodesManager.this.handleEvent(event);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void handleEvent(Event event) {
        if (!this.mEventDrivers.isEmpty()) {
            List<EventAnimationDriver> driversForKey = (List) this.mEventDrivers.get(event.getViewTag() + this.mCustomEventNamesResolver.resolveCustomEventName(event.getEventName()));
            if (driversForKey != null) {
                for (EventAnimationDriver driver : driversForKey) {
                    stopAnimationsForNode(driver.mValueNode);
                    event.dispatch(driver);
                    this.mRunUpdateNodeList.add(driver.mValueNode);
                }
                updateNodes(this.mRunUpdateNodeList);
                this.mRunUpdateNodeList.clear();
            }
        }
    }

    public void runUpdates(long frameTimeNanos) {
        UiThreadUtil.assertOnUiThread();
        boolean hasFinishedAnimations = false;
        for (int i = 0; i < this.mUpdatedNodes.size(); i++) {
            this.mRunUpdateNodeList.add((AnimatedNode) this.mUpdatedNodes.valueAt(i));
        }
        this.mUpdatedNodes.clear();
        for (int i2 = 0; i2 < this.mActiveAnimations.size(); i2++) {
            AnimationDriver animation = (AnimationDriver) this.mActiveAnimations.valueAt(i2);
            animation.runAnimationStep(frameTimeNanos);
            this.mRunUpdateNodeList.add(animation.mAnimatedValue);
            if (animation.mHasFinished) {
                hasFinishedAnimations = true;
            }
        }
        updateNodes(this.mRunUpdateNodeList);
        this.mRunUpdateNodeList.clear();
        if (hasFinishedAnimations) {
            for (int i3 = this.mActiveAnimations.size() - 1; i3 >= 0; i3--) {
                AnimationDriver animation2 = (AnimationDriver) this.mActiveAnimations.valueAt(i3);
                if (animation2.mHasFinished) {
                    WritableMap endCallbackResponse = Arguments.createMap();
                    endCallbackResponse.putBoolean("finished", true);
                    animation2.mEndCallback.invoke(endCallbackResponse);
                    this.mActiveAnimations.removeAt(i3);
                }
            }
        }
    }

    private void updateNodes(List<AnimatedNode> nodes) {
        int activeNodesCount = 0;
        int updatedNodesCount = 0;
        this.mAnimatedGraphBFSColor++;
        if (this.mAnimatedGraphBFSColor == 0) {
            this.mAnimatedGraphBFSColor++;
        }
        Queue<AnimatedNode> nodesQueue = new ArrayDeque<>();
        for (AnimatedNode node : nodes) {
            if (node.mBFSColor != this.mAnimatedGraphBFSColor) {
                node.mBFSColor = this.mAnimatedGraphBFSColor;
                activeNodesCount++;
                nodesQueue.add(node);
            }
        }
        while (!nodesQueue.isEmpty()) {
            AnimatedNode nextNode = (AnimatedNode) nodesQueue.poll();
            if (nextNode.mChildren != null) {
                for (int i = 0; i < nextNode.mChildren.size(); i++) {
                    AnimatedNode child = (AnimatedNode) nextNode.mChildren.get(i);
                    child.mActiveIncomingNodes++;
                    if (child.mBFSColor != this.mAnimatedGraphBFSColor) {
                        child.mBFSColor = this.mAnimatedGraphBFSColor;
                        activeNodesCount++;
                        nodesQueue.add(child);
                    }
                }
            }
        }
        this.mAnimatedGraphBFSColor++;
        if (this.mAnimatedGraphBFSColor == 0) {
            this.mAnimatedGraphBFSColor++;
        }
        for (AnimatedNode node2 : nodes) {
            if (node2.mActiveIncomingNodes == 0 && node2.mBFSColor != this.mAnimatedGraphBFSColor) {
                node2.mBFSColor = this.mAnimatedGraphBFSColor;
                updatedNodesCount++;
                nodesQueue.add(node2);
            }
        }
        while (!nodesQueue.isEmpty()) {
            AnimatedNode nextNode2 = (AnimatedNode) nodesQueue.poll();
            nextNode2.update();
            if (nextNode2 instanceof PropsAnimatedNode) {
                try {
                    ((PropsAnimatedNode) nextNode2).updateView();
                } catch (IllegalViewOperationException e) {
                    FLog.e(ReactConstants.TAG, "Native animation workaround, frame lost as result of race condition", (Throwable) e);
                }
            }
            if (nextNode2 instanceof ValueAnimatedNode) {
                ((ValueAnimatedNode) nextNode2).onValueUpdate();
            }
            if (nextNode2.mChildren != null) {
                for (int i2 = 0; i2 < nextNode2.mChildren.size(); i2++) {
                    AnimatedNode child2 = (AnimatedNode) nextNode2.mChildren.get(i2);
                    child2.mActiveIncomingNodes--;
                    if (child2.mBFSColor != this.mAnimatedGraphBFSColor && child2.mActiveIncomingNodes == 0) {
                        child2.mBFSColor = this.mAnimatedGraphBFSColor;
                        updatedNodesCount++;
                        nodesQueue.add(child2);
                    }
                }
            }
        }
        if (activeNodesCount != updatedNodesCount) {
            throw new IllegalStateException("Looks like animated nodes graph has cycles, there are " + activeNodesCount + " but toposort visited only " + updatedNodesCount);
        }
    }
}
