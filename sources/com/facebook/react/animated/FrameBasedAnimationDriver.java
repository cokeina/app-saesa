package com.facebook.react.animated;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;

class FrameBasedAnimationDriver extends AnimationDriver {
    private static final double FRAME_TIME_MILLIS = 16.666666666666668d;
    private int mCurrentLoop;
    private final double[] mFrames;
    private double mFromValue;
    private int mIterations;
    private long mStartFrameTimeNanos = -1;
    private final double mToValue;

    FrameBasedAnimationDriver(ReadableMap config) {
        int i;
        boolean z = true;
        ReadableArray frames = config.getArray("frames");
        int numberOfFrames = frames.size();
        this.mFrames = new double[numberOfFrames];
        for (int i2 = 0; i2 < numberOfFrames; i2++) {
            this.mFrames[i2] = frames.getDouble(i2);
        }
        this.mToValue = config.getDouble("toValue");
        if (config.hasKey("iterations")) {
            i = config.getInt("iterations");
        } else {
            i = 1;
        }
        this.mIterations = i;
        this.mCurrentLoop = 1;
        if (this.mIterations != 0) {
            z = false;
        }
        this.mHasFinished = z;
    }

    public void runAnimationStep(long frameTimeNanos) {
        double nextValue;
        if (this.mStartFrameTimeNanos < 0) {
            this.mStartFrameTimeNanos = frameTimeNanos;
            this.mFromValue = this.mAnimatedValue.mValue;
        }
        int frameIndex = (int) (((double) ((frameTimeNanos - this.mStartFrameTimeNanos) / 1000000)) / FRAME_TIME_MILLIS);
        if (frameIndex < 0) {
            throw new IllegalStateException("Calculated frame index should never be lower than 0");
        } else if (!this.mHasFinished) {
            if (frameIndex >= this.mFrames.length - 1) {
                nextValue = this.mToValue;
                if (this.mIterations == -1 || this.mCurrentLoop < this.mIterations) {
                    this.mStartFrameTimeNanos = frameTimeNanos;
                    this.mCurrentLoop++;
                } else {
                    this.mHasFinished = true;
                }
            } else {
                nextValue = this.mFromValue + (this.mFrames[frameIndex] * (this.mToValue - this.mFromValue));
            }
            this.mAnimatedValue.mValue = nextValue;
        }
    }
}
