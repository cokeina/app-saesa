package com.facebook.react.animated;

import com.facebook.react.bridge.ReadableMap;

class SpringAnimation extends AnimationDriver {
    private static final double MAX_DELTA_TIME_SEC = 0.064d;
    private static final double SOLVER_TIMESTEP_SEC = 0.001d;
    private int mCurrentLoop = 0;
    private final PhysicsState mCurrentState = new PhysicsState();
    private double mDisplacementFromRestThreshold;
    private double mEndValue;
    private double mInitialVelocity;
    private int mIterations;
    private long mLastTime;
    private double mOriginalValue;
    private boolean mOvershootClampingEnabled;
    private double mRestSpeedThreshold;
    private double mSpringDamping;
    private double mSpringMass;
    private boolean mSpringStarted;
    private double mSpringStiffness;
    private double mStartValue;
    private double mTimeAccumulator = 0.0d;

    private static class PhysicsState {
        double position;
        double velocity;

        private PhysicsState() {
        }
    }

    SpringAnimation(ReadableMap config) {
        int i;
        boolean z = true;
        this.mSpringStiffness = config.getDouble("stiffness");
        this.mSpringDamping = config.getDouble("damping");
        this.mSpringMass = config.getDouble("mass");
        this.mInitialVelocity = config.getDouble("initialVelocity");
        this.mCurrentState.velocity = this.mInitialVelocity;
        this.mEndValue = config.getDouble("toValue");
        this.mRestSpeedThreshold = config.getDouble("restSpeedThreshold");
        this.mDisplacementFromRestThreshold = config.getDouble("restDisplacementThreshold");
        this.mOvershootClampingEnabled = config.getBoolean("overshootClamping");
        if (config.hasKey("iterations")) {
            i = config.getInt("iterations");
        } else {
            i = 1;
        }
        this.mIterations = i;
        if (this.mIterations != 0) {
            z = false;
        }
        this.mHasFinished = z;
    }

    public void runAnimationStep(long frameTimeNanos) {
        long frameTimeMillis = frameTimeNanos / 1000000;
        if (!this.mSpringStarted) {
            if (this.mCurrentLoop == 0) {
                this.mOriginalValue = this.mAnimatedValue.mValue;
                this.mCurrentLoop = 1;
            }
            PhysicsState physicsState = this.mCurrentState;
            double d = this.mAnimatedValue.mValue;
            physicsState.position = d;
            this.mStartValue = d;
            this.mLastTime = frameTimeMillis;
            this.mTimeAccumulator = 0.0d;
            this.mSpringStarted = true;
        }
        advance(((double) (frameTimeMillis - this.mLastTime)) / 1000.0d);
        this.mLastTime = frameTimeMillis;
        this.mAnimatedValue.mValue = this.mCurrentState.position;
        if (!isAtRest()) {
            return;
        }
        if (this.mIterations == -1 || this.mCurrentLoop < this.mIterations) {
            this.mSpringStarted = false;
            this.mAnimatedValue.mValue = this.mOriginalValue;
            this.mCurrentLoop++;
            return;
        }
        this.mHasFinished = true;
    }

    private double getDisplacementDistanceForState(PhysicsState state) {
        return Math.abs(this.mEndValue - state.position);
    }

    private boolean isAtRest() {
        return Math.abs(this.mCurrentState.velocity) <= this.mRestSpeedThreshold && (getDisplacementDistanceForState(this.mCurrentState) <= this.mDisplacementFromRestThreshold || this.mSpringStiffness == 0.0d);
    }

    private boolean isOvershooting() {
        return this.mSpringStiffness > 0.0d && ((this.mStartValue < this.mEndValue && this.mCurrentState.position > this.mEndValue) || (this.mStartValue > this.mEndValue && this.mCurrentState.position < this.mEndValue));
    }

    private void advance(double realDeltaTime) {
        double position;
        double velocity;
        if (!isAtRest()) {
            double adjustedDeltaTime = realDeltaTime;
            if (realDeltaTime > MAX_DELTA_TIME_SEC) {
                adjustedDeltaTime = MAX_DELTA_TIME_SEC;
            }
            this.mTimeAccumulator += adjustedDeltaTime;
            double c = this.mSpringDamping;
            double m = this.mSpringMass;
            double k = this.mSpringStiffness;
            double v0 = -this.mInitialVelocity;
            double zeta = c / (2.0d * Math.sqrt(k * m));
            double omega0 = Math.sqrt(k / m);
            double omega1 = omega0 * Math.sqrt(1.0d - (zeta * zeta));
            double x0 = this.mEndValue - this.mStartValue;
            double t = this.mTimeAccumulator;
            if (zeta < 1.0d) {
                double envelope = Math.exp((-zeta) * omega0 * t);
                position = this.mEndValue - (((((((zeta * omega0) * x0) + v0) / omega1) * Math.sin(omega1 * t)) + (Math.cos(omega1 * t) * x0)) * envelope);
                velocity = (((zeta * omega0) * envelope) * (((Math.sin(omega1 * t) * (((zeta * omega0) * x0) + v0)) / omega1) + (Math.cos(omega1 * t) * x0))) - (((Math.cos(omega1 * t) * (((zeta * omega0) * x0) + v0)) - ((omega1 * x0) * Math.sin(omega1 * t))) * envelope);
            } else {
                double envelope2 = Math.exp((-omega0) * t);
                position = this.mEndValue - (((((omega0 * x0) + v0) * t) + x0) * envelope2);
                velocity = envelope2 * ((((t * omega0) - 1.0d) * v0) + (t * x0 * omega0 * omega0));
            }
            this.mCurrentState.position = position;
            this.mCurrentState.velocity = velocity;
            if (isAtRest() || (this.mOvershootClampingEnabled && isOvershooting())) {
                if (this.mSpringStiffness > 0.0d) {
                    this.mStartValue = this.mEndValue;
                    this.mCurrentState.position = this.mEndValue;
                } else {
                    this.mEndValue = this.mCurrentState.position;
                    this.mStartValue = this.mEndValue;
                }
                this.mCurrentState.velocity = 0.0d;
            }
        }
    }
}
