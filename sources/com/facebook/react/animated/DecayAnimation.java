package com.facebook.react.animated;

import com.facebook.react.bridge.ReadableMap;

public class DecayAnimation extends AnimationDriver {
    private int mCurrentLoop;
    private final double mDeceleration;
    private double mFromValue = 0.0d;
    private int mIterations;
    private double mLastValue = 0.0d;
    private long mStartFrameTimeMillis = -1;
    private final double mVelocity;

    public DecayAnimation(ReadableMap config) {
        int i;
        boolean z = true;
        this.mVelocity = config.getDouble("velocity");
        this.mDeceleration = config.getDouble("deceleration");
        if (config.hasKey("iterations")) {
            i = config.getInt("iterations");
        } else {
            i = 1;
        }
        this.mIterations = i;
        this.mCurrentLoop = 1;
        if (this.mIterations != 0) {
            z = false;
        }
        this.mHasFinished = z;
    }

    public void runAnimationStep(long frameTimeNanos) {
        long frameTimeMillis = frameTimeNanos / 1000000;
        if (this.mStartFrameTimeMillis == -1) {
            this.mStartFrameTimeMillis = frameTimeMillis - 16;
            if (this.mFromValue == this.mLastValue) {
                this.mFromValue = this.mAnimatedValue.mValue;
            } else {
                this.mAnimatedValue.mValue = this.mFromValue;
            }
            this.mLastValue = this.mAnimatedValue.mValue;
        }
        double value = this.mFromValue + ((this.mVelocity / (1.0d - this.mDeceleration)) * (1.0d - Math.exp((-(1.0d - this.mDeceleration)) * ((double) (frameTimeMillis - this.mStartFrameTimeMillis)))));
        if (Math.abs(this.mLastValue - value) < 0.1d) {
            if (this.mIterations == -1 || this.mCurrentLoop < this.mIterations) {
                this.mStartFrameTimeMillis = -1;
                this.mCurrentLoop++;
            } else {
                this.mHasFinished = true;
                return;
            }
        }
        this.mLastValue = value;
        this.mAnimatedValue.mValue = value;
    }
}
