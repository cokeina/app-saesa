package com.facebook.react.animated;

import com.facebook.infer.annotation.Assertions;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.annotations.VisibleForTesting;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;
import com.facebook.react.modules.core.ReactChoreographer;
import com.facebook.react.modules.core.ReactChoreographer.CallbackType;
import com.facebook.react.uimanager.GuardedFrameCallback;
import com.facebook.react.uimanager.NativeViewHierarchyManager;
import com.facebook.react.uimanager.UIBlock;
import com.facebook.react.uimanager.UIManagerModule;
import com.facebook.react.uimanager.UIManagerModuleListener;
import java.util.ArrayList;
import java.util.Iterator;
import javax.annotation.Nullable;

@ReactModule(name = "NativeAnimatedModule")
public class NativeAnimatedModule extends ReactContextBaseJavaModule implements LifecycleEventListener, UIManagerModuleListener {
    protected static final String NAME = "NativeAnimatedModule";
    /* access modifiers changed from: private */
    public final GuardedFrameCallback mAnimatedFrameCallback;
    @Nullable
    private NativeAnimatedNodesManager mNodesManager;
    private ArrayList<UIThreadOperation> mOperations = new ArrayList<>();
    private ArrayList<UIThreadOperation> mPreOperations = new ArrayList<>();
    /* access modifiers changed from: private */
    public final ReactChoreographer mReactChoreographer = ReactChoreographer.getInstance();

    private interface UIThreadOperation {
        void execute(NativeAnimatedNodesManager nativeAnimatedNodesManager);
    }

    public NativeAnimatedModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.mAnimatedFrameCallback = new GuardedFrameCallback(reactContext) {
            /* access modifiers changed from: protected */
            public void doFrameGuarded(long frameTimeNanos) {
                NativeAnimatedNodesManager nodesManager = NativeAnimatedModule.this.getNodesManager();
                if (nodesManager.hasActiveAnimations()) {
                    nodesManager.runUpdates(frameTimeNanos);
                }
                ((ReactChoreographer) Assertions.assertNotNull(NativeAnimatedModule.this.mReactChoreographer)).postFrameCallback(CallbackType.NATIVE_ANIMATED_MODULE, NativeAnimatedModule.this.mAnimatedFrameCallback);
            }
        };
    }

    public void initialize() {
        ReactApplicationContext reactCtx = getReactApplicationContext();
        UIManagerModule uiManager = (UIManagerModule) reactCtx.getNativeModule(UIManagerModule.class);
        reactCtx.addLifecycleEventListener(this);
        uiManager.addUIManagerListener(this);
    }

    public void onHostResume() {
        enqueueFrameCallback();
    }

    public void willDispatchViewUpdates(UIManagerModule uiManager) {
        if (!this.mOperations.isEmpty() || !this.mPreOperations.isEmpty()) {
            final ArrayList<UIThreadOperation> preOperations = this.mPreOperations;
            final ArrayList<UIThreadOperation> operations = this.mOperations;
            this.mPreOperations = new ArrayList<>();
            this.mOperations = new ArrayList<>();
            uiManager.prependUIBlock(new UIBlock() {
                public void execute(NativeViewHierarchyManager nativeViewHierarchyManager) {
                    NativeAnimatedNodesManager nodesManager = NativeAnimatedModule.this.getNodesManager();
                    Iterator it = preOperations.iterator();
                    while (it.hasNext()) {
                        ((UIThreadOperation) it.next()).execute(nodesManager);
                    }
                }
            });
            uiManager.addUIBlock(new UIBlock() {
                public void execute(NativeViewHierarchyManager nativeViewHierarchyManager) {
                    NativeAnimatedNodesManager nodesManager = NativeAnimatedModule.this.getNodesManager();
                    Iterator it = operations.iterator();
                    while (it.hasNext()) {
                        ((UIThreadOperation) it.next()).execute(nodesManager);
                    }
                }
            });
        }
    }

    public void onHostPause() {
        clearFrameCallback();
    }

    public void onHostDestroy() {
    }

    public String getName() {
        return NAME;
    }

    /* access modifiers changed from: private */
    public NativeAnimatedNodesManager getNodesManager() {
        if (this.mNodesManager == null) {
            this.mNodesManager = new NativeAnimatedNodesManager((UIManagerModule) getReactApplicationContext().getNativeModule(UIManagerModule.class));
        }
        return this.mNodesManager;
    }

    private void clearFrameCallback() {
        ((ReactChoreographer) Assertions.assertNotNull(this.mReactChoreographer)).removeFrameCallback(CallbackType.NATIVE_ANIMATED_MODULE, this.mAnimatedFrameCallback);
    }

    private void enqueueFrameCallback() {
        ((ReactChoreographer) Assertions.assertNotNull(this.mReactChoreographer)).postFrameCallback(CallbackType.NATIVE_ANIMATED_MODULE, this.mAnimatedFrameCallback);
    }

    @VisibleForTesting
    public void setNodesManager(NativeAnimatedNodesManager nodesManager) {
        this.mNodesManager = nodesManager;
    }

    @ReactMethod
    public void createAnimatedNode(final int tag, final ReadableMap config) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.createAnimatedNode(tag, config);
            }
        });
    }

    @ReactMethod
    public void startListeningToAnimatedNodeValue(final int tag) {
        final AnimatedNodeValueListener listener = new AnimatedNodeValueListener() {
            public void onValueUpdate(double value) {
                WritableMap onAnimatedValueData = Arguments.createMap();
                onAnimatedValueData.putInt("tag", tag);
                onAnimatedValueData.putDouble("value", value);
                ((RCTDeviceEventEmitter) NativeAnimatedModule.this.getReactApplicationContext().getJSModule(RCTDeviceEventEmitter.class)).emit("onAnimatedValueUpdate", onAnimatedValueData);
            }
        };
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.startListeningToAnimatedNodeValue(tag, listener);
            }
        });
    }

    @ReactMethod
    public void stopListeningToAnimatedNodeValue(final int tag) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.stopListeningToAnimatedNodeValue(tag);
            }
        });
    }

    @ReactMethod
    public void dropAnimatedNode(final int tag) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.dropAnimatedNode(tag);
            }
        });
    }

    @ReactMethod
    public void setAnimatedNodeValue(final int tag, final double value) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.setAnimatedNodeValue(tag, value);
            }
        });
    }

    @ReactMethod
    public void setAnimatedNodeOffset(final int tag, final double value) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.setAnimatedNodeOffset(tag, value);
            }
        });
    }

    @ReactMethod
    public void flattenAnimatedNodeOffset(final int tag) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.flattenAnimatedNodeOffset(tag);
            }
        });
    }

    @ReactMethod
    public void extractAnimatedNodeOffset(final int tag) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.extractAnimatedNodeOffset(tag);
            }
        });
    }

    @ReactMethod
    public void startAnimatingNode(int animationId, int animatedNodeTag, ReadableMap animationConfig, Callback endCallback) {
        final int i = animationId;
        final int i2 = animatedNodeTag;
        final ReadableMap readableMap = animationConfig;
        final Callback callback = endCallback;
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.startAnimatingNode(i, i2, readableMap, callback);
            }
        });
    }

    @ReactMethod
    public void stopAnimation(final int animationId) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.stopAnimation(animationId);
            }
        });
    }

    @ReactMethod
    public void connectAnimatedNodes(final int parentNodeTag, final int childNodeTag) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.connectAnimatedNodes(parentNodeTag, childNodeTag);
            }
        });
    }

    @ReactMethod
    public void disconnectAnimatedNodes(final int parentNodeTag, final int childNodeTag) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.disconnectAnimatedNodes(parentNodeTag, childNodeTag);
            }
        });
    }

    @ReactMethod
    public void connectAnimatedNodeToView(final int animatedNodeTag, final int viewTag) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.connectAnimatedNodeToView(animatedNodeTag, viewTag);
            }
        });
    }

    @ReactMethod
    public void disconnectAnimatedNodeFromView(final int animatedNodeTag, final int viewTag) {
        this.mPreOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.restoreDefaultValues(animatedNodeTag, viewTag);
            }
        });
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.disconnectAnimatedNodeFromView(animatedNodeTag, viewTag);
            }
        });
    }

    @ReactMethod
    public void addAnimatedEventToView(final int viewTag, final String eventName, final ReadableMap eventMapping) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.addAnimatedEventToView(viewTag, eventName, eventMapping);
            }
        });
    }

    @ReactMethod
    public void removeAnimatedEventFromView(final int viewTag, final String eventName, final int animatedValueTag) {
        this.mOperations.add(new UIThreadOperation() {
            public void execute(NativeAnimatedNodesManager animatedNodesManager) {
                animatedNodesManager.removeAnimatedEventFromView(viewTag, eventName, animatedValueTag);
            }
        });
    }
}
