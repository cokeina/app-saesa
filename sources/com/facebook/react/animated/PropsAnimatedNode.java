package com.facebook.react.animated;

import com.facebook.react.bridge.JSApplicationIllegalArgumentException;
import com.facebook.react.bridge.JavaOnlyMap;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.uimanager.ReactStylesDiffMap;
import com.facebook.react.uimanager.UIImplementation;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

class PropsAnimatedNode extends AnimatedNode {
    private int mConnectedViewTag = -1;
    private final ReactStylesDiffMap mDiffMap;
    private final NativeAnimatedNodesManager mNativeAnimatedNodesManager;
    private final JavaOnlyMap mPropMap;
    private final Map<String, Integer> mPropNodeMapping;
    private final UIImplementation mUIImplementation;

    PropsAnimatedNode(ReadableMap config, NativeAnimatedNodesManager nativeAnimatedNodesManager, UIImplementation uiImplementation) {
        ReadableMap props = config.getMap("props");
        ReadableMapKeySetIterator iter = props.keySetIterator();
        this.mPropNodeMapping = new HashMap();
        while (iter.hasNextKey()) {
            String propKey = iter.nextKey();
            this.mPropNodeMapping.put(propKey, Integer.valueOf(props.getInt(propKey)));
        }
        this.mPropMap = new JavaOnlyMap();
        this.mDiffMap = new ReactStylesDiffMap(this.mPropMap);
        this.mNativeAnimatedNodesManager = nativeAnimatedNodesManager;
        this.mUIImplementation = uiImplementation;
    }

    public void connectToView(int viewTag) {
        if (this.mConnectedViewTag != -1) {
            throw new JSApplicationIllegalArgumentException("Animated node " + this.mTag + " is already attached to a view");
        }
        this.mConnectedViewTag = viewTag;
    }

    public void disconnectFromView(int viewTag) {
        if (this.mConnectedViewTag != viewTag) {
            throw new JSApplicationIllegalArgumentException("Attempting to disconnect view that has not been connected with the given animated node");
        }
        this.mConnectedViewTag = -1;
    }

    public void restoreDefaultValues() {
        ReadableMapKeySetIterator it = this.mPropMap.keySetIterator();
        while (it.hasNextKey()) {
            this.mPropMap.putNull(it.nextKey());
        }
        this.mUIImplementation.synchronouslyUpdateViewOnUIThread(this.mConnectedViewTag, this.mDiffMap);
    }

    public final void updateView() {
        if (this.mConnectedViewTag != -1) {
            for (Entry<String, Integer> entry : this.mPropNodeMapping.entrySet()) {
                AnimatedNode node = this.mNativeAnimatedNodesManager.getNodeById(((Integer) entry.getValue()).intValue());
                if (node == null) {
                    throw new IllegalArgumentException("Mapped property node does not exists");
                } else if (node instanceof StyleAnimatedNode) {
                    ((StyleAnimatedNode) node).collectViewUpdates(this.mPropMap);
                } else if (node instanceof ValueAnimatedNode) {
                    this.mPropMap.putDouble((String) entry.getKey(), ((ValueAnimatedNode) node).getValue());
                } else {
                    throw new IllegalArgumentException("Unsupported type of node used in property node " + node.getClass());
                }
            }
            this.mUIImplementation.synchronouslyUpdateViewOnUIThread(this.mConnectedViewTag, this.mDiffMap);
        }
    }
}
