package com.facebook.react;

import android.app.Activity;
import android.app.Application;
import com.facebook.infer.annotation.Assertions;
import com.facebook.react.bridge.JSBundleLoader;
import com.facebook.react.bridge.JSCJavaScriptExecutorFactory;
import com.facebook.react.bridge.JavaScriptExecutorFactory;
import com.facebook.react.bridge.NativeModuleCallExceptionHandler;
import com.facebook.react.bridge.NotThreadSafeBridgeIdleDebugListener;
import com.facebook.react.common.LifecycleState;
import com.facebook.react.devsupport.RedBoxHandler;
import com.facebook.react.devsupport.interfaces.DevBundleDownloadListener;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.modules.systeminfo.AndroidInfoHelpers;
import com.facebook.react.uimanager.UIImplementationProvider;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;

public class ReactInstanceManagerBuilder {
    @Nullable
    private Application mApplication;
    @Nullable
    private NotThreadSafeBridgeIdleDebugListener mBridgeIdleDebugListener;
    @Nullable
    private Activity mCurrentActivity;
    @Nullable
    private DefaultHardwareBackBtnHandler mDefaultHardwareBackBtnHandler;
    private boolean mDelayViewManagerClassLoadsEnabled;
    @Nullable
    private DevBundleDownloadListener mDevBundleDownloadListener;
    @Nullable
    private LifecycleState mInitialLifecycleState;
    @Nullable
    private String mJSBundleAssetUrl;
    @Nullable
    private JSBundleLoader mJSBundleLoader;
    @Nullable
    private String mJSMainModulePath;
    @Nullable
    private JavaScriptExecutorFactory mJavaScriptExecutorFactory;
    private boolean mLazyNativeModulesEnabled;
    private boolean mLazyViewManagersEnabled;
    private int mMinNumShakes = 1;
    private int mMinTimeLeftInFrameForNonBatchedOperationMs = -1;
    @Nullable
    private NativeModuleCallExceptionHandler mNativeModuleCallExceptionHandler;
    private final List<ReactPackage> mPackages = new ArrayList();
    @Nullable
    private RedBoxHandler mRedBoxHandler;
    @Nullable
    private UIImplementationProvider mUIImplementationProvider;
    private boolean mUseDeveloperSupport;

    ReactInstanceManagerBuilder() {
    }

    public ReactInstanceManagerBuilder setUIImplementationProvider(@Nullable UIImplementationProvider uiImplementationProvider) {
        this.mUIImplementationProvider = uiImplementationProvider;
        return this;
    }

    public ReactInstanceManagerBuilder setJavaScriptExecutorFactory(@Nullable JavaScriptExecutorFactory javaScriptExecutorFactory) {
        this.mJavaScriptExecutorFactory = javaScriptExecutorFactory;
        return this;
    }

    public ReactInstanceManagerBuilder setBundleAssetName(String bundleAssetName) {
        this.mJSBundleAssetUrl = bundleAssetName == null ? null : "assets://" + bundleAssetName;
        this.mJSBundleLoader = null;
        return this;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public ReactInstanceManagerBuilder setJSBundleFile(String jsBundleFile) {
        if (!jsBundleFile.startsWith("assets://")) {
            return setJSBundleLoader(JSBundleLoader.createFileLoader(jsBundleFile));
        }
        this.mJSBundleAssetUrl = jsBundleFile;
        this.mJSBundleLoader = null;
        return this;
    }

    public ReactInstanceManagerBuilder setJSBundleLoader(JSBundleLoader jsBundleLoader) {
        this.mJSBundleLoader = jsBundleLoader;
        this.mJSBundleAssetUrl = null;
        return this;
    }

    public ReactInstanceManagerBuilder setJSMainModulePath(String jsMainModulePath) {
        this.mJSMainModulePath = jsMainModulePath;
        return this;
    }

    public ReactInstanceManagerBuilder addPackage(ReactPackage reactPackage) {
        this.mPackages.add(reactPackage);
        return this;
    }

    public ReactInstanceManagerBuilder addPackages(List<ReactPackage> reactPackages) {
        this.mPackages.addAll(reactPackages);
        return this;
    }

    public ReactInstanceManagerBuilder setBridgeIdleDebugListener(NotThreadSafeBridgeIdleDebugListener bridgeIdleDebugListener) {
        this.mBridgeIdleDebugListener = bridgeIdleDebugListener;
        return this;
    }

    public ReactInstanceManagerBuilder setApplication(Application application) {
        this.mApplication = application;
        return this;
    }

    public ReactInstanceManagerBuilder setCurrentActivity(Activity activity) {
        this.mCurrentActivity = activity;
        return this;
    }

    public ReactInstanceManagerBuilder setDefaultHardwareBackBtnHandler(DefaultHardwareBackBtnHandler defaultHardwareBackBtnHandler) {
        this.mDefaultHardwareBackBtnHandler = defaultHardwareBackBtnHandler;
        return this;
    }

    public ReactInstanceManagerBuilder setUseDeveloperSupport(boolean useDeveloperSupport) {
        this.mUseDeveloperSupport = useDeveloperSupport;
        return this;
    }

    public ReactInstanceManagerBuilder setInitialLifecycleState(LifecycleState initialLifecycleState) {
        this.mInitialLifecycleState = initialLifecycleState;
        return this;
    }

    public ReactInstanceManagerBuilder setNativeModuleCallExceptionHandler(NativeModuleCallExceptionHandler handler) {
        this.mNativeModuleCallExceptionHandler = handler;
        return this;
    }

    public ReactInstanceManagerBuilder setRedBoxHandler(@Nullable RedBoxHandler redBoxHandler) {
        this.mRedBoxHandler = redBoxHandler;
        return this;
    }

    public ReactInstanceManagerBuilder setLazyNativeModulesEnabled(boolean lazyNativeModulesEnabled) {
        this.mLazyNativeModulesEnabled = lazyNativeModulesEnabled;
        return this;
    }

    public ReactInstanceManagerBuilder setLazyViewManagersEnabled(boolean lazyViewManagersEnabled) {
        this.mLazyViewManagersEnabled = lazyViewManagersEnabled;
        return this;
    }

    public ReactInstanceManagerBuilder setDelayViewManagerClassLoadsEnabled(boolean delayViewManagerClassLoadsEnabled) {
        this.mDelayViewManagerClassLoadsEnabled = delayViewManagerClassLoadsEnabled;
        return this;
    }

    public ReactInstanceManagerBuilder setDevBundleDownloadListener(@Nullable DevBundleDownloadListener listener) {
        this.mDevBundleDownloadListener = listener;
        return this;
    }

    public ReactInstanceManagerBuilder setMinNumShakes(int minNumShakes) {
        this.mMinNumShakes = minNumShakes;
        return this;
    }

    public ReactInstanceManagerBuilder setMinTimeLeftInFrameForNonBatchedOperationMs(int minTimeLeftInFrameForNonBatchedOperationMs) {
        this.mMinTimeLeftInFrameForNonBatchedOperationMs = minTimeLeftInFrameForNonBatchedOperationMs;
        return this;
    }

    public ReactInstanceManager build() {
        Assertions.assertNotNull(this.mApplication, "Application property has not been set with this builder");
        Assertions.assertCondition((!this.mUseDeveloperSupport && this.mJSBundleAssetUrl == null && this.mJSBundleLoader == null) ? false : true, "JS Bundle File or Asset URL has to be provided when dev support is disabled");
        Assertions.assertCondition((this.mJSMainModulePath == null && this.mJSBundleAssetUrl == null && this.mJSBundleLoader == null) ? false : true, "Either MainModulePath or JS Bundle File needs to be provided");
        if (this.mUIImplementationProvider == null) {
            this.mUIImplementationProvider = new UIImplementationProvider();
        }
        return new ReactInstanceManager(this.mApplication, this.mCurrentActivity, this.mDefaultHardwareBackBtnHandler, this.mJavaScriptExecutorFactory == null ? new JSCJavaScriptExecutorFactory(this.mApplication.getPackageName(), AndroidInfoHelpers.getFriendlyDeviceName()) : this.mJavaScriptExecutorFactory, (this.mJSBundleLoader != null || this.mJSBundleAssetUrl == null) ? this.mJSBundleLoader : JSBundleLoader.createAssetLoader(this.mApplication, this.mJSBundleAssetUrl, false), this.mJSMainModulePath, this.mPackages, this.mUseDeveloperSupport, this.mBridgeIdleDebugListener, (LifecycleState) Assertions.assertNotNull(this.mInitialLifecycleState, "Initial lifecycle state was not set"), this.mUIImplementationProvider, this.mNativeModuleCallExceptionHandler, this.mRedBoxHandler, this.mLazyNativeModulesEnabled, this.mLazyViewManagersEnabled, this.mDelayViewManagerClassLoadsEnabled, this.mDevBundleDownloadListener, this.mMinNumShakes, this.mMinTimeLeftInFrameForNonBatchedOperationMs);
    }
}
