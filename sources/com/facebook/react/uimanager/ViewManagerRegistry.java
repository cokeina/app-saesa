package com.facebook.react.uimanager;

import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.UIManagerModule.ViewManagerResolver;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

public final class ViewManagerRegistry {
    @Nullable
    private final ViewManagerResolver mViewManagerResolver;
    private final Map<String, ViewManager> mViewManagers;

    public ViewManagerRegistry(ViewManagerResolver viewManagerResolver) {
        this.mViewManagers = MapBuilder.newHashMap();
        this.mViewManagerResolver = viewManagerResolver;
    }

    public ViewManagerRegistry(List<ViewManager> viewManagerList) {
        Map<String, ViewManager> viewManagerMap = MapBuilder.newHashMap();
        for (ViewManager viewManager : viewManagerList) {
            viewManagerMap.put(viewManager.getName(), viewManager);
        }
        this.mViewManagers = viewManagerMap;
        this.mViewManagerResolver = null;
    }

    public ViewManagerRegistry(Map<String, ViewManager> viewManagerMap) {
        if (viewManagerMap == null) {
            viewManagerMap = MapBuilder.newHashMap();
        }
        this.mViewManagers = viewManagerMap;
        this.mViewManagerResolver = null;
    }

    public ViewManager get(String className) {
        ViewManager viewManager = (ViewManager) this.mViewManagers.get(className);
        if (viewManager != null) {
            return viewManager;
        }
        if (this.mViewManagerResolver != null) {
            ViewManager viewManager2 = this.mViewManagerResolver.getViewManager(className);
            if (viewManager2 != null) {
                this.mViewManagers.put(className, viewManager2);
                return viewManager2;
            }
        }
        throw new IllegalViewOperationException("No ViewManager defined for class " + className);
    }
}
