package com.facebook.react.uimanager;

import com.facebook.react.bridge.ReadableMap;
import java.util.Arrays;
import java.util.HashSet;

public class ViewProps {
    public static final String ALIGN_CONTENT = "alignContent";
    public static final String ALIGN_ITEMS = "alignItems";
    public static final String ALIGN_SELF = "alignSelf";
    public static final String ALLOW_FONT_SCALING = "allowFontScaling";
    public static final String ASPECT_RATIO = "aspectRatio";
    public static final String AUTO = "auto";
    public static final String BACKGROUND_COLOR = "backgroundColor";
    public static final String BORDER_BOTTOM_COLOR = "borderBottomColor";
    public static final String BORDER_BOTTOM_END_RADIUS = "borderBottomEndRadius";
    public static final String BORDER_BOTTOM_LEFT_RADIUS = "borderBottomLeftRadius";
    public static final String BORDER_BOTTOM_RIGHT_RADIUS = "borderBottomRightRadius";
    public static final String BORDER_BOTTOM_START_RADIUS = "borderBottomStartRadius";
    public static final String BORDER_BOTTOM_WIDTH = "borderBottomWidth";
    public static final String BORDER_COLOR = "borderColor";
    public static final String BORDER_END_COLOR = "borderEndColor";
    public static final String BORDER_END_WIDTH = "borderEndWidth";
    public static final String BORDER_LEFT_COLOR = "borderLeftColor";
    public static final String BORDER_LEFT_WIDTH = "borderLeftWidth";
    public static final String BORDER_RADIUS = "borderRadius";
    public static final String BORDER_RIGHT_COLOR = "borderRightColor";
    public static final String BORDER_RIGHT_WIDTH = "borderRightWidth";
    public static final int[] BORDER_SPACING_TYPES = {8, 4, 5, 1, 3, 0, 2};
    public static final String BORDER_START_COLOR = "borderStartColor";
    public static final String BORDER_START_WIDTH = "borderStartWidth";
    public static final String BORDER_TOP_COLOR = "borderTopColor";
    public static final String BORDER_TOP_END_RADIUS = "borderTopEndRadius";
    public static final String BORDER_TOP_LEFT_RADIUS = "borderTopLeftRadius";
    public static final String BORDER_TOP_RIGHT_RADIUS = "borderTopRightRadius";
    public static final String BORDER_TOP_START_RADIUS = "borderTopStartRadius";
    public static final String BORDER_TOP_WIDTH = "borderTopWidth";
    public static final String BORDER_WIDTH = "borderWidth";
    public static final String BOTTOM = "bottom";
    public static final String BOX_NONE = "box-none";
    public static final String COLLAPSABLE = "collapsable";
    public static final String COLOR = "color";
    public static final String DISPLAY = "display";
    public static final String ELLIPSIZE_MODE = "ellipsizeMode";
    public static final String ENABLED = "enabled";
    public static final String END = "end";
    public static final String FLEX = "flex";
    public static final String FLEX_BASIS = "flexBasis";
    public static final String FLEX_DIRECTION = "flexDirection";
    public static final String FLEX_GROW = "flexGrow";
    public static final String FLEX_SHRINK = "flexShrink";
    public static final String FLEX_WRAP = "flexWrap";
    public static final String FONT_FAMILY = "fontFamily";
    public static final String FONT_SIZE = "fontSize";
    public static final String FONT_STYLE = "fontStyle";
    public static final String FONT_WEIGHT = "fontWeight";
    public static final String HEIGHT = "height";
    public static final String INCLUDE_FONT_PADDING = "includeFontPadding";
    public static final String JUSTIFY_CONTENT = "justifyContent";
    private static final HashSet<String> LAYOUT_ONLY_PROPS = new HashSet<>(Arrays.asList(new String[]{ALIGN_SELF, ALIGN_ITEMS, COLLAPSABLE, FLEX, FLEX_BASIS, FLEX_DIRECTION, FLEX_GROW, FLEX_SHRINK, FLEX_WRAP, JUSTIFY_CONTENT, OVERFLOW, ALIGN_CONTENT, DISPLAY, POSITION, RIGHT, TOP, BOTTOM, LEFT, START, END, "width", "height", MIN_WIDTH, MAX_WIDTH, MIN_HEIGHT, MAX_HEIGHT, MARGIN, MARGIN_VERTICAL, MARGIN_HORIZONTAL, MARGIN_LEFT, MARGIN_RIGHT, MARGIN_TOP, MARGIN_BOTTOM, MARGIN_START, MARGIN_END, PADDING, PADDING_VERTICAL, PADDING_HORIZONTAL, PADDING_LEFT, PADDING_RIGHT, PADDING_TOP, PADDING_BOTTOM, PADDING_START, PADDING_END}));
    public static final String LEFT = "left";
    public static final String LINE_HEIGHT = "lineHeight";
    public static final String MARGIN = "margin";
    public static final String MARGIN_BOTTOM = "marginBottom";
    public static final String MARGIN_END = "marginEnd";
    public static final String MARGIN_HORIZONTAL = "marginHorizontal";
    public static final String MARGIN_LEFT = "marginLeft";
    public static final String MARGIN_RIGHT = "marginRight";
    public static final String MARGIN_START = "marginStart";
    public static final String MARGIN_TOP = "marginTop";
    public static final String MARGIN_VERTICAL = "marginVertical";
    public static final String MAX_HEIGHT = "maxHeight";
    public static final String MAX_WIDTH = "maxWidth";
    public static final String MIN_HEIGHT = "minHeight";
    public static final String MIN_WIDTH = "minWidth";
    public static final String NEEDS_OFFSCREEN_ALPHA_COMPOSITING = "needsOffscreenAlphaCompositing";
    public static final String NONE = "none";
    public static final String NUMBER_OF_LINES = "numberOfLines";
    public static final String ON = "on";
    public static final String ON_LAYOUT = "onLayout";
    public static final String OPACITY = "opacity";
    public static final String OVERFLOW = "overflow";
    public static final String PADDING = "padding";
    public static final String PADDING_BOTTOM = "paddingBottom";
    public static final String PADDING_END = "paddingEnd";
    public static final String PADDING_HORIZONTAL = "paddingHorizontal";
    public static final String PADDING_LEFT = "paddingLeft";
    public static final int[] PADDING_MARGIN_SPACING_TYPES = {8, 7, 6, 4, 5, 1, 3, 0, 2};
    public static final String PADDING_RIGHT = "paddingRight";
    public static final String PADDING_START = "paddingStart";
    public static final String PADDING_TOP = "paddingTop";
    public static final String PADDING_VERTICAL = "paddingVertical";
    public static final String POINTER_EVENTS = "pointerEvents";
    public static final String POSITION = "position";
    public static final int[] POSITION_SPACING_TYPES = {4, 5, 1, 3};
    public static final String RESIZE_METHOD = "resizeMethod";
    public static final String RESIZE_MODE = "resizeMode";
    public static final String RIGHT = "right";
    public static final String START = "start";
    public static final String TEXT_ALIGN = "textAlign";
    public static final String TEXT_ALIGN_VERTICAL = "textAlignVertical";
    public static final String TEXT_BREAK_STRATEGY = "textBreakStrategy";
    public static final String TEXT_DECORATION_LINE = "textDecorationLine";
    public static final String TOP = "top";
    public static final String VIEW_CLASS_NAME = "RCTView";
    public static final String WIDTH = "width";
    public static boolean sIsOptimizationsEnabled;

    public static boolean isLayoutOnly(ReadableMap map, String prop) {
        boolean z = true;
        if (LAYOUT_ONLY_PROPS.contains(prop)) {
            return true;
        }
        if (POINTER_EVENTS.equals(prop)) {
            String value = map.getString(prop);
            if ("auto".equals(value) || BOX_NONE.equals(value)) {
                return true;
            }
            return false;
        } else if (!sIsOptimizationsEnabled) {
            return false;
        } else {
            char c = 65535;
            switch (prop.hashCode()) {
                case -1989576717:
                    if (prop.equals(BORDER_RIGHT_COLOR)) {
                        c = 3;
                        break;
                    }
                    break;
                case -1971292586:
                    if (prop.equals(BORDER_RIGHT_WIDTH)) {
                        c = 9;
                        break;
                    }
                    break;
                case -1470826662:
                    if (prop.equals(BORDER_TOP_COLOR)) {
                        c = 4;
                        break;
                    }
                    break;
                case -1452542531:
                    if (prop.equals(BORDER_TOP_WIDTH)) {
                        c = 8;
                        break;
                    }
                    break;
                case -1308858324:
                    if (prop.equals(BORDER_BOTTOM_COLOR)) {
                        c = 5;
                        break;
                    }
                    break;
                case -1290574193:
                    if (prop.equals(BORDER_BOTTOM_WIDTH)) {
                        c = 10;
                        break;
                    }
                    break;
                case -1267206133:
                    if (prop.equals(OPACITY)) {
                        c = 0;
                        break;
                    }
                    break;
                case -242276144:
                    if (prop.equals(BORDER_LEFT_COLOR)) {
                        c = 2;
                        break;
                    }
                    break;
                case -223992013:
                    if (prop.equals(BORDER_LEFT_WIDTH)) {
                        c = 7;
                        break;
                    }
                    break;
                case 529642498:
                    if (prop.equals(OVERFLOW)) {
                        c = 12;
                        break;
                    }
                    break;
                case 741115130:
                    if (prop.equals(BORDER_WIDTH)) {
                        c = 6;
                        break;
                    }
                    break;
                case 1288688105:
                    if (prop.equals(ON_LAYOUT)) {
                        c = 11;
                        break;
                    }
                    break;
                case 1349188574:
                    if (prop.equals(BORDER_RADIUS)) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    if (map.isNull(OPACITY) || map.getDouble(OPACITY) == 1.0d) {
                        return true;
                    }
                    return false;
                case 1:
                    if (map.hasKey(BACKGROUND_COLOR) && map.getInt(BACKGROUND_COLOR) != 0) {
                        return false;
                    }
                    if (!map.hasKey(BORDER_WIDTH) || map.isNull(BORDER_WIDTH) || map.getDouble(BORDER_WIDTH) == 0.0d) {
                        return true;
                    }
                    return false;
                case 2:
                    if (map.getInt(BORDER_LEFT_COLOR) != 0) {
                        z = false;
                    }
                    return z;
                case 3:
                    if (map.getInt(BORDER_RIGHT_COLOR) != 0) {
                        z = false;
                    }
                    return z;
                case 4:
                    if (map.getInt(BORDER_TOP_COLOR) != 0) {
                        z = false;
                    }
                    return z;
                case 5:
                    if (map.getInt(BORDER_BOTTOM_COLOR) != 0) {
                        z = false;
                    }
                    return z;
                case 6:
                    if (map.isNull(BORDER_WIDTH) || map.getDouble(BORDER_WIDTH) == 0.0d) {
                        return true;
                    }
                    return false;
                case 7:
                    if (map.isNull(BORDER_LEFT_WIDTH) || map.getDouble(BORDER_LEFT_WIDTH) == 0.0d) {
                        return true;
                    }
                    return false;
                case 8:
                    if (map.isNull(BORDER_TOP_WIDTH) || map.getDouble(BORDER_TOP_WIDTH) == 0.0d) {
                        return true;
                    }
                    return false;
                case 9:
                    if (map.isNull(BORDER_RIGHT_WIDTH) || map.getDouble(BORDER_RIGHT_WIDTH) == 0.0d) {
                        return true;
                    }
                    return false;
                case 10:
                    if (map.isNull(BORDER_BOTTOM_WIDTH) || map.getDouble(BORDER_BOTTOM_WIDTH) == 0.0d) {
                        return true;
                    }
                    return false;
                case 11:
                    return true;
                case 12:
                    return true;
                default:
                    return false;
            }
        }
    }
}
