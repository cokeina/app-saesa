package com.facebook.react.uimanager;

import android.support.annotation.Nullable;
import android.view.View;
import com.facebook.react.bridge.JSApplicationCausedNativeException;

public class IllegalViewOperationException extends JSApplicationCausedNativeException {
    @Nullable
    private View mView;

    public IllegalViewOperationException(String msg) {
        super(msg);
    }

    public IllegalViewOperationException(String msg, @Nullable View view, Throwable cause) {
        super(msg, cause);
        this.mView = view;
    }

    @Nullable
    public View getView() {
        return this.mView;
    }
}
