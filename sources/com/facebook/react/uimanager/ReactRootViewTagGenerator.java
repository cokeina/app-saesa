package com.facebook.react.uimanager;

public class ReactRootViewTagGenerator {
    private static final int ROOT_VIEW_TAG_INCREMENT = 10;
    private static int sNextRootViewTag = 1;

    public static synchronized int getNextRootViewTag() {
        int tag;
        synchronized (ReactRootViewTagGenerator.class) {
            tag = sNextRootViewTag;
            sNextRootViewTag += 10;
        }
        return tag;
    }
}
