package com.facebook.react.uimanager;

import android.os.SystemClock;
import com.facebook.common.logging.FLog;
import com.facebook.react.animation.Animation;
import com.facebook.react.animation.AnimationRegistry;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.GuardedRunnable;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.SoftAssertions;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.modules.core.ReactChoreographer;
import com.facebook.react.modules.core.ReactChoreographer.CallbackType;
import com.facebook.react.uimanager.debug.NotThreadSafeViewHierarchyUpdateDebugListener;
import com.facebook.systrace.Systrace;
import com.facebook.systrace.SystraceMessage;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;

public class UIViewOperationQueue {
    public static final int DEFAULT_MIN_TIME_LEFT_IN_FRAME_FOR_NONBATCHED_OPERATION_MS = 8;
    /* access modifiers changed from: private */
    public final AnimationRegistry mAnimationRegistry;
    private final Object mDispatchRunnablesLock = new Object();
    private final DispatchUIFrameCallback mDispatchUIFrameCallback;
    @GuardedBy("mDispatchRunnablesLock")
    private ArrayList<Runnable> mDispatchUIRunnables = new ArrayList<>();
    private boolean mIsDispatchUIFrameCallbackEnqueued = false;
    /* access modifiers changed from: private */
    public boolean mIsInIllegalUIState = false;
    /* access modifiers changed from: private */
    public boolean mIsProfilingNextBatch = false;
    /* access modifiers changed from: private */
    public final int[] mMeasureBuffer = new int[4];
    /* access modifiers changed from: private */
    public final NativeViewHierarchyManager mNativeViewHierarchyManager;
    /* access modifiers changed from: private */
    public long mNonBatchedExecutionTotalTime;
    /* access modifiers changed from: private */
    @GuardedBy("mNonBatchedOperationsLock")
    public ArrayDeque<UIOperation> mNonBatchedOperations = new ArrayDeque<>();
    /* access modifiers changed from: private */
    public final Object mNonBatchedOperationsLock = new Object();
    private ArrayList<UIOperation> mOperations = new ArrayList<>();
    private long mProfiledBatchBatchedExecutionTime;
    /* access modifiers changed from: private */
    public long mProfiledBatchCommitStartTime;
    /* access modifiers changed from: private */
    public long mProfiledBatchDispatchViewUpdatesTime;
    /* access modifiers changed from: private */
    public long mProfiledBatchLayoutTime;
    private long mProfiledBatchNonBatchedExecutionTime;
    /* access modifiers changed from: private */
    public long mProfiledBatchRunStartTime;
    private final ReactApplicationContext mReactApplicationContext;
    /* access modifiers changed from: private */
    @Nullable
    public NotThreadSafeViewHierarchyUpdateDebugListener mViewHierarchyUpdateDebugListener;

    private class AddAnimationOperation extends AnimationOperation {
        private final int mReactTag;
        private final Callback mSuccessCallback;

        private AddAnimationOperation(int reactTag, int animationID, Callback successCallback) {
            super(animationID);
            this.mReactTag = reactTag;
            this.mSuccessCallback = successCallback;
        }

        public void execute() {
            Animation animation = UIViewOperationQueue.this.mAnimationRegistry.getAnimation(this.mAnimationID);
            if (animation != null) {
                UIViewOperationQueue.this.mNativeViewHierarchyManager.startAnimationForNativeView(this.mReactTag, animation, this.mSuccessCallback);
                return;
            }
            throw new IllegalViewOperationException("Animation with id " + this.mAnimationID + " was not found");
        }
    }

    private static abstract class AnimationOperation implements UIOperation {
        protected final int mAnimationID;

        public AnimationOperation(int animationID) {
            this.mAnimationID = animationID;
        }
    }

    private final class ChangeJSResponderOperation extends ViewOperation {
        private final boolean mBlockNativeResponder;
        private final boolean mClearResponder;
        private final int mInitialTag;

        public ChangeJSResponderOperation(int tag, int initialTag, boolean clearResponder, boolean blockNativeResponder) {
            super(tag);
            this.mInitialTag = initialTag;
            this.mClearResponder = clearResponder;
            this.mBlockNativeResponder = blockNativeResponder;
        }

        public void execute() {
            if (!this.mClearResponder) {
                UIViewOperationQueue.this.mNativeViewHierarchyManager.setJSResponder(this.mTag, this.mInitialTag, this.mBlockNativeResponder);
            } else {
                UIViewOperationQueue.this.mNativeViewHierarchyManager.clearJSResponder();
            }
        }
    }

    private class ConfigureLayoutAnimationOperation implements UIOperation {
        private final ReadableMap mConfig;

        private ConfigureLayoutAnimationOperation(ReadableMap config) {
            this.mConfig = config;
        }

        public void execute() {
            UIViewOperationQueue.this.mNativeViewHierarchyManager.configureLayoutAnimation(this.mConfig);
        }
    }

    private final class CreateViewOperation extends ViewOperation {
        private final String mClassName;
        @Nullable
        private final ReactStylesDiffMap mInitialProps;
        private final ThemedReactContext mThemedContext;

        public CreateViewOperation(ThemedReactContext themedContext, int tag, String className, @Nullable ReactStylesDiffMap initialProps) {
            super(tag);
            this.mThemedContext = themedContext;
            this.mClassName = className;
            this.mInitialProps = initialProps;
            Systrace.startAsyncFlow(0, "createView", this.mTag);
        }

        public void execute() {
            Systrace.endAsyncFlow(0, "createView", this.mTag);
            UIViewOperationQueue.this.mNativeViewHierarchyManager.createView(this.mThemedContext, this.mTag, this.mClassName, this.mInitialProps);
        }
    }

    private final class DispatchCommandOperation extends ViewOperation {
        @Nullable
        private final ReadableArray mArgs;
        private final int mCommand;

        public DispatchCommandOperation(int tag, int command, @Nullable ReadableArray args) {
            super(tag);
            this.mCommand = command;
            this.mArgs = args;
        }

        public void execute() {
            UIViewOperationQueue.this.mNativeViewHierarchyManager.dispatchCommand(this.mTag, this.mCommand, this.mArgs);
        }
    }

    private class DispatchUIFrameCallback extends GuardedFrameCallback {
        private static final int FRAME_TIME_MS = 16;
        private final int mMinTimeLeftInFrameForNonBatchedOperationMs;

        private DispatchUIFrameCallback(ReactContext reactContext, int minTimeLeftInFrameForNonBatchedOperationMs) {
            super(reactContext);
            this.mMinTimeLeftInFrameForNonBatchedOperationMs = minTimeLeftInFrameForNonBatchedOperationMs;
        }

        /* JADX INFO: finally extract failed */
        public void doFrameGuarded(long frameTimeNanos) {
            if (UIViewOperationQueue.this.mIsInIllegalUIState) {
                FLog.w(ReactConstants.TAG, "Not flushing pending UI operations because of previously thrown Exception");
                return;
            }
            Systrace.beginSection(0, "dispatchNonBatchedUIOperations");
            try {
                dispatchPendingNonBatchedOperations(frameTimeNanos);
                Systrace.endSection(0);
                UIViewOperationQueue.this.flushPendingBatches();
                ReactChoreographer.getInstance().postFrameCallback(CallbackType.DISPATCH_UI, this);
            } catch (Throwable th) {
                Systrace.endSection(0);
                throw th;
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
            r2 = android.os.SystemClock.uptimeMillis();
            r1.execute();
            com.facebook.react.uimanager.UIViewOperationQueue.access$2402(r13.this$0, com.facebook.react.uimanager.UIViewOperationQueue.access$2400(r13.this$0) + (android.os.SystemClock.uptimeMillis() - r2));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0053, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0054, code lost:
            com.facebook.react.uimanager.UIViewOperationQueue.access$2002(r13.this$0, true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x005a, code lost:
            throw r0;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void dispatchPendingNonBatchedOperations(long r14) {
            /*
                r13 = this;
            L_0x0000:
                r6 = 16
                long r8 = java.lang.System.nanoTime()
                long r8 = r8 - r14
                r10 = 1000000(0xf4240, double:4.940656E-318)
                long r8 = r8 / r10
                long r4 = r6 - r8
                int r6 = r13.mMinTimeLeftInFrameForNonBatchedOperationMs
                long r6 = (long) r6
                int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r6 >= 0) goto L_0x0015
            L_0x0014:
                return
            L_0x0015:
                com.facebook.react.uimanager.UIViewOperationQueue r6 = com.facebook.react.uimanager.UIViewOperationQueue.this
                java.lang.Object r7 = r6.mNonBatchedOperationsLock
                monitor-enter(r7)
                com.facebook.react.uimanager.UIViewOperationQueue r6 = com.facebook.react.uimanager.UIViewOperationQueue.this     // Catch:{ all -> 0x002a }
                java.util.ArrayDeque r6 = r6.mNonBatchedOperations     // Catch:{ all -> 0x002a }
                boolean r6 = r6.isEmpty()     // Catch:{ all -> 0x002a }
                if (r6 == 0) goto L_0x002d
                monitor-exit(r7)     // Catch:{ all -> 0x002a }
                goto L_0x0014
            L_0x002a:
                r6 = move-exception
                monitor-exit(r7)     // Catch:{ all -> 0x002a }
                throw r6
            L_0x002d:
                com.facebook.react.uimanager.UIViewOperationQueue r6 = com.facebook.react.uimanager.UIViewOperationQueue.this     // Catch:{ all -> 0x002a }
                java.util.ArrayDeque r6 = r6.mNonBatchedOperations     // Catch:{ all -> 0x002a }
                java.lang.Object r1 = r6.pollFirst()     // Catch:{ all -> 0x002a }
                com.facebook.react.uimanager.UIViewOperationQueue$UIOperation r1 = (com.facebook.react.uimanager.UIViewOperationQueue.UIOperation) r1     // Catch:{ all -> 0x002a }
                monitor-exit(r7)     // Catch:{ all -> 0x002a }
                long r2 = android.os.SystemClock.uptimeMillis()     // Catch:{ Exception -> 0x0053 }
                r1.execute()     // Catch:{ Exception -> 0x0053 }
                com.facebook.react.uimanager.UIViewOperationQueue r6 = com.facebook.react.uimanager.UIViewOperationQueue.this     // Catch:{ Exception -> 0x0053 }
                com.facebook.react.uimanager.UIViewOperationQueue r7 = com.facebook.react.uimanager.UIViewOperationQueue.this     // Catch:{ Exception -> 0x0053 }
                long r8 = r7.mNonBatchedExecutionTotalTime     // Catch:{ Exception -> 0x0053 }
                long r10 = android.os.SystemClock.uptimeMillis()     // Catch:{ Exception -> 0x0053 }
                long r10 = r10 - r2
                long r8 = r8 + r10
                r6.mNonBatchedExecutionTotalTime = r8     // Catch:{ Exception -> 0x0053 }
                goto L_0x0000
            L_0x0053:
                r0 = move-exception
                com.facebook.react.uimanager.UIViewOperationQueue r6 = com.facebook.react.uimanager.UIViewOperationQueue.this
                r7 = 1
                r6.mIsInIllegalUIState = r7
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.uimanager.UIViewOperationQueue.DispatchUIFrameCallback.dispatchPendingNonBatchedOperations(long):void");
        }
    }

    private final class FindTargetForTouchOperation implements UIOperation {
        private final Callback mCallback;
        private final int mReactTag;
        private final float mTargetX;
        private final float mTargetY;

        private FindTargetForTouchOperation(int reactTag, float targetX, float targetY, Callback callback) {
            this.mReactTag = reactTag;
            this.mTargetX = targetX;
            this.mTargetY = targetY;
            this.mCallback = callback;
        }

        public void execute() {
            try {
                UIViewOperationQueue.this.mNativeViewHierarchyManager.measure(this.mReactTag, UIViewOperationQueue.this.mMeasureBuffer);
                float containerX = (float) UIViewOperationQueue.this.mMeasureBuffer[0];
                float containerY = (float) UIViewOperationQueue.this.mMeasureBuffer[1];
                int touchTargetReactTag = UIViewOperationQueue.this.mNativeViewHierarchyManager.findTargetTagForTouch(this.mReactTag, this.mTargetX, this.mTargetY);
                try {
                    UIViewOperationQueue.this.mNativeViewHierarchyManager.measure(touchTargetReactTag, UIViewOperationQueue.this.mMeasureBuffer);
                    float x = PixelUtil.toDIPFromPixel(((float) UIViewOperationQueue.this.mMeasureBuffer[0]) - containerX);
                    float y = PixelUtil.toDIPFromPixel(((float) UIViewOperationQueue.this.mMeasureBuffer[1]) - containerY);
                    float width = PixelUtil.toDIPFromPixel((float) UIViewOperationQueue.this.mMeasureBuffer[2]);
                    float height = PixelUtil.toDIPFromPixel((float) UIViewOperationQueue.this.mMeasureBuffer[3]);
                    this.mCallback.invoke(Integer.valueOf(touchTargetReactTag), Float.valueOf(x), Float.valueOf(y), Float.valueOf(width), Float.valueOf(height));
                } catch (IllegalViewOperationException e) {
                    this.mCallback.invoke(new Object[0]);
                }
            } catch (IllegalViewOperationException e2) {
                this.mCallback.invoke(new Object[0]);
            }
        }
    }

    private final class ManageChildrenOperation extends ViewOperation {
        @Nullable
        private final int[] mIndicesToRemove;
        @Nullable
        private final int[] mTagsToDelete;
        @Nullable
        private final ViewAtIndex[] mViewsToAdd;

        public ManageChildrenOperation(int tag, @Nullable int[] indicesToRemove, @Nullable ViewAtIndex[] viewsToAdd, @Nullable int[] tagsToDelete) {
            super(tag);
            this.mIndicesToRemove = indicesToRemove;
            this.mViewsToAdd = viewsToAdd;
            this.mTagsToDelete = tagsToDelete;
        }

        public void execute() {
            UIViewOperationQueue.this.mNativeViewHierarchyManager.manageChildren(this.mTag, this.mIndicesToRemove, this.mViewsToAdd, this.mTagsToDelete);
        }
    }

    private final class MeasureInWindowOperation implements UIOperation {
        private final Callback mCallback;
        private final int mReactTag;

        private MeasureInWindowOperation(int reactTag, Callback callback) {
            this.mReactTag = reactTag;
            this.mCallback = callback;
        }

        public void execute() {
            try {
                UIViewOperationQueue.this.mNativeViewHierarchyManager.measureInWindow(this.mReactTag, UIViewOperationQueue.this.mMeasureBuffer);
                float x = PixelUtil.toDIPFromPixel((float) UIViewOperationQueue.this.mMeasureBuffer[0]);
                float y = PixelUtil.toDIPFromPixel((float) UIViewOperationQueue.this.mMeasureBuffer[1]);
                float width = PixelUtil.toDIPFromPixel((float) UIViewOperationQueue.this.mMeasureBuffer[2]);
                float height = PixelUtil.toDIPFromPixel((float) UIViewOperationQueue.this.mMeasureBuffer[3]);
                this.mCallback.invoke(Float.valueOf(x), Float.valueOf(y), Float.valueOf(width), Float.valueOf(height));
            } catch (NoSuchNativeViewException e) {
                this.mCallback.invoke(new Object[0]);
            }
        }
    }

    private final class MeasureOperation implements UIOperation {
        private final Callback mCallback;
        private final int mReactTag;

        private MeasureOperation(int reactTag, Callback callback) {
            this.mReactTag = reactTag;
            this.mCallback = callback;
        }

        public void execute() {
            try {
                UIViewOperationQueue.this.mNativeViewHierarchyManager.measure(this.mReactTag, UIViewOperationQueue.this.mMeasureBuffer);
                float x = PixelUtil.toDIPFromPixel((float) UIViewOperationQueue.this.mMeasureBuffer[0]);
                float y = PixelUtil.toDIPFromPixel((float) UIViewOperationQueue.this.mMeasureBuffer[1]);
                float width = PixelUtil.toDIPFromPixel((float) UIViewOperationQueue.this.mMeasureBuffer[2]);
                float height = PixelUtil.toDIPFromPixel((float) UIViewOperationQueue.this.mMeasureBuffer[3]);
                this.mCallback.invoke(Integer.valueOf(0), Integer.valueOf(0), Float.valueOf(width), Float.valueOf(height), Float.valueOf(x), Float.valueOf(y));
            } catch (NoSuchNativeViewException e) {
                this.mCallback.invoke(new Object[0]);
            }
        }
    }

    private class RegisterAnimationOperation extends AnimationOperation {
        private final Animation mAnimation;

        private RegisterAnimationOperation(Animation animation) {
            super(animation.getAnimationID());
            this.mAnimation = animation;
        }

        public void execute() {
            UIViewOperationQueue.this.mAnimationRegistry.registerAnimation(this.mAnimation);
        }
    }

    private final class RemoveAnimationOperation extends AnimationOperation {
        private RemoveAnimationOperation(int animationID) {
            super(animationID);
        }

        public void execute() {
            Animation animation = UIViewOperationQueue.this.mAnimationRegistry.getAnimation(this.mAnimationID);
            if (animation != null) {
                animation.cancel();
            }
        }
    }

    private final class RemoveRootViewOperation extends ViewOperation {
        public RemoveRootViewOperation(int tag) {
            super(tag);
        }

        public void execute() {
            UIViewOperationQueue.this.mNativeViewHierarchyManager.removeRootView(this.mTag);
        }
    }

    private final class SendAccessibilityEvent extends ViewOperation {
        private final int mEventType;

        private SendAccessibilityEvent(int tag, int eventType) {
            super(tag);
            this.mEventType = eventType;
        }

        public void execute() {
            UIViewOperationQueue.this.mNativeViewHierarchyManager.sendAccessibilityEvent(this.mTag, this.mEventType);
        }
    }

    private final class SetChildrenOperation extends ViewOperation {
        private final ReadableArray mChildrenTags;

        public SetChildrenOperation(int tag, ReadableArray childrenTags) {
            super(tag);
            this.mChildrenTags = childrenTags;
        }

        public void execute() {
            UIViewOperationQueue.this.mNativeViewHierarchyManager.setChildren(this.mTag, this.mChildrenTags);
        }
    }

    private class SetLayoutAnimationEnabledOperation implements UIOperation {
        private final boolean mEnabled;

        private SetLayoutAnimationEnabledOperation(boolean enabled) {
            this.mEnabled = enabled;
        }

        public void execute() {
            UIViewOperationQueue.this.mNativeViewHierarchyManager.setLayoutAnimationEnabled(this.mEnabled);
        }
    }

    private final class ShowPopupMenuOperation extends ViewOperation {
        private final Callback mError;
        private final ReadableArray mItems;
        private final Callback mSuccess;

        public ShowPopupMenuOperation(int tag, ReadableArray items, Callback error, Callback success) {
            super(tag);
            this.mItems = items;
            this.mError = error;
            this.mSuccess = success;
        }

        public void execute() {
            UIViewOperationQueue.this.mNativeViewHierarchyManager.showPopupMenu(this.mTag, this.mItems, this.mSuccess, this.mError);
        }
    }

    private class UIBlockOperation implements UIOperation {
        private final UIBlock mBlock;

        public UIBlockOperation(UIBlock block) {
            this.mBlock = block;
        }

        public void execute() {
            this.mBlock.execute(UIViewOperationQueue.this.mNativeViewHierarchyManager);
        }
    }

    public interface UIOperation {
        void execute();
    }

    private final class UpdateLayoutOperation extends ViewOperation {
        private final int mHeight;
        private final int mParentTag;
        private final int mWidth;
        private final int mX;
        private final int mY;

        public UpdateLayoutOperation(int parentTag, int tag, int x, int y, int width, int height) {
            super(tag);
            this.mParentTag = parentTag;
            this.mX = x;
            this.mY = y;
            this.mWidth = width;
            this.mHeight = height;
            Systrace.startAsyncFlow(0, "updateLayout", this.mTag);
        }

        public void execute() {
            Systrace.endAsyncFlow(0, "updateLayout", this.mTag);
            UIViewOperationQueue.this.mNativeViewHierarchyManager.updateLayout(this.mParentTag, this.mTag, this.mX, this.mY, this.mWidth, this.mHeight);
        }
    }

    private final class UpdatePropertiesOperation extends ViewOperation {
        private final ReactStylesDiffMap mProps;

        private UpdatePropertiesOperation(int tag, ReactStylesDiffMap props) {
            super(tag);
            this.mProps = props;
        }

        public void execute() {
            UIViewOperationQueue.this.mNativeViewHierarchyManager.updateProperties(this.mTag, this.mProps);
        }
    }

    private final class UpdateViewExtraData extends ViewOperation {
        private final Object mExtraData;

        public UpdateViewExtraData(int tag, Object extraData) {
            super(tag);
            this.mExtraData = extraData;
        }

        public void execute() {
            UIViewOperationQueue.this.mNativeViewHierarchyManager.updateViewExtraData(this.mTag, this.mExtraData);
        }
    }

    private abstract class ViewOperation implements UIOperation {
        public int mTag;

        public ViewOperation(int tag) {
            this.mTag = tag;
        }
    }

    public UIViewOperationQueue(ReactApplicationContext reactContext, NativeViewHierarchyManager nativeViewHierarchyManager, int minTimeLeftInFrameForNonBatchedOperationMs) {
        this.mNativeViewHierarchyManager = nativeViewHierarchyManager;
        this.mAnimationRegistry = nativeViewHierarchyManager.getAnimationRegistry();
        if (minTimeLeftInFrameForNonBatchedOperationMs == -1) {
            minTimeLeftInFrameForNonBatchedOperationMs = 8;
        }
        this.mDispatchUIFrameCallback = new DispatchUIFrameCallback(reactContext, minTimeLeftInFrameForNonBatchedOperationMs);
        this.mReactApplicationContext = reactContext;
    }

    /* access modifiers changed from: 0000 */
    public NativeViewHierarchyManager getNativeViewHierarchyManager() {
        return this.mNativeViewHierarchyManager;
    }

    public void setViewHierarchyUpdateDebugListener(@Nullable NotThreadSafeViewHierarchyUpdateDebugListener listener) {
        this.mViewHierarchyUpdateDebugListener = listener;
    }

    public void profileNextBatch() {
        this.mIsProfilingNextBatch = true;
        this.mProfiledBatchCommitStartTime = 0;
    }

    public Map<String, Long> getProfiledBatchPerfCounters() {
        Map<String, Long> perfMap = new HashMap<>();
        perfMap.put("CommitStartTime", Long.valueOf(this.mProfiledBatchCommitStartTime));
        perfMap.put("LayoutTime", Long.valueOf(this.mProfiledBatchLayoutTime));
        perfMap.put("DispatchViewUpdatesTime", Long.valueOf(this.mProfiledBatchDispatchViewUpdatesTime));
        perfMap.put("RunStartTime", Long.valueOf(this.mProfiledBatchRunStartTime));
        perfMap.put("BatchedExecutionTime", Long.valueOf(this.mProfiledBatchBatchedExecutionTime));
        perfMap.put("NonBatchedExecutionTime", Long.valueOf(this.mProfiledBatchNonBatchedExecutionTime));
        return perfMap;
    }

    public boolean isEmpty() {
        return this.mOperations.isEmpty();
    }

    public void addRootView(int tag, SizeMonitoringFrameLayout rootView, ThemedReactContext themedRootContext) {
        this.mNativeViewHierarchyManager.addRootView(tag, rootView, themedRootContext);
    }

    /* access modifiers changed from: protected */
    public void enqueueUIOperation(UIOperation operation) {
        SoftAssertions.assertNotNull(operation);
        this.mOperations.add(operation);
    }

    public void enqueueRemoveRootView(int rootViewTag) {
        this.mOperations.add(new RemoveRootViewOperation(rootViewTag));
    }

    public void enqueueSetJSResponder(int tag, int initialTag, boolean blockNativeResponder) {
        this.mOperations.add(new ChangeJSResponderOperation(tag, initialTag, false, blockNativeResponder));
    }

    public void enqueueClearJSResponder() {
        this.mOperations.add(new ChangeJSResponderOperation(0, 0, true, false));
    }

    public void enqueueDispatchCommand(int reactTag, int commandId, ReadableArray commandArgs) {
        this.mOperations.add(new DispatchCommandOperation(reactTag, commandId, commandArgs));
    }

    public void enqueueUpdateExtraData(int reactTag, Object extraData) {
        this.mOperations.add(new UpdateViewExtraData(reactTag, extraData));
    }

    public void enqueueShowPopupMenu(int reactTag, ReadableArray items, Callback error, Callback success) {
        this.mOperations.add(new ShowPopupMenuOperation(reactTag, items, error, success));
    }

    public void enqueueCreateView(ThemedReactContext themedContext, int viewReactTag, String viewClassName, @Nullable ReactStylesDiffMap initialProps) {
        synchronized (this.mNonBatchedOperationsLock) {
            this.mNonBatchedOperations.addLast(new CreateViewOperation(themedContext, viewReactTag, viewClassName, initialProps));
        }
    }

    public void enqueueUpdateProperties(int reactTag, String className, ReactStylesDiffMap props) {
        this.mOperations.add(new UpdatePropertiesOperation(reactTag, props));
    }

    public void enqueueUpdateLayout(int parentTag, int reactTag, int x, int y, int width, int height) {
        this.mOperations.add(new UpdateLayoutOperation(parentTag, reactTag, x, y, width, height));
    }

    public void enqueueManageChildren(int reactTag, @Nullable int[] indicesToRemove, @Nullable ViewAtIndex[] viewsToAdd, @Nullable int[] tagsToDelete) {
        this.mOperations.add(new ManageChildrenOperation(reactTag, indicesToRemove, viewsToAdd, tagsToDelete));
    }

    public void enqueueSetChildren(int reactTag, ReadableArray childrenTags) {
        this.mOperations.add(new SetChildrenOperation(reactTag, childrenTags));
    }

    public void enqueueRegisterAnimation(Animation animation) {
        this.mOperations.add(new RegisterAnimationOperation(animation));
    }

    public void enqueueAddAnimation(int reactTag, int animationID, Callback onSuccess) {
        this.mOperations.add(new AddAnimationOperation(reactTag, animationID, onSuccess));
    }

    public void enqueueRemoveAnimation(int animationID) {
        this.mOperations.add(new RemoveAnimationOperation(animationID));
    }

    public void enqueueSetLayoutAnimationEnabled(boolean enabled) {
        this.mOperations.add(new SetLayoutAnimationEnabledOperation(enabled));
    }

    public void enqueueConfigureLayoutAnimation(ReadableMap config, Callback onSuccess, Callback onError) {
        this.mOperations.add(new ConfigureLayoutAnimationOperation(config));
    }

    public void enqueueMeasure(int reactTag, Callback callback) {
        this.mOperations.add(new MeasureOperation(reactTag, callback));
    }

    public void enqueueMeasureInWindow(int reactTag, Callback callback) {
        this.mOperations.add(new MeasureInWindowOperation(reactTag, callback));
    }

    public void enqueueFindTargetForTouch(int reactTag, float targetX, float targetY, Callback callback) {
        this.mOperations.add(new FindTargetForTouchOperation(reactTag, targetX, targetY, callback));
    }

    public void enqueueSendAccessibilityEvent(int tag, int eventType) {
        this.mOperations.add(new SendAccessibilityEvent(tag, eventType));
    }

    public void enqueueUIBlock(UIBlock block) {
        this.mOperations.add(new UIBlockOperation(block));
    }

    public void prependUIBlock(UIBlock block) {
        this.mOperations.add(0, new UIBlockOperation(block));
    }

    /* access modifiers changed from: 0000 */
    public void dispatchViewUpdates(int batchId, long commitStartTime, long layoutTime) {
        final ArrayList<UIOperation> batchedOperations;
        final ArrayDeque<UIOperation> nonBatchedOperations;
        SystraceMessage.beginSection(0, "UIViewOperationQueue.dispatchViewUpdates").arg("batchId", batchId).flush();
        try {
            final long dispatchViewUpdatesTime = SystemClock.uptimeMillis();
            if (!this.mOperations.isEmpty()) {
                batchedOperations = this.mOperations;
                this.mOperations = new ArrayList<>();
            } else {
                batchedOperations = null;
            }
            synchronized (this.mNonBatchedOperationsLock) {
                if (!this.mNonBatchedOperations.isEmpty()) {
                    nonBatchedOperations = this.mNonBatchedOperations;
                    this.mNonBatchedOperations = new ArrayDeque<>();
                } else {
                    nonBatchedOperations = null;
                }
            }
            if (this.mViewHierarchyUpdateDebugListener != null) {
                this.mViewHierarchyUpdateDebugListener.onViewHierarchyUpdateEnqueued();
            }
            final int i = batchId;
            final long j = commitStartTime;
            final long j2 = layoutTime;
            Runnable runOperations = new Runnable() {
                public void run() {
                    SystraceMessage.beginSection(0, "DispatchUI").arg("BatchId", i).flush();
                    try {
                        long runStartTime = SystemClock.uptimeMillis();
                        if (nonBatchedOperations != null) {
                            Iterator it = nonBatchedOperations.iterator();
                            while (it.hasNext()) {
                                ((UIOperation) it.next()).execute();
                            }
                        }
                        if (batchedOperations != null) {
                            Iterator it2 = batchedOperations.iterator();
                            while (it2.hasNext()) {
                                ((UIOperation) it2.next()).execute();
                            }
                        }
                        if (UIViewOperationQueue.this.mIsProfilingNextBatch && UIViewOperationQueue.this.mProfiledBatchCommitStartTime == 0) {
                            UIViewOperationQueue.this.mProfiledBatchCommitStartTime = j;
                            UIViewOperationQueue.this.mProfiledBatchLayoutTime = j2;
                            UIViewOperationQueue.this.mProfiledBatchDispatchViewUpdatesTime = dispatchViewUpdatesTime;
                            UIViewOperationQueue.this.mProfiledBatchRunStartTime = runStartTime;
                            Systrace.beginAsyncSection(0, "delayBeforeDispatchViewUpdates", 0, UIViewOperationQueue.this.mProfiledBatchCommitStartTime * 1000000);
                            Systrace.endAsyncSection(0, "delayBeforeDispatchViewUpdates", 0, UIViewOperationQueue.this.mProfiledBatchDispatchViewUpdatesTime * 1000000);
                            Systrace.beginAsyncSection(0, "delayBeforeBatchRunStart", 0, UIViewOperationQueue.this.mProfiledBatchDispatchViewUpdatesTime * 1000000);
                            Systrace.endAsyncSection(0, "delayBeforeBatchRunStart", 0, UIViewOperationQueue.this.mProfiledBatchRunStartTime * 1000000);
                        }
                        UIViewOperationQueue.this.mNativeViewHierarchyManager.clearLayoutAnimation();
                        if (UIViewOperationQueue.this.mViewHierarchyUpdateDebugListener != null) {
                            UIViewOperationQueue.this.mViewHierarchyUpdateDebugListener.onViewHierarchyUpdateFinished();
                        }
                        Systrace.endSection(0);
                    } catch (Exception e) {
                        UIViewOperationQueue.this.mIsInIllegalUIState = true;
                        throw e;
                    } catch (Throwable th) {
                        Systrace.endSection(0);
                        throw th;
                    }
                }
            };
            SystraceMessage.beginSection(0, "acquiring mDispatchRunnablesLock").arg("batchId", batchId).flush();
            synchronized (this.mDispatchRunnablesLock) {
                Systrace.endSection(0);
                this.mDispatchUIRunnables.add(runOperations);
            }
            if (!this.mIsDispatchUIFrameCallbackEnqueued) {
                UiThreadUtil.runOnUiThread(new GuardedRunnable(this.mReactApplicationContext) {
                    public void runGuarded() {
                        UIViewOperationQueue.this.flushPendingBatches();
                    }
                });
            }
            Systrace.endSection(0);
        } catch (Throwable th) {
            Systrace.endSection(0);
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    public void resumeFrameCallback() {
        this.mIsDispatchUIFrameCallbackEnqueued = true;
        ReactChoreographer.getInstance().postFrameCallback(CallbackType.DISPATCH_UI, this.mDispatchUIFrameCallback);
    }

    /* access modifiers changed from: 0000 */
    public void pauseFrameCallback() {
        this.mIsDispatchUIFrameCallbackEnqueued = false;
        ReactChoreographer.getInstance().removeFrameCallback(CallbackType.DISPATCH_UI, this.mDispatchUIFrameCallback);
        flushPendingBatches();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0024, code lost:
        r6 = android.os.SystemClock.uptimeMillis();
        r2 = r9.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        if (r2.hasNext() == false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        ((java.lang.Runnable) r2.next()).run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0043, code lost:
        if (r10.mIsProfilingNextBatch == false) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0045, code lost:
        r10.mProfiledBatchBatchedExecutionTime = android.os.SystemClock.uptimeMillis() - r6;
        r10.mProfiledBatchNonBatchedExecutionTime = r10.mNonBatchedExecutionTotalTime;
        r10.mIsProfilingNextBatch = false;
        com.facebook.systrace.Systrace.beginAsyncSection(0, "batchedExecutionTime", 0, 1000000 * r6);
        com.facebook.systrace.Systrace.endAsyncSection(0, "batchedExecutionTime", 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0060, code lost:
        r10.mNonBatchedExecutionTotalTime = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void flushPendingBatches() {
        /*
            r10 = this;
            r0 = 0
            r3 = 0
            boolean r2 = r10.mIsInIllegalUIState
            if (r2 == 0) goto L_0x000f
            java.lang.String r0 = "ReactNative"
            java.lang.String r1 = "Not flushing pending UI operations because of previously thrown Exception"
            com.facebook.common.logging.FLog.w(r0, r1)
        L_0x000e:
            return
        L_0x000f:
            java.lang.Object r2 = r10.mDispatchRunnablesLock
            monitor-enter(r2)
            java.util.ArrayList<java.lang.Runnable> r4 = r10.mDispatchUIRunnables     // Catch:{ all -> 0x003e }
            boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x003e }
            if (r4 != 0) goto L_0x003c
            java.util.ArrayList<java.lang.Runnable> r9 = r10.mDispatchUIRunnables     // Catch:{ all -> 0x003e }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x003e }
            r4.<init>()     // Catch:{ all -> 0x003e }
            r10.mDispatchUIRunnables = r4     // Catch:{ all -> 0x003e }
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            long r6 = android.os.SystemClock.uptimeMillis()
            java.util.Iterator r2 = r9.iterator()
        L_0x002c:
            boolean r4 = r2.hasNext()
            if (r4 == 0) goto L_0x0041
            java.lang.Object r8 = r2.next()
            java.lang.Runnable r8 = (java.lang.Runnable) r8
            r8.run()
            goto L_0x002c
        L_0x003c:
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            goto L_0x000e
        L_0x003e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            throw r0
        L_0x0041:
            boolean r2 = r10.mIsProfilingNextBatch
            if (r2 == 0) goto L_0x0060
            long r4 = android.os.SystemClock.uptimeMillis()
            long r4 = r4 - r6
            r10.mProfiledBatchBatchedExecutionTime = r4
            long r4 = r10.mNonBatchedExecutionTotalTime
            r10.mProfiledBatchNonBatchedExecutionTime = r4
            r10.mIsProfilingNextBatch = r3
            java.lang.String r2 = "batchedExecutionTime"
            r4 = 1000000(0xf4240, double:4.940656E-318)
            long r4 = r4 * r6
            com.facebook.systrace.Systrace.beginAsyncSection(r0, r2, r3, r4)
            java.lang.String r2 = "batchedExecutionTime"
            com.facebook.systrace.Systrace.endAsyncSection(r0, r2, r3)
        L_0x0060:
            r10.mNonBatchedExecutionTotalTime = r0
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.uimanager.UIViewOperationQueue.flushPendingBatches():void");
    }
}
