package com.facebook.react.uimanager.util;

import android.view.View;
import android.view.ViewGroup;
import com.facebook.react.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.annotation.Nullable;

public class ReactFindViewUtil {
    private static final Map<OnMultipleViewsFoundListener, Set<String>> mOnMultipleViewsFoundListener = new HashMap();
    private static final List<OnViewFoundListener> mOnViewFoundListeners = new ArrayList();

    public interface OnMultipleViewsFoundListener {
        void onViewFound(View view, String str);
    }

    public interface OnViewFoundListener {
        String getNativeId();

        void onViewFound(View view);
    }

    @Nullable
    public static View findView(View root, String nativeId) {
        String tag = getNativeId(root);
        if (tag != null && tag.equals(nativeId)) {
            return root;
        }
        if (root instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) root;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View view = findView(viewGroup.getChildAt(i), nativeId);
                if (view != null) {
                    return view;
                }
            }
        }
        return null;
    }

    public static void findView(View root, OnViewFoundListener onViewFoundListener) {
        View view = findView(root, onViewFoundListener.getNativeId());
        if (view != null) {
            onViewFoundListener.onViewFound(view);
        }
        addViewListener(onViewFoundListener);
    }

    public static void addViewListener(OnViewFoundListener onViewFoundListener) {
        mOnViewFoundListeners.add(onViewFoundListener);
    }

    public static void removeViewListener(OnViewFoundListener onViewFoundListener) {
        mOnViewFoundListeners.remove(onViewFoundListener);
    }

    public static void addViewsListener(OnMultipleViewsFoundListener listener, Set<String> ids) {
        mOnMultipleViewsFoundListener.put(listener, ids);
    }

    public static void removeViewsListener(OnMultipleViewsFoundListener listener) {
        mOnMultipleViewsFoundListener.remove(listener);
    }

    public static void notifyViewRendered(View view) {
        String nativeId = getNativeId(view);
        if (nativeId != null) {
            Iterator<OnViewFoundListener> iterator = mOnViewFoundListeners.iterator();
            while (iterator.hasNext()) {
                OnViewFoundListener listener = (OnViewFoundListener) iterator.next();
                if (nativeId != null && nativeId.equals(listener.getNativeId())) {
                    listener.onViewFound(view);
                    iterator.remove();
                }
            }
            Iterator<Entry<OnMultipleViewsFoundListener, Set<String>>> viewIterator = mOnMultipleViewsFoundListener.entrySet().iterator();
            while (viewIterator.hasNext()) {
                Entry<OnMultipleViewsFoundListener, Set<String>> entry = (Entry) viewIterator.next();
                Set<String> nativeIds = (Set) entry.getValue();
                if (nativeIds.contains(nativeId)) {
                    ((OnMultipleViewsFoundListener) entry.getKey()).onViewFound(view, nativeId);
                    nativeIds.remove(nativeId);
                }
                if (nativeIds.isEmpty()) {
                    viewIterator.remove();
                }
            }
        }
    }

    @Nullable
    private static String getNativeId(View view) {
        Object tag = view.getTag(R.id.view_tag_native_id);
        if (tag instanceof String) {
            return (String) tag;
        }
        return null;
    }
}
