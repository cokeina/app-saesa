package com.facebook.react.uimanager;

import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import com.facebook.common.logging.FLog;
import com.facebook.debug.holder.PrinterHolder;
import com.facebook.debug.tags.ReactDebugOverlayTags;
import com.facebook.react.animation.Animation;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.GuardedRunnable;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.OnBatchCompleteListener;
import com.facebook.react.bridge.PerformanceCounter;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMarker;
import com.facebook.react.bridge.ReactMarkerConstants;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.uimanager.SizeMonitoringFrameLayout.OnSizeChangedListener;
import com.facebook.react.uimanager.debug.NotThreadSafeViewHierarchyUpdateDebugListener;
import com.facebook.react.uimanager.events.EventDispatcher;
import com.facebook.systrace.Systrace;
import com.facebook.systrace.SystraceMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

@ReactModule(name = "UIManager")
public class UIManagerModule extends ReactContextBaseJavaModule implements OnBatchCompleteListener, LifecycleEventListener, PerformanceCounter {
    private static final boolean DEBUG = PrinterHolder.getPrinter().shouldDisplayLogMessage(ReactDebugOverlayTags.UI_MANAGER);
    protected static final String NAME = "UIManager";
    private int mBatchId = 0;
    /* access modifiers changed from: private */
    public final Map<String, Object> mCustomDirectEvents;
    private final EventDispatcher mEventDispatcher;
    private final List<UIManagerModuleListener> mListeners = new ArrayList();
    private final MemoryTrimCallback mMemoryTrimCallback = new MemoryTrimCallback();
    private final Map<String, Object> mModuleConstants;
    /* access modifiers changed from: private */
    public final UIImplementation mUIImplementation;

    public interface CustomEventNamesResolver {
        @Nullable
        String resolveCustomEventName(String str);
    }

    private class MemoryTrimCallback implements ComponentCallbacks2 {
        private MemoryTrimCallback() {
        }

        public void onTrimMemory(int level) {
            if (level >= 60) {
                YogaNodePool.get().clear();
            }
        }

        public void onConfigurationChanged(Configuration newConfig) {
        }

        public void onLowMemory() {
        }
    }

    public interface ViewManagerResolver {
        @Nullable
        ViewManager getViewManager(String str);

        List<String> getViewManagerNames();
    }

    public UIManagerModule(ReactApplicationContext reactContext, ViewManagerResolver viewManagerResolver, UIImplementationProvider uiImplementationProvider, int minTimeLeftInFrameForNonBatchedOperationMs) {
        super(reactContext);
        DisplayMetricsHolder.initDisplayMetricsIfNotInitialized(reactContext);
        this.mEventDispatcher = new EventDispatcher(reactContext);
        this.mModuleConstants = createConstants(viewManagerResolver);
        this.mCustomDirectEvents = UIManagerModuleConstants.getDirectEventTypeConstants();
        this.mUIImplementation = uiImplementationProvider.createUIImplementation(reactContext, viewManagerResolver, this.mEventDispatcher, minTimeLeftInFrameForNonBatchedOperationMs);
        reactContext.addLifecycleEventListener(this);
    }

    public UIManagerModule(ReactApplicationContext reactContext, List<ViewManager> viewManagersList, UIImplementationProvider uiImplementationProvider, int minTimeLeftInFrameForNonBatchedOperationMs) {
        super(reactContext);
        DisplayMetricsHolder.initDisplayMetricsIfNotInitialized(reactContext);
        this.mEventDispatcher = new EventDispatcher(reactContext);
        this.mCustomDirectEvents = MapBuilder.newHashMap();
        this.mModuleConstants = createConstants(viewManagersList, null, this.mCustomDirectEvents);
        this.mUIImplementation = uiImplementationProvider.createUIImplementation(reactContext, viewManagersList, this.mEventDispatcher, minTimeLeftInFrameForNonBatchedOperationMs);
        reactContext.addLifecycleEventListener(this);
    }

    public UIImplementation getUIImplementation() {
        return this.mUIImplementation;
    }

    public String getName() {
        return NAME;
    }

    public Map<String, Object> getConstants() {
        return this.mModuleConstants;
    }

    public void initialize() {
        getReactApplicationContext().registerComponentCallbacks(this.mMemoryTrimCallback);
    }

    public void onHostResume() {
        this.mUIImplementation.onHostResume();
    }

    public void onHostPause() {
        this.mUIImplementation.onHostPause();
    }

    public void onHostDestroy() {
        this.mUIImplementation.onHostDestroy();
    }

    public void onCatalystInstanceDestroy() {
        super.onCatalystInstanceDestroy();
        this.mEventDispatcher.onCatalystInstanceDestroyed();
        getReactApplicationContext().unregisterComponentCallbacks(this.mMemoryTrimCallback);
        YogaNodePool.get().clear();
        ViewManagerPropertyUpdater.clear();
    }

    private static Map<String, Object> createConstants(ViewManagerResolver viewManagerResolver) {
        ReactMarker.logMarker(ReactMarkerConstants.CREATE_UI_MANAGER_MODULE_CONSTANTS_START);
        Systrace.beginSection(0, "CreateUIManagerConstants");
        try {
            return UIManagerModuleConstantsHelper.createConstants(viewManagerResolver);
        } finally {
            Systrace.endSection(0);
            ReactMarker.logMarker(ReactMarkerConstants.CREATE_UI_MANAGER_MODULE_CONSTANTS_END);
        }
    }

    private static Map<String, Object> createConstants(List<ViewManager> viewManagers, @Nullable Map<String, Object> customBubblingEvents, @Nullable Map<String, Object> customDirectEvents) {
        ReactMarker.logMarker(ReactMarkerConstants.CREATE_UI_MANAGER_MODULE_CONSTANTS_START);
        Systrace.beginSection(0, "CreateUIManagerConstants");
        try {
            return UIManagerModuleConstantsHelper.createConstants(viewManagers, customBubblingEvents, customDirectEvents);
        } finally {
            Systrace.endSection(0);
            ReactMarker.logMarker(ReactMarkerConstants.CREATE_UI_MANAGER_MODULE_CONSTANTS_END);
        }
    }

    @Nullable
    @ReactMethod(isBlockingSynchronousMethod = true)
    public WritableMap getConstantsForViewManager(String viewManagerName) {
        ViewManager targetView;
        WritableNativeMap writableNativeMap = null;
        if (viewManagerName != null) {
            targetView = this.mUIImplementation.resolveViewManager(viewManagerName);
        } else {
            targetView = null;
        }
        if (targetView != null) {
            SystraceMessage.beginSection(0, "UIManagerModule.getConstantsForViewManager").arg("ViewManager", (Object) targetView.getName()).arg("Lazy", (Object) Boolean.valueOf(true)).flush();
            try {
                Map<String, Object> viewManagerConstants = UIManagerModuleConstantsHelper.createConstantsForViewManager(targetView, null, null, null, this.mCustomDirectEvents);
                if (viewManagerConstants != null) {
                    writableNativeMap = Arguments.makeNativeMap(viewManagerConstants);
                } else {
                    SystraceMessage.endSection(0).flush();
                }
            } finally {
                SystraceMessage.endSection(0).flush();
            }
        }
        return writableNativeMap;
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public WritableMap getDefaultEventTypes() {
        return Arguments.makeNativeMap(UIManagerModuleConstantsHelper.getDefaultExportableEventTypes());
    }

    public CustomEventNamesResolver getDirectEventNamesResolver() {
        return new CustomEventNamesResolver() {
            @Nullable
            public String resolveCustomEventName(String eventName) {
                Map<String, String> customEventType = (Map) UIManagerModule.this.mCustomDirectEvents.get(eventName);
                if (customEventType != null) {
                    return (String) customEventType.get("registrationName");
                }
                return eventName;
            }
        };
    }

    public Map<String, Long> getPerformanceCounters() {
        return this.mUIImplementation.getProfiledBatchPerfCounters();
    }

    public <T extends SizeMonitoringFrameLayout & MeasureSpecProvider> int addRootView(T rootView) {
        Systrace.beginSection(0, "UIManagerModule.addRootView");
        final int tag = ReactRootViewTagGenerator.getNextRootViewTag();
        final ReactApplicationContext reactApplicationContext = getReactApplicationContext();
        this.mUIImplementation.registerRootView(rootView, tag, new ThemedReactContext(reactApplicationContext, rootView.getContext()));
        rootView.setOnSizeChangedListener(new OnSizeChangedListener() {
            public void onSizeChanged(final int width, final int height, int oldW, int oldH) {
                reactApplicationContext.runOnNativeModulesQueueThread(new GuardedRunnable(reactApplicationContext) {
                    public void runGuarded() {
                        UIManagerModule.this.updateNodeSize(tag, width, height);
                    }
                });
            }
        });
        Systrace.endSection(0);
        return tag;
    }

    @ReactMethod
    public void removeRootView(int rootViewTag) {
        this.mUIImplementation.removeRootView(rootViewTag);
    }

    public void updateNodeSize(int nodeViewTag, int newWidth, int newHeight) {
        getReactApplicationContext().assertOnNativeModulesQueueThread();
        this.mUIImplementation.updateNodeSize(nodeViewTag, newWidth, newHeight);
    }

    public void setViewLocalData(final int tag, final Object data) {
        ReactApplicationContext reactApplicationContext = getReactApplicationContext();
        reactApplicationContext.assertOnUiQueueThread();
        reactApplicationContext.runOnNativeModulesQueueThread(new GuardedRunnable(reactApplicationContext) {
            public void runGuarded() {
                UIManagerModule.this.mUIImplementation.setViewLocalData(tag, data);
            }
        });
    }

    @ReactMethod
    public void createView(int tag, String className, int rootViewTag, ReadableMap props) {
        if (DEBUG) {
            String message = "(UIManager.createView) tag: " + tag + ", class: " + className + ", props: " + props;
            FLog.d(ReactConstants.TAG, message);
            PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.UI_MANAGER, message);
        }
        this.mUIImplementation.createView(tag, className, rootViewTag, props);
    }

    @ReactMethod
    public void updateView(int tag, String className, ReadableMap props) {
        if (DEBUG) {
            String message = "(UIManager.updateView) tag: " + tag + ", class: " + className + ", props: " + props;
            FLog.d(ReactConstants.TAG, message);
            PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.UI_MANAGER, message);
        }
        this.mUIImplementation.updateView(tag, className, props);
    }

    @ReactMethod
    public void manageChildren(int viewTag, @Nullable ReadableArray moveFrom, @Nullable ReadableArray moveTo, @Nullable ReadableArray addChildTags, @Nullable ReadableArray addAtIndices, @Nullable ReadableArray removeFrom) {
        if (DEBUG) {
            String message = "(UIManager.manageChildren) tag: " + viewTag + ", moveFrom: " + moveFrom + ", moveTo: " + moveTo + ", addTags: " + addChildTags + ", atIndices: " + addAtIndices + ", removeFrom: " + removeFrom;
            FLog.d(ReactConstants.TAG, message);
            PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.UI_MANAGER, message);
        }
        this.mUIImplementation.manageChildren(viewTag, moveFrom, moveTo, addChildTags, addAtIndices, removeFrom);
    }

    @ReactMethod
    public void setChildren(int viewTag, ReadableArray childrenTags) {
        if (DEBUG) {
            String message = "(UIManager.setChildren) tag: " + viewTag + ", children: " + childrenTags;
            FLog.d(ReactConstants.TAG, message);
            PrinterHolder.getPrinter().logMessage(ReactDebugOverlayTags.UI_MANAGER, message);
        }
        this.mUIImplementation.setChildren(viewTag, childrenTags);
    }

    @ReactMethod
    public void replaceExistingNonRootView(int oldTag, int newTag) {
        this.mUIImplementation.replaceExistingNonRootView(oldTag, newTag);
    }

    @ReactMethod
    public void removeSubviewsFromContainerWithID(int containerTag) {
        this.mUIImplementation.removeSubviewsFromContainerWithID(containerTag);
    }

    @ReactMethod
    public void measure(int reactTag, Callback callback) {
        this.mUIImplementation.measure(reactTag, callback);
    }

    @ReactMethod
    public void measureInWindow(int reactTag, Callback callback) {
        this.mUIImplementation.measureInWindow(reactTag, callback);
    }

    @ReactMethod
    public void measureLayout(int tag, int ancestorTag, Callback errorCallback, Callback successCallback) {
        this.mUIImplementation.measureLayout(tag, ancestorTag, errorCallback, successCallback);
    }

    @ReactMethod
    public void measureLayoutRelativeToParent(int tag, Callback errorCallback, Callback successCallback) {
        this.mUIImplementation.measureLayoutRelativeToParent(tag, errorCallback, successCallback);
    }

    @ReactMethod
    public void findSubviewIn(int reactTag, ReadableArray point, Callback callback) {
        this.mUIImplementation.findSubviewIn(reactTag, (float) Math.round(PixelUtil.toPixelFromDIP(point.getDouble(0))), (float) Math.round(PixelUtil.toPixelFromDIP(point.getDouble(1))), callback);
    }

    @ReactMethod
    public void viewIsDescendantOf(int reactTag, int ancestorReactTag, Callback callback) {
        this.mUIImplementation.viewIsDescendantOf(reactTag, ancestorReactTag, callback);
    }

    public void registerAnimation(Animation animation) {
        this.mUIImplementation.registerAnimation(animation);
    }

    public void addAnimation(int reactTag, int animationID, Callback onSuccess) {
        this.mUIImplementation.addAnimation(reactTag, animationID, onSuccess);
    }

    public void removeAnimation(int reactTag, int animationID) {
        this.mUIImplementation.removeAnimation(reactTag, animationID);
    }

    @ReactMethod
    public void setJSResponder(int reactTag, boolean blockNativeResponder) {
        this.mUIImplementation.setJSResponder(reactTag, blockNativeResponder);
    }

    @ReactMethod
    public void clearJSResponder() {
        this.mUIImplementation.clearJSResponder();
    }

    @ReactMethod
    public void dispatchViewManagerCommand(int reactTag, int commandId, ReadableArray commandArgs) {
        this.mUIImplementation.dispatchViewManagerCommand(reactTag, commandId, commandArgs);
    }

    @ReactMethod
    public void showPopupMenu(int reactTag, ReadableArray items, Callback error, Callback success) {
        this.mUIImplementation.showPopupMenu(reactTag, items, error, success);
    }

    @ReactMethod
    public void setLayoutAnimationEnabledExperimental(boolean enabled) {
        this.mUIImplementation.setLayoutAnimationEnabledExperimental(enabled);
    }

    @ReactMethod
    public void configureNextLayoutAnimation(ReadableMap config, Callback success, Callback error) {
        this.mUIImplementation.configureNextLayoutAnimation(config, success, error);
    }

    public void onBatchComplete() {
        int batchId = this.mBatchId;
        this.mBatchId++;
        SystraceMessage.beginSection(0, "onBatchCompleteUI").arg("BatchId", batchId).flush();
        for (UIManagerModuleListener listener : this.mListeners) {
            listener.willDispatchViewUpdates(this);
        }
        try {
            this.mUIImplementation.dispatchViewUpdates(batchId);
        } finally {
            Systrace.endSection(0);
        }
    }

    public void setViewHierarchyUpdateDebugListener(@Nullable NotThreadSafeViewHierarchyUpdateDebugListener listener) {
        this.mUIImplementation.setViewHierarchyUpdateDebugListener(listener);
    }

    public EventDispatcher getEventDispatcher() {
        return this.mEventDispatcher;
    }

    @ReactMethod
    public void sendAccessibilityEvent(int tag, int eventType) {
        this.mUIImplementation.sendAccessibilityEvent(tag, eventType);
    }

    public void addUIBlock(UIBlock block) {
        this.mUIImplementation.addUIBlock(block);
    }

    public void prependUIBlock(UIBlock block) {
        this.mUIImplementation.prependUIBlock(block);
    }

    public void addUIManagerListener(UIManagerModuleListener listener) {
        this.mListeners.add(listener);
    }

    public void removeUIManagerListener(UIManagerModuleListener listener) {
        this.mListeners.remove(listener);
    }

    public int resolveRootTagFromReactTag(int reactTag) {
        return this.mUIImplementation.resolveRootTagFromReactTag(reactTag);
    }

    public void invalidateNodeLayout(int tag) {
        ReactShadowNode node = this.mUIImplementation.resolveShadowNode(tag);
        if (node == null) {
            FLog.w(ReactConstants.TAG, "Warning : attempted to dirty a non-existent react shadow node. reactTag=" + tag);
        } else {
            node.dirty();
        }
    }

    public void updateRootLayoutSpecs(int rootViewTag, int widthMeasureSpec, int heightMeasureSpec) {
        this.mUIImplementation.updateRootView(rootViewTag, widthMeasureSpec, heightMeasureSpec);
        this.mUIImplementation.dispatchViewUpdates(-1);
    }
}
