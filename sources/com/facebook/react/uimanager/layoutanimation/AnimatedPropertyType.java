package com.facebook.react.uimanager.layoutanimation;

import com.facebook.react.uimanager.ViewProps;

enum AnimatedPropertyType {
    OPACITY(ViewProps.OPACITY),
    SCALE_XY("scaleXY");
    
    private final String mName;

    private AnimatedPropertyType(String name) {
        this.mName = name;
    }

    public static AnimatedPropertyType fromString(String name) {
        AnimatedPropertyType[] values;
        for (AnimatedPropertyType property : values()) {
            if (property.toString().equalsIgnoreCase(name)) {
                return property;
            }
        }
        throw new IllegalArgumentException("Unsupported animated property : " + name);
    }

    public String toString() {
        return this.mName;
    }
}
