package com.facebook.react.uimanager;

import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.UIManagerModule.ViewManagerResolver;
import com.facebook.systrace.Systrace;
import com.facebook.systrace.SystraceMessage;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

class UIManagerModuleConstantsHelper {
    private static final String BUBBLING_EVENTS_KEY = "bubblingEventTypes";
    private static final String DIRECT_EVENTS_KEY = "directEventTypes";

    UIManagerModuleConstantsHelper() {
    }

    static Map<String, Object> createConstants(ViewManagerResolver resolver) {
        Map<String, Object> constants = UIManagerModuleConstants.getConstants();
        constants.put("ViewManagerNames", resolver.getViewManagerNames());
        return constants;
    }

    static Map<String, Object> getDefaultExportableEventTypes() {
        return MapBuilder.of(BUBBLING_EVENTS_KEY, UIManagerModuleConstants.getBubblingEventTypeConstants(), DIRECT_EVENTS_KEY, UIManagerModuleConstants.getDirectEventTypeConstants());
    }

    static Map<String, Object> createConstants(List<ViewManager> viewManagers, @Nullable Map<String, Object> allBubblingEventTypes, @Nullable Map<String, Object> allDirectEventTypes) {
        Map<String, Object> constants = UIManagerModuleConstants.getConstants();
        Map genericBubblingEventTypes = UIManagerModuleConstants.getBubblingEventTypeConstants();
        Map genericDirectEventTypes = UIManagerModuleConstants.getDirectEventTypeConstants();
        if (allBubblingEventTypes != null) {
            allBubblingEventTypes.putAll(genericBubblingEventTypes);
        }
        if (allDirectEventTypes != null) {
            allDirectEventTypes.putAll(genericDirectEventTypes);
        }
        for (ViewManager viewManager : viewManagers) {
            String viewManagerName = viewManager.getName();
            SystraceMessage.beginSection(0, "UIManagerModuleConstantsHelper.createConstants").arg("ViewManager", (Object) viewManagerName).arg("Lazy", (Object) Boolean.valueOf(false)).flush();
            try {
                Map viewManagerConstants = createConstantsForViewManager(viewManager, null, null, allBubblingEventTypes, allDirectEventTypes);
                if (!viewManagerConstants.isEmpty()) {
                    constants.put(viewManagerName, viewManagerConstants);
                }
            } finally {
                Systrace.endSection(0);
            }
        }
        constants.put("genericBubblingEventTypes", genericBubblingEventTypes);
        constants.put("genericDirectEventTypes", genericDirectEventTypes);
        return constants;
    }

    static Map<String, Object> createConstantsForViewManager(ViewManager viewManager, @Nullable Map defaultBubblingEvents, @Nullable Map defaultDirectEvents, @Nullable Map cumulativeBubblingEventTypes, @Nullable Map cumulativeDirectEventTypes) {
        Map<String, Object> viewManagerConstants = MapBuilder.newHashMap();
        Map viewManagerBubblingEvents = viewManager.getExportedCustomBubblingEventTypeConstants();
        if (viewManagerBubblingEvents != null) {
            recursiveMerge(cumulativeBubblingEventTypes, viewManagerBubblingEvents);
            recursiveMerge(viewManagerBubblingEvents, defaultBubblingEvents);
            viewManagerConstants.put(BUBBLING_EVENTS_KEY, viewManagerBubblingEvents);
        } else if (defaultBubblingEvents != null) {
            viewManagerConstants.put(BUBBLING_EVENTS_KEY, defaultBubblingEvents);
        }
        Map viewManagerDirectEvents = viewManager.getExportedCustomDirectEventTypeConstants();
        if (viewManagerDirectEvents != null) {
            recursiveMerge(cumulativeDirectEventTypes, viewManagerDirectEvents);
            recursiveMerge(viewManagerDirectEvents, defaultDirectEvents);
            viewManagerConstants.put(DIRECT_EVENTS_KEY, viewManagerDirectEvents);
        } else if (defaultDirectEvents != null) {
            viewManagerConstants.put(DIRECT_EVENTS_KEY, defaultDirectEvents);
        }
        Map customViewConstants = viewManager.getExportedViewConstants();
        if (customViewConstants != null) {
            viewManagerConstants.put("Constants", customViewConstants);
        }
        Map viewManagerCommands = viewManager.getCommandsMap();
        if (viewManagerCommands != null) {
            viewManagerConstants.put("Commands", viewManagerCommands);
        }
        Map<String, String> viewManagerNativeProps = viewManager.getNativeProps();
        if (!viewManagerNativeProps.isEmpty()) {
            viewManagerConstants.put("NativeProps", viewManagerNativeProps);
        }
        return viewManagerConstants;
    }

    private static void recursiveMerge(@Nullable Map dest, @Nullable Map source) {
        if (dest != null && source != null && !source.isEmpty()) {
            for (Object key : source.keySet()) {
                Object sourceValue = source.get(key);
                Object destValue = dest.get(key);
                if (destValue == null || !(sourceValue instanceof Map) || !(destValue instanceof Map)) {
                    dest.put(key, sourceValue);
                } else {
                    recursiveMerge((Map) destValue, (Map) sourceValue);
                }
            }
        }
    }
}
