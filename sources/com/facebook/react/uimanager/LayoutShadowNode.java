package com.facebook.react.uimanager;

import com.facebook.react.bridge.Dynamic;
import com.facebook.react.bridge.JSApplicationIllegalArgumentException;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.devsupport.StackTraceHelper;
import com.facebook.react.modules.i18nmanager.I18nUtil;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.annotations.ReactPropGroup;
import com.facebook.yoga.YogaAlign;
import com.facebook.yoga.YogaDisplay;
import com.facebook.yoga.YogaFlexDirection;
import com.facebook.yoga.YogaJustify;
import com.facebook.yoga.YogaOverflow;
import com.facebook.yoga.YogaPositionType;
import com.facebook.yoga.YogaUnit;
import com.facebook.yoga.YogaWrap;
import javax.annotation.Nullable;

public class LayoutShadowNode extends ReactShadowNodeImpl {
    private final MutableYogaValue mTempYogaValue = new MutableYogaValue();

    private static class MutableYogaValue {
        YogaUnit unit;
        float value;

        private MutableYogaValue() {
        }

        /* access modifiers changed from: 0000 */
        public void setFromDynamic(Dynamic dynamic) {
            if (dynamic.isNull()) {
                this.unit = YogaUnit.UNDEFINED;
                this.value = Float.NaN;
            } else if (dynamic.getType() == ReadableType.String) {
                String s = dynamic.asString();
                if (s.equals("auto")) {
                    this.unit = YogaUnit.AUTO;
                    this.value = Float.NaN;
                } else if (s.endsWith("%")) {
                    this.unit = YogaUnit.PERCENT;
                    this.value = Float.parseFloat(s.substring(0, s.length() - 1));
                } else {
                    throw new IllegalArgumentException("Unknown value: " + s);
                }
            } else {
                this.unit = YogaUnit.POINT;
                this.value = PixelUtil.toPixelFromDIP(dynamic.asDouble());
            }
        }
    }

    @ReactProp(name = "width")
    public void setWidth(Dynamic width) {
        if (!isVirtual()) {
            this.mTempYogaValue.setFromDynamic(width);
            switch (this.mTempYogaValue.unit) {
                case POINT:
                case UNDEFINED:
                    setStyleWidth(this.mTempYogaValue.value);
                    break;
                case AUTO:
                    setStyleWidthAuto();
                    break;
                case PERCENT:
                    setStyleWidthPercent(this.mTempYogaValue.value);
                    break;
            }
            width.recycle();
        }
    }

    @ReactProp(name = "minWidth")
    public void setMinWidth(Dynamic minWidth) {
        if (!isVirtual()) {
            this.mTempYogaValue.setFromDynamic(minWidth);
            switch (this.mTempYogaValue.unit) {
                case POINT:
                case UNDEFINED:
                    setStyleMinWidth(this.mTempYogaValue.value);
                    break;
                case PERCENT:
                    setStyleMinWidthPercent(this.mTempYogaValue.value);
                    break;
            }
            minWidth.recycle();
        }
    }

    @ReactProp(name = "maxWidth")
    public void setMaxWidth(Dynamic maxWidth) {
        if (!isVirtual()) {
            this.mTempYogaValue.setFromDynamic(maxWidth);
            switch (this.mTempYogaValue.unit) {
                case POINT:
                case UNDEFINED:
                    setStyleMaxWidth(this.mTempYogaValue.value);
                    break;
                case PERCENT:
                    setStyleMaxWidthPercent(this.mTempYogaValue.value);
                    break;
            }
            maxWidth.recycle();
        }
    }

    @ReactProp(name = "height")
    public void setHeight(Dynamic height) {
        if (!isVirtual()) {
            this.mTempYogaValue.setFromDynamic(height);
            switch (this.mTempYogaValue.unit) {
                case POINT:
                case UNDEFINED:
                    setStyleHeight(this.mTempYogaValue.value);
                    break;
                case AUTO:
                    setStyleHeightAuto();
                    break;
                case PERCENT:
                    setStyleHeightPercent(this.mTempYogaValue.value);
                    break;
            }
            height.recycle();
        }
    }

    @ReactProp(name = "minHeight")
    public void setMinHeight(Dynamic minHeight) {
        if (!isVirtual()) {
            this.mTempYogaValue.setFromDynamic(minHeight);
            switch (this.mTempYogaValue.unit) {
                case POINT:
                case UNDEFINED:
                    setStyleMinHeight(this.mTempYogaValue.value);
                    break;
                case PERCENT:
                    setStyleMinHeightPercent(this.mTempYogaValue.value);
                    break;
            }
            minHeight.recycle();
        }
    }

    @ReactProp(name = "maxHeight")
    public void setMaxHeight(Dynamic maxHeight) {
        if (!isVirtual()) {
            this.mTempYogaValue.setFromDynamic(maxHeight);
            switch (this.mTempYogaValue.unit) {
                case POINT:
                case UNDEFINED:
                    setStyleMaxHeight(this.mTempYogaValue.value);
                    break;
                case PERCENT:
                    setStyleMaxHeightPercent(this.mTempYogaValue.value);
                    break;
            }
            maxHeight.recycle();
        }
    }

    @ReactProp(defaultFloat = 0.0f, name = "flex")
    public void setFlex(float flex) {
        if (!isVirtual()) {
            super.setFlex(flex);
        }
    }

    @ReactProp(defaultFloat = 0.0f, name = "flexGrow")
    public void setFlexGrow(float flexGrow) {
        if (!isVirtual()) {
            super.setFlexGrow(flexGrow);
        }
    }

    @ReactProp(defaultFloat = 0.0f, name = "flexShrink")
    public void setFlexShrink(float flexShrink) {
        if (!isVirtual()) {
            super.setFlexShrink(flexShrink);
        }
    }

    @ReactProp(name = "flexBasis")
    public void setFlexBasis(Dynamic flexBasis) {
        if (!isVirtual()) {
            this.mTempYogaValue.setFromDynamic(flexBasis);
            switch (this.mTempYogaValue.unit) {
                case POINT:
                case UNDEFINED:
                    setFlexBasis(this.mTempYogaValue.value);
                    break;
                case AUTO:
                    setFlexBasisAuto();
                    break;
                case PERCENT:
                    setFlexBasisPercent(this.mTempYogaValue.value);
                    break;
            }
            flexBasis.recycle();
        }
    }

    @ReactProp(defaultFloat = Float.NaN, name = "aspectRatio")
    public void setAspectRatio(float aspectRatio) {
        setStyleAspectRatio(aspectRatio);
    }

    @ReactProp(name = "flexDirection")
    public void setFlexDirection(@Nullable String flexDirection) {
        if (!isVirtual()) {
            if (flexDirection == null) {
                setFlexDirection(YogaFlexDirection.COLUMN);
                return;
            }
            char c = 65535;
            switch (flexDirection.hashCode()) {
                case -1448970769:
                    if (flexDirection.equals("row-reverse")) {
                        c = 3;
                        break;
                    }
                    break;
                case -1354837162:
                    if (flexDirection.equals(StackTraceHelper.COLUMN_KEY)) {
                        c = 0;
                        break;
                    }
                    break;
                case 113114:
                    if (flexDirection.equals("row")) {
                        c = 2;
                        break;
                    }
                    break;
                case 1272730475:
                    if (flexDirection.equals("column-reverse")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    setFlexDirection(YogaFlexDirection.COLUMN);
                    return;
                case 1:
                    setFlexDirection(YogaFlexDirection.COLUMN_REVERSE);
                    return;
                case 2:
                    setFlexDirection(YogaFlexDirection.ROW);
                    return;
                case 3:
                    setFlexDirection(YogaFlexDirection.ROW_REVERSE);
                    return;
                default:
                    throw new JSApplicationIllegalArgumentException("invalid value for flexDirection: " + flexDirection);
            }
        }
    }

    @ReactProp(name = "flexWrap")
    public void setFlexWrap(@Nullable String flexWrap) {
        if (!isVirtual()) {
            if (flexWrap == null) {
                setFlexWrap(YogaWrap.NO_WRAP);
                return;
            }
            char c = 65535;
            switch (flexWrap.hashCode()) {
                case -1039592053:
                    if (flexWrap.equals("nowrap")) {
                        c = 0;
                        break;
                    }
                    break;
                case 3657802:
                    if (flexWrap.equals("wrap")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    setFlexWrap(YogaWrap.NO_WRAP);
                    return;
                case 1:
                    setFlexWrap(YogaWrap.WRAP);
                    return;
                default:
                    throw new JSApplicationIllegalArgumentException("invalid value for flexWrap: " + flexWrap);
            }
        }
    }

    @ReactProp(name = "alignSelf")
    public void setAlignSelf(@Nullable String alignSelf) {
        if (!isVirtual()) {
            if (alignSelf == null) {
                setAlignSelf(YogaAlign.AUTO);
                return;
            }
            char c = 65535;
            switch (alignSelf.hashCode()) {
                case -1881872635:
                    if (alignSelf.equals("stretch")) {
                        c = 4;
                        break;
                    }
                    break;
                case -1720785339:
                    if (alignSelf.equals("baseline")) {
                        c = 5;
                        break;
                    }
                    break;
                case -1364013995:
                    if (alignSelf.equals("center")) {
                        c = 2;
                        break;
                    }
                    break;
                case -46581362:
                    if (alignSelf.equals("flex-start")) {
                        c = 1;
                        break;
                    }
                    break;
                case 3005871:
                    if (alignSelf.equals("auto")) {
                        c = 0;
                        break;
                    }
                    break;
                case 441309761:
                    if (alignSelf.equals("space-between")) {
                        c = 6;
                        break;
                    }
                    break;
                case 1742952711:
                    if (alignSelf.equals("flex-end")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1937124468:
                    if (alignSelf.equals("space-around")) {
                        c = 7;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    setAlignSelf(YogaAlign.AUTO);
                    return;
                case 1:
                    setAlignSelf(YogaAlign.FLEX_START);
                    return;
                case 2:
                    setAlignSelf(YogaAlign.CENTER);
                    return;
                case 3:
                    setAlignSelf(YogaAlign.FLEX_END);
                    return;
                case 4:
                    setAlignSelf(YogaAlign.STRETCH);
                    return;
                case 5:
                    setAlignSelf(YogaAlign.BASELINE);
                    return;
                case 6:
                    setAlignSelf(YogaAlign.SPACE_BETWEEN);
                    return;
                case 7:
                    setAlignSelf(YogaAlign.SPACE_AROUND);
                    return;
                default:
                    throw new JSApplicationIllegalArgumentException("invalid value for alignSelf: " + alignSelf);
            }
        }
    }

    @ReactProp(name = "alignItems")
    public void setAlignItems(@Nullable String alignItems) {
        if (!isVirtual()) {
            if (alignItems == null) {
                setAlignItems(YogaAlign.STRETCH);
                return;
            }
            char c = 65535;
            switch (alignItems.hashCode()) {
                case -1881872635:
                    if (alignItems.equals("stretch")) {
                        c = 4;
                        break;
                    }
                    break;
                case -1720785339:
                    if (alignItems.equals("baseline")) {
                        c = 5;
                        break;
                    }
                    break;
                case -1364013995:
                    if (alignItems.equals("center")) {
                        c = 2;
                        break;
                    }
                    break;
                case -46581362:
                    if (alignItems.equals("flex-start")) {
                        c = 1;
                        break;
                    }
                    break;
                case 3005871:
                    if (alignItems.equals("auto")) {
                        c = 0;
                        break;
                    }
                    break;
                case 441309761:
                    if (alignItems.equals("space-between")) {
                        c = 6;
                        break;
                    }
                    break;
                case 1742952711:
                    if (alignItems.equals("flex-end")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1937124468:
                    if (alignItems.equals("space-around")) {
                        c = 7;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    setAlignItems(YogaAlign.AUTO);
                    return;
                case 1:
                    setAlignItems(YogaAlign.FLEX_START);
                    return;
                case 2:
                    setAlignItems(YogaAlign.CENTER);
                    return;
                case 3:
                    setAlignItems(YogaAlign.FLEX_END);
                    return;
                case 4:
                    setAlignItems(YogaAlign.STRETCH);
                    return;
                case 5:
                    setAlignItems(YogaAlign.BASELINE);
                    return;
                case 6:
                    setAlignItems(YogaAlign.SPACE_BETWEEN);
                    return;
                case 7:
                    setAlignItems(YogaAlign.SPACE_AROUND);
                    return;
                default:
                    throw new JSApplicationIllegalArgumentException("invalid value for alignItems: " + alignItems);
            }
        }
    }

    @ReactProp(name = "alignContent")
    public void setAlignContent(@Nullable String alignContent) {
        if (!isVirtual()) {
            if (alignContent == null) {
                setAlignContent(YogaAlign.FLEX_START);
                return;
            }
            char c = 65535;
            switch (alignContent.hashCode()) {
                case -1881872635:
                    if (alignContent.equals("stretch")) {
                        c = 4;
                        break;
                    }
                    break;
                case -1720785339:
                    if (alignContent.equals("baseline")) {
                        c = 5;
                        break;
                    }
                    break;
                case -1364013995:
                    if (alignContent.equals("center")) {
                        c = 2;
                        break;
                    }
                    break;
                case -46581362:
                    if (alignContent.equals("flex-start")) {
                        c = 1;
                        break;
                    }
                    break;
                case 3005871:
                    if (alignContent.equals("auto")) {
                        c = 0;
                        break;
                    }
                    break;
                case 441309761:
                    if (alignContent.equals("space-between")) {
                        c = 6;
                        break;
                    }
                    break;
                case 1742952711:
                    if (alignContent.equals("flex-end")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1937124468:
                    if (alignContent.equals("space-around")) {
                        c = 7;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    setAlignContent(YogaAlign.AUTO);
                    return;
                case 1:
                    setAlignContent(YogaAlign.FLEX_START);
                    return;
                case 2:
                    setAlignContent(YogaAlign.CENTER);
                    return;
                case 3:
                    setAlignContent(YogaAlign.FLEX_END);
                    return;
                case 4:
                    setAlignContent(YogaAlign.STRETCH);
                    return;
                case 5:
                    setAlignContent(YogaAlign.BASELINE);
                    return;
                case 6:
                    setAlignContent(YogaAlign.SPACE_BETWEEN);
                    return;
                case 7:
                    setAlignContent(YogaAlign.SPACE_AROUND);
                    return;
                default:
                    throw new JSApplicationIllegalArgumentException("invalid value for alignContent: " + alignContent);
            }
        }
    }

    @ReactProp(name = "justifyContent")
    public void setJustifyContent(@Nullable String justifyContent) {
        if (!isVirtual()) {
            if (justifyContent == null) {
                setJustifyContent(YogaJustify.FLEX_START);
                return;
            }
            char c = 65535;
            switch (justifyContent.hashCode()) {
                case -1364013995:
                    if (justifyContent.equals("center")) {
                        c = 1;
                        break;
                    }
                    break;
                case -46581362:
                    if (justifyContent.equals("flex-start")) {
                        c = 0;
                        break;
                    }
                    break;
                case 441309761:
                    if (justifyContent.equals("space-between")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1742952711:
                    if (justifyContent.equals("flex-end")) {
                        c = 2;
                        break;
                    }
                    break;
                case 1937124468:
                    if (justifyContent.equals("space-around")) {
                        c = 4;
                        break;
                    }
                    break;
                case 2055030478:
                    if (justifyContent.equals("space-evenly")) {
                        c = 5;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    setJustifyContent(YogaJustify.FLEX_START);
                    return;
                case 1:
                    setJustifyContent(YogaJustify.CENTER);
                    return;
                case 2:
                    setJustifyContent(YogaJustify.FLEX_END);
                    return;
                case 3:
                    setJustifyContent(YogaJustify.SPACE_BETWEEN);
                    return;
                case 4:
                    setJustifyContent(YogaJustify.SPACE_AROUND);
                    return;
                case 5:
                    setJustifyContent(YogaJustify.SPACE_EVENLY);
                    return;
                default:
                    throw new JSApplicationIllegalArgumentException("invalid value for justifyContent: " + justifyContent);
            }
        }
    }

    @ReactProp(name = "overflow")
    public void setOverflow(@Nullable String overflow) {
        if (!isVirtual()) {
            if (overflow == null) {
                setOverflow(YogaOverflow.VISIBLE);
                return;
            }
            char c = 65535;
            switch (overflow.hashCode()) {
                case -1217487446:
                    if (overflow.equals("hidden")) {
                        c = 1;
                        break;
                    }
                    break;
                case -907680051:
                    if (overflow.equals("scroll")) {
                        c = 2;
                        break;
                    }
                    break;
                case 466743410:
                    if (overflow.equals("visible")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    setOverflow(YogaOverflow.VISIBLE);
                    return;
                case 1:
                    setOverflow(YogaOverflow.HIDDEN);
                    return;
                case 2:
                    setOverflow(YogaOverflow.SCROLL);
                    return;
                default:
                    throw new JSApplicationIllegalArgumentException("invalid value for overflow: " + overflow);
            }
        }
    }

    @ReactProp(name = "display")
    public void setDisplay(@Nullable String display) {
        if (!isVirtual()) {
            if (display == null) {
                setDisplay(YogaDisplay.FLEX);
                return;
            }
            char c = 65535;
            switch (display.hashCode()) {
                case 3145721:
                    if (display.equals(ViewProps.FLEX)) {
                        c = 0;
                        break;
                    }
                    break;
                case 3387192:
                    if (display.equals(ViewProps.NONE)) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    setDisplay(YogaDisplay.FLEX);
                    return;
                case 1:
                    setDisplay(YogaDisplay.NONE);
                    return;
                default:
                    throw new JSApplicationIllegalArgumentException("invalid value for display: " + display);
            }
        }
    }

    @ReactPropGroup(names = {"margin", "marginVertical", "marginHorizontal", "marginStart", "marginEnd", "marginTop", "marginBottom", "marginLeft", "marginRight"})
    public void setMargins(int index, Dynamic margin) {
        if (!isVirtual()) {
            int spacingType = maybeTransformLeftRightToStartEnd(ViewProps.PADDING_MARGIN_SPACING_TYPES[index]);
            this.mTempYogaValue.setFromDynamic(margin);
            switch (this.mTempYogaValue.unit) {
                case POINT:
                case UNDEFINED:
                    setMargin(spacingType, this.mTempYogaValue.value);
                    break;
                case AUTO:
                    setMarginAuto(spacingType);
                    break;
                case PERCENT:
                    setMarginPercent(spacingType, this.mTempYogaValue.value);
                    break;
            }
            margin.recycle();
        }
    }

    @ReactPropGroup(names = {"padding", "paddingVertical", "paddingHorizontal", "paddingStart", "paddingEnd", "paddingTop", "paddingBottom", "paddingLeft", "paddingRight"})
    public void setPaddings(int index, Dynamic padding) {
        if (!isVirtual()) {
            int spacingType = maybeTransformLeftRightToStartEnd(ViewProps.PADDING_MARGIN_SPACING_TYPES[index]);
            this.mTempYogaValue.setFromDynamic(padding);
            switch (this.mTempYogaValue.unit) {
                case POINT:
                case UNDEFINED:
                    setPadding(spacingType, this.mTempYogaValue.value);
                    break;
                case PERCENT:
                    setPaddingPercent(spacingType, this.mTempYogaValue.value);
                    break;
            }
            padding.recycle();
        }
    }

    @ReactPropGroup(defaultFloat = Float.NaN, names = {"borderWidth", "borderStartWidth", "borderEndWidth", "borderTopWidth", "borderBottomWidth", "borderLeftWidth", "borderRightWidth"})
    public void setBorderWidths(int index, float borderWidth) {
        if (!isVirtual()) {
            setBorder(maybeTransformLeftRightToStartEnd(ViewProps.BORDER_SPACING_TYPES[index]), PixelUtil.toPixelFromDIP(borderWidth));
        }
    }

    @ReactPropGroup(names = {"start", "end", "left", "right", "top", "bottom"})
    public void setPositionValues(int index, Dynamic position) {
        if (!isVirtual()) {
            int spacingType = maybeTransformLeftRightToStartEnd(new int[]{4, 5, 0, 2, 1, 3}[index]);
            this.mTempYogaValue.setFromDynamic(position);
            switch (this.mTempYogaValue.unit) {
                case POINT:
                case UNDEFINED:
                    setPosition(spacingType, this.mTempYogaValue.value);
                    break;
                case PERCENT:
                    setPositionPercent(spacingType, this.mTempYogaValue.value);
                    break;
            }
            position.recycle();
        }
    }

    private int maybeTransformLeftRightToStartEnd(int spacingType) {
        if (!I18nUtil.getInstance().doLeftAndRightSwapInRTL(getThemedContext())) {
            return spacingType;
        }
        switch (spacingType) {
            case 0:
                return 4;
            case 2:
                return 5;
            default:
                return spacingType;
        }
    }

    @ReactProp(name = "position")
    public void setPosition(@Nullable String position) {
        if (!isVirtual()) {
            if (position == null) {
                setPositionType(YogaPositionType.RELATIVE);
                return;
            }
            char c = 65535;
            switch (position.hashCode()) {
                case -554435892:
                    if (position.equals("relative")) {
                        c = 0;
                        break;
                    }
                    break;
                case 1728122231:
                    if (position.equals("absolute")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    setPositionType(YogaPositionType.RELATIVE);
                    return;
                case 1:
                    setPositionType(YogaPositionType.ABSOLUTE);
                    return;
                default:
                    throw new JSApplicationIllegalArgumentException("invalid value for position: " + position);
            }
        }
    }

    @ReactProp(name = "onLayout")
    public void setShouldNotifyOnLayout(boolean shouldNotifyOnLayout) {
        super.setShouldNotifyOnLayout(shouldNotifyOnLayout);
    }
}
