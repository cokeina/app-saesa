package com.facebook.react.uimanager;

import com.facebook.infer.annotation.Assertions;
import com.facebook.react.uimanager.annotations.ReactPropertyHolder;
import com.facebook.yoga.YogaAlign;
import com.facebook.yoga.YogaBaselineFunction;
import com.facebook.yoga.YogaConfig;
import com.facebook.yoga.YogaConstants;
import com.facebook.yoga.YogaDirection;
import com.facebook.yoga.YogaDisplay;
import com.facebook.yoga.YogaEdge;
import com.facebook.yoga.YogaFlexDirection;
import com.facebook.yoga.YogaJustify;
import com.facebook.yoga.YogaMeasureFunction;
import com.facebook.yoga.YogaNode;
import com.facebook.yoga.YogaOverflow;
import com.facebook.yoga.YogaPositionType;
import com.facebook.yoga.YogaValue;
import com.facebook.yoga.YogaWrap;
import java.util.ArrayList;
import java.util.Arrays;
import javax.annotation.Nullable;

@ReactPropertyHolder
public class ReactShadowNodeImpl implements ReactShadowNode<ReactShadowNodeImpl> {
    private static YogaConfig sYogaConfig;
    @Nullable
    private ArrayList<ReactShadowNodeImpl> mChildren;
    private final Spacing mDefaultPadding = new Spacing(0.0f);
    private boolean mIsLayoutOnly;
    @Nullable
    private ArrayList<ReactShadowNodeImpl> mNativeChildren;
    @Nullable
    private ReactShadowNodeImpl mNativeParent;
    private boolean mNodeUpdated = true;
    private final float[] mPadding = new float[9];
    private final boolean[] mPaddingIsPercent = new boolean[9];
    @Nullable
    private ReactShadowNodeImpl mParent;
    private int mReactTag;
    @Nullable
    private ReactShadowNodeImpl mRootNode;
    private int mScreenHeight;
    private int mScreenWidth;
    private int mScreenX;
    private int mScreenY;
    private boolean mShouldNotifyOnLayout;
    @Nullable
    private ThemedReactContext mThemedContext;
    private int mTotalNativeChildren = 0;
    @Nullable
    private String mViewClassName;
    private final YogaNode mYogaNode;

    public ReactShadowNodeImpl() {
        if (!isVirtual()) {
            YogaNode node = (YogaNode) YogaNodePool.get().acquire();
            if (sYogaConfig == null) {
                sYogaConfig = new YogaConfig();
                sYogaConfig.setPointScaleFactor(0.0f);
                sYogaConfig.setUseLegacyStretchBehaviour(true);
            }
            if (node == null) {
                node = new YogaNode(sYogaConfig);
            }
            this.mYogaNode = node;
            Arrays.fill(this.mPadding, Float.NaN);
            return;
        }
        this.mYogaNode = null;
    }

    public boolean isVirtual() {
        return false;
    }

    public boolean isVirtualAnchor() {
        return false;
    }

    public boolean isYogaLeafNode() {
        return isMeasureDefined();
    }

    public final String getViewClass() {
        return (String) Assertions.assertNotNull(this.mViewClassName);
    }

    public final boolean hasUpdates() {
        return this.mNodeUpdated || hasNewLayout() || isDirty();
    }

    public final void markUpdateSeen() {
        this.mNodeUpdated = false;
        if (hasNewLayout()) {
            markLayoutSeen();
        }
    }

    public void markUpdated() {
        if (!this.mNodeUpdated) {
            this.mNodeUpdated = true;
            ReactShadowNodeImpl parent = getParent();
            if (parent != null) {
                parent.markUpdated();
            }
        }
    }

    public final boolean hasUnseenUpdates() {
        return this.mNodeUpdated;
    }

    public void dirty() {
        if (!isVirtual()) {
            this.mYogaNode.dirty();
        }
    }

    public final boolean isDirty() {
        return this.mYogaNode != null && this.mYogaNode.isDirty();
    }

    public void addChildAt(ReactShadowNodeImpl child, int i) {
        if (child.getParent() != null) {
            throw new IllegalViewOperationException("Tried to add child that already has a parent! Remove it from its parent first.");
        }
        if (this.mChildren == null) {
            this.mChildren = new ArrayList<>(4);
        }
        this.mChildren.add(i, child);
        child.mParent = this;
        if (this.mYogaNode != null && !isYogaLeafNode()) {
            YogaNode childYogaNode = child.mYogaNode;
            if (childYogaNode == null) {
                throw new RuntimeException("Cannot add a child that doesn't have a YogaNode to a parent without a measure function! (Trying to add a '" + child.getClass().getSimpleName() + "' to a '" + getClass().getSimpleName() + "')");
            }
            this.mYogaNode.addChildAt(childYogaNode, i);
        }
        markUpdated();
        int increase = child.isLayoutOnly() ? child.getTotalNativeChildren() : 1;
        this.mTotalNativeChildren += increase;
        updateNativeChildrenCountInParent(increase);
    }

    public ReactShadowNodeImpl removeChildAt(int i) {
        if (this.mChildren == null) {
            throw new ArrayIndexOutOfBoundsException("Index " + i + " out of bounds: node has no children");
        }
        ReactShadowNodeImpl removed = (ReactShadowNodeImpl) this.mChildren.remove(i);
        removed.mParent = null;
        if (this.mYogaNode != null && !isYogaLeafNode()) {
            this.mYogaNode.removeChildAt(i);
        }
        markUpdated();
        int decrease = removed.isLayoutOnly() ? removed.getTotalNativeChildren() : 1;
        this.mTotalNativeChildren -= decrease;
        updateNativeChildrenCountInParent(-decrease);
        return removed;
    }

    public final int getChildCount() {
        if (this.mChildren == null) {
            return 0;
        }
        return this.mChildren.size();
    }

    public final ReactShadowNodeImpl getChildAt(int i) {
        if (this.mChildren != null) {
            return (ReactShadowNodeImpl) this.mChildren.get(i);
        }
        throw new ArrayIndexOutOfBoundsException("Index " + i + " out of bounds: node has no children");
    }

    public final int indexOf(ReactShadowNodeImpl child) {
        if (this.mChildren == null) {
            return -1;
        }
        return this.mChildren.indexOf(child);
    }

    public void removeAndDisposeAllChildren() {
        int i;
        if (getChildCount() != 0) {
            int decrease = 0;
            for (int i2 = getChildCount() - 1; i2 >= 0; i2--) {
                if (this.mYogaNode != null && !isYogaLeafNode()) {
                    this.mYogaNode.removeChildAt(i2);
                }
                ReactShadowNodeImpl toRemove = getChildAt(i2);
                toRemove.mParent = null;
                toRemove.dispose();
                if (toRemove.isLayoutOnly()) {
                    i = toRemove.getTotalNativeChildren();
                } else {
                    i = 1;
                }
                decrease += i;
            }
            ((ArrayList) Assertions.assertNotNull(this.mChildren)).clear();
            markUpdated();
            this.mTotalNativeChildren -= decrease;
            updateNativeChildrenCountInParent(-decrease);
        }
    }

    private void updateNativeChildrenCountInParent(int delta) {
        if (this.mIsLayoutOnly) {
            ReactShadowNodeImpl parent = getParent();
            while (parent != null) {
                parent.mTotalNativeChildren += delta;
                if (parent.isLayoutOnly()) {
                    parent = parent.getParent();
                } else {
                    return;
                }
            }
        }
    }

    public void onBeforeLayout() {
    }

    public final void updateProperties(ReactStylesDiffMap props) {
        ViewManagerPropertyUpdater.updateProps(this, props);
        onAfterUpdateTransaction();
    }

    public void onAfterUpdateTransaction() {
    }

    public void onCollectExtraUpdates(UIViewOperationQueue uiViewOperationQueue) {
    }

    public boolean dispatchUpdates(float absoluteX, float absoluteY, UIViewOperationQueue uiViewOperationQueue, NativeViewHierarchyOptimizer nativeViewHierarchyOptimizer) {
        if (this.mNodeUpdated) {
            onCollectExtraUpdates(uiViewOperationQueue);
        }
        if (!hasNewLayout()) {
            return false;
        }
        float layoutX = getLayoutX();
        float layoutY = getLayoutY();
        int newAbsoluteLeft = Math.round(absoluteX + layoutX);
        int newAbsoluteTop = Math.round(absoluteY + layoutY);
        int newAbsoluteRight = Math.round(absoluteX + layoutX + getLayoutWidth());
        int newAbsoluteBottom = Math.round(absoluteY + layoutY + getLayoutHeight());
        int newScreenX = Math.round(layoutX);
        int newScreenY = Math.round(layoutY);
        int newScreenWidth = newAbsoluteRight - newAbsoluteLeft;
        int newScreenHeight = newAbsoluteBottom - newAbsoluteTop;
        boolean layoutHasChanged = (newScreenX == this.mScreenX && newScreenY == this.mScreenY && newScreenWidth == this.mScreenWidth && newScreenHeight == this.mScreenHeight) ? false : true;
        this.mScreenX = newScreenX;
        this.mScreenY = newScreenY;
        this.mScreenWidth = newScreenWidth;
        this.mScreenHeight = newScreenHeight;
        if (!layoutHasChanged) {
            return layoutHasChanged;
        }
        nativeViewHierarchyOptimizer.handleUpdateLayout(this);
        return layoutHasChanged;
    }

    public final int getReactTag() {
        return this.mReactTag;
    }

    public void setReactTag(int reactTag) {
        this.mReactTag = reactTag;
    }

    public final ReactShadowNodeImpl getRootNode() {
        return (ReactShadowNodeImpl) Assertions.assertNotNull(this.mRootNode);
    }

    public final void setRootNode(ReactShadowNodeImpl rootNode) {
        this.mRootNode = rootNode;
    }

    public final void setViewClassName(String viewClassName) {
        this.mViewClassName = viewClassName;
    }

    @Nullable
    public final ReactShadowNodeImpl getParent() {
        return this.mParent;
    }

    public final ThemedReactContext getThemedContext() {
        return (ThemedReactContext) Assertions.assertNotNull(this.mThemedContext);
    }

    public void setThemedContext(ThemedReactContext themedContext) {
        this.mThemedContext = themedContext;
    }

    public final boolean shouldNotifyOnLayout() {
        return this.mShouldNotifyOnLayout;
    }

    public void calculateLayout() {
        this.mYogaNode.calculateLayout(Float.NaN, Float.NaN);
    }

    public final boolean hasNewLayout() {
        return this.mYogaNode != null && this.mYogaNode.hasNewLayout();
    }

    public final void markLayoutSeen() {
        if (this.mYogaNode != null) {
            this.mYogaNode.markLayoutSeen();
        }
    }

    public final void addNativeChildAt(ReactShadowNodeImpl child, int nativeIndex) {
        boolean z = true;
        Assertions.assertCondition(!this.mIsLayoutOnly);
        if (child.mIsLayoutOnly) {
            z = false;
        }
        Assertions.assertCondition(z);
        if (this.mNativeChildren == null) {
            this.mNativeChildren = new ArrayList<>(4);
        }
        this.mNativeChildren.add(nativeIndex, child);
        child.mNativeParent = this;
    }

    public final ReactShadowNodeImpl removeNativeChildAt(int i) {
        Assertions.assertNotNull(this.mNativeChildren);
        ReactShadowNodeImpl removed = (ReactShadowNodeImpl) this.mNativeChildren.remove(i);
        removed.mNativeParent = null;
        return removed;
    }

    public final void removeAllNativeChildren() {
        if (this.mNativeChildren != null) {
            for (int i = this.mNativeChildren.size() - 1; i >= 0; i--) {
                ((ReactShadowNodeImpl) this.mNativeChildren.get(i)).mNativeParent = null;
            }
            this.mNativeChildren.clear();
        }
    }

    public final int getNativeChildCount() {
        if (this.mNativeChildren == null) {
            return 0;
        }
        return this.mNativeChildren.size();
    }

    public final int indexOfNativeChild(ReactShadowNodeImpl nativeChild) {
        Assertions.assertNotNull(this.mNativeChildren);
        return this.mNativeChildren.indexOf(nativeChild);
    }

    @Nullable
    public final ReactShadowNodeImpl getNativeParent() {
        return this.mNativeParent;
    }

    public final void setIsLayoutOnly(boolean isLayoutOnly) {
        boolean z;
        boolean z2;
        boolean z3 = true;
        if (getParent() == null) {
            z = true;
        } else {
            z = false;
        }
        Assertions.assertCondition(z, "Must remove from no opt parent first");
        if (this.mNativeParent == null) {
            z2 = true;
        } else {
            z2 = false;
        }
        Assertions.assertCondition(z2, "Must remove from native parent first");
        if (getNativeChildCount() != 0) {
            z3 = false;
        }
        Assertions.assertCondition(z3, "Must remove all native children first");
        this.mIsLayoutOnly = isLayoutOnly;
    }

    public final boolean isLayoutOnly() {
        return this.mIsLayoutOnly;
    }

    public final int getTotalNativeChildren() {
        return this.mTotalNativeChildren;
    }

    public boolean isDescendantOf(ReactShadowNodeImpl ancestorNode) {
        for (ReactShadowNodeImpl parentNode = getParent(); parentNode != null; parentNode = parentNode.getParent()) {
            if (parentNode == ancestorNode) {
                return true;
            }
        }
        return false;
    }

    public void setLocalData(Object data) {
    }

    public final int getNativeOffsetForChild(ReactShadowNodeImpl child) {
        int i;
        int index = 0;
        boolean found = false;
        int i2 = 0;
        while (true) {
            if (i2 >= getChildCount()) {
                break;
            }
            ReactShadowNodeImpl current = getChildAt(i2);
            if (child == current) {
                found = true;
                break;
            }
            if (current.isLayoutOnly()) {
                i = current.getTotalNativeChildren();
            } else {
                i = 1;
            }
            index += i;
            i2++;
        }
        if (found) {
            return index;
        }
        throw new RuntimeException("Child " + child.getReactTag() + " was not a child of " + this.mReactTag);
    }

    public final float getLayoutX() {
        return this.mYogaNode.getLayoutX();
    }

    public final float getLayoutY() {
        return this.mYogaNode.getLayoutY();
    }

    public final float getLayoutWidth() {
        return this.mYogaNode.getLayoutWidth();
    }

    public final float getLayoutHeight() {
        return this.mYogaNode.getLayoutHeight();
    }

    public int getScreenX() {
        return this.mScreenX;
    }

    public int getScreenY() {
        return this.mScreenY;
    }

    public int getScreenWidth() {
        return this.mScreenWidth;
    }

    public int getScreenHeight() {
        return this.mScreenHeight;
    }

    public final YogaDirection getLayoutDirection() {
        return this.mYogaNode.getLayoutDirection();
    }

    public void setLayoutDirection(YogaDirection direction) {
        this.mYogaNode.setDirection(direction);
    }

    public final YogaValue getStyleWidth() {
        return this.mYogaNode.getWidth();
    }

    public void setStyleWidth(float widthPx) {
        this.mYogaNode.setWidth(widthPx);
    }

    public void setStyleWidthPercent(float percent) {
        this.mYogaNode.setWidthPercent(percent);
    }

    public void setStyleWidthAuto() {
        this.mYogaNode.setWidthAuto();
    }

    public void setStyleMinWidth(float widthPx) {
        this.mYogaNode.setMinWidth(widthPx);
    }

    public void setStyleMinWidthPercent(float percent) {
        this.mYogaNode.setMinWidthPercent(percent);
    }

    public void setStyleMaxWidth(float widthPx) {
        this.mYogaNode.setMaxWidth(widthPx);
    }

    public void setStyleMaxWidthPercent(float percent) {
        this.mYogaNode.setMaxWidthPercent(percent);
    }

    public final YogaValue getStyleHeight() {
        return this.mYogaNode.getHeight();
    }

    public void setStyleHeight(float heightPx) {
        this.mYogaNode.setHeight(heightPx);
    }

    public void setStyleHeightPercent(float percent) {
        this.mYogaNode.setHeightPercent(percent);
    }

    public void setStyleHeightAuto() {
        this.mYogaNode.setHeightAuto();
    }

    public void setStyleMinHeight(float widthPx) {
        this.mYogaNode.setMinHeight(widthPx);
    }

    public void setStyleMinHeightPercent(float percent) {
        this.mYogaNode.setMinHeightPercent(percent);
    }

    public void setStyleMaxHeight(float widthPx) {
        this.mYogaNode.setMaxHeight(widthPx);
    }

    public void setStyleMaxHeightPercent(float percent) {
        this.mYogaNode.setMaxHeightPercent(percent);
    }

    public void setFlex(float flex) {
        this.mYogaNode.setFlex(flex);
    }

    public void setFlexGrow(float flexGrow) {
        this.mYogaNode.setFlexGrow(flexGrow);
    }

    public void setFlexShrink(float flexShrink) {
        this.mYogaNode.setFlexShrink(flexShrink);
    }

    public void setFlexBasis(float flexBasis) {
        this.mYogaNode.setFlexBasis(flexBasis);
    }

    public void setFlexBasisAuto() {
        this.mYogaNode.setFlexBasisAuto();
    }

    public void setFlexBasisPercent(float percent) {
        this.mYogaNode.setFlexBasisPercent(percent);
    }

    public void setStyleAspectRatio(float aspectRatio) {
        this.mYogaNode.setAspectRatio(aspectRatio);
    }

    public void setFlexDirection(YogaFlexDirection flexDirection) {
        this.mYogaNode.setFlexDirection(flexDirection);
    }

    public void setFlexWrap(YogaWrap wrap) {
        this.mYogaNode.setWrap(wrap);
    }

    public void setAlignSelf(YogaAlign alignSelf) {
        this.mYogaNode.setAlignSelf(alignSelf);
    }

    public void setAlignItems(YogaAlign alignItems) {
        this.mYogaNode.setAlignItems(alignItems);
    }

    public void setAlignContent(YogaAlign alignContent) {
        this.mYogaNode.setAlignContent(alignContent);
    }

    public void setJustifyContent(YogaJustify justifyContent) {
        this.mYogaNode.setJustifyContent(justifyContent);
    }

    public void setOverflow(YogaOverflow overflow) {
        this.mYogaNode.setOverflow(overflow);
    }

    public void setDisplay(YogaDisplay display) {
        this.mYogaNode.setDisplay(display);
    }

    public void setMargin(int spacingType, float margin) {
        this.mYogaNode.setMargin(YogaEdge.fromInt(spacingType), margin);
    }

    public void setMarginPercent(int spacingType, float percent) {
        this.mYogaNode.setMarginPercent(YogaEdge.fromInt(spacingType), percent);
    }

    public void setMarginAuto(int spacingType) {
        this.mYogaNode.setMarginAuto(YogaEdge.fromInt(spacingType));
    }

    public final float getPadding(int spacingType) {
        return this.mYogaNode.getLayoutPadding(YogaEdge.fromInt(spacingType));
    }

    public final YogaValue getStylePadding(int spacingType) {
        return this.mYogaNode.getPadding(YogaEdge.fromInt(spacingType));
    }

    public void setDefaultPadding(int spacingType, float padding) {
        this.mDefaultPadding.set(spacingType, padding);
        updatePadding();
    }

    public void setPadding(int spacingType, float padding) {
        this.mPadding[spacingType] = padding;
        this.mPaddingIsPercent[spacingType] = false;
        updatePadding();
    }

    public void setPaddingPercent(int spacingType, float percent) {
        this.mPadding[spacingType] = percent;
        this.mPaddingIsPercent[spacingType] = !YogaConstants.isUndefined(percent);
        updatePadding();
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void updatePadding() {
        /*
            r5 = this;
            r4 = 8
            r0 = 0
        L_0x0003:
            if (r0 > r4) goto L_0x00b2
            if (r0 == 0) goto L_0x0010
            r1 = 2
            if (r0 == r1) goto L_0x0010
            r1 = 4
            if (r0 == r1) goto L_0x0010
            r1 = 5
            if (r0 != r1) goto L_0x0041
        L_0x0010:
            float[] r1 = r5.mPadding
            r1 = r1[r0]
            boolean r1 = com.facebook.yoga.YogaConstants.isUndefined(r1)
            if (r1 == 0) goto L_0x0090
            float[] r1 = r5.mPadding
            r2 = 6
            r1 = r1[r2]
            boolean r1 = com.facebook.yoga.YogaConstants.isUndefined(r1)
            if (r1 == 0) goto L_0x0090
            float[] r1 = r5.mPadding
            r1 = r1[r4]
            boolean r1 = com.facebook.yoga.YogaConstants.isUndefined(r1)
            if (r1 == 0) goto L_0x0090
            com.facebook.yoga.YogaNode r1 = r5.mYogaNode
            com.facebook.yoga.YogaEdge r2 = com.facebook.yoga.YogaEdge.fromInt(r0)
            com.facebook.react.uimanager.Spacing r3 = r5.mDefaultPadding
            float r3 = r3.getRaw(r0)
            r1.setPadding(r2, r3)
        L_0x003e:
            int r0 = r0 + 1
            goto L_0x0003
        L_0x0041:
            r1 = 1
            if (r0 == r1) goto L_0x0047
            r1 = 3
            if (r0 != r1) goto L_0x0076
        L_0x0047:
            float[] r1 = r5.mPadding
            r1 = r1[r0]
            boolean r1 = com.facebook.yoga.YogaConstants.isUndefined(r1)
            if (r1 == 0) goto L_0x0090
            float[] r1 = r5.mPadding
            r2 = 7
            r1 = r1[r2]
            boolean r1 = com.facebook.yoga.YogaConstants.isUndefined(r1)
            if (r1 == 0) goto L_0x0090
            float[] r1 = r5.mPadding
            r1 = r1[r4]
            boolean r1 = com.facebook.yoga.YogaConstants.isUndefined(r1)
            if (r1 == 0) goto L_0x0090
            com.facebook.yoga.YogaNode r1 = r5.mYogaNode
            com.facebook.yoga.YogaEdge r2 = com.facebook.yoga.YogaEdge.fromInt(r0)
            com.facebook.react.uimanager.Spacing r3 = r5.mDefaultPadding
            float r3 = r3.getRaw(r0)
            r1.setPadding(r2, r3)
            goto L_0x003e
        L_0x0076:
            float[] r1 = r5.mPadding
            r1 = r1[r0]
            boolean r1 = com.facebook.yoga.YogaConstants.isUndefined(r1)
            if (r1 == 0) goto L_0x0090
            com.facebook.yoga.YogaNode r1 = r5.mYogaNode
            com.facebook.yoga.YogaEdge r2 = com.facebook.yoga.YogaEdge.fromInt(r0)
            com.facebook.react.uimanager.Spacing r3 = r5.mDefaultPadding
            float r3 = r3.getRaw(r0)
            r1.setPadding(r2, r3)
            goto L_0x003e
        L_0x0090:
            boolean[] r1 = r5.mPaddingIsPercent
            boolean r1 = r1[r0]
            if (r1 == 0) goto L_0x00a4
            com.facebook.yoga.YogaNode r1 = r5.mYogaNode
            com.facebook.yoga.YogaEdge r2 = com.facebook.yoga.YogaEdge.fromInt(r0)
            float[] r3 = r5.mPadding
            r3 = r3[r0]
            r1.setPaddingPercent(r2, r3)
            goto L_0x003e
        L_0x00a4:
            com.facebook.yoga.YogaNode r1 = r5.mYogaNode
            com.facebook.yoga.YogaEdge r2 = com.facebook.yoga.YogaEdge.fromInt(r0)
            float[] r3 = r5.mPadding
            r3 = r3[r0]
            r1.setPadding(r2, r3)
            goto L_0x003e
        L_0x00b2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.uimanager.ReactShadowNodeImpl.updatePadding():void");
    }

    public void setBorder(int spacingType, float borderWidth) {
        this.mYogaNode.setBorder(YogaEdge.fromInt(spacingType), borderWidth);
    }

    public void setPosition(int spacingType, float position) {
        this.mYogaNode.setPosition(YogaEdge.fromInt(spacingType), position);
    }

    public void setPositionPercent(int spacingType, float percent) {
        this.mYogaNode.setPositionPercent(YogaEdge.fromInt(spacingType), percent);
    }

    public void setPositionType(YogaPositionType positionType) {
        this.mYogaNode.setPositionType(positionType);
    }

    public void setShouldNotifyOnLayout(boolean shouldNotifyOnLayout) {
        this.mShouldNotifyOnLayout = shouldNotifyOnLayout;
    }

    public void setBaselineFunction(YogaBaselineFunction baselineFunction) {
        this.mYogaNode.setBaselineFunction(baselineFunction);
    }

    public void setMeasureFunction(YogaMeasureFunction measureFunction) {
        if (!((measureFunction == null) ^ this.mYogaNode.isMeasureDefined()) || getChildCount() == 0) {
            this.mYogaNode.setMeasureFunction(measureFunction);
            return;
        }
        throw new RuntimeException("Since a node with a measure function does not add any native yoga children, it's not safe to transition to/from having a measure function unless a node has no children");
    }

    public boolean isMeasureDefined() {
        return this.mYogaNode.isMeasureDefined();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        toStringWithIndentation(sb, 0);
        return sb.toString();
    }

    private void toStringWithIndentation(StringBuilder result, int level) {
        for (int i = 0; i < level; i++) {
            result.append("__");
        }
        result.append(getClass().getSimpleName()).append(" ");
        if (this.mYogaNode != null) {
            result.append(getLayoutWidth()).append(",").append(getLayoutHeight());
        } else {
            result.append("(virtual node)");
        }
        result.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        if (getChildCount() != 0) {
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                getChildAt(i2).toStringWithIndentation(result, level + 1);
            }
        }
    }

    public void dispose() {
        if (this.mYogaNode != null) {
            this.mYogaNode.reset();
            YogaNodePool.get().release(this.mYogaNode);
        }
    }
}
