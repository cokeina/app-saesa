package com.facebook.react.uimanager;

import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.annotation.Nullable;

public class ViewGroupDrawingOrderHelper {
    @Nullable
    private int[] mDrawingOrderIndices;
    private int mNumberOfChildrenWithZIndex = 0;
    private final ViewGroup mViewGroup;

    public ViewGroupDrawingOrderHelper(ViewGroup viewGroup) {
        this.mViewGroup = viewGroup;
    }

    public void handleAddView(View view) {
        if (ViewGroupManager.getViewZIndex(view) != null) {
            this.mNumberOfChildrenWithZIndex++;
        }
        this.mDrawingOrderIndices = null;
    }

    public void handleRemoveView(View view) {
        if (ViewGroupManager.getViewZIndex(view) != null) {
            this.mNumberOfChildrenWithZIndex--;
        }
        this.mDrawingOrderIndices = null;
    }

    public boolean shouldEnableCustomDrawingOrder() {
        return this.mNumberOfChildrenWithZIndex > 0;
    }

    public int getChildDrawingOrder(int childCount, int index) {
        if (this.mDrawingOrderIndices == null) {
            ArrayList<View> viewsToSort = new ArrayList<>();
            for (int i = 0; i < childCount; i++) {
                viewsToSort.add(this.mViewGroup.getChildAt(i));
            }
            Collections.sort(viewsToSort, new Comparator<View>() {
                public int compare(View view1, View view2) {
                    Integer view1ZIndex = ViewGroupManager.getViewZIndex(view1);
                    if (view1ZIndex == null) {
                        view1ZIndex = Integer.valueOf(0);
                    }
                    Integer view2ZIndex = ViewGroupManager.getViewZIndex(view2);
                    if (view2ZIndex == null) {
                        view2ZIndex = Integer.valueOf(0);
                    }
                    return view1ZIndex.intValue() - view2ZIndex.intValue();
                }
            });
            this.mDrawingOrderIndices = new int[childCount];
            for (int i2 = 0; i2 < childCount; i2++) {
                this.mDrawingOrderIndices[i2] = this.mViewGroup.indexOfChild((View) viewsToSort.get(i2));
            }
        }
        return this.mDrawingOrderIndices[index];
    }

    public void update() {
        this.mNumberOfChildrenWithZIndex = 0;
        for (int i = 0; i < this.mViewGroup.getChildCount(); i++) {
            if (ViewGroupManager.getViewZIndex(this.mViewGroup.getChildAt(i)) != null) {
                this.mNumberOfChildrenWithZIndex++;
            }
        }
        this.mDrawingOrderIndices = null;
    }
}
