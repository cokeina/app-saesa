package com.facebook.react.flat;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.view.ViewCompat;
import javax.annotation.Nullable;

final class DrawBorder extends AbstractDrawBorder {
    private static final int ALL_BITS_SET = -1;
    private static final int ALL_BITS_UNSET = 0;
    private static final int BORDER_BOTTOM_COLOR_SET = 16;
    private static final int BORDER_LEFT_COLOR_SET = 2;
    private static final int BORDER_PATH_EFFECT_DIRTY = 32;
    private static final int BORDER_RIGHT_COLOR_SET = 8;
    private static final int BORDER_STYLE_DASHED = 2;
    private static final int BORDER_STYLE_DOTTED = 1;
    private static final int BORDER_STYLE_SOLID = 0;
    private static final int BORDER_TOP_COLOR_SET = 4;
    private static final Paint PAINT = new Paint(1);
    private static final float[] TMP_FLOAT_ARRAY = new float[4];
    private int mBackgroundColor;
    private int mBorderBottomColor;
    private float mBorderBottomWidth;
    private int mBorderLeftColor;
    private float mBorderLeftWidth;
    private int mBorderRightColor;
    private float mBorderRightWidth;
    private int mBorderStyle = 0;
    private int mBorderTopColor;
    private float mBorderTopWidth;
    @Nullable
    private DashPathEffect mPathEffectForBorderStyle;
    @Nullable
    private Path mPathForBorder;

    DrawBorder() {
    }

    public void setBorderWidth(int position, float borderWidth) {
        switch (position) {
            case 0:
                this.mBorderLeftWidth = borderWidth;
                return;
            case 1:
                this.mBorderTopWidth = borderWidth;
                return;
            case 2:
                this.mBorderRightWidth = borderWidth;
                return;
            case 3:
                this.mBorderBottomWidth = borderWidth;
                return;
            case 8:
                setBorderWidth(borderWidth);
                return;
            default:
                return;
        }
    }

    public float getBorderWidth(int position) {
        switch (position) {
            case 0:
                return this.mBorderLeftWidth;
            case 1:
                return this.mBorderTopWidth;
            case 2:
                return this.mBorderRightWidth;
            case 3:
                return this.mBorderBottomWidth;
            case 8:
                return getBorderWidth();
            default:
                return 0.0f;
        }
    }

    public void setBorderStyle(@Nullable String style) {
        if ("dotted".equals(style)) {
            this.mBorderStyle = 1;
        } else if ("dashed".equals(style)) {
            this.mBorderStyle = 2;
        } else {
            this.mBorderStyle = 0;
        }
        setFlag(32);
    }

    public void resetBorderColor(int position) {
        switch (position) {
            case 0:
                resetFlag(2);
                return;
            case 1:
                resetFlag(4);
                return;
            case 2:
                resetFlag(8);
                return;
            case 3:
                resetFlag(16);
                return;
            case 8:
                setBorderColor(ViewCompat.MEASURED_STATE_MASK);
                return;
            default:
                return;
        }
    }

    public void setBorderColor(int position, int borderColor) {
        switch (position) {
            case 0:
                this.mBorderLeftColor = borderColor;
                setFlag(2);
                return;
            case 1:
                this.mBorderTopColor = borderColor;
                setFlag(4);
                return;
            case 2:
                this.mBorderRightColor = borderColor;
                setFlag(8);
                return;
            case 3:
                this.mBorderBottomColor = borderColor;
                setFlag(16);
                return;
            case 8:
                setBorderColor(borderColor);
                return;
            default:
                return;
        }
    }

    public int getBorderColor(int position) {
        int defaultColor = getBorderColor();
        switch (position) {
            case 0:
                return resolveBorderColor(2, this.mBorderLeftColor, defaultColor);
            case 1:
                return resolveBorderColor(4, this.mBorderTopColor, defaultColor);
            case 2:
                return resolveBorderColor(8, this.mBorderRightColor, defaultColor);
            case 3:
                return resolveBorderColor(16, this.mBorderBottomColor, defaultColor);
            default:
                return defaultColor;
        }
    }

    public void setBackgroundColor(int backgroundColor) {
        this.mBackgroundColor = backgroundColor;
    }

    public int getBackgroundColor() {
        return this.mBackgroundColor;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (getBorderRadius() >= 0.5f || getPathEffectForBorderStyle() != null) {
            drawRoundedBorders(canvas);
        } else {
            drawRectangularBorders(canvas);
        }
    }

    /* access modifiers changed from: protected */
    @Nullable
    public DashPathEffect getPathEffectForBorderStyle() {
        if (isFlagSet(32)) {
            switch (this.mBorderStyle) {
                case 1:
                    this.mPathEffectForBorderStyle = createDashPathEffect(getBorderWidth());
                    break;
                case 2:
                    this.mPathEffectForBorderStyle = createDashPathEffect(getBorderWidth() * 3.0f);
                    break;
                default:
                    this.mPathEffectForBorderStyle = null;
                    break;
            }
            resetFlag(32);
        }
        return this.mPathEffectForBorderStyle;
    }

    private void drawRoundedBorders(Canvas canvas) {
        if (this.mBackgroundColor != 0) {
            PAINT.setColor(this.mBackgroundColor);
            canvas.drawPath(getPathForBorderRadius(), PAINT);
        }
        drawBorders(canvas);
    }

    private static int fastBorderCompatibleColorOrZero(float borderLeft, float borderTop, float borderRight, float borderBottom, int colorLeft, int colorTop, int colorRight, int colorBottom) {
        int i;
        int i2 = -1;
        if (borderLeft > 0.0f) {
            i = colorLeft;
        } else {
            i = -1;
        }
        int i3 = (borderRight > 0.0f ? colorRight : -1) & i & (borderTop > 0.0f ? colorTop : -1);
        if (borderBottom > 0.0f) {
            i2 = colorBottom;
        }
        int andSmear = i3 & i2;
        if (borderLeft <= 0.0f) {
            colorLeft = 0;
        }
        if (borderTop <= 0.0f) {
            colorTop = 0;
        }
        int i4 = colorLeft | colorTop;
        if (borderRight <= 0.0f) {
            colorRight = 0;
        }
        int i5 = i4 | colorRight;
        if (borderBottom <= 0.0f) {
            colorBottom = 0;
        }
        if (andSmear == (i5 | colorBottom)) {
            return andSmear;
        }
        return 0;
    }

    private void drawRectangularBorders(Canvas canvas) {
        int defaultColor = getBorderColor();
        float defaultWidth = getBorderWidth();
        float top = getTop();
        float borderTop = resolveWidth(this.mBorderTopWidth, defaultWidth);
        float topInset = top + borderTop;
        int topColor = resolveBorderColor(4, this.mBorderTopColor, defaultColor);
        float bottom = getBottom();
        float borderBottom = resolveWidth(this.mBorderBottomWidth, defaultWidth);
        float bottomInset = bottom - borderBottom;
        int bottomColor = resolveBorderColor(16, this.mBorderBottomColor, defaultColor);
        float left = getLeft();
        float borderLeft = resolveWidth(this.mBorderLeftWidth, defaultWidth);
        float leftInset = left + borderLeft;
        int leftColor = resolveBorderColor(2, this.mBorderLeftColor, defaultColor);
        float right = getRight();
        float borderRight = resolveWidth(this.mBorderRightWidth, defaultWidth);
        float rightInset = right - borderRight;
        int rightColor = resolveBorderColor(8, this.mBorderRightColor, defaultColor);
        int fastBorderColor = fastBorderCompatibleColorOrZero(borderLeft, borderTop, borderRight, borderBottom, leftColor, topColor, rightColor, bottomColor);
        if (fastBorderColor == 0) {
            if (this.mPathForBorder == null) {
                this.mPathForBorder = new Path();
            }
            if (Color.alpha(this.mBackgroundColor) != 0) {
                PAINT.setColor(this.mBackgroundColor);
                canvas.drawRect(left, top, right, bottom, PAINT);
            }
            if (!(borderTop == 0.0f || Color.alpha(topColor) == 0)) {
                PAINT.setColor(topColor);
                updatePathForTopBorder(this.mPathForBorder, top, topInset, left, leftInset, right, rightInset);
                canvas.drawPath(this.mPathForBorder, PAINT);
            }
            if (!(borderBottom == 0.0f || Color.alpha(bottomColor) == 0)) {
                PAINT.setColor(bottomColor);
                updatePathForBottomBorder(this.mPathForBorder, bottom, bottomInset, left, leftInset, right, rightInset);
                canvas.drawPath(this.mPathForBorder, PAINT);
            }
            if (!(borderLeft == 0.0f || Color.alpha(leftColor) == 0)) {
                PAINT.setColor(leftColor);
                updatePathForLeftBorder(this.mPathForBorder, top, topInset, bottom, bottomInset, left, leftInset);
                canvas.drawPath(this.mPathForBorder, PAINT);
            }
            if (borderRight != 0.0f && Color.alpha(rightColor) != 0) {
                PAINT.setColor(rightColor);
                updatePathForRightBorder(this.mPathForBorder, top, topInset, bottom, bottomInset, right, rightInset);
                canvas.drawPath(this.mPathForBorder, PAINT);
            }
        } else if (Color.alpha(fastBorderColor) != 0) {
            if (Color.alpha(this.mBackgroundColor) != 0) {
                PAINT.setColor(this.mBackgroundColor);
                if (Color.alpha(fastBorderColor) == 255) {
                    canvas.drawRect(leftInset, topInset, rightInset, bottomInset, PAINT);
                } else {
                    canvas.drawRect(left, top, right, bottom, PAINT);
                }
            }
            PAINT.setColor(fastBorderColor);
            if (borderLeft > 0.0f) {
                canvas.drawRect(left, top, leftInset, bottom - borderBottom, PAINT);
            }
            if (borderTop > 0.0f) {
                canvas.drawRect(left + borderLeft, top, right, topInset, PAINT);
            }
            if (borderRight > 0.0f) {
                canvas.drawRect(rightInset, top + borderTop, right, bottom, PAINT);
            }
            if (borderBottom > 0.0f) {
                canvas.drawRect(left, bottomInset, right - borderRight, bottom, PAINT);
            }
        }
    }

    private static void updatePathForTopBorder(Path path, float top, float topInset, float left, float leftInset, float right, float rightInset) {
        path.reset();
        path.moveTo(left, top);
        path.lineTo(leftInset, topInset);
        path.lineTo(rightInset, topInset);
        path.lineTo(right, top);
        path.lineTo(left, top);
    }

    private static void updatePathForBottomBorder(Path path, float bottom, float bottomInset, float left, float leftInset, float right, float rightInset) {
        path.reset();
        path.moveTo(left, bottom);
        path.lineTo(right, bottom);
        path.lineTo(rightInset, bottomInset);
        path.lineTo(leftInset, bottomInset);
        path.lineTo(left, bottom);
    }

    private static void updatePathForLeftBorder(Path path, float top, float topInset, float bottom, float bottomInset, float left, float leftInset) {
        path.reset();
        path.moveTo(left, top);
        path.lineTo(leftInset, topInset);
        path.lineTo(leftInset, bottomInset);
        path.lineTo(left, bottom);
        path.lineTo(left, top);
    }

    private static void updatePathForRightBorder(Path path, float top, float topInset, float bottom, float bottomInset, float right, float rightInset) {
        path.reset();
        path.moveTo(right, top);
        path.lineTo(right, bottom);
        path.lineTo(rightInset, bottomInset);
        path.lineTo(rightInset, topInset);
        path.lineTo(right, top);
    }

    private int resolveBorderColor(int flag, int color, int defaultColor) {
        return isFlagSet(flag) ? color : defaultColor;
    }

    private static float resolveWidth(float width, float defaultWidth) {
        return (width == 0.0f || width != width) ? defaultWidth : width;
    }

    private static DashPathEffect createDashPathEffect(float borderWidth) {
        for (int i = 0; i < 4; i++) {
            TMP_FLOAT_ARRAY[i] = borderWidth;
        }
        return new DashPathEffect(TMP_FLOAT_ARRAY, 0.0f);
    }
}
