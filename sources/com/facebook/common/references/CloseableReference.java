package com.facebook.common.references;

import com.facebook.common.internal.Closeables;
import com.facebook.common.internal.Preconditions;
import com.facebook.common.internal.VisibleForTesting;
import com.facebook.common.logging.FLog;
import java.io.Closeable;
import java.io.IOException;
import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;

public abstract class CloseableReference<T> implements Cloneable, Closeable {
    private static final ResourceReleaser<Closeable> DEFAULT_CLOSEABLE_RELEASER = new ResourceReleaser<Closeable>() {
        public void release(Closeable value) {
            try {
                Closeables.close(value, true);
            } catch (IOException e) {
            }
        }
    };
    /* access modifiers changed from: private */
    public static Class<CloseableReference> TAG = CloseableReference.class;
    private static volatile boolean sUseFinalizers = true;

    private static class CloseableReferenceWithFinalizer<T> extends CloseableReference<T> {
        @GuardedBy("this")
        private boolean mIsClosed;
        private final SharedReference<T> mSharedReference;

        private CloseableReferenceWithFinalizer(SharedReference<T> sharedReference) {
            this.mIsClosed = false;
            this.mSharedReference = (SharedReference) Preconditions.checkNotNull(sharedReference);
            sharedReference.addReference();
        }

        private CloseableReferenceWithFinalizer(T t, ResourceReleaser<T> resourceReleaser) {
            this.mIsClosed = false;
            this.mSharedReference = new SharedReference<>(t, resourceReleaser);
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            try {
                synchronized (this) {
                    if (!this.mIsClosed) {
                        FLog.w(CloseableReference.TAG, "Finalized without closing: %x %x (type = %s)", Integer.valueOf(System.identityHashCode(this)), Integer.valueOf(System.identityHashCode(this.mSharedReference)), this.mSharedReference.get().getClass().getSimpleName());
                        close();
                        super.finalize();
                    }
                }
            } finally {
                super.finalize();
            }
        }

        public synchronized T get() {
            Preconditions.checkState(!this.mIsClosed);
            return this.mSharedReference.get();
        }

        public synchronized CloseableReference<T> clone() {
            Preconditions.checkState(isValid());
            return new CloseableReferenceWithFinalizer(this.mSharedReference);
        }

        public synchronized CloseableReference<T> cloneOrNull() {
            CloseableReference<T> closeableReference;
            if (isValid()) {
                closeableReference = clone();
            } else {
                closeableReference = null;
            }
            return closeableReference;
        }

        public synchronized boolean isValid() {
            return !this.mIsClosed;
        }

        public synchronized SharedReference<T> getUnderlyingReferenceTestOnly() {
            return this.mSharedReference;
        }

        public int getValueHash() {
            if (isValid()) {
                return System.identityHashCode(this.mSharedReference.get());
            }
            return 0;
        }

        public void close() {
            synchronized (this) {
                if (!this.mIsClosed) {
                    this.mIsClosed = true;
                    this.mSharedReference.deleteReference();
                }
            }
        }
    }

    private static class CloseableReferenceWithoutFinalizer<T> extends CloseableReference<T> {
        /* access modifiers changed from: private */
        public static final ReferenceQueue<CloseableReference> REF_QUEUE = new ReferenceQueue<>();
        private final Destructor mDestructor;
        /* access modifiers changed from: private */
        public final SharedReference<T> mSharedReference;

        private static class Destructor extends PhantomReference<CloseableReference> {
            @GuardedBy("Destructor.class")
            private static Destructor sHead;
            @GuardedBy("this")
            private boolean destroyed;
            private final SharedReference mSharedReference;
            @GuardedBy("Destructor.class")
            private Destructor next;
            @GuardedBy("Destructor.class")
            private Destructor previous;

            public Destructor(CloseableReferenceWithoutFinalizer referent, ReferenceQueue<? super CloseableReference> referenceQueue) {
                super(referent, referenceQueue);
                this.mSharedReference = referent.mSharedReference;
                synchronized (Destructor.class) {
                    if (sHead != null) {
                        sHead.next = this;
                        this.previous = sHead;
                    }
                    sHead = this;
                }
            }

            public synchronized boolean isDestroyed() {
                return this.destroyed;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:12:0x0011, code lost:
                if (r6.previous == null) goto L_0x0019;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:13:0x0013, code lost:
                r6.previous.next = r6.next;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:15:0x001b, code lost:
                if (r6.next == null) goto L_0x0063;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:16:0x001d, code lost:
                r6.next.previous = r6.previous;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:17:0x0023, code lost:
                monitor-exit(r1);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:18:0x0024, code lost:
                if (r7 != false) goto L_0x005a;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:19:0x0026, code lost:
                com.facebook.common.logging.FLog.w(com.facebook.common.references.CloseableReference.access$300(), "GCed without closing: %x %x (type = %s)", java.lang.Integer.valueOf(java.lang.System.identityHashCode(r6)), java.lang.Integer.valueOf(java.lang.System.identityHashCode(r6.mSharedReference)), r6.mSharedReference.get().getClass().getSimpleName());
             */
            /* JADX WARNING: Code restructure failed: missing block: B:20:0x005a, code lost:
                r6.mSharedReference.deleteReference();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
                sHead = r6.previous;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
                return;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:8:0x000c, code lost:
                r1 = com.facebook.common.references.CloseableReference.CloseableReferenceWithoutFinalizer.Destructor.class;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:9:0x000e, code lost:
                monitor-enter(r1);
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void destroy(boolean r7) {
                /*
                    r6 = this;
                    r5 = 1
                    monitor-enter(r6)
                    boolean r0 = r6.destroyed     // Catch:{ all -> 0x0060 }
                    if (r0 == 0) goto L_0x0008
                    monitor-exit(r6)     // Catch:{ all -> 0x0060 }
                L_0x0007:
                    return
                L_0x0008:
                    r0 = 1
                    r6.destroyed = r0     // Catch:{ all -> 0x0060 }
                    monitor-exit(r6)     // Catch:{ all -> 0x0060 }
                    java.lang.Class<com.facebook.common.references.CloseableReference$CloseableReferenceWithoutFinalizer$Destructor> r1 = com.facebook.common.references.CloseableReference.CloseableReferenceWithoutFinalizer.Destructor.class
                    monitor-enter(r1)
                    com.facebook.common.references.CloseableReference$CloseableReferenceWithoutFinalizer$Destructor r0 = r6.previous     // Catch:{ all -> 0x0068 }
                    if (r0 == 0) goto L_0x0019
                    com.facebook.common.references.CloseableReference$CloseableReferenceWithoutFinalizer$Destructor r0 = r6.previous     // Catch:{ all -> 0x0068 }
                    com.facebook.common.references.CloseableReference$CloseableReferenceWithoutFinalizer$Destructor r2 = r6.next     // Catch:{ all -> 0x0068 }
                    r0.next = r2     // Catch:{ all -> 0x0068 }
                L_0x0019:
                    com.facebook.common.references.CloseableReference$CloseableReferenceWithoutFinalizer$Destructor r0 = r6.next     // Catch:{ all -> 0x0068 }
                    if (r0 == 0) goto L_0x0063
                    com.facebook.common.references.CloseableReference$CloseableReferenceWithoutFinalizer$Destructor r0 = r6.next     // Catch:{ all -> 0x0068 }
                    com.facebook.common.references.CloseableReference$CloseableReferenceWithoutFinalizer$Destructor r2 = r6.previous     // Catch:{ all -> 0x0068 }
                    r0.previous = r2     // Catch:{ all -> 0x0068 }
                L_0x0023:
                    monitor-exit(r1)     // Catch:{ all -> 0x0068 }
                    if (r7 != 0) goto L_0x005a
                    java.lang.Class r0 = com.facebook.common.references.CloseableReference.TAG
                    java.lang.String r1 = "GCed without closing: %x %x (type = %s)"
                    r2 = 3
                    java.lang.Object[] r2 = new java.lang.Object[r2]
                    r3 = 0
                    int r4 = java.lang.System.identityHashCode(r6)
                    java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
                    r2[r3] = r4
                    com.facebook.common.references.SharedReference r3 = r6.mSharedReference
                    int r3 = java.lang.System.identityHashCode(r3)
                    java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
                    r2[r5] = r3
                    r3 = 2
                    com.facebook.common.references.SharedReference r4 = r6.mSharedReference
                    java.lang.Object r4 = r4.get()
                    java.lang.Class r4 = r4.getClass()
                    java.lang.String r4 = r4.getSimpleName()
                    r2[r3] = r4
                    com.facebook.common.logging.FLog.w(r0, r1, r2)
                L_0x005a:
                    com.facebook.common.references.SharedReference r0 = r6.mSharedReference
                    r0.deleteReference()
                    goto L_0x0007
                L_0x0060:
                    r0 = move-exception
                    monitor-exit(r6)     // Catch:{ all -> 0x0060 }
                    throw r0
                L_0x0063:
                    com.facebook.common.references.CloseableReference$CloseableReferenceWithoutFinalizer$Destructor r0 = r6.previous     // Catch:{ all -> 0x0068 }
                    sHead = r0     // Catch:{ all -> 0x0068 }
                    goto L_0x0023
                L_0x0068:
                    r0 = move-exception
                    monitor-exit(r1)     // Catch:{ all -> 0x0068 }
                    throw r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.references.CloseableReference.CloseableReferenceWithoutFinalizer.Destructor.destroy(boolean):void");
            }
        }

        static {
            new Thread(new Runnable() {
                public void run() {
                    while (true) {
                        try {
                            ((Destructor) CloseableReferenceWithoutFinalizer.REF_QUEUE.remove()).destroy(false);
                        } catch (InterruptedException e) {
                        }
                    }
                }
            }, "CloseableReferenceDestructorThread").start();
        }

        private CloseableReferenceWithoutFinalizer(SharedReference<T> sharedReference) {
            this.mSharedReference = (SharedReference) Preconditions.checkNotNull(sharedReference);
            sharedReference.addReference();
            this.mDestructor = new Destructor(this, REF_QUEUE);
        }

        private CloseableReferenceWithoutFinalizer(T t, ResourceReleaser<T> resourceReleaser) {
            this.mSharedReference = new SharedReference<>(t, resourceReleaser);
            this.mDestructor = new Destructor(this, REF_QUEUE);
        }

        public void close() {
            this.mDestructor.destroy(true);
        }

        public T get() {
            T t;
            synchronized (this.mDestructor) {
                Preconditions.checkState(!this.mDestructor.isDestroyed());
                t = this.mSharedReference.get();
            }
            return t;
        }

        public CloseableReference<T> clone() {
            CloseableReferenceWithoutFinalizer closeableReferenceWithoutFinalizer;
            synchronized (this.mDestructor) {
                Preconditions.checkState(!this.mDestructor.isDestroyed());
                closeableReferenceWithoutFinalizer = new CloseableReferenceWithoutFinalizer(this.mSharedReference);
            }
            return closeableReferenceWithoutFinalizer;
        }

        public CloseableReference<T> cloneOrNull() {
            CloseableReferenceWithoutFinalizer closeableReferenceWithoutFinalizer;
            synchronized (this.mDestructor) {
                if (!this.mDestructor.isDestroyed()) {
                    closeableReferenceWithoutFinalizer = new CloseableReferenceWithoutFinalizer(this.mSharedReference);
                } else {
                    closeableReferenceWithoutFinalizer = null;
                }
            }
            return closeableReferenceWithoutFinalizer;
        }

        public boolean isValid() {
            return !this.mDestructor.isDestroyed();
        }

        public SharedReference<T> getUnderlyingReferenceTestOnly() {
            return this.mSharedReference;
        }

        public int getValueHash() {
            int i;
            synchronized (this.mDestructor) {
                i = isValid() ? System.identityHashCode(this.mSharedReference.get()) : 0;
            }
            return i;
        }
    }

    public abstract CloseableReference<T> clone();

    public abstract CloseableReference<T> cloneOrNull();

    public abstract void close();

    public abstract T get();

    @VisibleForTesting
    public abstract SharedReference<T> getUnderlyingReferenceTestOnly();

    public abstract int getValueHash();

    public abstract boolean isValid();

    @Nullable
    public static <T extends Closeable> CloseableReference<T> of(@Nullable T t) {
        if (t == null) {
            return null;
        }
        return makeCloseableReference(t, DEFAULT_CLOSEABLE_RELEASER);
    }

    @Nullable
    public static <T> CloseableReference<T> of(@Nullable T t, ResourceReleaser<T> resourceReleaser) {
        if (t == null) {
            return null;
        }
        return makeCloseableReference(t, resourceReleaser);
    }

    private static <T> CloseableReference<T> makeCloseableReference(@Nullable T t, ResourceReleaser<T> resourceReleaser) {
        if (sUseFinalizers) {
            return new CloseableReferenceWithFinalizer(t, resourceReleaser);
        }
        return new CloseableReferenceWithoutFinalizer(t, resourceReleaser);
    }

    public static boolean isValid(@Nullable CloseableReference<?> ref) {
        return ref != null && ref.isValid();
    }

    @Nullable
    public static <T> CloseableReference<T> cloneOrNull(@Nullable CloseableReference<T> ref) {
        if (ref != null) {
            return ref.cloneOrNull();
        }
        return null;
    }

    public static <T> List<CloseableReference<T>> cloneOrNull(Collection<CloseableReference<T>> refs) {
        if (refs == null) {
            return null;
        }
        List<CloseableReference<T>> ret = new ArrayList<>(refs.size());
        for (CloseableReference<T> ref : refs) {
            ret.add(cloneOrNull(ref));
        }
        return ret;
    }

    public static void closeSafely(@Nullable CloseableReference<?> ref) {
        if (ref != null) {
            ref.close();
        }
    }

    public static void closeSafely(@Nullable Iterable<? extends CloseableReference<?>> references) {
        if (references != null) {
            for (CloseableReference<?> ref : references) {
                closeSafely(ref);
            }
        }
    }

    public static void setUseFinalizers(boolean useFinalizers) {
        sUseFinalizers = useFinalizers;
    }
}
