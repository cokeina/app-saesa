package com.facebook.debug.holder;

import com.facebook.debug.debugoverlay.model.DebugOverlayTag;

public class NoopPrinter implements Printer {
    public static final NoopPrinter INSTANCE = new NoopPrinter();

    private NoopPrinter() {
    }

    public void logMessage(DebugOverlayTag tag, String message, Object... args) {
    }

    public void logMessage(DebugOverlayTag tag, String message) {
    }

    public boolean shouldDisplayLogMessage(DebugOverlayTag tag) {
        return false;
    }
}
