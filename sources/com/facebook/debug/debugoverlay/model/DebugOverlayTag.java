package com.facebook.debug.debugoverlay.model;

import javax.annotation.concurrent.Immutable;

@Immutable
public class DebugOverlayTag {
    public final int color;
    public final String description;
    public final String name;

    public DebugOverlayTag(String name2, String description2, int color2) {
        this.name = name2;
        this.description = description2;
        this.color = color2;
    }
}
