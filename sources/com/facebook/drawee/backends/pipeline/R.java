package com.facebook.drawee.backends.pipeline;

public final class R {

    public static final class attr {
        public static final int actualImageResource = 2130772065;
        public static final int actualImageScaleType = 2130772027;
        public static final int actualImageUri = 2130772064;
        public static final int backgroundImage = 2130772028;
        public static final int fadeDuration = 2130772016;
        public static final int failureImage = 2130772022;
        public static final int failureImageScaleType = 2130772023;
        public static final int overlayImage = 2130772029;
        public static final int placeholderImage = 2130772018;
        public static final int placeholderImageScaleType = 2130772019;
        public static final int pressedStateOverlayImage = 2130772030;
        public static final int progressBarAutoRotateInterval = 2130772026;
        public static final int progressBarImage = 2130772024;
        public static final int progressBarImageScaleType = 2130772025;
        public static final int retryImage = 2130772020;
        public static final int retryImageScaleType = 2130772021;
        public static final int roundAsCircle = 2130772031;
        public static final int roundBottomLeft = 2130772036;
        public static final int roundBottomRight = 2130772035;
        public static final int roundTopLeft = 2130772033;
        public static final int roundTopRight = 2130772034;
        public static final int roundWithOverlayColor = 2130772037;
        public static final int roundedCornerRadius = 2130772032;
        public static final int roundingBorderColor = 2130772039;
        public static final int roundingBorderPadding = 2130772040;
        public static final int roundingBorderWidth = 2130772038;
        public static final int viewAspectRatio = 2130772017;
    }

    public static final class id {
        public static final int center = 2131623962;
        public static final int centerCrop = 2131623963;
        public static final int centerInside = 2131623964;
        public static final int fitCenter = 2131623965;
        public static final int fitEnd = 2131623966;
        public static final int fitStart = 2131623967;
        public static final int fitXY = 2131623968;
        public static final int focusCrop = 2131623969;
        public static final int none = 2131623952;
    }

    public static final class styleable {
        public static final int[] GenericDraweeHierarchy = {com.audiomobile.R.attr.fadeDuration, com.audiomobile.R.attr.viewAspectRatio, com.audiomobile.R.attr.placeholderImage, com.audiomobile.R.attr.placeholderImageScaleType, com.audiomobile.R.attr.retryImage, com.audiomobile.R.attr.retryImageScaleType, com.audiomobile.R.attr.failureImage, com.audiomobile.R.attr.failureImageScaleType, com.audiomobile.R.attr.progressBarImage, com.audiomobile.R.attr.progressBarImageScaleType, com.audiomobile.R.attr.progressBarAutoRotateInterval, com.audiomobile.R.attr.actualImageScaleType, com.audiomobile.R.attr.backgroundImage, com.audiomobile.R.attr.overlayImage, com.audiomobile.R.attr.pressedStateOverlayImage, com.audiomobile.R.attr.roundAsCircle, com.audiomobile.R.attr.roundedCornerRadius, com.audiomobile.R.attr.roundTopLeft, com.audiomobile.R.attr.roundTopRight, com.audiomobile.R.attr.roundBottomRight, com.audiomobile.R.attr.roundBottomLeft, com.audiomobile.R.attr.roundWithOverlayColor, com.audiomobile.R.attr.roundingBorderWidth, com.audiomobile.R.attr.roundingBorderColor, com.audiomobile.R.attr.roundingBorderPadding};
        public static final int GenericDraweeHierarchy_actualImageScaleType = 11;
        public static final int GenericDraweeHierarchy_backgroundImage = 12;
        public static final int GenericDraweeHierarchy_fadeDuration = 0;
        public static final int GenericDraweeHierarchy_failureImage = 6;
        public static final int GenericDraweeHierarchy_failureImageScaleType = 7;
        public static final int GenericDraweeHierarchy_overlayImage = 13;
        public static final int GenericDraweeHierarchy_placeholderImage = 2;
        public static final int GenericDraweeHierarchy_placeholderImageScaleType = 3;
        public static final int GenericDraweeHierarchy_pressedStateOverlayImage = 14;
        public static final int GenericDraweeHierarchy_progressBarAutoRotateInterval = 10;
        public static final int GenericDraweeHierarchy_progressBarImage = 8;
        public static final int GenericDraweeHierarchy_progressBarImageScaleType = 9;
        public static final int GenericDraweeHierarchy_retryImage = 4;
        public static final int GenericDraweeHierarchy_retryImageScaleType = 5;
        public static final int GenericDraweeHierarchy_roundAsCircle = 15;
        public static final int GenericDraweeHierarchy_roundBottomLeft = 20;
        public static final int GenericDraweeHierarchy_roundBottomRight = 19;
        public static final int GenericDraweeHierarchy_roundTopLeft = 17;
        public static final int GenericDraweeHierarchy_roundTopRight = 18;
        public static final int GenericDraweeHierarchy_roundWithOverlayColor = 21;
        public static final int GenericDraweeHierarchy_roundedCornerRadius = 16;
        public static final int GenericDraweeHierarchy_roundingBorderColor = 23;
        public static final int GenericDraweeHierarchy_roundingBorderPadding = 24;
        public static final int GenericDraweeHierarchy_roundingBorderWidth = 22;
        public static final int GenericDraweeHierarchy_viewAspectRatio = 1;
        public static final int[] SimpleDraweeView = {com.audiomobile.R.attr.actualImageUri, com.audiomobile.R.attr.actualImageResource};
        public static final int SimpleDraweeView_actualImageResource = 1;
        public static final int SimpleDraweeView_actualImageUri = 0;
    }
}
