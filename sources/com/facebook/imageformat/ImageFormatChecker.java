package com.facebook.imageformat;

import com.facebook.common.internal.ByteStreams;
import com.facebook.common.internal.Closeables;
import com.facebook.common.internal.Preconditions;
import com.facebook.common.internal.Throwables;
import com.facebook.imageformat.ImageFormat.FormatChecker;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.annotation.Nullable;

public class ImageFormatChecker {
    private static ImageFormatChecker sInstance;
    @Nullable
    private List<FormatChecker> mCustomImageFormatCheckers;
    private final FormatChecker mDefaultFormatChecker = new DefaultImageFormatChecker();
    private int mMaxHeaderLength;

    private ImageFormatChecker() {
        updateMaxHeaderLength();
    }

    public void setCustomImageFormatCheckers(@Nullable List<FormatChecker> customImageFormatCheckers) {
        this.mCustomImageFormatCheckers = customImageFormatCheckers;
        updateMaxHeaderLength();
    }

    public ImageFormat determineImageFormat(InputStream is) throws IOException {
        Preconditions.checkNotNull(is);
        byte[] imageHeaderBytes = new byte[this.mMaxHeaderLength];
        int headerSize = readHeaderFromStream(this.mMaxHeaderLength, is, imageHeaderBytes);
        if (this.mCustomImageFormatCheckers != null) {
            for (FormatChecker formatChecker : this.mCustomImageFormatCheckers) {
                ImageFormat format = formatChecker.determineFormat(imageHeaderBytes, headerSize);
                if (format != null && format != ImageFormat.UNKNOWN) {
                    return format;
                }
            }
        }
        ImageFormat format2 = this.mDefaultFormatChecker.determineFormat(imageHeaderBytes, headerSize);
        if (format2 == null) {
            return ImageFormat.UNKNOWN;
        }
        return format2;
    }

    private void updateMaxHeaderLength() {
        this.mMaxHeaderLength = this.mDefaultFormatChecker.getHeaderSize();
        if (this.mCustomImageFormatCheckers != null) {
            for (FormatChecker checker : this.mCustomImageFormatCheckers) {
                this.mMaxHeaderLength = Math.max(this.mMaxHeaderLength, checker.getHeaderSize());
            }
        }
    }

    private static int readHeaderFromStream(int maxHeaderLength, InputStream is, byte[] imageHeaderBytes) throws IOException {
        Preconditions.checkNotNull(is);
        Preconditions.checkNotNull(imageHeaderBytes);
        Preconditions.checkArgument(imageHeaderBytes.length >= maxHeaderLength);
        if (!is.markSupported()) {
            return ByteStreams.read(is, imageHeaderBytes, 0, maxHeaderLength);
        }
        try {
            is.mark(maxHeaderLength);
            return ByteStreams.read(is, imageHeaderBytes, 0, maxHeaderLength);
        } finally {
            is.reset();
        }
    }

    public static synchronized ImageFormatChecker getInstance() {
        ImageFormatChecker imageFormatChecker;
        synchronized (ImageFormatChecker.class) {
            if (sInstance == null) {
                sInstance = new ImageFormatChecker();
            }
            imageFormatChecker = sInstance;
        }
        return imageFormatChecker;
    }

    public static ImageFormat getImageFormat(InputStream is) throws IOException {
        return getInstance().determineImageFormat(is);
    }

    public static ImageFormat getImageFormat_WrapIOException(InputStream is) {
        try {
            return getImageFormat(is);
        } catch (IOException ioe) {
            throw Throwables.propagate(ioe);
        }
    }

    public static ImageFormat getImageFormat(String filename) {
        ImageFormat imageFormat;
        FileInputStream fileInputStream = null;
        try {
            FileInputStream fileInputStream2 = new FileInputStream(filename);
            try {
                imageFormat = getImageFormat((InputStream) fileInputStream2);
                Closeables.closeQuietly((InputStream) fileInputStream2);
                InputStream inputStream = fileInputStream2;
            } catch (IOException e) {
                fileInputStream = fileInputStream2;
                try {
                    imageFormat = ImageFormat.UNKNOWN;
                    Closeables.closeQuietly((InputStream) fileInputStream);
                    return imageFormat;
                } catch (Throwable th) {
                    th = th;
                    Closeables.closeQuietly((InputStream) fileInputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = fileInputStream2;
                Closeables.closeQuietly((InputStream) fileInputStream);
                throw th;
            }
        } catch (IOException e2) {
            imageFormat = ImageFormat.UNKNOWN;
            Closeables.closeQuietly((InputStream) fileInputStream);
            return imageFormat;
        }
        return imageFormat;
    }
}
