package com.facebook.fbui.textlayoutbuilder;

public final class R {

    public static final class styleable {
        public static final int[] TextAppearance = {16842901, 16842902, 16842903, 16842904, 16843105, 16843106, 16843107, 16843108, com.audiomobile.R.attr.textAllCaps};
        public static final int TextAppearance_android_shadowColor = 4;
        public static final int TextAppearance_android_shadowDx = 5;
        public static final int TextAppearance_android_shadowDy = 6;
        public static final int TextAppearance_android_shadowRadius = 7;
        public static final int TextAppearance_android_textColor = 3;
        public static final int TextAppearance_android_textSize = 0;
        public static final int TextAppearance_android_textStyle = 2;
        public static final int[] TextStyle = {16842804, 16842901, 16842903, 16842904, 16842923, 16843091, 16843101, 16843105, 16843106, 16843107, 16843108};
        public static final int TextStyle_android_ellipsize = 4;
        public static final int TextStyle_android_maxLines = 5;
        public static final int TextStyle_android_shadowColor = 7;
        public static final int TextStyle_android_shadowDx = 8;
        public static final int TextStyle_android_shadowDy = 9;
        public static final int TextStyle_android_shadowRadius = 10;
        public static final int TextStyle_android_singleLine = 6;
        public static final int TextStyle_android_textAppearance = 0;
        public static final int TextStyle_android_textColor = 3;
        public static final int TextStyle_android_textSize = 1;
        public static final int TextStyle_android_textStyle = 2;
    }
}
