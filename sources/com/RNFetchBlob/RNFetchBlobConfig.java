package com.RNFetchBlob;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;

public class RNFetchBlobConfig {
    public ReadableMap addAndroidDownloads;
    public String appendExt;
    public Boolean auto;
    public ReadableArray binaryContentTypes = null;
    public Boolean fileCache;
    public Boolean followRedirect = Boolean.valueOf(true);
    public Boolean increment = Boolean.valueOf(false);
    public String key;
    public String mime;
    public Boolean overwrite = Boolean.valueOf(true);
    public String path;
    public long timeout = 60000;
    public Boolean trusty;

    RNFetchBlobConfig(ReadableMap options) {
        String str;
        boolean z;
        boolean z2;
        String str2 = null;
        boolean z3 = false;
        if (options != null) {
            this.fileCache = Boolean.valueOf(options.hasKey("fileCache") ? options.getBoolean("fileCache") : false);
            if (options.hasKey(RNFetchBlobConst.RNFB_RESPONSE_PATH)) {
                str = options.getString(RNFetchBlobConst.RNFB_RESPONSE_PATH);
            } else {
                str = null;
            }
            this.path = str;
            this.appendExt = options.hasKey("appendExt") ? options.getString("appendExt") : "";
            if (options.hasKey("trusty")) {
                z = options.getBoolean("trusty");
            } else {
                z = false;
            }
            this.trusty = Boolean.valueOf(z);
            if (options.hasKey("addAndroidDownloads")) {
                this.addAndroidDownloads = options.getMap("addAndroidDownloads");
            }
            if (options.hasKey("binaryContentTypes")) {
                this.binaryContentTypes = options.getArray("binaryContentTypes");
            }
            if (this.path != null && this.path.toLowerCase().contains("?append=true")) {
                this.overwrite = Boolean.valueOf(false);
            }
            if (options.hasKey("overwrite")) {
                this.overwrite = Boolean.valueOf(options.getBoolean("overwrite"));
            }
            if (options.hasKey("followRedirect")) {
                this.followRedirect = Boolean.valueOf(options.getBoolean("followRedirect"));
            }
            this.key = options.hasKey("key") ? options.getString("key") : null;
            if (options.hasKey("contentType")) {
                str2 = options.getString("contentType");
            }
            this.mime = str2;
            if (options.hasKey("increment")) {
                z2 = options.getBoolean("increment");
            } else {
                z2 = false;
            }
            this.increment = Boolean.valueOf(z2);
            if (options.hasKey("auto")) {
                z3 = options.getBoolean("auto");
            }
            this.auto = Boolean.valueOf(z3);
            if (options.hasKey("timeout")) {
                this.timeout = (long) options.getInt("timeout");
            }
        }
    }
}
