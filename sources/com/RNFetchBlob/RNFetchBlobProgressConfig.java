package com.RNFetchBlob;

public class RNFetchBlobProgressConfig {
    int count = -1;
    public boolean enable = false;
    public int interval = -1;
    long lastTick = 0;
    int tick = 0;
    public ReportType type = ReportType.Download;

    public enum ReportType {
        Upload,
        Download
    }

    RNFetchBlobProgressConfig(boolean report, int interval2, int count2, ReportType type2) {
        this.enable = report;
        this.interval = interval2;
        this.type = type2;
        this.count = count2;
    }

    public boolean shouldReport(float progress) {
        boolean result;
        boolean checkCount = true;
        if (this.count > 0 && progress > 0.0f) {
            checkCount = Math.floor((double) (((float) this.count) * progress)) > ((double) this.tick);
        }
        if (System.currentTimeMillis() - this.lastTick <= ((long) this.interval) || !this.enable || !checkCount) {
            result = false;
        } else {
            result = true;
        }
        if (result) {
            this.tick++;
            this.lastTick = System.currentTimeMillis();
        }
        return result;
    }
}
