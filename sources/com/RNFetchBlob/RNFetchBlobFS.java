package com.RNFetchBlob;

import android.content.res.AssetFileDescriptor;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import android.util.Base64;
import com.RNFetchBlob.Utils.PathResolver;
import com.facebook.common.util.UriUtil;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class RNFetchBlobFS {
    static HashMap<String, RNFetchBlobFS> fileStreams = new HashMap<>();
    boolean append = false;
    RCTDeviceEventEmitter emitter;
    String encoding = RNFetchBlobConst.RNFB_RESPONSE_BASE64;
    ReactApplicationContext mCtx;
    OutputStream writeStreamInstance = null;

    RNFetchBlobFS(ReactApplicationContext ctx) {
        this.mCtx = ctx;
        this.emitter = (RCTDeviceEventEmitter) ctx.getJSModule(RCTDeviceEventEmitter.class);
    }

    static String getExternalFilePath(ReactApplicationContext ctx, String taskId, RNFetchBlobConfig config) {
        if (config.path != null) {
            return config.path;
        }
        if (!config.fileCache.booleanValue() || config.appendExt == null) {
            return getTmpPath(ctx, taskId);
        }
        return getTmpPath(ctx, taskId) + "." + config.appendExt;
    }

    public static void writeFile(String path, String encoding2, String data, boolean append2, Promise promise) {
        int written;
        try {
            File f = new File(path);
            File dir = f.getParentFile();
            if (!dir.exists()) {
                dir.mkdirs();
            }
            FileOutputStream fout = new FileOutputStream(f, append2);
            if (encoding2.equalsIgnoreCase(RNFetchBlobConst.DATA_ENCODE_URI)) {
                String data2 = normalizePath(data);
                File src = new File(data2);
                if (!src.exists()) {
                    promise.reject("RNfetchBlob writeFileError", "source file : " + data2 + "not exists");
                    fout.close();
                    return;
                }
                FileInputStream fin = new FileInputStream(src);
                byte[] buffer = new byte[10240];
                written = 0;
                while (true) {
                    int read = fin.read(buffer);
                    if (read <= 0) {
                        break;
                    }
                    fout.write(buffer, 0, read);
                    written += read;
                }
                fin.close();
            } else {
                byte[] bytes = stringToBytes(data, encoding2);
                fout.write(bytes);
                written = bytes.length;
            }
            fout.close();
            promise.resolve(Integer.valueOf(written));
        } catch (Exception e) {
            promise.reject("RNFetchBlob writeFileError", e.getLocalizedMessage());
        }
    }

    public static void writeFile(String path, ReadableArray data, boolean append2, Promise promise) {
        try {
            File f = new File(path);
            File dir = f.getParentFile();
            if (!dir.exists()) {
                dir.mkdirs();
            }
            FileOutputStream os = new FileOutputStream(f, append2);
            byte[] bytes = new byte[data.size()];
            for (int i = 0; i < data.size(); i++) {
                bytes[i] = (byte) data.getInt(i);
            }
            os.write(bytes);
            os.close();
            promise.resolve(Integer.valueOf(data.size()));
        } catch (Exception e) {
            promise.reject("RNFetchBlob writeFileError", e.getLocalizedMessage());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b A[Catch:{ Exception -> 0x0071 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0094 A[Catch:{ Exception -> 0x0071 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009e A[Catch:{ Exception -> 0x0071 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a8 A[Catch:{ Exception -> 0x0071 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b2 A[Catch:{ Exception -> 0x0071 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00bb A[Catch:{ Exception -> 0x0071 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00cf A[Catch:{ Exception -> 0x0071 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void readFile(java.lang.String r13, java.lang.String r14, com.facebook.react.bridge.Promise r15) {
        /*
            java.lang.String r7 = normalizePath(r13)
            if (r7 == 0) goto L_0x0007
            r13 = r7
        L_0x0007:
            if (r7 == 0) goto L_0x0054
            java.lang.String r10 = "bundle-assets://"
            boolean r10 = r7.startsWith(r10)     // Catch:{ Exception -> 0x0071 }
            if (r10 == 0) goto L_0x0054
            java.lang.String r10 = "bundle-assets://"
            java.lang.String r11 = ""
            java.lang.String r1 = r13.replace(r10, r11)     // Catch:{ Exception -> 0x0071 }
            com.facebook.react.bridge.ReactApplicationContext r10 = com.RNFetchBlob.RNFetchBlob.RCTContext     // Catch:{ Exception -> 0x0071 }
            android.content.res.AssetManager r10 = r10.getAssets()     // Catch:{ Exception -> 0x0071 }
            android.content.res.AssetFileDescriptor r10 = r10.openFd(r1)     // Catch:{ Exception -> 0x0071 }
            long r8 = r10.getLength()     // Catch:{ Exception -> 0x0071 }
            int r10 = (int) r8     // Catch:{ Exception -> 0x0071 }
            byte[] r3 = new byte[r10]     // Catch:{ Exception -> 0x0071 }
            com.facebook.react.bridge.ReactApplicationContext r10 = com.RNFetchBlob.RNFetchBlob.RCTContext     // Catch:{ Exception -> 0x0071 }
            android.content.res.AssetManager r10 = r10.getAssets()     // Catch:{ Exception -> 0x0071 }
            java.io.InputStream r6 = r10.open(r1)     // Catch:{ Exception -> 0x0071 }
            r10 = 0
            int r11 = (int) r8     // Catch:{ Exception -> 0x0071 }
            r6.read(r3, r10, r11)     // Catch:{ Exception -> 0x0071 }
            r6.close()     // Catch:{ Exception -> 0x0071 }
        L_0x003c:
            java.lang.String r11 = r14.toLowerCase()     // Catch:{ Exception -> 0x0071 }
            r10 = -1
            int r12 = r11.hashCode()     // Catch:{ Exception -> 0x0071 }
            switch(r12) {
                case -1396204209: goto L_0x0094;
                case 3600241: goto L_0x00a8;
                case 93106001: goto L_0x009e;
                default: goto L_0x0048;
            }     // Catch:{ Exception -> 0x0071 }
        L_0x0048:
            switch(r10) {
                case 0: goto L_0x00b2;
                case 1: goto L_0x00bb;
                case 2: goto L_0x00cf;
                default: goto L_0x004b;
            }     // Catch:{ Exception -> 0x0071 }
        L_0x004b:
            java.lang.String r10 = new java.lang.String     // Catch:{ Exception -> 0x0071 }
            r10.<init>(r3)     // Catch:{ Exception -> 0x0071 }
            r15.resolve(r10)     // Catch:{ Exception -> 0x0071 }
        L_0x0053:
            return
        L_0x0054:
            if (r7 != 0) goto L_0x007c
            com.facebook.react.bridge.ReactApplicationContext r10 = com.RNFetchBlob.RNFetchBlob.RCTContext     // Catch:{ Exception -> 0x0071 }
            android.content.ContentResolver r10 = r10.getContentResolver()     // Catch:{ Exception -> 0x0071 }
            android.net.Uri r11 = android.net.Uri.parse(r13)     // Catch:{ Exception -> 0x0071 }
            java.io.InputStream r6 = r10.openInputStream(r11)     // Catch:{ Exception -> 0x0071 }
            int r8 = r6.available()     // Catch:{ Exception -> 0x0071 }
            byte[] r3 = new byte[r8]     // Catch:{ Exception -> 0x0071 }
            r6.read(r3)     // Catch:{ Exception -> 0x0071 }
            r6.close()     // Catch:{ Exception -> 0x0071 }
            goto L_0x003c
        L_0x0071:
            r4 = move-exception
            java.lang.String r10 = "ReadFile Error"
            java.lang.String r11 = r4.getLocalizedMessage()
            r15.reject(r10, r11)
            goto L_0x0053
        L_0x007c:
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x0071 }
            r5.<init>(r13)     // Catch:{ Exception -> 0x0071 }
            long r10 = r5.length()     // Catch:{ Exception -> 0x0071 }
            int r8 = (int) r10     // Catch:{ Exception -> 0x0071 }
            byte[] r3 = new byte[r8]     // Catch:{ Exception -> 0x0071 }
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0071 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x0071 }
            r6.read(r3)     // Catch:{ Exception -> 0x0071 }
            r6.close()     // Catch:{ Exception -> 0x0071 }
            goto L_0x003c
        L_0x0094:
            java.lang.String r12 = "base64"
            boolean r11 = r11.equals(r12)     // Catch:{ Exception -> 0x0071 }
            if (r11 == 0) goto L_0x0048
            r10 = 0
            goto L_0x0048
        L_0x009e:
            java.lang.String r12 = "ascii"
            boolean r11 = r11.equals(r12)     // Catch:{ Exception -> 0x0071 }
            if (r11 == 0) goto L_0x0048
            r10 = 1
            goto L_0x0048
        L_0x00a8:
            java.lang.String r12 = "utf8"
            boolean r11 = r11.equals(r12)     // Catch:{ Exception -> 0x0071 }
            if (r11 == 0) goto L_0x0048
            r10 = 2
            goto L_0x0048
        L_0x00b2:
            r10 = 2
            java.lang.String r10 = android.util.Base64.encodeToString(r3, r10)     // Catch:{ Exception -> 0x0071 }
            r15.resolve(r10)     // Catch:{ Exception -> 0x0071 }
            goto L_0x0053
        L_0x00bb:
            com.facebook.react.bridge.WritableArray r0 = com.facebook.react.bridge.Arguments.createArray()     // Catch:{ Exception -> 0x0071 }
            int r11 = r3.length     // Catch:{ Exception -> 0x0071 }
            r10 = 0
        L_0x00c1:
            if (r10 >= r11) goto L_0x00cb
            byte r2 = r3[r10]     // Catch:{ Exception -> 0x0071 }
            r0.pushInt(r2)     // Catch:{ Exception -> 0x0071 }
            int r10 = r10 + 1
            goto L_0x00c1
        L_0x00cb:
            r15.resolve(r0)     // Catch:{ Exception -> 0x0071 }
            goto L_0x0053
        L_0x00cf:
            java.lang.String r10 = new java.lang.String     // Catch:{ Exception -> 0x0071 }
            r10.<init>(r3)     // Catch:{ Exception -> 0x0071 }
            r15.resolve(r10)     // Catch:{ Exception -> 0x0071 }
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.RNFetchBlob.RNFetchBlobFS.readFile(java.lang.String, java.lang.String, com.facebook.react.bridge.Promise):void");
    }

    public static Map<String, Object> getSystemfolders(ReactApplicationContext ctx) {
        Map<String, Object> res = new HashMap<>();
        res.put("DocumentDir", ctx.getFilesDir().getAbsolutePath());
        res.put("CacheDir", ctx.getCacheDir().getAbsolutePath());
        res.put("DCIMDir", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath());
        res.put("PictureDir", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath());
        res.put("MusicDir", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getAbsolutePath());
        res.put("DownloadDir", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath());
        res.put("MovieDir", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getAbsolutePath());
        res.put("RingtoneDir", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES).getAbsolutePath());
        if (Environment.getExternalStorageState().equals("mounted")) {
            res.put("SDCardDir", Environment.getExternalStorageDirectory().getAbsolutePath());
            res.put("SDCardApplicationDir", ctx.getExternalFilesDir(null).getParentFile().getAbsolutePath());
        }
        res.put("MainBundleDir", ctx.getApplicationInfo().dataDir);
        return res;
    }

    public static String getTmpPath(ReactApplicationContext ctx, String taskId) {
        return RNFetchBlob.RCTContext.getFilesDir() + "/RNFetchBlobTmp_" + taskId;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0048 A[Catch:{ Exception -> 0x007c }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c8 A[Catch:{ Exception -> 0x007c }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0166 A[Catch:{ Exception -> 0x007c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void readStream(java.lang.String r18, java.lang.String r19, int r20, int r21, java.lang.String r22) {
        /*
            r17 = this;
            java.lang.String r13 = normalizePath(r18)
            if (r13 == 0) goto L_0x0008
            r18 = r13
        L_0x0008:
            java.lang.String r14 = "base64"
            r0 = r19
            boolean r14 = r0.equalsIgnoreCase(r14)     // Catch:{ Exception -> 0x007c }
            if (r14 == 0) goto L_0x00a5
            r4 = 4095(0xfff, float:5.738E-42)
        L_0x0014:
            if (r20 <= 0) goto L_0x0018
            r4 = r20
        L_0x0018:
            if (r13 == 0) goto L_0x00a9
            java.lang.String r14 = "bundle-assets://"
            r0 = r18
            boolean r14 = r0.startsWith(r14)     // Catch:{ Exception -> 0x007c }
            if (r14 == 0) goto L_0x00a9
            com.facebook.react.bridge.ReactApplicationContext r14 = com.RNFetchBlob.RNFetchBlob.RCTContext     // Catch:{ Exception -> 0x007c }
            android.content.res.AssetManager r14 = r14.getAssets()     // Catch:{ Exception -> 0x007c }
            java.lang.String r15 = "bundle-assets://"
            java.lang.String r16 = ""
            r0 = r18
            r1 = r16
            java.lang.String r15 = r0.replace(r15, r1)     // Catch:{ Exception -> 0x007c }
            java.io.InputStream r10 = r14.open(r15)     // Catch:{ Exception -> 0x007c }
        L_0x003a:
            byte[] r2 = new byte[r4]     // Catch:{ Exception -> 0x007c }
            r6 = 0
            r9 = 0
            java.lang.String r14 = "utf8"
            r0 = r19
            boolean r14 = r0.equalsIgnoreCase(r14)     // Catch:{ Exception -> 0x007c }
            if (r14 == 0) goto L_0x00c8
            java.lang.String r14 = "UTF-8"
            java.nio.charset.Charset r14 = java.nio.charset.Charset.forName(r14)     // Catch:{ Exception -> 0x007c }
            java.nio.charset.CharsetEncoder r7 = r14.newEncoder()     // Catch:{ Exception -> 0x007c }
        L_0x0052:
            int r6 = r10.read(r2)     // Catch:{ Exception -> 0x007c }
            r14 = -1
            if (r6 == r14) goto L_0x0164
            java.nio.ByteBuffer r14 = java.nio.ByteBuffer.wrap(r2)     // Catch:{ Exception -> 0x007c }
            java.nio.CharBuffer r14 = r14.asCharBuffer()     // Catch:{ Exception -> 0x007c }
            r7.encode(r14)     // Catch:{ Exception -> 0x007c }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x007c }
            r14 = 0
            r3.<init>(r2, r14, r6)     // Catch:{ Exception -> 0x007c }
            java.lang.String r14 = "data"
            r0 = r17
            r1 = r22
            r0.emitStreamEvent(r1, r14, r3)     // Catch:{ Exception -> 0x007c }
            if (r21 <= 0) goto L_0x0052
            r0 = r21
            long r14 = (long) r0     // Catch:{ Exception -> 0x007c }
            android.os.SystemClock.sleep(r14)     // Catch:{ Exception -> 0x007c }
            goto L_0x0052
        L_0x007c:
            r8 = move-exception
            java.lang.String r14 = "warn"
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r16 = "Failed to convert data to "
            java.lang.StringBuilder r15 = r15.append(r16)
            r0 = r19
            java.lang.StringBuilder r15 = r15.append(r0)
            java.lang.String r16 = " encoded string, this might due to the source data is not able to convert using this encoding."
            java.lang.StringBuilder r15 = r15.append(r16)
            java.lang.String r15 = r15.toString()
            r0 = r17
            r1 = r22
            r0.emitStreamEvent(r1, r14, r15)
            r8.printStackTrace()
        L_0x00a4:
            return
        L_0x00a5:
            r4 = 4096(0x1000, float:5.74E-42)
            goto L_0x0014
        L_0x00a9:
            if (r13 != 0) goto L_0x00ba
            com.facebook.react.bridge.ReactApplicationContext r14 = com.RNFetchBlob.RNFetchBlob.RCTContext     // Catch:{ Exception -> 0x007c }
            android.content.ContentResolver r14 = r14.getContentResolver()     // Catch:{ Exception -> 0x007c }
            android.net.Uri r15 = android.net.Uri.parse(r18)     // Catch:{ Exception -> 0x007c }
            java.io.InputStream r10 = r14.openInputStream(r15)     // Catch:{ Exception -> 0x007c }
            goto L_0x003a
        L_0x00ba:
            java.io.FileInputStream r10 = new java.io.FileInputStream     // Catch:{ Exception -> 0x007c }
            java.io.File r14 = new java.io.File     // Catch:{ Exception -> 0x007c }
            r0 = r18
            r14.<init>(r0)     // Catch:{ Exception -> 0x007c }
            r10.<init>(r14)     // Catch:{ Exception -> 0x007c }
            goto L_0x003a
        L_0x00c8:
            java.lang.String r14 = "ascii"
            r0 = r19
            boolean r14 = r0.equalsIgnoreCase(r14)     // Catch:{ Exception -> 0x007c }
            if (r14 == 0) goto L_0x00fa
        L_0x00d2:
            int r6 = r10.read(r2)     // Catch:{ Exception -> 0x007c }
            r14 = -1
            if (r6 == r14) goto L_0x0164
            com.facebook.react.bridge.WritableArray r3 = com.facebook.react.bridge.Arguments.createArray()     // Catch:{ Exception -> 0x007c }
            r11 = 0
        L_0x00de:
            if (r11 >= r6) goto L_0x00e8
            byte r14 = r2[r11]     // Catch:{ Exception -> 0x007c }
            r3.pushInt(r14)     // Catch:{ Exception -> 0x007c }
            int r11 = r11 + 1
            goto L_0x00de
        L_0x00e8:
            java.lang.String r14 = "data"
            r0 = r17
            r1 = r22
            r0.emitStreamEvent(r1, r14, r3)     // Catch:{ Exception -> 0x007c }
            if (r21 <= 0) goto L_0x00d2
            r0 = r21
            long r14 = (long) r0     // Catch:{ Exception -> 0x007c }
            android.os.SystemClock.sleep(r14)     // Catch:{ Exception -> 0x007c }
            goto L_0x00d2
        L_0x00fa:
            java.lang.String r14 = "base64"
            r0 = r19
            boolean r14 = r0.equalsIgnoreCase(r14)     // Catch:{ Exception -> 0x007c }
            if (r14 == 0) goto L_0x013f
        L_0x0104:
            int r6 = r10.read(r2)     // Catch:{ Exception -> 0x007c }
            r14 = -1
            if (r6 == r14) goto L_0x0164
            if (r6 >= r4) goto L_0x0130
            byte[] r5 = new byte[r6]     // Catch:{ Exception -> 0x007c }
            r11 = 0
        L_0x0110:
            if (r11 >= r6) goto L_0x0119
            byte r14 = r2[r11]     // Catch:{ Exception -> 0x007c }
            r5[r11] = r14     // Catch:{ Exception -> 0x007c }
            int r11 = r11 + 1
            goto L_0x0110
        L_0x0119:
            java.lang.String r14 = "data"
            r15 = 2
            java.lang.String r15 = android.util.Base64.encodeToString(r5, r15)     // Catch:{ Exception -> 0x007c }
            r0 = r17
            r1 = r22
            r0.emitStreamEvent(r1, r14, r15)     // Catch:{ Exception -> 0x007c }
        L_0x0127:
            if (r21 <= 0) goto L_0x0104
            r0 = r21
            long r14 = (long) r0     // Catch:{ Exception -> 0x007c }
            android.os.SystemClock.sleep(r14)     // Catch:{ Exception -> 0x007c }
            goto L_0x0104
        L_0x0130:
            java.lang.String r14 = "data"
            r15 = 2
            java.lang.String r15 = android.util.Base64.encodeToString(r2, r15)     // Catch:{ Exception -> 0x007c }
            r0 = r17
            r1 = r22
            r0.emitStreamEvent(r1, r14, r15)     // Catch:{ Exception -> 0x007c }
            goto L_0x0127
        L_0x013f:
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007c }
            r14.<init>()     // Catch:{ Exception -> 0x007c }
            java.lang.String r15 = "unrecognized encoding `"
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x007c }
            r0 = r19
            java.lang.StringBuilder r14 = r14.append(r0)     // Catch:{ Exception -> 0x007c }
            java.lang.String r15 = "`"
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x007c }
            java.lang.String r12 = r14.toString()     // Catch:{ Exception -> 0x007c }
            java.lang.String r14 = "error"
            r0 = r17
            r1 = r22
            r0.emitStreamEvent(r1, r14, r12)     // Catch:{ Exception -> 0x007c }
            r9 = 1
        L_0x0164:
            if (r9 != 0) goto L_0x0171
            java.lang.String r14 = "end"
            java.lang.String r15 = ""
            r0 = r17
            r1 = r22
            r0.emitStreamEvent(r1, r14, r15)     // Catch:{ Exception -> 0x007c }
        L_0x0171:
            r10.close()     // Catch:{ Exception -> 0x007c }
            goto L_0x00a4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.RNFetchBlob.RNFetchBlobFS.readStream(java.lang.String, java.lang.String, int, int, java.lang.String):void");
    }

    public void writeStream(String path, String encoding2, boolean append2, Callback callback) {
        File dest = new File(path);
        if (!dest.exists() || dest.isDirectory()) {
            callback.invoke("write stream error: target path `" + path + "` may not exists or it's a folder");
            return;
        }
        try {
            OutputStream fs = new FileOutputStream(path, append2);
            this.encoding = encoding2;
            this.append = append2;
            String streamId = UUID.randomUUID().toString();
            fileStreams.put(streamId, this);
            this.writeStreamInstance = fs;
            callback.invoke(null, streamId);
        } catch (Exception err) {
            callback.invoke("write stream error: failed to create write stream at path `" + path + "` " + err.getLocalizedMessage());
        }
    }

    static void writeChunk(String streamId, String data, Callback callback) {
        RNFetchBlobFS fs = (RNFetchBlobFS) fileStreams.get(streamId);
        try {
            fs.writeStreamInstance.write(stringToBytes(data, fs.encoding));
            callback.invoke(new Object[0]);
        } catch (Exception e) {
            callback.invoke(e.getLocalizedMessage());
        }
    }

    static void writeArrayChunk(String streamId, ReadableArray data, Callback callback) {
        try {
            OutputStream stream = ((RNFetchBlobFS) fileStreams.get(streamId)).writeStreamInstance;
            byte[] chunk = new byte[data.size()];
            for (int i = 0; i < data.size(); i++) {
                chunk[i] = (byte) data.getInt(i);
            }
            stream.write(chunk);
            callback.invoke(new Object[0]);
        } catch (Exception e) {
            callback.invoke(e.getLocalizedMessage());
        }
    }

    static void closeStream(String streamId, Callback callback) {
        try {
            OutputStream stream = ((RNFetchBlobFS) fileStreams.get(streamId)).writeStreamInstance;
            fileStreams.remove(streamId);
            stream.close();
            callback.invoke(new Object[0]);
        } catch (Exception err) {
            callback.invoke(err.getLocalizedMessage());
        }
    }

    static void unlink(String path, Callback callback) {
        try {
            deleteRecursive(new File(path));
            callback.invoke(null, Boolean.valueOf(true));
        } catch (Exception err) {
            if (err != null) {
                callback.invoke(err.getLocalizedMessage(), Boolean.valueOf(false));
            }
        }
    }

    static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteRecursive(child);
            }
        }
        fileOrDirectory.delete();
    }

    static void mkdir(String path, Callback callback) {
        File dest = new File(path);
        if (dest.exists()) {
            callback.invoke("mkdir error: failed to create folder at `" + path + "` folder already exists");
            return;
        }
        dest.mkdirs();
        callback.invoke(new Object[0]);
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x008a A[SYNTHETIC, Splitter:B:30:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008f A[Catch:{ Exception -> 0x0099 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00ca A[SYNTHETIC, Splitter:B:48:0x00ca] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00cf A[Catch:{ Exception -> 0x00d9 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void cp(java.lang.String r13, java.lang.String r14, com.facebook.react.bridge.Callback r15) {
        /*
            r12 = 1
            r11 = 0
            java.lang.String r13 = normalizePath(r13)
            r3 = 0
            r5 = 0
            boolean r7 = isPathExists(r13)     // Catch:{ Exception -> 0x00e9 }
            if (r7 != 0) goto L_0x004e
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x00e9 }
            r8 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e9 }
            r9.<init>()     // Catch:{ Exception -> 0x00e9 }
            java.lang.String r10 = "cp error: source file at path`"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x00e9 }
            java.lang.StringBuilder r9 = r9.append(r13)     // Catch:{ Exception -> 0x00e9 }
            java.lang.String r10 = "` not exists"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x00e9 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x00e9 }
            r7[r8] = r9     // Catch:{ Exception -> 0x00e9 }
            r15.invoke(r7)     // Catch:{ Exception -> 0x00e9 }
            if (r3 == 0) goto L_0x0035
            r3.close()     // Catch:{ Exception -> 0x0041 }
        L_0x0035:
            if (r5 == 0) goto L_0x003a
            r5.close()     // Catch:{ Exception -> 0x0041 }
        L_0x003a:
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0041 }
            r15.invoke(r7)     // Catch:{ Exception -> 0x0041 }
        L_0x0040:
            return
        L_0x0041:
            r1 = move-exception
            java.lang.Object[] r7 = new java.lang.Object[r12]
            java.lang.String r8 = r1.getLocalizedMessage()
            r7[r11] = r8
            r15.invoke(r7)
            goto L_0x0040
        L_0x004e:
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x00e9 }
            r7.<init>(r14)     // Catch:{ Exception -> 0x00e9 }
            boolean r7 = r7.exists()     // Catch:{ Exception -> 0x00e9 }
            if (r7 != 0) goto L_0x0061
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x00e9 }
            r7.<init>(r14)     // Catch:{ Exception -> 0x00e9 }
            r7.createNewFile()     // Catch:{ Exception -> 0x00e9 }
        L_0x0061:
            java.io.InputStream r3 = inputStreamFromPath(r13)     // Catch:{ Exception -> 0x00e9 }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00e9 }
            r6.<init>(r14)     // Catch:{ Exception -> 0x00e9 }
            r7 = 10240(0x2800, float:1.4349E-41)
            byte[] r0 = new byte[r7]     // Catch:{ Exception -> 0x0079, all -> 0x00e6 }
        L_0x006e:
            int r4 = r3.read(r0)     // Catch:{ Exception -> 0x0079, all -> 0x00e6 }
            if (r4 <= 0) goto L_0x00a6
            r7 = 0
            r6.write(r0, r7, r4)     // Catch:{ Exception -> 0x0079, all -> 0x00e6 }
            goto L_0x006e
        L_0x0079:
            r2 = move-exception
            r5 = r6
        L_0x007b:
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x00c7 }
            r8 = 0
            java.lang.String r9 = r2.getLocalizedMessage()     // Catch:{ all -> 0x00c7 }
            r7[r8] = r9     // Catch:{ all -> 0x00c7 }
            r15.invoke(r7)     // Catch:{ all -> 0x00c7 }
            if (r3 == 0) goto L_0x008d
            r3.close()     // Catch:{ Exception -> 0x0099 }
        L_0x008d:
            if (r5 == 0) goto L_0x0092
            r5.close()     // Catch:{ Exception -> 0x0099 }
        L_0x0092:
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0099 }
            r15.invoke(r7)     // Catch:{ Exception -> 0x0099 }
            goto L_0x0040
        L_0x0099:
            r1 = move-exception
            java.lang.Object[] r7 = new java.lang.Object[r12]
            java.lang.String r8 = r1.getLocalizedMessage()
            r7[r11] = r8
            r15.invoke(r7)
            goto L_0x0040
        L_0x00a6:
            if (r3 == 0) goto L_0x00ab
            r3.close()     // Catch:{ Exception -> 0x00b8 }
        L_0x00ab:
            if (r6 == 0) goto L_0x00b0
            r6.close()     // Catch:{ Exception -> 0x00b8 }
        L_0x00b0:
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x00b8 }
            r15.invoke(r7)     // Catch:{ Exception -> 0x00b8 }
            r5 = r6
            goto L_0x0040
        L_0x00b8:
            r1 = move-exception
            java.lang.Object[] r7 = new java.lang.Object[r12]
            java.lang.String r8 = r1.getLocalizedMessage()
            r7[r11] = r8
            r15.invoke(r7)
            r5 = r6
            goto L_0x0040
        L_0x00c7:
            r7 = move-exception
        L_0x00c8:
            if (r3 == 0) goto L_0x00cd
            r3.close()     // Catch:{ Exception -> 0x00d9 }
        L_0x00cd:
            if (r5 == 0) goto L_0x00d2
            r5.close()     // Catch:{ Exception -> 0x00d9 }
        L_0x00d2:
            r8 = 0
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x00d9 }
            r15.invoke(r8)     // Catch:{ Exception -> 0x00d9 }
        L_0x00d8:
            throw r7
        L_0x00d9:
            r1 = move-exception
            java.lang.Object[] r8 = new java.lang.Object[r12]
            java.lang.String r9 = r1.getLocalizedMessage()
            r8[r11] = r9
            r15.invoke(r8)
            goto L_0x00d8
        L_0x00e6:
            r7 = move-exception
            r5 = r6
            goto L_0x00c8
        L_0x00e9:
            r2 = move-exception
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.RNFetchBlob.RNFetchBlobFS.cp(java.lang.String, java.lang.String, com.facebook.react.bridge.Callback):void");
    }

    static void mv(String path, String dest, Callback callback) {
        File src = new File(path);
        if (!src.exists()) {
            callback.invoke("mv error: source file at path `" + path + "` does not exists");
            return;
        }
        src.renameTo(new File(dest));
        callback.invoke(new Object[0]);
    }

    static void exists(String path, Callback callback) {
        if (isAsset(path)) {
            try {
                AssetFileDescriptor openFd = RNFetchBlob.RCTContext.getAssets().openFd(path.replace(RNFetchBlobConst.FILE_PREFIX_BUNDLE_ASSET, ""));
                callback.invoke(Boolean.valueOf(true), Boolean.valueOf(false));
            } catch (IOException e) {
                callback.invoke(Boolean.valueOf(false), Boolean.valueOf(false));
            }
        } else {
            String path2 = normalizePath(path);
            callback.invoke(Boolean.valueOf(new File(path2).exists()), Boolean.valueOf(new File(path2).isDirectory()));
        }
    }

    static void ls(String path, Callback callback) {
        String path2 = normalizePath(path);
        File src = new File(path2);
        if (!src.exists() || !src.isDirectory()) {
            callback.invoke("ls error: failed to list path `" + path2 + "` for it is not exist or it is not a folder");
            return;
        }
        String[] files = new File(path2).list();
        WritableArray arg = Arguments.createArray();
        for (String i : files) {
            arg.pushString(i);
        }
        callback.invoke(null, arg);
    }

    public static void slice(String src, String dest, int start, int end, String encode, Promise promise) {
        try {
            String src2 = normalizePath(src);
            File file = new File(src2);
            if (!file.exists()) {
                promise.reject("RNFetchBlob.slice error", "source file : " + src2 + " not exists");
                return;
            }
            long expected = Math.min(file.length(), (long) end) - ((long) start);
            long now = 0;
            File file2 = new File(src2);
            FileInputStream in = new FileInputStream(file2);
            File file3 = new File(dest);
            FileOutputStream out = new FileOutputStream(file3);
            in.skip((long) start);
            byte[] buffer = new byte[10240];
            while (now < expected) {
                long read = (long) in.read(buffer, 0, 10240);
                long remain = expected - now;
                if (read <= 0) {
                    break;
                }
                out.write(buffer, 0, (int) Math.min(remain, read));
                now += read;
            }
            in.close();
            out.flush();
            out.close();
            promise.resolve(dest);
        } catch (Exception e) {
            e.printStackTrace();
            promise.reject(e.getLocalizedMessage());
        }
    }

    static void lstat(String path, final Callback callback) {
        String path2 = normalizePath(path);
        new AsyncTask<String, Integer, Integer>() {
            /* access modifiers changed from: protected */
            public Integer doInBackground(String... args) {
                WritableArray res = Arguments.createArray();
                if (args[0] == null) {
                    callback.invoke("lstat error: the path specified for lstat is either `null` or `undefined`.");
                    return Integer.valueOf(0);
                }
                File src = new File(args[0]);
                if (!src.exists()) {
                    callback.invoke("lstat error: failed to list path `" + args[0] + "` for it is not exist or it is not a folder");
                    return Integer.valueOf(0);
                }
                if (src.isDirectory()) {
                    String[] files = src.list();
                    int length = files.length;
                    for (int i = 0; i < length; i++) {
                        res.pushMap(RNFetchBlobFS.statFile(src.getPath() + "/" + files[i]));
                    }
                } else {
                    res.pushMap(RNFetchBlobFS.statFile(src.getAbsolutePath()));
                }
                callback.invoke(null, res);
                return Integer.valueOf(0);
            }
        }.execute(new String[]{path2});
    }

    static void stat(String path, Callback callback) {
        try {
            String path2 = normalizePath(path);
            WritableMap result = statFile(path2);
            if (result == null) {
                callback.invoke("stat error: failed to list path `" + path2 + "` for it is not exist or it is not a folder", null);
                return;
            }
            callback.invoke(null, result);
        } catch (Exception err) {
            callback.invoke(err.getLocalizedMessage());
        }
    }

    static WritableMap statFile(String path) {
        try {
            String path2 = normalizePath(path);
            WritableMap stat = Arguments.createMap();
            if (isAsset(path2)) {
                String name = path2.replace(RNFetchBlobConst.FILE_PREFIX_BUNDLE_ASSET, "");
                AssetFileDescriptor fd = RNFetchBlob.RCTContext.getAssets().openFd(name);
                stat.putString("filename", name);
                stat.putString(RNFetchBlobConst.RNFB_RESPONSE_PATH, path2);
                stat.putString("type", UriUtil.LOCAL_ASSET_SCHEME);
                stat.putString("size", String.valueOf(fd.getLength()));
                stat.putInt("lastModified", 0);
                return stat;
            }
            File target = new File(path2);
            if (!target.exists()) {
                return null;
            }
            stat.putString("filename", target.getName());
            stat.putString(RNFetchBlobConst.RNFB_RESPONSE_PATH, target.getPath());
            stat.putString("type", target.isDirectory() ? "directory" : UriUtil.LOCAL_FILE_SCHEME);
            stat.putString("size", String.valueOf(target.length()));
            stat.putString("lastModified", String.valueOf(target.lastModified()));
            return stat;
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void scanFile(String[] path, String[] mimes, final Callback callback) {
        try {
            MediaScannerConnection.scanFile(this.mCtx, path, mimes, new OnScanCompletedListener() {
                public void onScanCompleted(String s, Uri uri) {
                    callback.invoke(null, Boolean.valueOf(true));
                }
            });
        } catch (Exception err) {
            callback.invoke(err.getLocalizedMessage(), null);
        }
    }

    static void createFile(String path, String data, String encoding2, Callback callback) {
        try {
            File dest = new File(path);
            boolean created = dest.createNewFile();
            if (encoding2.equals(RNFetchBlobConst.DATA_ENCODE_URI)) {
                File src = new File(data.replace(RNFetchBlobConst.FILE_PREFIX, ""));
                if (!src.exists()) {
                    callback.invoke("RNfetchBlob writeFileError", "source file : " + data + "not exists");
                    return;
                }
                FileInputStream fin = new FileInputStream(src);
                OutputStream ostream = new FileOutputStream(dest);
                byte[] buffer = new byte[10240];
                for (int read = fin.read(buffer); read > 0; read = fin.read(buffer)) {
                    ostream.write(buffer, 0, read);
                }
                fin.close();
                ostream.close();
            } else if (!created) {
                callback.invoke("create file error: failed to create file at path `" + path + "` for its parent path may not exists, or the file already exists. If you intended to overwrite the existing file use fs.writeFile instead.");
                return;
            } else {
                new FileOutputStream(dest).write(stringToBytes(data, encoding2));
            }
            callback.invoke(null, path);
        } catch (Exception err) {
            callback.invoke(err.getLocalizedMessage());
        }
    }

    static void createFileASCII(String path, ReadableArray data, Callback callback) {
        try {
            File dest = new File(path);
            if (dest.exists()) {
                callback.invoke("create file error: failed to create file at path `" + path + "`, file already exists.");
            } else if (!dest.createNewFile()) {
                callback.invoke("create file error: failed to create file at path `" + path + "` for its parent path may not exists");
            } else {
                OutputStream ostream = new FileOutputStream(dest);
                byte[] chunk = new byte[data.size()];
                for (int i = 0; i < data.size(); i++) {
                    chunk[i] = (byte) data.getInt(i);
                }
                ostream.write(chunk);
                callback.invoke(null, path);
            }
        } catch (Exception err) {
            callback.invoke(err.getLocalizedMessage());
        }
    }

    static void df(Callback callback) {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        WritableMap args = Arguments.createMap();
        if (VERSION.SDK_INT >= 18) {
            args.putString("internal_free", String.valueOf(stat.getFreeBytes()));
            args.putString("internal_total", String.valueOf(stat.getTotalBytes()));
            StatFs statEx = new StatFs(Environment.getExternalStorageDirectory().getPath());
            args.putString("external_free", String.valueOf(statEx.getFreeBytes()));
            args.putString("external_total", String.valueOf(statEx.getTotalBytes()));
        }
        callback.invoke(null, args);
    }

    static void removeSession(ReadableArray paths, final Callback callback) {
        new AsyncTask<ReadableArray, Integer, Integer>() {
            /* access modifiers changed from: protected */
            public Integer doInBackground(ReadableArray... paths) {
                int i = 0;
                while (i < paths[0].size()) {
                    try {
                        File f = new File(paths[0].getString(i));
                        if (f.exists()) {
                            f.delete();
                        }
                        i++;
                    } catch (Exception err) {
                        callback.invoke(err.getLocalizedMessage());
                    }
                }
                callback.invoke(null, Boolean.valueOf(true));
                return Integer.valueOf(paths[0].size());
            }
        }.execute(new ReadableArray[]{paths});
    }

    private static byte[] stringToBytes(String data, String encoding2) {
        if (encoding2.equalsIgnoreCase("ascii")) {
            return data.getBytes(Charset.forName("US-ASCII"));
        }
        if (encoding2.toLowerCase().contains(RNFetchBlobConst.RNFB_RESPONSE_BASE64)) {
            return Base64.decode(data, 2);
        }
        if (encoding2.equalsIgnoreCase(RNFetchBlobConst.RNFB_RESPONSE_UTF8)) {
            return data.getBytes(Charset.forName("UTF-8"));
        }
        return data.getBytes(Charset.forName("US-ASCII"));
    }

    private void emitStreamEvent(String streamName, String event, String data) {
        WritableMap eventData = Arguments.createMap();
        eventData.putString("event", event);
        eventData.putString("detail", data);
        this.emitter.emit(streamName, eventData);
    }

    private void emitStreamEvent(String streamName, String event, WritableArray data) {
        WritableMap eventData = Arguments.createMap();
        eventData.putString("event", event);
        eventData.putArray("detail", data);
        this.emitter.emit(streamName, eventData);
    }

    /* access modifiers changed from: 0000 */
    public void emitFSData(String taskId, String event, String data) {
        WritableMap eventData = Arguments.createMap();
        eventData.putString("event", event);
        eventData.putString("detail", data);
        this.emitter.emit("RNFetchBlobStream" + taskId, eventData);
    }

    static InputStream inputStreamFromPath(String path) throws IOException {
        if (path.startsWith(RNFetchBlobConst.FILE_PREFIX_BUNDLE_ASSET)) {
            return RNFetchBlob.RCTContext.getAssets().open(path.replace(RNFetchBlobConst.FILE_PREFIX_BUNDLE_ASSET, ""));
        }
        return new FileInputStream(new File(path));
    }

    static boolean isPathExists(String path) {
        if (!path.startsWith(RNFetchBlobConst.FILE_PREFIX_BUNDLE_ASSET)) {
            return new File(path).exists();
        }
        try {
            RNFetchBlob.RCTContext.getAssets().open(path.replace(RNFetchBlobConst.FILE_PREFIX_BUNDLE_ASSET, ""));
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    static boolean isAsset(String path) {
        if (path != null) {
            return path.startsWith(RNFetchBlobConst.FILE_PREFIX_BUNDLE_ASSET);
        }
        return false;
    }

    static String normalizePath(String path) {
        if (path == null) {
            return null;
        }
        if (!path.matches("\\w+\\:.*")) {
            return path;
        }
        if (path.startsWith("file://")) {
            return path.replace("file://", "");
        }
        return !path.startsWith(RNFetchBlobConst.FILE_PREFIX_BUNDLE_ASSET) ? PathResolver.getRealPathFromURI(RNFetchBlob.RCTContext, Uri.parse(path)) : path;
    }
}
