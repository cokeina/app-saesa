package com.audiomobile;

import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {
    /* access modifiers changed from: protected */
    public String getMainComponentName() {
        return "AudioMobile";
    }
}
