package com.rnim.rn.audio;

import android.content.Context;
import android.media.MediaRecorder;
import android.os.Build.VERSION;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

class AudioRecorderManager extends ReactContextBaseJavaModule {
    private static final String CachesDirectoryPath = "CachesDirectoryPath";
    private static final String DocumentDirectoryPath = "DocumentDirectoryPath";
    private static final String DownloadsDirectoryPath = "DownloadsDirectoryPath";
    private static final String LibraryDirectoryPath = "LibraryDirectoryPath";
    private static final String MainBundlePath = "MainBundlePath";
    private static final String MusicDirectoryPath = "MusicDirectoryPath";
    private static final String PicturesDirectoryPath = "PicturesDirectoryPath";
    private static final String TAG = "ReactNativeAudio";
    private Context context;
    private String currentOutputFile;
    private boolean isPauseResumeCapable = false;
    /* access modifiers changed from: private */
    public boolean isPaused = false;
    private boolean isRecording = false;
    private Method pauseMethod = null;
    private MediaRecorder recorder;
    private Method resumeMethod = null;
    /* access modifiers changed from: private */
    public StopWatch stopWatch;
    private Timer timer;

    public AudioRecorderManager(ReactApplicationContext reactContext) {
        boolean z = false;
        super(reactContext);
        this.context = reactContext;
        this.stopWatch = new StopWatch();
        if (VERSION.SDK_INT > 23) {
            z = true;
        }
        this.isPauseResumeCapable = z;
        if (this.isPauseResumeCapable) {
            try {
                this.pauseMethod = MediaRecorder.class.getMethod("pause", new Class[0]);
                this.resumeMethod = MediaRecorder.class.getMethod("resume", new Class[0]);
            } catch (NoSuchMethodException e) {
                Log.d("ERROR", "Failed to get a reference to pause and/or resume method");
            }
        }
    }

    public Map<String, Object> getConstants() {
        Map<String, Object> constants = new HashMap<>();
        constants.put(DocumentDirectoryPath, getReactApplicationContext().getFilesDir().getAbsolutePath());
        constants.put(PicturesDirectoryPath, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath());
        constants.put(MainBundlePath, "");
        constants.put(CachesDirectoryPath, getReactApplicationContext().getCacheDir().getAbsolutePath());
        constants.put(LibraryDirectoryPath, "");
        constants.put(MusicDirectoryPath, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getAbsolutePath());
        constants.put(DownloadsDirectoryPath, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath());
        return constants;
    }

    public String getName() {
        return "AudioRecorderManager";
    }

    @ReactMethod
    public void checkAuthorizationStatus(Promise promise) {
        promise.resolve(Boolean.valueOf(ContextCompat.checkSelfPermission(getCurrentActivity(), "android.permission.RECORD_AUDIO") == 0));
    }

    @ReactMethod
    public void prepareRecordingAtPath(String recordingPath, ReadableMap recordingSettings, Promise promise) {
        if (this.isRecording) {
            logAndRejectPromise(promise, "INVALID_STATE", "Please call stopRecording before starting recording");
        }
        this.recorder = new MediaRecorder();
        try {
            this.recorder.setAudioSource(1);
            this.recorder.setOutputFormat(getOutputFormatFromString(recordingSettings.getString("OutputFormat")));
            this.recorder.setAudioEncoder(getAudioEncoderFromString(recordingSettings.getString("AudioEncoding")));
            this.recorder.setAudioSamplingRate(recordingSettings.getInt("SampleRate"));
            this.recorder.setAudioChannels(recordingSettings.getInt("Channels"));
            this.recorder.setAudioEncodingBitRate(recordingSettings.getInt("AudioEncodingBitRate"));
            this.recorder.setOutputFile(recordingPath);
            this.currentOutputFile = recordingPath;
            try {
                this.recorder.prepare();
                promise.resolve(this.currentOutputFile);
            } catch (Exception e) {
                logAndRejectPromise(promise, "COULDNT_PREPARE_RECORDING_AT_PATH " + recordingPath, e.getMessage());
            }
        } catch (Exception e2) {
            logAndRejectPromise(promise, "COULDNT_CONFIGURE_MEDIA_RECORDER", "Make sure you've added RECORD_AUDIO permission to your AndroidManifest.xml file " + e2.getMessage());
        }
    }

    private int getAudioEncoderFromString(String audioEncoder) {
        char c = 65535;
        switch (audioEncoder.hashCode()) {
            case -1413784883:
                if (audioEncoder.equals("amr_nb")) {
                    c = 2;
                    break;
                }
                break;
            case -1413784604:
                if (audioEncoder.equals("amr_wb")) {
                    c = 3;
                    break;
                }
                break;
            case -1235069279:
                if (audioEncoder.equals("aac_eld")) {
                    c = 1;
                    break;
                }
                break;
            case -1221333503:
                if (audioEncoder.equals("he_aac")) {
                    c = 4;
                    break;
                }
                break;
            case -810722925:
                if (audioEncoder.equals("vorbis")) {
                    c = 5;
                    break;
                }
                break;
            case 96323:
                if (audioEncoder.equals("aac")) {
                    c = 0;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return 3;
            case 1:
                return 5;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 4;
            case 5:
                return 6;
            default:
                Log.d("INVALID_AUDIO_ENCODER", "USING MediaRecorder.AudioEncoder.DEFAULT instead of " + audioEncoder + ": " + 0);
                return 0;
        }
    }

    private int getOutputFormatFromString(String outputFormat) {
        char c = 65535;
        switch (outputFormat.hashCode()) {
            case -1558681978:
                if (outputFormat.equals("three_gpp")) {
                    c = 4;
                    break;
                }
                break;
            case -1413784883:
                if (outputFormat.equals("amr_nb")) {
                    c = 2;
                    break;
                }
                break;
            case -1413784604:
                if (outputFormat.equals("amr_wb")) {
                    c = 3;
                    break;
                }
                break;
            case -1067844614:
                if (outputFormat.equals("mpeg_4")) {
                    c = 0;
                    break;
                }
                break;
            case 3645337:
                if (outputFormat.equals("webm")) {
                    c = 5;
                    break;
                }
                break;
            case 367431774:
                if (outputFormat.equals("aac_adts")) {
                    c = 1;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return 2;
            case 1:
                return 6;
            case 2:
                return 3;
            case 3:
                return 4;
            case 4:
                return 1;
            case 5:
                return 9;
            default:
                Log.d("INVALID_OUPUT_FORMAT", "USING MediaRecorder.OutputFormat.DEFAULT : 0");
                return 0;
        }
    }

    @ReactMethod
    public void startRecording(Promise promise) {
        if (this.recorder == null) {
            logAndRejectPromise(promise, "RECORDING_NOT_PREPARED", "Please call prepareRecordingAtPath before starting recording");
        } else if (this.isRecording) {
            logAndRejectPromise(promise, "INVALID_STATE", "Please call stopRecording before starting recording");
        } else {
            this.recorder.start();
            this.stopWatch.reset();
            this.stopWatch.start();
            this.isRecording = true;
            this.isPaused = false;
            startTimer();
            promise.resolve(this.currentOutputFile);
        }
    }

    @ReactMethod
    public void stopRecording(Promise promise) {
        if (!this.isRecording) {
            logAndRejectPromise(promise, "INVALID_STATE", "Please call startRecording before stopping recording");
            return;
        }
        stopTimer();
        this.isRecording = false;
        this.isPaused = false;
        try {
            this.recorder.stop();
            this.recorder.release();
            this.stopWatch.stop();
            this.recorder = null;
            promise.resolve(this.currentOutputFile);
            sendEvent("recordingFinished", null);
        } catch (RuntimeException e) {
            logAndRejectPromise(promise, "RUNTIME_EXCEPTION", "No valid audio data received. You may be using a device that can't record audio.");
            this.recorder = null;
        } catch (Throwable th) {
            this.recorder = null;
            throw th;
        }
    }

    @ReactMethod
    public void pauseRecording(Promise promise) {
        if (!this.isPauseResumeCapable || this.pauseMethod == null) {
            logAndRejectPromise(promise, "RUNTIME_EXCEPTION", "Method not available on this version of Android.");
            return;
        }
        if (!this.isPaused) {
            try {
                this.pauseMethod.invoke(this.recorder, new Object[0]);
                this.stopWatch.stop();
            } catch (IllegalAccessException | RuntimeException | InvocationTargetException e) {
                e.printStackTrace();
                logAndRejectPromise(promise, "RUNTIME_EXCEPTION", "Method not available on this version of Android.");
                return;
            }
        }
        this.isPaused = true;
        promise.resolve(null);
    }

    @ReactMethod
    public void resumeRecording(Promise promise) {
        if (!this.isPauseResumeCapable || this.resumeMethod == null) {
            logAndRejectPromise(promise, "RUNTIME_EXCEPTION", "Method not available on this version of Android.");
            return;
        }
        if (this.isPaused) {
            try {
                this.resumeMethod.invoke(this.recorder, new Object[0]);
                this.stopWatch.start();
            } catch (IllegalAccessException | RuntimeException | InvocationTargetException e) {
                e.printStackTrace();
                logAndRejectPromise(promise, "RUNTIME_EXCEPTION", "Method not available on this version of Android.");
                return;
            }
        }
        this.isPaused = false;
        promise.resolve(null);
    }

    private void startTimer() {
        this.timer = new Timer();
        this.timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (!AudioRecorderManager.this.isPaused) {
                    WritableMap body = Arguments.createMap();
                    body.putDouble("currentTime", (double) AudioRecorderManager.this.stopWatch.getTimeSeconds());
                    AudioRecorderManager.this.sendEvent("recordingProgress", body);
                }
            }
        }, 0, 1000);
    }

    private void stopTimer() {
        if (this.timer != null) {
            this.timer.cancel();
            this.timer.purge();
            this.timer = null;
        }
    }

    /* access modifiers changed from: private */
    public void sendEvent(String eventName, Object params) {
        ((RCTDeviceEventEmitter) getReactApplicationContext().getJSModule(RCTDeviceEventEmitter.class)).emit(eventName, params);
    }

    private void logAndRejectPromise(Promise promise, String errorCode, String errorMessage) {
        Log.e(TAG, errorMessage);
        promise.reject(errorCode, errorMessage);
    }
}
