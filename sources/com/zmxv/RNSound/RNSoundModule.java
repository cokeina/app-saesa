package com.zmxv.RNSound;

import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Build.VERSION;
import android.util.Log;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RNSoundModule extends ReactContextBaseJavaModule implements OnAudioFocusChangeListener {
    static final Object NULL = null;
    String category;
    ReactApplicationContext context;
    Double focusedPlayerKey;
    Boolean mixWithOthers = Boolean.valueOf(true);
    Map<Double, MediaPlayer> playerPool = new HashMap();
    Boolean wasPlayingBeforeFocusChange = Boolean.valueOf(false);

    public RNSoundModule(ReactApplicationContext context2) {
        super(context2);
        this.context = context2;
        this.category = null;
    }

    /* access modifiers changed from: private */
    public void setOnPlay(boolean isPlaying, Double playerKey) {
        ReactApplicationContext reactApplicationContext = this.context;
        WritableMap params = Arguments.createMap();
        params.putBoolean("isPlaying", isPlaying);
        params.putDouble("playerKey", playerKey.doubleValue());
    }

    public String getName() {
        return "RNSound";
    }

    @ReactMethod
    public void prepare(String fileName, Double key, ReadableMap options, final Callback callback) {
        MediaPlayer player = createMediaPlayer(fileName);
        if (player == null) {
            WritableMap e = Arguments.createMap();
            e.putInt("code", -1);
            e.putString("message", "resource not found");
            return;
        }
        this.playerPool.put(key, player);
        if (this.category != null) {
            Integer category2 = null;
            String str = this.category;
            char c = 65535;
            switch (str.hashCode()) {
                case -1803461041:
                    if (str.equals("System")) {
                        c = 2;
                        break;
                    }
                    break;
                case 2547280:
                    if (str.equals("Ring")) {
                        c = 4;
                        break;
                    }
                    break;
                case 82833682:
                    if (str.equals("Voice")) {
                        c = 3;
                        break;
                    }
                    break;
                case 772508280:
                    if (str.equals("Ambient")) {
                        c = 1;
                        break;
                    }
                    break;
                case 1943812667:
                    if (str.equals("Playback")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    category2 = Integer.valueOf(3);
                    break;
                case 1:
                    category2 = Integer.valueOf(5);
                    break;
                case 2:
                    category2 = Integer.valueOf(1);
                    break;
                case 3:
                    category2 = Integer.valueOf(0);
                    break;
                case 4:
                    category2 = Integer.valueOf(2);
                    break;
                default:
                    Log.e("RNSoundModule", String.format("Unrecognised category %s", new Object[]{this.category}));
                    break;
            }
            if (category2 != null) {
                player.setAudioStreamType(category2.intValue());
            }
        }
        player.setOnPreparedListener(new OnPreparedListener() {
            boolean callbackWasCalled = false;

            public synchronized void onPrepared(MediaPlayer mp) {
                if (!this.callbackWasCalled) {
                    this.callbackWasCalled = true;
                    WritableMap props = Arguments.createMap();
                    props.putDouble("duration", ((double) mp.getDuration()) * 0.001d);
                    try {
                        callback.invoke(RNSoundModule.NULL, props);
                    } catch (RuntimeException runtimeException) {
                        Log.e("RNSoundModule", "Exception", runtimeException);
                    }
                }
                return;
            }
        });
        player.setOnErrorListener(new OnErrorListener() {
            boolean callbackWasCalled = false;

            public synchronized boolean onError(MediaPlayer mp, int what, int extra) {
                if (!this.callbackWasCalled) {
                    this.callbackWasCalled = true;
                    try {
                        WritableMap props = Arguments.createMap();
                        props.putInt("what", what);
                        props.putInt("extra", extra);
                        callback.invoke(props, RNSoundModule.NULL);
                    } catch (RuntimeException runtimeException) {
                        Log.e("RNSoundModule", "Exception", runtimeException);
                    }
                }
                return true;
            }
        });
        try {
            if (!options.hasKey("loadSync") || !options.getBoolean("loadSync")) {
                player.prepareAsync();
            } else {
                player.prepare();
            }
        } catch (Exception ignored) {
            Log.e("RNSoundModule", "Exception", ignored);
        }
    }

    /* access modifiers changed from: protected */
    public MediaPlayer createMediaPlayer(String fileName) {
        int res = this.context.getResources().getIdentifier(fileName, "raw", this.context.getPackageName());
        MediaPlayer mediaPlayer = new MediaPlayer();
        if (res != 0) {
            try {
                AssetFileDescriptor afd = this.context.getResources().openRawResourceFd(res);
                mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                afd.close();
                return mediaPlayer;
            } catch (IOException e) {
                Log.e("RNSoundModule", "Exception", e);
                return null;
            }
        } else if (fileName.startsWith("http://") || fileName.startsWith("https://")) {
            mediaPlayer.setAudioStreamType(3);
            Log.i("RNSoundModule", fileName);
            try {
                mediaPlayer.setDataSource(fileName);
                return mediaPlayer;
            } catch (IOException e2) {
                Log.e("RNSoundModule", "Exception", e2);
                return null;
            }
        } else if (fileName.startsWith("asset:/")) {
            try {
                AssetFileDescriptor descriptor = this.context.getAssets().openFd(fileName.replace("asset:/", ""));
                mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                descriptor.close();
                return mediaPlayer;
            } catch (IOException e3) {
                Log.e("RNSoundModule", "Exception", e3);
                return null;
            }
        } else if (!new File(fileName).exists()) {
            return null;
        } else {
            mediaPlayer.setAudioStreamType(3);
            Log.i("RNSoundModule", fileName);
            try {
                mediaPlayer.setDataSource(fileName);
                return mediaPlayer;
            } catch (IOException e4) {
                Log.e("RNSoundModule", "Exception", e4);
                return null;
            }
        }
    }

    @ReactMethod
    public void play(final Double key, final Callback callback) {
        MediaPlayer player = (MediaPlayer) this.playerPool.get(key);
        if (player == null) {
            setOnPlay(false, key);
            if (callback != null) {
                callback.invoke(Boolean.valueOf(false));
            }
        } else if (!player.isPlaying()) {
            if (!this.mixWithOthers.booleanValue()) {
                ((AudioManager) this.context.getSystemService("audio")).requestAudioFocus(this, 3, 1);
                this.focusedPlayerKey = key;
            }
            player.setOnCompletionListener(new OnCompletionListener() {
                boolean callbackWasCalled = false;

                public synchronized void onCompletion(MediaPlayer mp) {
                    if (!mp.isLooping()) {
                        RNSoundModule.this.setOnPlay(false, key);
                        if (!this.callbackWasCalled) {
                            this.callbackWasCalled = true;
                            try {
                                callback.invoke(Boolean.valueOf(true));
                            } catch (Exception e) {
                            }
                        }
                    }
                }
            });
            player.setOnErrorListener(new OnErrorListener() {
                boolean callbackWasCalled = false;

                public synchronized boolean onError(MediaPlayer mp, int what, int extra) {
                    RNSoundModule.this.setOnPlay(false, key);
                    if (!this.callbackWasCalled) {
                        this.callbackWasCalled = true;
                        try {
                            callback.invoke(Boolean.valueOf(true));
                        } catch (Exception e) {
                        }
                    }
                    return true;
                }
            });
            player.start();
            setOnPlay(true, key);
        }
    }

    @ReactMethod
    public void pause(Double key, Callback callback) {
        MediaPlayer player = (MediaPlayer) this.playerPool.get(key);
        if (player != null && player.isPlaying()) {
            player.pause();
        }
        if (callback != null) {
            callback.invoke(new Object[0]);
        }
    }

    @ReactMethod
    public void stop(Double key, Callback callback) {
        MediaPlayer player = (MediaPlayer) this.playerPool.get(key);
        if (player != null && player.isPlaying()) {
            player.pause();
            player.seekTo(0);
        }
        if (!this.mixWithOthers.booleanValue() && key == this.focusedPlayerKey) {
            ((AudioManager) this.context.getSystemService("audio")).abandonAudioFocus(this);
        }
        callback.invoke(new Object[0]);
    }

    @ReactMethod
    public void reset(Double key) {
        MediaPlayer player = (MediaPlayer) this.playerPool.get(key);
        if (player != null) {
            player.reset();
        }
    }

    @ReactMethod
    public void release(Double key) {
        MediaPlayer player = (MediaPlayer) this.playerPool.get(key);
        if (player != null) {
            player.release();
            this.playerPool.remove(key);
            if (!this.mixWithOthers.booleanValue() && key == this.focusedPlayerKey) {
                ((AudioManager) this.context.getSystemService("audio")).abandonAudioFocus(this);
            }
        }
    }

    @ReactMethod
    public void setVolume(Double key, Float left, Float right) {
        MediaPlayer player = (MediaPlayer) this.playerPool.get(key);
        if (player != null) {
            player.setVolume(left.floatValue(), right.floatValue());
        }
    }

    @ReactMethod
    public void getSystemVolume(Callback callback) {
        try {
            AudioManager audioManager = (AudioManager) this.context.getSystemService("audio");
            callback.invoke(NULL, Float.valueOf(((float) audioManager.getStreamVolume(3)) / ((float) audioManager.getStreamMaxVolume(3))));
        } catch (Exception error) {
            WritableMap e = Arguments.createMap();
            e.putInt("code", -1);
            e.putString("message", error.getMessage());
            callback.invoke(e);
        }
    }

    @ReactMethod
    public void setSystemVolume(Float value) {
        AudioManager audioManager = (AudioManager) this.context.getSystemService("audio");
        audioManager.setStreamVolume(3, Math.round(((float) audioManager.getStreamMaxVolume(3)) * value.floatValue()), 0);
    }

    @ReactMethod
    public void setLooping(Double key, Boolean looping) {
        MediaPlayer player = (MediaPlayer) this.playerPool.get(key);
        if (player != null) {
            player.setLooping(looping.booleanValue());
        }
    }

    @ReactMethod
    public void setSpeed(Double key, Float speed) {
        if (VERSION.SDK_INT < 23) {
            Log.w("RNSoundModule", "setSpeed ignored due to sdk limit");
            return;
        }
        MediaPlayer player = (MediaPlayer) this.playerPool.get(key);
        if (player != null) {
            player.setPlaybackParams(player.getPlaybackParams().setSpeed(speed.floatValue()));
        }
    }

    @ReactMethod
    public void setCurrentTime(Double key, Float sec) {
        MediaPlayer player = (MediaPlayer) this.playerPool.get(key);
        if (player != null) {
            player.seekTo(Math.round(sec.floatValue() * 1000.0f));
        }
    }

    @ReactMethod
    public void getCurrentTime(Double key, Callback callback) {
        MediaPlayer player = (MediaPlayer) this.playerPool.get(key);
        if (player == null) {
            callback.invoke(Integer.valueOf(-1), Boolean.valueOf(false));
            return;
        }
        callback.invoke(Double.valueOf(((double) player.getCurrentPosition()) * 0.001d), Boolean.valueOf(player.isPlaying()));
    }

    @ReactMethod
    public void setSpeakerphoneOn(Double key, Boolean speaker) {
        MediaPlayer player = (MediaPlayer) this.playerPool.get(key);
        if (player != null) {
            player.setAudioStreamType(3);
            ReactApplicationContext reactApplicationContext = this.context;
            ReactApplicationContext reactApplicationContext2 = this.context;
            AudioManager audioManager = (AudioManager) reactApplicationContext.getSystemService("audio");
            if (speaker.booleanValue()) {
                audioManager.setMode(3);
            } else {
                audioManager.setMode(0);
            }
            audioManager.setSpeakerphoneOn(speaker.booleanValue());
        }
    }

    @ReactMethod
    public void setCategory(String category2, Boolean mixWithOthers2) {
        this.category = category2;
        this.mixWithOthers = mixWithOthers2;
    }

    public void onAudioFocusChange(int focusChange) {
        if (!this.mixWithOthers.booleanValue()) {
            MediaPlayer player = (MediaPlayer) this.playerPool.get(this.focusedPlayerKey);
            if (player == null) {
                return;
            }
            if (focusChange <= 0) {
                this.wasPlayingBeforeFocusChange = Boolean.valueOf(player.isPlaying());
                if (this.wasPlayingBeforeFocusChange.booleanValue()) {
                    pause(this.focusedPlayerKey, null);
                }
            } else if (this.wasPlayingBeforeFocusChange.booleanValue()) {
                play(this.focusedPlayerKey, null);
                this.wasPlayingBeforeFocusChange = Boolean.valueOf(false);
            }
        }
    }

    @ReactMethod
    public void enable(Boolean enabled) {
    }

    public Map<String, Object> getConstants() {
        Map<String, Object> constants = new HashMap<>();
        constants.put("IsAndroid", Boolean.valueOf(true));
        return constants;
    }
}
