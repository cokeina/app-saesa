package com.oblador.vectoricons;

import android.graphics.Typeface;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import java.util.HashMap;
import java.util.Map;

public class VectorIconsModule extends ReactContextBaseJavaModule {
    public static final String REACT_CLASS = "RNVectorIconsModule";
    private static final Map<String, Typeface> sTypefaceCache = new HashMap();

    public String getName() {
        return REACT_CLASS;
    }

    public VectorIconsModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0213 A[SYNTHETIC, Splitter:B:26:0x0213] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0239 A[SYNTHETIC, Splitter:B:36:0x0239] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0248 A[SYNTHETIC, Splitter:B:43:0x0248] */
    /* JADX WARNING: Removed duplicated region for block: B:59:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:62:? A[RETURN, SYNTHETIC] */
    @com.facebook.react.bridge.ReactMethod
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void getImageForFont(java.lang.String r28, java.lang.String r29, java.lang.Integer r30, java.lang.Integer r31, com.facebook.react.bridge.Callback r32) {
        /*
            r27 = this;
            com.facebook.react.bridge.ReactApplicationContext r13 = r27.getReactApplicationContext()
            java.io.File r9 = r13.getCacheDir()
            java.lang.StringBuilder r24 = new java.lang.StringBuilder
            r24.<init>()
            java.lang.String r25 = r9.getAbsolutePath()
            java.lang.StringBuilder r24 = r24.append(r25)
            java.lang.String r25 = "/"
            java.lang.StringBuilder r24 = r24.append(r25)
            java.lang.String r10 = r24.toString()
            android.content.res.Resources r24 = r13.getResources()
            android.util.DisplayMetrics r24 = r24.getDisplayMetrics()
            r0 = r24
            float r0 = r0.density
            r19 = r0
            java.lang.StringBuilder r24 = new java.lang.StringBuilder
            r24.<init>()
            java.lang.String r25 = "@"
            java.lang.StringBuilder r25 = r24.append(r25)
            r0 = r19
            int r0 = (int) r0
            r24 = r0
            r0 = r24
            float r0 = (float) r0
            r24 = r0
            int r24 = (r19 > r24 ? 1 : (r19 == r24 ? 0 : -1))
            if (r24 != 0) goto L_0x0118
            r0 = r19
            int r0 = (int) r0
            r24 = r0
            java.lang.String r24 = java.lang.Integer.toString(r24)
        L_0x004f:
            r0 = r25
            r1 = r24
            java.lang.StringBuilder r24 = r0.append(r1)
            java.lang.String r25 = "x"
            java.lang.StringBuilder r24 = r24.append(r25)
            java.lang.String r20 = r24.toString()
            int r24 = r30.intValue()
            r0 = r24
            float r0 = (float) r0
            r24 = r0
            float r24 = r24 * r19
            int r21 = java.lang.Math.round(r24)
            java.lang.StringBuilder r24 = new java.lang.StringBuilder
            r24.<init>()
            r0 = r24
            r1 = r28
            java.lang.StringBuilder r24 = r0.append(r1)
            java.lang.String r25 = ":"
            java.lang.StringBuilder r24 = r24.append(r25)
            r0 = r24
            r1 = r29
            java.lang.StringBuilder r24 = r0.append(r1)
            java.lang.String r25 = ":"
            java.lang.StringBuilder r24 = r24.append(r25)
            r0 = r24
            r1 = r31
            java.lang.StringBuilder r24 = r0.append(r1)
            java.lang.String r11 = r24.toString()
            int r24 = r11.hashCode()
            r25 = 32
            java.lang.String r17 = java.lang.Integer.toString(r24, r25)
            java.lang.StringBuilder r24 = new java.lang.StringBuilder
            r24.<init>()
            r0 = r24
            java.lang.StringBuilder r24 = r0.append(r10)
            r0 = r24
            r1 = r17
            java.lang.StringBuilder r24 = r0.append(r1)
            java.lang.String r25 = "_"
            java.lang.StringBuilder r24 = r24.append(r25)
            int r25 = r30.intValue()
            java.lang.String r25 = java.lang.Integer.toString(r25)
            java.lang.StringBuilder r24 = r24.append(r25)
            r0 = r24
            r1 = r20
            java.lang.StringBuilder r24 = r0.append(r1)
            java.lang.String r25 = ".png"
            java.lang.StringBuilder r24 = r24.append(r25)
            java.lang.String r7 = r24.toString()
            java.lang.StringBuilder r24 = new java.lang.StringBuilder
            r24.<init>()
            java.lang.String r25 = "file://"
            java.lang.StringBuilder r24 = r24.append(r25)
            r0 = r24
            java.lang.StringBuilder r24 = r0.append(r7)
            java.lang.String r8 = r24.toString()
            java.io.File r6 = new java.io.File
            r6.<init>(r7)
            boolean r24 = r6.exists()
            if (r24 == 0) goto L_0x011e
            r24 = 2
            r0 = r24
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r24 = r0
            r25 = 0
            r26 = 0
            r24[r25] = r26
            r25 = 1
            r24[r25] = r8
            r0 = r32
            r1 = r24
            r0.invoke(r1)
        L_0x0117:
            return
        L_0x0118:
            java.lang.String r24 = java.lang.Float.toString(r19)
            goto L_0x004f
        L_0x011e:
            r15 = 0
            com.facebook.react.views.text.ReactFontManager r24 = com.facebook.react.views.text.ReactFontManager.getInstance()
            r25 = 0
            android.content.res.AssetManager r26 = r13.getAssets()
            r0 = r24
            r1 = r28
            r2 = r25
            r3 = r26
            android.graphics.Typeface r23 = r0.getTypeface(r1, r2, r3)
            android.graphics.Paint r18 = new android.graphics.Paint
            r18.<init>()
            r0 = r18
            r1 = r23
            r0.setTypeface(r1)
            int r24 = r31.intValue()
            r0 = r18
            r1 = r24
            r0.setColor(r1)
            r0 = r21
            float r0 = (float) r0
            r24 = r0
            r0 = r18
            r1 = r24
            r0.setTextSize(r1)
            r24 = 1
            r0 = r18
            r1 = r24
            r0.setAntiAlias(r1)
            android.graphics.Rect r22 = new android.graphics.Rect
            r22.<init>()
            r24 = 0
            int r25 = r29.length()
            r0 = r18
            r1 = r29
            r2 = r24
            r3 = r25
            r4 = r22
            r0.getTextBounds(r1, r2, r3, r4)
            int r24 = r22.width()
            int r25 = r22.height()
            android.graphics.Bitmap$Config r26 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createBitmap(r24, r25, r26)
            android.graphics.Canvas r12 = new android.graphics.Canvas
            r12.<init>(r5)
            r0 = r22
            int r0 = r0.left
            r24 = r0
            r0 = r24
            int r0 = -r0
            r24 = r0
            r0 = r24
            float r0 = (float) r0
            r24 = r0
            r0 = r22
            int r0 = r0.top
            r25 = r0
            r0 = r25
            int r0 = -r0
            r25 = r0
            r0 = r25
            float r0 = (float) r0
            r25 = r0
            r0 = r29
            r1 = r24
            r2 = r25
            r3 = r18
            r12.drawText(r0, r1, r2, r3)
            java.io.FileOutputStream r16 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x01f9, IOException -> 0x021f }
            r0 = r16
            r0.<init>(r6)     // Catch:{ FileNotFoundException -> 0x01f9, IOException -> 0x021f }
            android.graphics.Bitmap$CompressFormat r24 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ FileNotFoundException -> 0x025a, IOException -> 0x0256, all -> 0x0252 }
            r25 = 100
            r0 = r24
            r1 = r25
            r2 = r16
            r5.compress(r0, r1, r2)     // Catch:{ FileNotFoundException -> 0x025a, IOException -> 0x0256, all -> 0x0252 }
            r16.flush()     // Catch:{ FileNotFoundException -> 0x025a, IOException -> 0x0256, all -> 0x0252 }
            r16.close()     // Catch:{ FileNotFoundException -> 0x025a, IOException -> 0x0256, all -> 0x0252 }
            r15 = 0
            r24 = 2
            r0 = r24
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ FileNotFoundException -> 0x01f9, IOException -> 0x021f }
            r24 = r0
            r25 = 0
            r26 = 0
            r24[r25] = r26     // Catch:{ FileNotFoundException -> 0x01f9, IOException -> 0x021f }
            r25 = 1
            r24[r25] = r8     // Catch:{ FileNotFoundException -> 0x01f9, IOException -> 0x021f }
            r0 = r32
            r1 = r24
            r0.invoke(r1)     // Catch:{ FileNotFoundException -> 0x01f9, IOException -> 0x021f }
            if (r15 == 0) goto L_0x0117
            r15.close()     // Catch:{ IOException -> 0x01f3 }
            r15 = 0
            goto L_0x0117
        L_0x01f3:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x0117
        L_0x01f9:
            r14 = move-exception
        L_0x01fa:
            r24 = 1
            r0 = r24
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0245 }
            r24 = r0
            r25 = 0
            java.lang.String r26 = r14.getMessage()     // Catch:{ all -> 0x0245 }
            r24[r25] = r26     // Catch:{ all -> 0x0245 }
            r0 = r32
            r1 = r24
            r0.invoke(r1)     // Catch:{ all -> 0x0245 }
            if (r15 == 0) goto L_0x0117
            r15.close()     // Catch:{ IOException -> 0x0219 }
            r15 = 0
            goto L_0x0117
        L_0x0219:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x0117
        L_0x021f:
            r14 = move-exception
        L_0x0220:
            r24 = 1
            r0 = r24
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0245 }
            r24 = r0
            r25 = 0
            java.lang.String r26 = r14.getMessage()     // Catch:{ all -> 0x0245 }
            r24[r25] = r26     // Catch:{ all -> 0x0245 }
            r0 = r32
            r1 = r24
            r0.invoke(r1)     // Catch:{ all -> 0x0245 }
            if (r15 == 0) goto L_0x0117
            r15.close()     // Catch:{ IOException -> 0x023f }
            r15 = 0
            goto L_0x0117
        L_0x023f:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x0117
        L_0x0245:
            r24 = move-exception
        L_0x0246:
            if (r15 == 0) goto L_0x024c
            r15.close()     // Catch:{ IOException -> 0x024d }
            r15 = 0
        L_0x024c:
            throw r24
        L_0x024d:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x024c
        L_0x0252:
            r24 = move-exception
            r15 = r16
            goto L_0x0246
        L_0x0256:
            r14 = move-exception
            r15 = r16
            goto L_0x0220
        L_0x025a:
            r14 = move-exception
            r15 = r16
            goto L_0x01fa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.oblador.vectoricons.VectorIconsModule.getImageForFont(java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer, com.facebook.react.bridge.Callback):void");
    }
}
